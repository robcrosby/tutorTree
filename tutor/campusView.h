//
//  campusView.h
//  tutor
//
//  Created by Robert Crosby on 7/8/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "References.h"

@interface campusView : NSObject <CLLocationManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,MKMapViewDelegate> {
    UIView *view;
    MKMapView *map;
    CLLocationManager *location;
    CLLocation *currentLocation;
    NSMutableArray *campusLocations;
    UICollectionView *campusLocationCollection;
    UIView *preview;
    UILabel *currentTitle,*currentHours;
    NSDictionary *currentPlace;
    UIViewController *parentView;
    
}

-(id)initWithParent:(UIViewController*)tparent;
+(instancetype)withParent:(UIViewController*)tparent;
-(UIView*)campusMap;
-(CGFloat)height;

-(void)setYCoordinate:(CGFloat)y;
-(void)zoomToLocation;
@end
