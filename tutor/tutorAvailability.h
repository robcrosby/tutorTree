//
//  tutorAvailability.h
//  tutor
//
//  Created by Robert Crosby on 11/4/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface tutorAvailability : NSObject {
    NSString *identifier,*name,*url,*token;
    NSNumber *cost;
    NSMutableArray *numberAvailability,*dictionaryAvailability;
}

-(id)initWithID:(NSString*)ident andAvailability:(NSArray*)avail andSessions:(NSDictionary*)sessions andName:(NSString*)tname andURL:(NSString*)turl andPPH:(NSNumber*)number andToken:(NSString*)ttoken;
-(id)initWithID:(NSString*)ident andSessions:(NSDictionary*)sessions;

-(NSString*)identifier;
-(NSString*)name;
-(NSURL*)url;
-(NSNumber*)pph;
-(NSString*)token;

-(BOOL)checkAvailabilityonWeekday:(NSInteger)weekday andTime:(NSInteger)time;
-(NSInteger)earliestAvailabilityOn:(NSInteger)weekday;
-(NSDictionary*)availabilityOnWeekday:(NSInteger)weekday andTime:(NSInteger)time;

+(BOOL)verifyAvailability:(NSNumber*)availability andSessions:(NSDictionary*)sessions onWeekday:(NSInteger)tweekday withTime:(NSInteger)time andDurattion:(NSInteger)duration withMaxWeekly:(NSNumber*)maxWeekly;

                                 
@end

NS_ASSUME_NONNULL_END
