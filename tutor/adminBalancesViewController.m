//
//  adminBalancesViewController.m
//  tutor
//
//  Created by Robert Crosby on 4/16/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "adminBalancesViewController.h"

@interface adminBalancesViewController ()

@end

@implementation adminBalancesViewController

- (void)viewDidLoad {
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin])];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [References screenWidth]-30, 44)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = @"Balances";
    
    header.textColor = [UIColor darkTextColor];
    header.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:header];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
//    UIButton *find = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-44, [References topMargin], 50, 44)];
//    [find setTitle:@"Find" forState:UIControlStateNormal];
//    [find setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
//    [find.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
//    [find setTitleColor:[References accentColor] forState:UIControlStateNormal];
//    [find addTarget:self action:@selector(find) forControlEvents:UIControlEventTouchUpInside];
//    [menuBar addSubview:find];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    [self.view sendSubviewToBack:menuBar];
    table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height, 0, [References bottomMargin], 0);
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    [self.view sendSubviewToBack:table];
    [super viewDidLoad];
    balances = [[NSMutableArray alloc] init];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.removeFromSuperViewOnHide = YES;
    hud.margin = 10.f;
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        for (id key in snapshot.value) {
            NSDictionary *dict = snapshot.value[key];
            CGFloat balance = [self calculateBalance:[dict objectForKey:@"income"] andSpending:[dict objectForKey:@"spending"]];
            if (balance > 0) {
                [self->balances addObject:@{@"email":dict[@"email"],@"name":dict[@"name"],@"balance":[NSNumber numberWithFloat:balance]}];
            }
        }
        [hud hideAnimated:true];
        NSLog(@"%@",self->balances);
        [self->table reloadData];
        
    }];
    // Do any additional setup after loading the view.
}

-(void)close {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(CGFloat)calculateBalance:(NSDictionary*)income andSpending:(NSDictionary*)spending {
    CGFloat currentBalance = 0;
    if (income) {
        for (id key in income) {
            NSDictionary *td = income[key];
            NSNumber *n = td[@"amount"];
            currentBalance+=n.floatValue;
            
        }
    }
    if (spending) {
        for (id key in spending) {
            NSDictionary *td = spending[key];
            NSNumber *n = td[@"amount"];
            if (![(NSString*)td[@"type"] isEqualToString:@"Apple Pay"] && ![(NSString*)td[@"type"] isEqualToString:@"Venmo"]) {
                currentBalance-=n.floatValue;
            } else if ([(NSNumber*)td[@"usedBalance"] floatValue] > 0) {
                currentBalance-=[(NSNumber*)td[@"usedBalance"] floatValue];
            }
        }
    }
    return currentBalance;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:[balances[indexPath.row] objectForKey:@"email"]];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[balances[indexPath.row] objectForKey:@"email"]];
        cell.textLabel.text = [balances[indexPath.row] objectForKey:@"name"];
        cell.detailTextLabel.text = [balances[indexPath.row] objectForKey:@"email"];
        UILabel *balance = [[UILabel alloc] initWithFrame:CGRectMake([References screenWidth]-15-70, 0, 70, cell.frame.size.height)];
        balance.textAlignment = NSTextAlignmentRight;
        balance.text = [NSString stringWithFormat:@"$%.2f",[(NSNumber*)[balances[indexPath.row] objectForKey:@"balance"] floatValue]];
        [cell addSubview:balance];
    }
    
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return balances.count;
}


@end
