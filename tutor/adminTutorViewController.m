//
//  adminTutorViewController.m
//  tutor
//
//  Created by Robert Crosby on 8/27/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "adminTutorViewController.h"

@interface adminTutorViewController ()

@end

@implementation adminTutorViewController

@synthesize tutorDict;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createStatic];
    // Do any additional setup after loading the view.
}

-(void)createStatic {
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 96+[References topMargin])];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(8, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+44, [References screenWidth]-24, 52)];
    header.textAlignment = NSTextAlignmentLeft;
    header.text = tutorDict[@"name"];
    header.textColor = [UIColor darkTextColor];
    header.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBold];
    [menuBar addSubview:header];
    UILabel *blur = [[UILabel alloc] initWithFrame:menuBar.bounds];
    [References blurView:blur];
    [menuBar addSubview:blur];
    [menuBar sendSubviewToBack:blur];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight]) style:UITableViewStyleGrouped];
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height, 0, 44, 0);
    table.scrollIndicatorInsets = table.contentInset;
    table.dataSource = self;
    table.delegate = self;
    [self.view addSubview:table];
    [self.view addSubview:menuBar];
    NSDictionary *spendingDict = [tutorDict[@"data"] objectForKey:@"spending"];
    NSDictionary *incomeDict = [tutorDict[@"data"] objectForKey:@"income"];
    spending = [[NSMutableArray alloc] init];
    income = [[NSMutableArray alloc] init];
    if (spendingDict) {
        for (id key in spendingDict) {
            NSLog(@"spending,%@,%@",key,spendingDict[key]);
            [spending addObject:@{@"id":key,@"data":spendingDict[key],@"date":[spendingDict[key] objectForKey:@"numberDate"]}];
        }
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                     ascending:YES];
        spending = [[NSMutableArray alloc] initWithArray:[spending sortedArrayUsingDescriptors:@[sortDescriptor]]];
    }
    if (incomeDict) {
        for (id key in incomeDict) {
            NSLog(@"oincome,%@,%@",key,incomeDict[key]);
            [income addObject:@{@"id":key,@"data":incomeDict[key]}];
        }
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                     ascending:NO];
        income = [[NSMutableArray alloc] initWithArray:[income sortedArrayUsingDescriptors:@[sortDescriptor]]];
    }
    [table reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)close {
//    [self.navigationController popViewControllerAnimated:YES];
    NSLog(@"close");
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth]-30, 25)];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 25)];
    title.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
    title.textColor = [UIColor lightGrayColor];
    if (section == 0) {
        title.text = @"SPENDING";
    } else {
        title.text = @"INCOME";
    }
    [header addSubview:title];
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 25;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSDictionary *spendingDict = spending[indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:spendingDict[@"id"]];
        if (!cell) {
            NSDictionary *invoiceData = spendingDict[@"data"];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:spendingDict[@"id"]];
            cell.textLabel.text = [NSString stringWithFormat:@"$%@",invoiceData[@"amount"]];
            cell.detailTextLabel.text = spendingDict[@"id"];
        }
        return cell;
    } else {
        NSDictionary *incomeDict = income[indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:incomeDict[@"id"]];
        if (!cell) {
            NSDictionary *invoiceData = incomeDict[@"data"];
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:incomeDict[@"id"]];
            cell.textLabel.text = [NSString stringWithFormat:@"$%@",invoiceData[@"amount"]];
            cell.detailTextLabel.text = incomeDict[@"id"];
        }
        return cell;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return spending.count;
    } else if (section == 1) {
        return income.count;
    } else {
        return 0;
    }
}


@end
