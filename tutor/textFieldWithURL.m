//
//  textFieldWithURL.m
//  tutor
//
//  Created by Robert Crosby on 12/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "textFieldWithURL.h"

@implementation textFieldWithURL

-(id)initWithURL:(NSString*)tUrl {
    self = [super init];
    if (self) {
        self.autocorrectionType = UITextAutocorrectionTypeNo;
        self.returnKeyType = UIReturnKeyDone;
        url = tUrl;
    }
    return self;
}

-(NSString*)getURL {
    return url;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
