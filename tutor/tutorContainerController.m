//
//  tutorContainerController.m
//  tutor
//
//  Created by Robert Crosby on 10/24/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "tutorContainerController.h"

@interface tutorContainerController ()

@end

@implementation tutorContainerController

-(void)viewWillAppear:(BOOL)animated {
    self.view.backgroundColor = [References background];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return [References statusBarColor];
}
    
- (void)viewDidLoad {
    tutor = [[tutorObject alloc] initWithID:_tutorID andCompletion:^(bool finished) {
        if (finished) {
            NSLog(@"Loaded");
        }
    }];
    [self.view addSubview:[tutor bookTutorView:self withInsets:UIEdgeInsetsMake([References topMargin]+44, 0, [References bottomMargin]+50, 0)]];
    menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin])];
    [References blurView:menuBar];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accent] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)close {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
