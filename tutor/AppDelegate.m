//
//  AppDelegate.m
//  tutor
//
//  Created by Robert Crosby on 1/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize navController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRConfiguration.sharedInstance setLoggerLevel:FIRLoggerLevelError];
    [FIRApp configure];
//    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
//    NSDictionary * dict = [defs dictionaryRepresentation];
//    for (id key in dict) {
//        if (![(NSString*)key isEqualToString:@"currentDomain"] && ![(NSString*)key isEqualToString:@"school"] && ![(NSString*)key isEqualToString:@"clientToken"] && ![(NSString*)key isEqualToString:@"currentVersion"] && ![(NSString*)key isEqualToString:@"agreement"] && ![(NSString*)key isEqualToString:@"v2"] && ![(NSString*)key isEqualToString:@"departmentDatabases"]) {
//            [defs removeObjectForKey:key];
//        }
//    }
    #if DEBUG
        NSLog(@"SANDBOX");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"sandbox"];
    #else
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"sandbox"];
    #endif
//    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"openTime"];
//    [defs synchronize];
    // Override point for customization after application launch.
    [BTAppSwitch setReturnURLScheme:@"com.moop.tutor-ios.payments"];
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"v2"] != true) {
//        [[FIRAuth auth] signOut:nil];
//    }
    
    if ([FIRAuth auth].currentUser) {
        NSLog(@"uid: %@",[[FIRAuth auth] currentUser].uid);
        [self clearBadge];
        self.navController = [[UINavigationController alloc] initWithRootViewController:[[homeViewController alloc] init]];
//        self.navController = [[UINavigationController alloc] initWithRootViewController:[[nHomeViewController alloc] init]];
    } else {
        self.navController = [[UINavigationController alloc] initWithRootViewController:[[nGettingStartedViewController alloc] init]];
    }
    
    
    self.navController.navigationBarHidden = YES;
    self.window.rootViewController = self.navController;
    [self.window makeKeyAndVisible];
    return YES;
}

-(void)applicationWillResignActive:(UIApplication *)application {
    NSDate *open = [[NSUserDefaults standardUserDefaults] objectForKey:@"openTime"];
    NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:open];
    [FIRAnalytics logEventWithName:@"duration" parameters:@{@"runtime":[NSNumber numberWithDouble:time]}];
    NSLog(@"closing: %f",time);
    [self.window endEditing:YES];
}
-(void)clearBadge {
    @try {
        if ([FIRAuth auth].currentUser)  {
            if ([[UIApplication sharedApplication] applicationIconBadgeNumber] > 0) {
                [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] child:@"badge"] removeValue];
                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
            }
        }

    } @catch (NSException *exception) {
        NSLog(@"error");
    }
    
    
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    if ([url.scheme localizedCaseInsensitiveCompare:@"com.moop.tutor-ios.payments"] == NSOrderedSame) {
        return [BTAppSwitch handleOpenURL:url options:options];
    }
    return NO;
}

// If you support iOS 7 or 8, add the following method.
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([url.scheme localizedCaseInsensitiveCompare:@"com.moop.tutor-ios.payments"] == NSOrderedSame) {
        return [BTAppSwitch handleOpenURL:url sourceApplication:sourceApplication];
    }
    return NO;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self.window endEditing:YES];
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [self clearBadge];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"openTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self clearBadge];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *oldToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
    NSLog(@"content---%@", token);
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([oldToken isEqualToString:token]) {
        NSLog(@"already saved token");
    } else {
        NSLog(@"updating token");
        if ([[FIRAuth auth] currentUser]) {
            [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[[FIRAuth auth] currentUser].uid] child:@"token"] setValue:token withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                } else {
                    NSLog(@"%@",error.localizedDescription);
                }
            }];
        }
        
    }
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler {
    NSLog(@"RECIEVED");
    NSInteger notifications = [[NSUserDefaults standardUserDefaults] integerForKey:@"notifications"] + 1;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:notifications];
    [[NSUserDefaults standardUserDefaults] setInteger:notifications forKey:@"notifications"];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    NSInteger notifications = [[NSUserDefaults standardUserDefaults] integerForKey:@"notifications"] + 1;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:notifications];
    [[NSUserDefaults standardUserDefaults] setInteger:notifications forKey:@"notifications"];
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
}

@end
