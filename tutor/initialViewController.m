//
//  initialViewController.m
//  tutor
//
//  Created by Robert Crosby on 1/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "initialViewController.h"

#define animationSpeed 0.5
#define distanceToMove 40
#define DARKGREEN "#1B5E20"
#define LIGHTGREY "#9E9E9E"

@interface initialViewController ()


@end

@implementation initialViewController

-(void)viewWillAppear:(BOOL)animated {

}

-(void)viewDidAppear:(BOOL)animated {
    
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


- (void)viewDidLoad {
    [self createBackgroundVideo];
    self.ref = [[FIRDatabase database] reference];
    [super viewDidLoad];
    [self performSelector:@selector(startUpLabels) withObject:nil afterDelay:1];
}

-(void)nextButton {
    if (entryField.tag == 5) {
        [self password];
    } else if (entryField.tag == 6) {
        if (verifyCode == entryField.text) {
            [self password];
        }
    }
    
}

-(void)errorFeedback:(NSString*)message {
    UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
    [feedback prepare];
    [feedback notificationOccurred:UINotificationFeedbackTypeError];
    [entryField setText:@""];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [entryField resignFirstResponder];
    if (entryField.tag == 1) {
        email = entryField.text;
        NSLog(@"email: %@",email);
        if ([email containsString:@"uoregon.edu"] || [email containsString:@"rcros97@me.com"]) {
            [[FIRAuth auth] fetchProvidersForEmail:email completion:^(NSArray<NSString *> * _Nullable providers, NSError * _Nullable error) {
                if (!error) {
                    if (providers.count > 0) {
                        isSignIn = YES;
                        [self password];
                    } else {
                        isSignIn = NO;
                        [self secondStep];
                    }
                }
            }];
//            [[[[[FIRDatabase database] reference] child:@"users"] child:@"emailLookup"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//                for (id key in snapshot.value) {
//                    NSLog(@"%@",snapshot.value[key]);
//                }
////
//            }];
        } else {
            [self errorFeedback:@"Only Oregon Emails Currently"];
        }
    }
    if (entryField.tag == 2) {
        name = entryField.text;
        NSLog(@"name: %@",name);
        [self thirdStep];
    }
    if (entryField.tag == 3) {
        phone = entryField.text;
        NSLog(@"phone: %@",phone);
        [self fourthStep:NO];
    }
    if (entryField.tag == 4) {
        age = entryField.text;
        NSLog(@"age: %@",age);
        [self fifthStep];
    }
    if (entryField.tag == 7) {
        password = entryField.text;
        [self continueWithSignUp:!isSignIn];
    }
    return YES;
}


-(void)goBack {
    if (entryField.tag == 1) {
        UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
        [feedback prepare];
        [feedback notificationOccurred:UINotificationFeedbackTypeWarning];
        [entryField setText:@""];
    } else if (entryField.tag == 2) {
        [self firstStep:YES];
    } else if (entryField.tag == 3) {
        [self secondStep];
    } else if (entryField.tag == 4) {
        [self thirdStep];
    } else if (entryField.tag == 5) {
        [self fourthStep:YES];
    } else if (entryField.tag == 6) {
        [self fifthStep];
    } else if (entryField.tag == 7) {
        [self firstStep:YES];
    } else {
        UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
        [feedback prepare];
        [feedback notificationOccurred:UINotificationFeedbackTypeWarning];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startUpLabels {
    mainHeading = [[UILabel alloc] initWithFrame:CGRectMake(20, 80-distanceToMove, [References screenWidth]-40, 60)];
    mainHeading.font = [UIFont systemFontOfSize:50 weight:UIFontWeightBlack];
    mainHeading.textColor = [UIColor whiteColor];
    mainHeading.text = @"TutorTree";
    [References textShadow:mainHeading];
    mainHeading.alpha = 0;
    [self.view addSubview:mainHeading];
    subHeading = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:mainHeading]+25-distanceToMove, [References screenWidth]-50, 80)];
    subHeading.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
    subHeading.textColor = [UIColor whiteColor];
    subHeading.numberOfLines = 4;
    subHeading.text = @"Follow the steps below to become a tutor or a student in a few minutes.\n\nIf you've already signed up, enter that email below.";
    [References textShadow:subHeading];
    subHeading.alpha = 0;
    [self.view addSubview:subHeading];
    if ([FIRAuth auth].currentUser) {
        mainHeading.text = @"Tutoring...";
    }
    [UIView animateWithDuration:animationSpeed animations:^(void){
        if (![FIRAuth auth].currentUser) {
            subHeading.alpha = 1;
            subHeading.frame = CGRectMake(subHeading.frame.origin.x, subHeading.frame.origin.y+distanceToMove, subHeading.frame.size.width, subHeading.frame.size.height);
        }
        mainHeading.alpha = 1;
        mainHeading.frame = CGRectMake(mainHeading.frame.origin.x, mainHeading.frame.origin.y+distanceToMove, mainHeading.frame.size.width, mainHeading.frame.size.height);
        
    } completion:^(bool finished){
        if (finished) {
            if ([FIRAuth auth].currentUser) {
                [self performSelector:@selector(openApp) withObject:[NSNumber numberWithBool:NO] afterDelay:2.0];
            } else {
                [self firstStep:NO];
            }
        }
    }];
}

-(void)firstStep:(bool)isBack{
    if (!isBack) {
        mainInstruction = [[UILabel alloc] initWithFrame:CGRectMake(20, [References screenHeight]/3-distanceToMove, [References screenWidth]-40, 30)];
        mainInstruction.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
        mainInstruction.textColor = [UIColor whiteColor];
        mainInstruction.text = @"What's your Oregon email?";
        mainInstruction.textAlignment = NSTextAlignmentCenter;
        [References textShadow:mainInstruction];
        mainInstruction.alpha = 0;
        [self.view addSubview:mainInstruction];
        entryField = [[UITextField alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:mainInstruction]+50-distanceToMove, [References screenWidth]-40, 30)];
        [entryField setValue:[UIColor lightGrayColor]
                  forKeyPath:@"_placeholderLabel.textColor"];
        [entryField setBorderStyle:UITextBorderStyleNone];
        [entryField setKeyboardAppearance:UIKeyboardAppearanceDark];
        entryField.textAlignment = NSTextAlignmentCenter;
        [entryField setTextColor:[UIColor whiteColor]];
        [entryField setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightBold]];
        [entryField setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.00]];
        [entryField setPlaceholder:@"@uoregon.edu email"];
        entryField.delegate = self;
        [entryField setKeyboardType:UIKeyboardTypeEmailAddress];
        [References textShadow:entryField];
        entryField.alpha = 0;
        [self.view addSubview:entryField];
        entryLine = [[UIView alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:entryField]+45-distanceToMove, [References screenWidth]-40, 1)];
        [entryLine setBackgroundColor:[UIColor whiteColor]];
        [References bottomshadow:entryLine];
        entryLine.alpha = 0;
        [self.view addSubview:entryLine];
        
        next = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-20-100, entryLine.frame.origin.y+40-distanceToMove, 100, 50)];
        [next.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightBold]];
        [next setTitle:@"Next" forState:UIControlStateNormal];
        [next setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [next addTarget:self action:@selector(textFieldShouldReturn:) forControlEvents:UIControlEventTouchUpInside];
        [next addTarget:self action:@selector(nextButton) forControlEvents:UIControlEventTouchUpInside];
        [References textShadow:next];
        next.alpha = 0;
        [self.view addSubview:next];
        
        back = [[UIButton alloc] initWithFrame:CGRectMake(20, entryLine.frame.origin.y+40-distanceToMove, 100, 50)];
        [back.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightBold]];
        [back setTitle:@"Back" forState:UIControlStateNormal];
        [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [back addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
        [References textShadow:back];
        back.alpha = 0;
        [self.view addSubview:back];
        [UIView animateWithDuration:animationSpeed animations:^(void){
            mainInstruction.alpha = 1;
            mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
            entryField.alpha = 1;
            entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
            entryLine.alpha = 1;
            entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
            back.alpha = 1;
            back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
            next.alpha = 1;
            next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
        } completion:^(bool finished){
            if (finished) {
                entryField.tag = 1;
                [entryField becomeFirstResponder];
            }
        }];
    } else {
        
        [UIView animateWithDuration:animationSpeed animations:^(void){
            mainInstruction.alpha = 0;
            mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y-distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
            entryField.alpha = 0;
            entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y-distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
            entryLine.alpha = 0;
            entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y-distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
            back.alpha = 0;
            back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y-distanceToMove, back.frame.size.width, back.frame.size.height);
            next.alpha = 0;
            next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y-distanceToMove, next.frame.size.width, next.frame.size.height);
        } completion:^(bool finished){
            if (finished) {
                mainInstruction.text = @"What's your email?";
                entryField.text = @"";
                entryField.placeholder = @"Email";
                entryField.keyboardType = UIKeyboardTypeEmailAddress;
                [UIView animateWithDuration:animationSpeed animations:^(void){
                    mainInstruction.alpha = 1;
                    mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
                    entryField.alpha = 1;
                    entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
                    entryLine.alpha = 1;
                    entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
                    back.alpha = 1;
                    back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
                    next.alpha = 1;
                    next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
                } completion:^(bool finished){
                    if (finished) {
                        entryField.tag = 1;
                        [entryField becomeFirstResponder];
                    }
                }];
            }
        }];
        
    }
    
}

-(void)secondStep {
    [UIView animateWithDuration:animationSpeed animations:^(void){
        mainInstruction.alpha = 0;
        mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y-distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
        entryField.alpha = 0;
        entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y-distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
        entryLine.alpha = 0;
        entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y-distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
        back.alpha = 0;
        back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y-distanceToMove, back.frame.size.width, back.frame.size.height);
        next.alpha = 0;
        next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y-distanceToMove, next.frame.size.width, next.frame.size.height);
    } completion:^(bool finished){
        if (finished) {
            mainInstruction.text = @"What's your name?";
            entryField.text = @"";
            entryField.placeholder = @"Name";
            entryField.keyboardType = UIKeyboardTypeAlphabet;
            [UIView animateWithDuration:animationSpeed animations:^(void){
                mainInstruction.alpha = 1;
                mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
                entryField.alpha = 1;
                entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
                entryLine.alpha = 1;
                entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
                back.alpha = 1;
                back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
                next.alpha = 1;
                next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
            } completion:^(bool finished){
                if (finished) {
                    entryField.tag = 2;
                    [entryField becomeFirstResponder];
                }
            }];
        }
    }];
}

-(void)thirdStep {
    [UIView animateWithDuration:animationSpeed animations:^(void){
        mainInstruction.alpha = 0;
        mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y-distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
        entryField.alpha = 0;
        entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y-distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
        entryLine.alpha = 0;
        entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y-distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
        back.alpha = 0;
        back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y-distanceToMove, back.frame.size.width, back.frame.size.height);
        next.alpha = 0;
        next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y-distanceToMove, next.frame.size.width, next.frame.size.height);
    } completion:^(bool finished){
        if (finished) {
            mainInstruction.text = @"What's your phone number?";
            entryField.text = @"";
            entryField.placeholder = @"Phone Number";
            entryField.keyboardType = UIKeyboardTypePhonePad;
            [UIView animateWithDuration:animationSpeed animations:^(void){
                mainInstruction.alpha = 1;
                mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
                entryField.alpha = 1;
                entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
                entryLine.alpha = 1;
                entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
                back.alpha = 1;
                back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
                next.alpha = 1;
                next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
            } completion:^(bool finished){
                if (finished) {
                    entryField.tag = 3;
                    [entryField becomeFirstResponder];
                }
            }];
        }
    }];
}

-(void)fourthStep:(bool)isBack {
    [UIView animateWithDuration:animationSpeed animations:^(void){
        if (isBack) {
            profileImage.alpha = 0;
            profileImage.frame = CGRectMake(profileImage.frame.origin.x, profileImage.frame.origin.y-distanceToMove, profileImage.frame.size.width, profileImage.frame.size.height);
        } else {
            entryField.alpha = 0;
            entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y-distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
            entryLine.alpha = 0;
            entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y-distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
        }
        mainInstruction.alpha = 0;
        mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y-distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
        back.alpha = 0;
        back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y-distanceToMove, back.frame.size.width, back.frame.size.height);
        next.alpha = 0;
        next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y-distanceToMove, next.frame.size.width, next.frame.size.height);
    } completion:^(bool finished){
        if (finished) {
            mainInstruction.text = @"How old are you?";
            entryField.text = @"";
            entryField.placeholder = @"Age";
            entryField.keyboardType = UIKeyboardTypePhonePad;
            [UIView animateWithDuration:animationSpeed animations:^(void){
                mainInstruction.alpha = 1;
                mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
                entryField.alpha = 1;
                entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
                entryLine.alpha = 1;
                entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
                back.alpha = 1;
                back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
                next.alpha = 1;
                next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
            } completion:^(bool finished){
                if (finished) {
                    entryField.tag = 4;
                    [entryField becomeFirstResponder];
                }
            }];
        }
    }];
}

-(void)fifthStep {
    [UIView animateWithDuration:animationSpeed animations:^(void){
        mainInstruction.alpha = 0;
        mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y-distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
        entryField.alpha = 0;
        entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y-distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
        entryLine.alpha = 0;
        entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y-distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
        back.alpha = 0;
        back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y-distanceToMove, back.frame.size.width, back.frame.size.height);
        next.alpha = 0;
        next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y-distanceToMove, next.frame.size.width, next.frame.size.height);
    } completion:^(bool finished){
        if (finished) {
            mainInstruction.text = @"Add a picture";
            profileImage = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]/2-60, [References getBottomYofView:mainInstruction]+60-distanceToMove, 120, 120)];
            [profileImage setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:1]];
            [profileImage.titleLabel setFont:[UIFont systemFontOfSize:32 weight:UIFontWeightLight]];
            [profileImage setTitleColor:[[UIColor blackColor] colorWithAlphaComponent:0.1] forState:UIControlStateNormal];
            [profileImage setTitle:@"+" forState:UIControlStateNormal];
            [profileImage addTarget:self action:@selector(choosePhoto) forControlEvents:UIControlEventTouchUpInside];
            [References cardButton:profileImage];
            profileImage.alpha = 0;
            [self.view addSubview:profileImage];
            [UIView animateWithDuration:animationSpeed animations:^(void){
                mainInstruction.alpha = 1;
                mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
                profileImage.alpha = 1;
                profileImage.frame = CGRectMake(profileImage.frame.origin.x, profileImage.frame.origin.y+distanceToMove, profileImage.frame.size.width, profileImage.frame.size.height);
                back.alpha = 1;
                back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
                next.alpha = 1;
                next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
            } completion:^(bool finished){
                if (finished) {
                    entryField.tag = 5;
                }
            }];
        }
    }];
}

-(void)verifyStep {
    [UIView animateWithDuration:animationSpeed animations:^(void){
        mainInstruction.alpha = 0;
        mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y-distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
        profileImage.alpha = 0;
        profileImage.frame = CGRectMake(profileImage.frame.origin.x, profileImage.frame.origin.y-distanceToMove, profileImage.frame.size.width, profileImage.frame.size.height);
        back.alpha = 0;
        back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y-distanceToMove, back.frame.size.width, back.frame.size.height);
        next.alpha = 0;
        next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y-distanceToMove, next.frame.size.width, next.frame.size.height);
        
    } completion:^(bool finished){
        if (finished) {
            
            mainInstruction.text = @"Verification Code";
            entryField.text = @"";
            entryField.placeholder = @"Code";
            entryField.keyboardType = UIKeyboardTypePhonePad;
            [UIView animateWithDuration:animationSpeed animations:^(void){
                mainHeading.text = @"Almost...";
                subHeading.text = @"You should recieve an email with a code to verify your elgibility.";
                mainInstruction.alpha = 1;
                mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
                entryField.alpha = 1;
                entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
                entryLine.alpha = 1;
                entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
                back.alpha = 1;
                back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
                next.alpha = 1;
                next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
            } completion:^(bool finished){
                if (finished) {
                    entryField.tag = 6;
                    [entryField becomeFirstResponder];
                    [entryField addTarget:self
                                  action:@selector(textFieldDidChange:)
                        forControlEvents:UIControlEventEditingChanged];
                }
            }];
        }
    }];
}

-(void)password {
    [UIView animateWithDuration:animationSpeed animations:^(void){
        profileImage.alpha = 0;
        profileImage.frame = CGRectMake(profileImage.frame.origin.x, profileImage.frame.origin.y-distanceToMove, profileImage.frame.size.width, profileImage.frame.size.height);
        mainInstruction.alpha = 0;
        mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y-distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
        back.alpha = 0;
        back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y-distanceToMove, back.frame.size.width, back.frame.size.height);
        next.alpha = 0;
        next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y-distanceToMove, next.frame.size.width, next.frame.size.height);
    } completion:^(bool finished){
        if (finished) {
            if (isSignIn) {
                mainInstruction.text = @"Enter your password";
            } else {
                mainInstruction.text = @"Create a password";
            }
            entryField.text = @"";
            entryField.placeholder = @"••••••••";
            entryField.secureTextEntry = YES;
            entryField.keyboardType = UIKeyboardTypeAlphabet;
            [UIView animateWithDuration:animationSpeed animations:^(void){
                mainInstruction.alpha = 1;
                mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
                entryField.alpha = 1;
                entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
                entryLine.alpha = 1;
                entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
                back.alpha = 1;
                back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
                next.alpha = 1;
                next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
            } completion:^(bool finished){
                if (finished) {
                    entryField.tag = 7;
                    [entryField becomeFirstResponder];
                }
            }];
        }
    }];
}

-(void)continueWithSignUp:(bool)isSignUp {
    [UIView animateWithDuration:animationSpeed animations:^(void){
            mainInstruction.frame = CGRectMake(mainInstruction.frame.origin.x, mainInstruction.frame.origin.y+distanceToMove, mainInstruction.frame.size.width, mainInstruction.frame.size.height);
            mainInstruction.alpha = 0;
            entryField.frame = CGRectMake(entryField.frame.origin.x, entryField.frame.origin.y+distanceToMove, entryField.frame.size.width, entryField.frame.size.height);
            entryField.alpha = 0;
            back.frame = CGRectMake(back.frame.origin.x, back.frame.origin.y+distanceToMove, back.frame.size.width, back.frame.size.height);
            back.alpha = 0;
            next.frame = CGRectMake(next.frame.origin.x, next.frame.origin.y+distanceToMove, next.frame.size.width, next.frame.size.height);
            next.alpha = 0;
            entryLine.frame = CGRectMake(entryLine.frame.origin.x, entryLine.frame.origin.y+distanceToMove, entryLine.frame.size.width, entryLine.frame.size.height);
            entryLine.alpha = 0;
            mainHeading.text = @"Welcome";
            mainHeading.frame = CGRectMake(mainHeading.frame.origin.x, mainHeading.frame.origin.y+distanceToMove, mainHeading.frame.size.width, mainHeading.frame.size.height);
            subHeading.frame = CGRectMake(subHeading.frame.origin.x, subHeading.frame.origin.y+distanceToMove, subHeading.frame.size.width, subHeading.frame.size.height-20);
            subHeading.alpha = 1;
            subHeading.text = @"One Second...";
        } completion:^(bool complete){
            if (complete) {
                    if (isSignUp) {
                        [[FIRAuth auth] createUserWithEmail:email password:password completion:^(FIRAuthDataResult * _Nullable authResult, NSError * _Nullable error) {
                            dispatch_async(dispatch_get_main_queue(), ^(){
                            NSString *institution = email;
                            institution = [[institution componentsSeparatedByString:@"@"] objectAtIndex:1];
                            institution = [[institution componentsSeparatedByString:@"."] objectAtIndex:0];
                            [[NSUserDefaults standardUserDefaults] setObject:institution forKey:@"institution"];
                            [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
                            [[NSUserDefaults standardUserDefaults] setObject:name forKey:@"name"];
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            [self useImage:authResult.user];
                            });
                        }];
                    } else {
                        [[FIRAuth auth] signInWithEmail:email password:password completion:^(FIRAuthDataResult * _Nullable authResult, NSError * _Nullable error) {
                            if (!error) {
                                dispatch_async(dispatch_get_main_queue(), ^(){
                                    
                                NSString *institution = email;
                                institution = [[institution componentsSeparatedByString:@"@"] objectAtIndex:1];
                                institution = [[institution componentsSeparatedByString:@"."] objectAtIndex:0];
                                [[NSUserDefaults standardUserDefaults] setObject:institution forKey:@"institution"];
                                [[NSUserDefaults standardUserDefaults] setObject:email forKey:@"email"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                [self openApp:[NSNumber numberWithBool:NO] andImage:nil];
                                });
                            }
                        }];
            }
        }
        }];

}

-(void)textFieldDidChange:(UITextField*)textfield {
    if (textfield.tag == 6) {
            if ([textfield.text isEqualToString:verifyCode]) {
                [self password];
                [textfield resignFirstResponder];
                UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
                [feedback prepare];
                [feedback notificationOccurred:UINotificationFeedbackTypeSuccess];
            } else if (entryField.text.length == 6) {
                UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
                [feedback prepare];
                [feedback notificationOccurred:UINotificationFeedbackTypeError];
                [entryField setText:[entryField.text stringByReplacingCharactersInRange:NSMakeRange(5, 1) withString:@""]];
            }
        
    }
}

-(void)createBackgroundVideo {
    NSString *thePath=[[NSBundle mainBundle] pathForResource:@"whiteboard" ofType:@"mp4"];
    NSURL *theurl=[NSURL fileURLWithPath:thePath];
    _backgroundVideo = [AVPlayer playerWithURL:theurl];
    _backgroundVideo.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    AVPlayerLayer *videoLayer = [AVPlayerLayer playerLayerWithPlayer:_backgroundVideo];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionMixWithOthers error:nil];
    videoLayer.frame = CGRectMake(-1.5*[References screenWidth], 0, [References screenWidth]*2.5, [References screenHeight]);
    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:videoLayer];
    [_backgroundVideo play];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[_backgroundVideo currentItem]];
}

- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero completionHandler:nil];
}

-(void)choosePhoto {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing  = YES;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    chosenImage = [info valueForKey:UIImagePickerControllerEditedImage];
    [profileImage setBackgroundImage:chosenImage forState:UIControlStateNormal];
    
    [References cornerRadius:profileImage radius:8.0];
    //Or you can get the image url from AssetsLibrary
    //NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];

    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)useImage:(FIRUser*)user{
    FIRStorage *storage = [FIRStorage storage];
    FIRStorageReference *storageRef = [storage reference];

    NSData *imgData = UIImageJPEGRepresentation(chosenImage, 1);
    NSString *ref = [NSString stringWithFormat:@"images/%@.jpg",[References randomStringWithLength:16]];
    // Create a reference to the file you want to upload
    FIRStorageReference *imageRef = [storageRef child:ref];

    // Upload the file to the path "images/rivers.jpg"
    [imageRef putData:imgData
             metadata:nil
           completion:^(FIRStorageMetadata *metadata,
                        NSError *error) {
               if (error != nil) {
                   dispatch_async(dispatch_get_main_queue(), ^(){
                       [self openApp:[NSNumber numberWithBool:YES] andImage:@"https://images.pexels.com/photos/242236/pexels-photo-242236.jpeg?cs=srgb&dl=background-cement-concrete-242236.jpg&fm=jpg"];
                   });
                   
                   NSLog(@"ERRO: %@",error.localizedDescription);
                   // Uh-oh, an error occurred!
               } else {
                   // Metadata contains file metadata such as size, content-type, and download URL.
                   dispatch_async(dispatch_get_main_queue(), ^(){
                       [imageRef downloadURLWithCompletion:^(NSURL * _Nullable URL, NSError * _Nullable error) {
                           [self openApp:[NSNumber numberWithBool:YES] andImage:URL.absoluteString];
                       }];
                   });
                   
               }
           }];
}

-(BOOL)addUserToDatabase:(FIRUser*)user withImage:(NSString*)image{

    
    return YES;
}

-(void)createFirebaseTutor {
    self.ref = [[[[[[FIRDatabase database] reference] child:@"users"] child:[[FIRAuth auth] currentUser].uid]  child:@"tutorInfo"] child:@"availability"];
    int minute = 0;
    NSArray *days = [[NSArray alloc] initWithObjects:@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday", nil];
    for (int c = 0; c < days.count; c++) {
        for (int a = 0; a < 48; a++) {
            NSString *timeString = [NSString stringWithFormat:@"%i",minute];
            [[[self.ref child:days[c]] child:timeString] setValue:@0];
            minute = minute + 30;
        }
        minute = 0;
    }

}

-(void)openApp:(NSNumber*)signup andImage:(NSString*)image {
//    feedViewController *initialHelpView = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"feedViewController"];
//    initialHelpView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    initialHelpView.isSignUp = signup;
//    initialHelpView.name = name;
//    initialHelpView.email = email;
//    initialHelpView.age = age;
//    if (image != nil) {
//        initialHelpView.image = image;
//    }
//    [self presentViewController:initialHelpView animated:YES completion:nil];
}



@end
