//
//  References.h
//  Hitch for iOS
//
//  Created by Robert Crosby on 11/15/16.
//  Copyright © 2016 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AFHTTPSessionManager.h>

@import Firebase;

@interface References : UIViewController <NSURLConnectionDataDelegate> {
    NSMutableData *_responseData;
    NSString *result;
}

+(void)titleToastMessage:(NSString *)message andTitle:(NSString*)title andView:(UIViewController *)view andClose:(bool)close;
+(void)selectionFeedback:(UIImpactFeedbackStyle)style;
+(void)successFeedback;
+(void)warningFeedback;
+(void)postError:(NSString*)error;
+(NSString *)backendAddress;
+(UIColor *)colorFromHexString:(NSString *)hexString;
+(CGFloat)screenWidth;
+(CGFloat)screenHeight;
+(void)borderColor:(UIView*)view color:(UIColor*)color;
+(void)cornerRadius:(UIView*)view radius:(float)radius;
+(void)bottomshadow:(UIView*)view;
+(void)topshadow:(UIView*)view;
+(void)biggerbottomshadow:(UIView*)view;
+(void)biggestbottomshadow:(UIView*)view;
+(void)tighterBottomShadow:(UIView*)view;
+(void)textShadow:(UILabel*)view;
+(void)cardButton:(UIView*)view;
+(void)fade:(UIView*)view alpha:(float)alpha;
+(void)fromoffscreen:(UIView*)view where:(NSString*)where;
+(void)fromonscreen:(UIView*)view where:(NSString*)where;
+(void)justMoveOffScreen:(UIView*)view where:(NSString*)where;
+(void)justMoveOnScreen:(UIView*)view where:(NSString*)where;
+(void)fadeThenMove:(UIView*)view where:(NSString*)where;
+(void)fadeIn:(UIView*)view;
+(void)fadeOut:(UIView*)view;
+(void)shift:(UIView*)view X:(float)X Y:(float)Y W:(float)W H:(float)H;
+(void)adjustHeight:(UIView*)view H:(float)H;
+(void)moveUp:(UIView*)view yChange:(float)yChange;
+(void)moveDown:(UIView*)view yChange:(float)yChange;
+(void)moveBackDown:(UIView*)view frame:(CGRect)frame;
+(void)blurView:(UIView*)view;
+(void)darkBlurView:(UIView*)view;
+(void)textFieldInset:(UITextField*)text;
+(void)fadeColor:(UIView*)view color:(UIColor*)color;
+(void)fadeButtonTextColor:(UIButton*)view color:(UIColor*)color;
+(void)fadeButtonColor:(UIButton*)view color:(UIColor*)color;
+(void)fadeLabelTextColor:(UILabel*)view color:(UIColor*)color;
+(void)tintUIButton:(UIButton*)button color:(UIColor*)color;
+(void)tintUIImage:(UIImageView*)image color:(UIColor*)color;
+(void)createVerticalLine:(UIView*)superview atX:(int)x andY:(int)y inFront:(bool)inFront andHeight:(int)height;
+(void)createLine:(UIView*)superview  xPos:(int)xPos yPos:(int)yPos inFront:(bool)inFront;
+(void)createDarkLine:(UIView*)superview  xPos:(int)xPos yPos:(int)yPos inFront:(bool)inFront;
+(void)createLinewithWidth:(UIView*)superview  xPos:(int)xPos yPos:(int)yPos inFront:(bool)inFront andWidth:(int)width;
+(void)ViewToLine:(UIView*)superview withView:(UIView*)view xPos:(int)xPos yPos:(int)yPos;
+(void)fadeLabelText:(UILabel*)view newText:(NSString*)newText;
+(void)fadePlaceholderText:(UITextField*)view newText:(NSString*)newText;
+(void)moveHorizontal:(UIView*)view where:(NSString*)where;
+(NSString *) randomStringWithLength: (int) len;
+(NSString *) randomNumberLetterStringWithLength: (int) len;
+(NSString *) randomIntWithLength: (int) len;
+(UIView*)createGradient:(UIColor*)colorA andColor:(UIColor*)colorB withFrame:(CGRect)frame;
+(UIColor*)returnColorWithPreset:(NSString*)preset;
+(UIView*)createGradientwithFrame:(CGRect)frame andPreset:(NSString*)preset;
+(void)fadeButtonText:(UIButton*)view text:(NSString*)text;
+(void)toastMessage:(NSString *)message andView:(UIViewController *)view andClose:(bool)close;
+(void)createNotification:(UIViewController*)parentView andMessage:(NSString*)message andColor:(UIColor*)color andHeight:(int)height  andTextColor:(UIColor*)textColor andCompletion:(void (^)(int result))completionHandler;
+(CAGradientLayer*)createGradient:(UIColor*)colorA andColor:(UIColor*)colorB andFrame:(UIView*)frame;
+(void)parallax:(UIView*)view;
+(UIColor*)systemColor:(NSString*)color;
+(void)fullScreenToast:(NSString*)text inView:(UIViewController*)view withSuccess:(BOOL)success andClose:(BOOL)close;
+(NSString*)randomizeString:(NSString*)string;
+(NSString*)normalizeString:(NSString*)string;
+(void)lightblurView:(UIView *)view;
+(int)getBottomYofView:(UIView*)view;
+(CGFloat)topMargin;
+(CGFloat)bottomMargin;

+(UIColor*)lightGrey;
+(UIColor*)darkText;
+(UIColor*)dominantColor;
+(UIColor*)colorAtIndex:(int)index;
+(UIColor*)altColorAtIndex:(int)index;
+(void)roundTopCorners:(UIView*)view withRadius:(CGFloat)radius;
+(void)roundLeftCorners:(UIView*)view withRadius:(CGFloat)radius;
+(void)roundRightCorners:(UIView*)view withRadius:(CGFloat)radius;
+(void)roundBottomCorners:(UIView*)view withRadius:(CGFloat)radius;
+(CGRect)fixedWidthSizeWithFont:(UIFont*)font andString:(NSString*)string andLabel:(UIView*)label;
+(CGRect)fixedHeightSizeWithFont:(UIFont*)font andString:(NSString*)string andLabel:(UIView*)label;
+(void)toastNotificationBottom:(UIView*)view andMessage:(NSString*)message;
+(NSString*)departmentAbbreviation:(NSString*)department;
+(UIView*)noData:(NSString*)title andMessage:(NSString*)message;
+(UIView*)aboutAndTOS:(UIView*)belowView;
+(UIView*)loadingView:(NSString*)message;
+(NSDate *)dateWithOutTime:(NSDate *)datDate;
+(UIColor*)schoolMainColor;
+(UIColor*)schoolInputColor;
+(UIColor*)schoolInputTextColor;
+(UIColor*)schoolAccentColor;
+(UIColor*)schoolTextColor;
+(UIStatusBarStyle)schoolStatusBar;

+(void)adjustLabelHeight:(UILabel*)label;
+(void)adjustLabelWidth:(UILabel*)label;

+(void)postActivity:(NSString *)tag andDescription:(NSString *)description andCompletion:(void (^)(bool result))completionHandler;




+(void)sendTestNotification;
+(void)sendNotification:(NSString*)uid withSubject:(NSString*)subject andMessage:(NSString*)message forceEmail:(BOOL)force andCompletion:(void (^)(bool result))completionHandler;
+(void)sendEmail:(NSString*)email withSubject:(NSString*)subject andMessage:(NSString*)message andCompletion:(void (^)(bool result))completionHandler;

+(NSString*)getSchoolDomain;

+(BOOL)showUser:(NSString*)uid;
+(void)adjustLabelCharacterSpacing:(UILabel*)label withSpacing:(CGFloat)spacing;

// DARK MODE
+(UIColor*)accentColor;
+(UIColor*)background;
+(UIColor*)cells;
+(UIColor*)highlight;
+(UIColor*)accent;
+(UIColor*)primaryText;
+(UIColor*)secondaryText;
+(UIStatusBarStyle)statusBarColor;
+(UIColor*)chatOther;
+(UIColor*)chatSender;
+(UIColor*)chatTextSender;
+(UIColor*)chatTextOther;
+(UIKeyboardAppearance)keyboard;
@end

