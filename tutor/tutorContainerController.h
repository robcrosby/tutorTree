//
//  tutorContainerController.h
//  tutor
//
//  Created by Robert Crosby on 10/24/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "tutorObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface tutorContainerController : UIViewController {
    tutorObject *tutor;
    UIView *menuBar;
}

@property (strong,nonatomic) NSString *tutorID;

@end

NS_ASSUME_NONNULL_END
