//
//  tutorViewController.h
//  tutor
//
//  Created by Robert Crosby on 8/13/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import "tutorNode.h"
#import <HCSStarRatingView.h>
#import <SDWebImage/UIImageView+WebCache.h>

NS_ASSUME_NONNULL_BEGIN

@interface tutorViewController : UIViewController <UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate> {
    CGFloat currentY;
    UIView *menuBar;
    UIScrollView *mainScroll;
    UILabel *bioField,*name,*banner;
    HCSStarRatingView *overallRating;
    NSMutableArray *ratingObjects,*courseObjects;
    UICollectionView *ratingCollection,*courseCollection;
    UITableView *scheduleView;
    int selectedDate;
    NSInteger startTime;
    NSInteger duration;
    NSInteger selectedDateIndex;
    NSMutableArray *availableDays,*selectedDayAvailability,*selectedCells,*showDate,*sectionHeights;
    NSInteger overallRatingValue;
    NSString *startTimeString;
    NSDate *currentDateObject;
    UIButton *bottomButton;
    
    UILabel *backdrop;
    UIScrollView *bookFromHere;
    
}

@property (nonatomic, strong) tutorNode *tutor;

@end

NS_ASSUME_NONNULL_END
