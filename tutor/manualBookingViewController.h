//
//  manualBookingViewController.h
//  tutor
//
//  Created by Robert Crosby on 2/17/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import <MBProgressHUD.h>
@import Firebase;
NS_ASSUME_NONNULL_BEGIN

@interface manualBookingViewController : UIViewController <UITextFieldDelegate> {
    UIScrollView *view;
    UIDatePicker *startDatePicker,*endDatePicker;
    UIButton *tutorEmail,*studentEmail;
    UITextField *course,*amountForTutor,*invoiceID;
    NSString *studentID,*tutorID,*studentName,*tutorName,*studentToken,*tutorToken;
    UISwitch *preconfirmed;
}

@end

NS_ASSUME_NONNULL_END
