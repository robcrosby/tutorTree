//
//  courseDatabase.m
//  tutor
//
//  Created by Robert Crosby on 5/11/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "courseDatabase.h"

@implementation courseDatabase

@synthesize next;
@synthesize parent;

-(id)initAsHead {
    self = [super init];
    if (self) {
        next = NULL;
        parent = NULL;
        isHead = YES;
    }
    return self;
}

-(id)initWithDatabase:(NSDictionary*)course andName:(NSString*)name andDeparment:(NSString*)department{
    self = [super init];
    if (self) {
        tutorCount = 0;
        studentIDs = [[NSMutableArray alloc] init];
        tutors = [[NSMutableArray alloc] initWithArray:[[course objectForKey:@"tutors"] allKeys]];
        tutorCount = tutors.count;
        courseInfo = course[@"info"];
        courseNumber = name;
        courseDepartment = department;
        next = NULL;
        parent = NULL;
        notificationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        notificationLabel.backgroundColor = [References colorFromHexString:@"#2E7D32"];
        notificationLabel.text = @"1";
        notificationLabel.textAlignment = NSTextAlignmentCenter;
        notificationLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
        notificationLabel.textColor = [UIColor whiteColor];
    }
    return self;
}

+(instancetype)databaseWithDictionary:(NSDictionary*)course andName:(NSString*)name andDeparment:(NSString*)department {
    return [[courseDatabase alloc] initWithDatabase:course andName:name andDeparment:department];
}

-(BOOL)isHead {
    return isHead;
}
-(NSArray*)tutors {
    return tutors;
}

-(UILabel*)notificationLabel:(UIView*)inView {
    NSInteger height = (inView.frame.size.height-10)/3;
    notificationLabel.frame = CGRectMake(inView.frame.origin.x+inView.frame.size.width-height-35, [References getBottomYofView:inView]/2-(height/2), height, height);
    [References cornerRadius:notificationLabel radius:notificationLabel.frame.size.width/2];
    notificationLabel.text = [NSString stringWithFormat:@"%li",tutors.count];
    return notificationLabel;
}

-(bool)insertCourse:(courseDatabase*)course {
    if ([self courseNumber] > [course courseNumber]) {
        self.parent.next = course;
        course.parent = self.parent;
        course.next = self;
        self.parent = course;
        return true;
    } else {
        if (self.next == NULL) {
            self.next = course;
            course.parent = self;
            return true;
        } else {
            return [self.next insertCourse:course];
        }
    }
}



-(bool)amTutor {
    if ([tutors containsObject:[FIRAuth auth].currentUser.uid]) {
        return YES;
    } else {
        return NO;
    }
}


-(NSString*)printCourse {
    return [NSString stringWithFormat:@"%@ %@\ntutors: %@",courseDepartment,courseNumber,tutors];
}

-(NSInteger)courseNumber {
    return courseNumber.integerValue;
}

-(NSString*)courseNumberString {
    return courseNumber;
}

-(NSString*)courseDepartmentAndNumber {
    return [NSString stringWithFormat:@"%@: %@",courseDepartment,courseNumber];
}

-(NSString*)courseDepartment {
    return courseDepartment;
}

-(NSInteger)numberOfTutors {
    return tutors.count;
}

-(NSString*)tutorAtIndex:(int)index {
    return tutors[index];
}

-(BOOL)removeTutor {
    for (NSString* key in tutors) {
        if ([key isEqualToString:[FIRAuth auth].currentUser.uid]) {
            [tutors removeObject:key];
            tutorCount--;
        }
    }
    return TRUE;
}

-(BOOL)addTutor {
    [tutors addObject:[FIRAuth auth].currentUser.uid];
    return TRUE;
}
@end
