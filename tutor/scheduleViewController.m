
//
//  scheduleViewController.m
//  tutor
//
//  Created by Robert Crosby on 11/4/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "scheduleViewController.h"

@interface scheduleViewController ()

@end

@implementation scheduleViewController

-(CGFloat)screenWidth {
    return self.view.bounds.size.width;
}

-(CGFloat)screenHeight {
    return self.view.bounds.size.height;
}

-(id)initWithCourse:(NSString *)tcourse andTutors:(NSArray *)ttutors andCourse:(nonnull courseDatabase *)tCourseD{
    self = [super init];
    if (self) {
        statusBarIsHidden = false;
        courseDa = tCourseD;
        selectedRow = -1;
        course = tcourse;
        tutors = [[NSMutableArray alloc] init];
        [self populateTutor:ttutors atIndex:0];
        self.view.backgroundColor = [References background];
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        loadingView = [[UIView alloc] initWithFrame:self.view.bounds];
        loadingView.backgroundColor = [References background];
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake([self screenWidth]/2-30, [self screenHeight]/3, 60, 60)];
        icon.image = [UIImage imageNamed:[[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] ? @"invert-icon.png" : @"icon.png"];
        [loadingView addSubview:icon];
        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:[[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] ? UIActivityIndicatorViewStyleWhite : UIActivityIndicatorViewStyleGray];
        spinner.frame = CGRectMake([self screenWidth]/2-(spinner.frame.size.width/2), [References getBottomYofView:icon]+25, spinner.frame.size.width, spinner.frame.size.height);
        [spinner startAnimating];
        [loadingView addSubview:spinner];
        [self.view addSubview:loadingView];
        [self.view bringSubviewToFront:loadingView];
        
    }
    return self;
}

-(void)populateTutor:(NSArray*)array atIndex:(int)index {
    if (index == array.count) {
        [self createMenuBar];
    } else {
        [[[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:array[index]] child:@"sessions"] queryOrderedByChild:@"start"] queryStartingAtValue:[NSNumber numberWithDouble:[NSDate date].timeIntervalSince1970-86400]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSDictionary *t_sessions = snapshot.value;
            [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:array[index]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if (snapshot.exists) {
                    NSArray *availability = snapshot.value[@"availability"];
                    NSNumber *cost = snapshot.value[@"PPH"];
                    tutorAvailability *tutor = [[tutorAvailability alloc] initWithID:array[index] andAvailability:availability andSessions:t_sessions andName:snapshot.value[@"name"] andURL:snapshot.value[@"profileURL"] andPPH:cost andToken:snapshot.value[@"token"]];
                    [self->tutors addObject:tutor];
                    [self populateTutor:array atIndex:index+1];
                }
            }];
        }];
//
//            [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:array[index]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//                if (snapshot.exists) {
//                    NSDictionary *sessions = snapshot.value[@"sessions"];
//
//                }
//            }];
        
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

-(void)createMenuBar {
    menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], 44+[References topMargin]+54)];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [self screenWidth]-30, 44)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = [course stringByReplacingOccurrencesOfString:@":" withString:@" "];
    
    header.textColor = [References primaryText];
    header.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:header];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accent] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    [self.view sendSubviewToBack:menuBar];
    bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, [self screenHeight]-[References bottomMargin]-54-50, [self screenWidth], [References bottomMargin]+54+50)];
    [References blurView:bottomBar];
    [References createLine:bottomBar xPos:0 yPos:0 inFront:YES];
    controller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], 44)];
    controller.showsHorizontalScrollIndicator = NO;
    [self calculateDays];
    [self createSchedule];
}

-(UIButton*)dateButton:(NSDate*)date andIndex:(int)index{
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(15+((([self screenWidth]-30)/7)*index), [References topMargin]+35, ([self screenWidth]-30)/7, 54)];
    button.tag = index;
    UILabel *daynumber = [[UILabel alloc] initWithFrame:CGRectMake(0, 2.5, button.frame.size.width, button.frame.size.width-15)];
    daynumber.textColor = [References primaryText];
    daynumber.font = [UIFont systemFontOfSize:24 weight:UIFontWeightLight];
    [daynumber setAdjustsFontSizeToFitWidth:YES];
//    daynumber.backgroundColor = [UIColor darkTextColor];
    daynumber.textAlignment = NSTextAlignmentCenter;
    [References cornerRadius:daynumber radius:5];
    fmt.dateFormat = @"d";
    daynumber.text = [fmt stringFromDate:date];
    UILabel *weekday = [[UILabel alloc] initWithFrame:CGRectMake(2.5, [References getBottomYofView:daynumber], button.frame.size.width-5, button.frame.size.height-([References getBottomYofView:daynumber]))];
    weekday.font = [UIFont systemFontOfSize:8 weight:UIFontWeightSemibold];
    weekday.textColor = [References secondaryText];
    weekday.textAlignment = NSTextAlignmentCenter;
    fmt.dateFormat = @"EEE";
    weekday.text = [[fmt stringFromDate:date] uppercaseString];
    [button addSubview:daynumber];
    [button addSubview:weekday];
    [button addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}

-(void)changeDate:(UIButton*)sender {
    selectedRow = -1;
    currentDateInt = sender.tag;
    weekDayInt = [dateInts[sender.tag] integerValue];
    currentDateObject = dates[sender.tag];
    [UIView animateWithDuration:0.25 animations:^{
        self->dateIndicator.frame = CGRectMake(sender.frame.origin.x, self->dateIndicator.frame.origin.y, self->dateIndicator.frame.size.width, self->dateIndicator.frame.size.height);
    }];
    [self updateSchedule];
    [References selectionFeedback:UIImpactFeedbackStyleMedium];
}

-(void)calculateDays {
    dates = [[NSMutableArray alloc] initWithCapacity:7];
    dateInts = [[NSMutableArray alloc] initWithCapacity:7];
    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
    [components setHour: 0];
    [components setMinute: 0];
    [components setSecond: 0];
    NSDate *date = [components date];
    currentDateInt = 0;
    currentDateObject = date;
    for (int a = 0; a < 7; a++) {
        NSDateComponents *innerComponents = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: date];
        NSInteger weekday = [innerComponents weekday];
        if (weekday == 1) {
            weekday = 6;
        } else {
            weekday-=2;
        }
        if (a == 0) {
            weekDayInt = weekday;
            dateIndicator = [[UIView alloc] initWithFrame:CGRectMake(15, [References topMargin]+34+5, ([self screenWidth]-15)/7, 59-2.5)];
            dateIndicator.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
            [References cornerRadius:dateIndicator radius:5];
            [menuBar addSubview:dateIndicator];
        }
        [menuBar addSubview:[self dateButton:date andIndex:a]];
        [dates addObject:date];
        [dateInts addObject:[NSNumber numberWithInteger:weekday]];
        date = [date dateByAddingTimeInterval:86400];
    }

}

-(void)close {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [self dismissViewControllerAnimated:true completion:nil];
        // ipad
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)createSchedule {
    controller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:menuBar], [self screenWidth], [self screenHeight]-[References bottomMargin]-[References getBottomYofView:menuBar])];
    controller.alwaysBounceHorizontal = YES;
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth]*5, controller.frame.size.height-(54+50))];
    table.backgroundColor = [UIColor clearColor];
    table.showsVerticalScrollIndicator = NO;
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.delegate = self;
    table.dataSource = self;
    [controller addSubview:table];
    bottomTutors = [[UIView alloc] initWithFrame:CGRectMake(0, controller.frame.size.height-54-50, [self screenWidth]*4, 54+50+[References bottomMargin])];
    [References blurView:bottomTutors];
    [References createLine:bottomTutors xPos:0 yPos:bottomTutors.frame.size.height-1 inFront:YES];
    [controller addSubview:bottomTutors];
    bottomTutors.clipsToBounds = false;
    [self.view addSubview:controller];
    [self.view sendSubviewToBack:controller];
    [self updateSchedule];
    noAvailableTimes = [[UIView alloc] initWithFrame:controller.bounds];
    [References blurView:noAvailableTimes];
    UILabel *noAvailable = [[UILabel alloc] initWithFrame:CGRectMake(30, 25, controller.frame.size.width-60, 0)];
    noAvailable.text = @"There are no tutors available on this day.";
    noAvailable.textAlignment = NSTextAlignmentCenter;
    noAvailable.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    noAvailable.textColor = [UIColor lightGrayColor];
    noAvailable.numberOfLines = 0;
    noAvailable.frame = CGRectMake(noAvailable.frame.origin.x, noAvailable.frame.origin.y, noAvailable.frame.size.width, [References fixedWidthSizeWithFont:noAvailable.font andString:noAvailable.text andLabel:noAvailable].size.height);
    [noAvailableTimes addSubview:noAvailable];
    noAvailableTimes.alpha = 0;
}

-(void)updateSchedule {
    
    [UIView animateWithDuration:0.25 animations:^{
        noAvailableTimes.alpha = 0;
        self->bottomTutors.alpha = 0;
        self->table.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [noAvailableTimes removeFromSuperview];
            for (id view in self->bottomTutors.subviews) {
                if ([view isKindOfClass:UIButton.class] && view != self->tutorCourse) {
                    [view removeFromSuperview];
                }
            }
            if ([[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian] isDateInToday:currentDateObject]) {
                NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
                earliestRow = [components hour]*2;
            } else {
                earliestRow = 47;
            }
            if (!tutorCourse) {
                tutorCourse = [[UIButton alloc] initWithFrame:CGRectMake(0, 1, 55, 52)];
                [tutorCourse setImageEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
                tutorCourse.alpha = 0;
                [tutorCourse setImage:[UIImage imageNamed:@"addTutor.png"] forState:UIControlStateNormal];
                [tutorCourse addTarget:self action:@selector(tutorCourse) forControlEvents:UIControlEventTouchUpInside];
                [References tintUIButton:tutorCourse color:[UIColor darkTextColor]];
                [self checkTutor];
                [bottomTutors addSubview:tutorCourse];
            }
            bool noAvailable = true;
            CGFloat offset = 55;
            for (tutorAvailability *tutor in self->tutors) {
                if ([tutor checkAvailabilityonWeekday:self->currentDateInt andTime:48]) {
                    noAvailable = false;
                    if ([tutor earliestAvailabilityOn:self->currentDateInt] < earliestRow) {
                        earliestRow = [tutor earliestAvailabilityOn:self->currentDateInt];
                    }
                    
                    UIButton *tutorButton = [[UIButton alloc] initWithFrame:CGRectMake(offset, 0, 100, 54+50)];
                    [tutorButton setTitle:[tutor identifier] forState:UIControlStateNormal];
                    [tutorButton setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                    [tutorButton addTarget:self action:@selector(openTutor:) forControlEvents:UIControlEventTouchUpInside];
                    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(offset, 0, 3, self->bottomBar.frame.size.height)];
                    line.backgroundColor = [References schoolMainColor];
                    [self->bottomTutors addSubview:line];
//                    tutorButton.clipsToBounds = YES;
//                    [References createVerticalLine:tutorButton atX:0 andY:0 inFront:YES andHeight:tutorButton.frame.size.height];
                    UILabel *tutorName = [[UILabel alloc] initWithFrame:CGRectMake(5, 51, tutorButton.frame.size.width-10, tutorButton.frame.size.height-51)];
                    tutorName.text = [[tutor name] stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
                    tutorName.numberOfLines = 2;
                    tutorName.textColor = [References primaryText];
                    tutorName.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
                    tutorName.textAlignment = NSTextAlignmentCenter;
                    [tutorButton addSubview:tutorName];
                    UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(tutorButton.frame.size.width/2-20, 10, 40, 40)];
                    [img sd_setImageWithURL:[tutor url]];
                    [References cornerRadius:img radius:img.frame.size.width/2];
                    [tutorButton addSubview:img];
                    [self->bottomTutors addSubview:tutorButton];
                    
                    offset+=100;
                    NSLog(@"%@",[tutor name]);
                        for (int b = 0; b < 48; b++) {
                            if ([tutor checkAvailabilityonWeekday:self->currentDateInt andTime:b]) {
                                NSLog(@"%@",[tutor availabilityOnWeekday:self->currentDateInt andTime:b]);
                            }
                        }
                        
                    
                    
                }
            }
            bool noTutors;
            if (offset == 55) {
                noTutors = YES;
            } else {
                noTutors = NO;
                if (offset > [self screenWidth]) {
                    controller.contentSize = CGSizeMake(offset+25, controller.frame.size.height);
                } else {
                    controller.contentSize = controller.bounds.size;
                }
            }
            
            [self->table reloadData];
            if (noAvailable) {
                [self->controller addSubview:noAvailableTimes];
            } else {
                [noAvailableTimes removeFromSuperview];
            }
            [UIView animateWithDuration:0.25 animations:^{
                if (loadingView) {
                    loadingView.alpha = 0;
                }
                
                self->table.alpha = 1;
                self->bottomTutors.alpha = 1;
                if (noAvailable) {
                    self->noAvailableTimes.alpha = 1;
                }
                [controller bringSubviewToFront:self->bottomTutors];
            } completion:^(BOOL finished) {
                if (finished) {
                    if (earliestRow < 48) {
                        if (earliestRow > 2) {
                            earliestRow = earliestRow - 2;
                        }
                    } else {
                        earliestRow = 0;
                    }
                    [self->table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:earliestRow inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                    [loadingView removeFromSuperview];
                    loadingView = nil;
                }
            }];
        }
    }];
    
}
             
-(void)checkTutor {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] child:@"tutor"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            [[[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[References getSchoolDomain]] child:courseDa.courseDepartment] child:courseDa.courseNumberString] child:@"tutors"] child:[FIRAuth auth].currentUser.uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if (snapshot.exists) {
                 amTutor = YES;
                 [tutorCourse setImage:[UIImage imageNamed:@"endTutor.png"] forState:UIControlStateNormal];
                 [References tintUIButton:tutorCourse color:[References primaryText]];
                 tutorCourse.hidden = NO;
                 tutorCourse.alpha = 1;
             } else {
                 [tutorCourse setImage:[UIImage imageNamed:@"addTutor.png"] forState:UIControlStateNormal];
                 [References tintUIButton:tutorCourse color:[References primaryText]];
                 tutorCourse.hidden = NO;
                 tutorCourse.alpha = 1;
             }
         }];
        } else {
            tutorCourse.hidden = YES;
        }
 }];
}

-(void)tutorCourse {
    if (amTutor) {
        __block UIView *processingPayment = [References loadingView:@"Removing..."];
        [self.view addSubview:processingPayment];
        [UIView animateWithDuration:0.25 animations:^{
            processingPayment.alpha = 1;
        } completion:^(BOOL finished) {
            NSDictionary *dict = @{[NSString stringWithFormat:@"updateDatabase/%@/%@/%@/tutors/%@",[References getSchoolDomain],courseDa.courseDepartment,courseDa.courseNumberString,[FIRAuth auth].currentUser.uid]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/courses/%@:%@",[FIRAuth auth].currentUser.uid,courseDa.courseDepartment,courseDa.courseNumberString]:[NSNull null]};
            [[[FIRDatabase database] reference] updateChildValues:dict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                for (int a = 0; a < tutors.count; a++) {
                    tutorAvailability *tu = tutors[a];
                    if ([[tu identifier] isEqualToString:[FIRAuth auth].currentUser.uid]) {
                        [tutors removeObject:tu];
                        break;
                    }
                }
                [courseDa removeTutor];
                [self updateSchedule];
                [tutorCourse setImage:[UIImage imageNamed:@"addTutor.png"] forState:UIControlStateNormal];
                [References tintUIButton:tutorCourse color:[UIColor darkTextColor]];
                amTutor = NO;
                [UIView animateWithDuration:0.25 animations:^{
                    processingPayment.alpha = 0;
                } completion:^(BOOL finished) {
                    [processingPayment removeFromSuperview];
                    processingPayment = nil;
                }];
            }];
        }];
    } else {
        __block UIView *processingPayment = [References loadingView:@"Tutoring..."];
        [self.view addSubview:processingPayment];
        [UIView animateWithDuration:0.25 animations:^{
            processingPayment.alpha = 1;
        } completion:^(BOOL finished) {
            NSDictionary *dict = @{[NSString stringWithFormat:@"updateDatabase/%@/%@/%@/tutors/%@",[References getSchoolDomain],self->courseDa.courseDepartment,courseDa.courseNumberString,[FIRAuth auth].currentUser.uid]:[FIRAuth auth].currentUser.uid,[NSString stringWithFormat:@"updateDatabase/users/%@/courses/%@:%@",[FIRAuth auth].currentUser.uid,courseDa.courseDepartment,courseDa.courseNumberString]:[NSString stringWithFormat:@"%@:%@",courseDa.courseDepartment,self->courseDa.courseNumberString]};
            [[[FIRDatabase database] reference] updateChildValues:dict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    if (snapshot.exists) {
                        NSDictionary *sessions = snapshot.value[@"sessions"];
                        NSArray *availability = snapshot.value[@"availability"];
                        NSNumber *cost = snapshot.value[@"PPH"];
                        tutorAvailability *tutor = [[tutorAvailability alloc] initWithID:[FIRAuth auth].currentUser.uid andAvailability:availability andSessions:sessions andName:snapshot.value[@"name"] andURL:snapshot.value[@"profileURL"] andPPH:cost andToken:snapshot.value[@"token"]];
                        [self->tutors addObject:tutor];
                        [courseDa addTutor];
                        [self updateSchedule];
                        [tutorCourse setImage:[UIImage imageNamed:@"endTutor.png"] forState:UIControlStateNormal];
                        [References tintUIButton:tutorCourse color:[UIColor darkTextColor]];
                        amTutor = YES;
                        [UIView animateWithDuration:0.25 animations:^{
                            processingPayment.alpha = 0;
                        } completion:^(BOOL finished) {
                            [processingPayment removeFromSuperview];
                            processingPayment = nil;
                        }];
                    }
                }];
               
            }];
        }];
    }
}

-(void)openTutor:(UIButton*)sender {
    tutorContainerController *tvc = [[tutorContainerController alloc] init];
    tvc.tutorID = sender.titleLabel.text;
    [self.navigationController pushViewController:tvc animated:YES];
}

-(NSString*)minutesToString:(NSInteger)minutes {
    NSString *ampm;
    if ((minutes/30) > 24) {
        ampm = @" PM";
    } else {
        ampm = @" AM";
    }
    NSInteger hour = minutes/60;
    NSInteger min = minutes - (hour*60);
    if (hour > 12) {
        hour-=12;
    }
    if (hour == 0) {
        hour = 12;
    }
    NSString *final = [NSString stringWithFormat:@"%li:z %@",hour,ampm];
    if (min > 0) {
        final = [final stringByReplacingOccurrencesOfString:@":z" withString:[NSString stringWithFormat:@":%li",min]];
    } else {
        final = [final stringByReplacingOccurrencesOfString:@":z" withString:@":00"];
    }
    return final;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%liday:%litime",currentDateInt,indexPath.row]];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%liday:%litime",currentDateInt,indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        if (indexPath.row % 2 == 0) {
            UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
            time.font = [UIFont systemFontOfSize:8 weight:UIFontWeightSemibold];
            time.textColor = [References secondaryText];
            time.text = [NSString stringWithFormat:@"%@",[self minutesToString:indexPath.row*30]];
            time.textAlignment = NSTextAlignmentRight;
            time.frame = CGRectMake(0, 0, 50, [References fixedWidthSizeWithFont:time.font andString:time.text andLabel:time].size.height);
            [cell addSubview:time];
        }
        CGFloat offset = 55;
        for (tutorAvailability *tutor in self->tutors) {
            if ([tutor checkAvailabilityonWeekday:self->currentDateInt andTime:48]) {
                if ([tutor checkAvailabilityonWeekday:self->currentDateInt andTime:indexPath.row]) {
                    bookButton *tutorButton = [[bookButton alloc] initWithData:[tutor availabilityOnWeekday:self->currentDateInt andTime:indexPath.row] andTutor:tutor];
                    tutorButton.tag = indexPath.row;
                tutorButton.frame = CGRectMake(offset, 1, 100, 30);
                [tutorButton setTitleColor:[References primaryText] forState:UIControlStateNormal];
                [tutorButton setTitle:@"Available" forState:UIControlStateNormal];
                    tutorButton.tag = indexPath.row;
                [tutorButton setBackgroundColor:[References cells]];
                [tutorButton.titleLabel setFont:[UIFont systemFontOfSize:8 weight:UIFontWeightMedium]];
                    [tutorButton addTarget:self action:@selector(expandTutor:) forControlEvents:UIControlEventTouchUpInside];
                    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 3, tutorButton.frame.size.height)];
                    line.backgroundColor = [References accent];
                    [tutorButton addSubview:line];
                [cell addSubview:tutorButton];
//                if ([tutor checkAvailabilityonWeekday:self->currentDateInt andTime:indexPath.row+1] && ![tutor checkAvailabilityonWeekday:self->currentDateInt andTime:indexPath.row-1]) {
//                    [References roundTopCorners:tutorButton withRadius:5];
//                } else if (![tutor checkAvailabilityonWeekday:self->currentDateInt andTime:indexPath.row+1] && [tutor checkAvailabilityonWeekday:self->currentDateInt andTime:indexPath.row-1]) {
//                    [References roundBottomCorners:tutorButton withRadius:5];
//                } else if (![tutor checkAvailabilityonWeekday:self->currentDateInt andTime:indexPath.row+1] && ![tutor checkAvailabilityonWeekday:self->currentDateInt andTime:indexPath.row-1]) {
//                    [References cornerRadius:tutorButton radius:5];
//                }
                    
            }
                offset+=100;
            }
        }
    }
    if (selectedRow != -1) {
        if (selectedRow == indexPath.row) {
            if (cell.alpha != 0.25) {
                [UIView animateWithDuration:0.25 animations:^{
                    cell.alpha = 0.25;
                }];
            }
            
            
        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

-(void)expandTutor:(bookButton*)sender {
    [sender printData];
    selectedRow = sender.tag;
    UITableViewCell *cell = [table cellForRowAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    if (bookView) {
        [UIView animateWithDuration:0.25 animations:^{
            
            [bookView removeFromSuperview];
            [table beginUpdates];
            [table endUpdates];
        } completion:^(BOOL finished) {
            if (finished) {
                for (UIView *view in bookView.subviews) {
                    [view removeFromSuperview];
                }
                bookView = nil;
                [self addBookViewToCell:cell andButton:sender];
            }
        }];
    } else {
        [self addBookViewToCell:cell andButton:sender];
    }
    
}

-(UIStatusBarAnimation)preferredStatusBarUpdateAnimation {
    return UIStatusBarAnimationSlide;
}

-(BOOL)prefersStatusBarHidden {
    return statusBarIsHidden;
}

-(void)addBookViewToCell:(UITableViewCell*)cell andButton:(bookButton*)sender {
    selectedStartDate = [sender startDate];
    selectedDuration = 1;
    selectedMaxDuration = [sender duration];
    selectedEndDate = [selectedStartDate dateByAddingTimeInterval:1800*(selectedDuration)];
    bookView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [self screenHeight], [self screenWidth], 0)];
    NSDateFormatter *dfmt = [[NSDateFormatter alloc] init];
    dfmt.dateFormat = @"EEEE, MMMM d, YYYY";
    NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
    tfmt.dateFormat = @"h:mm a";
    [References createLine:bookView xPos:0 yPos:0 inFront:YES];
//    UILabel *blackHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], 70)];
//    blackHeader.backgroundColor = [UIColor blackColor];
//    [bookView addSubview:blackHeader];
//    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, [self screenWidth]-30, 30)];
//    header.textAlignment = NSTextAlignmentCenter;
//    header.text = @"Confirm Details";
//    header.textColor = [UIColor whiteColor];
//    header.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
//    [bookView addSubview:header];
    UIImageView *otherImage = [[UIImageView alloc] initWithFrame:CGRectMake([self screenWidth]/2-(25), 15, 50, 50)];
    otherImage.layer.borderColor = [References secondaryText].CGColor;
    otherImage.layer.borderWidth = 1;
    [References cornerRadius:otherImage radius:otherImage.frame.size.height/2];
    [otherImage sd_setImageWithURL:[[sender tutor] url]];
    [bookView addSubview:otherImage];
    UILabel *receiptName = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:otherImage]+5, [self screenWidth]-30, 20)];
    receiptName.text = [[sender tutor] name];
    receiptName.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    receiptName.textColor = [References primaryText];
    receiptName.textAlignment = NSTextAlignmentCenter;
    [bookView addSubview:receiptName];
    UILabel *receiptCourseHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptName]+10, 100, 20)];
    receiptCourseHeader.text = @"Course";
    receiptCourseHeader.font = receiptName.font;
    receiptCourseHeader.textColor = [References secondaryText];
    [bookView addSubview:receiptCourseHeader];
    UILabel *receiptCourse = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptCourseHeader.frame.origin.y, [self screenWidth]-15-(15+100+5), receiptCourseHeader.frame.size.height)];
    receiptCourse.text = course;
    receiptCourse.font = receiptName.font;
    receiptCourse.textColor = [References primaryText];
    receiptCourse.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:receiptCourse];
    UILabel *receiptDateHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptCourseHeader]+10, 100, 20)];
    receiptDateHeader.text = @"Date";
    receiptDateHeader.font = receiptName.font;
    receiptDateHeader.textColor = [References secondaryText];
    [bookView addSubview:receiptDateHeader];
    UILabel *receiptDate = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptDateHeader.frame.origin.y, [self screenWidth]-15-(15+100+5), receiptDateHeader.frame.size.height)];
    receiptDate.text = [dfmt stringFromDate:selectedStartDate];
    receiptDate.font = receiptName.font;
    receiptDate.textColor = [References primaryText];
    receiptDate.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:receiptDate];
    UILabel *receiptStartHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptDateHeader]+10, 100, 20)];
    receiptStartHeader.text = @"Time";
    receiptStartHeader.font = receiptName.font;
    receiptStartHeader.textColor = [References secondaryText];
    [bookView addSubview:receiptStartHeader];
    UILabel *receiptTime = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptStartHeader.frame.origin.y, [self screenWidth]-15-(15+100+5), receiptStartHeader.frame.size.height)];
    receiptTime.text = [tfmt stringFromDate:selectedStartDate];
    receiptTime.font = receiptName.font;
    receiptTime.textColor = [References primaryText];
    receiptTime.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:receiptTime];
    UILabel *receiptDurationHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptStartHeader]+10, 100, 20)];
    receiptDurationHeader.text = @"Duration";
    receiptDurationHeader.font = receiptName.font;
    receiptDurationHeader.textColor = [References secondaryText];
    [bookView addSubview:receiptDurationHeader];
    receiptDurationHeader.frame = CGRectMake(receiptDurationHeader.frame.origin.x, receiptDurationHeader.frame.origin.y, [References fixedHeightSizeWithFont:receiptDurationHeader.font andString:receiptDurationHeader.text andLabel:receiptDurationHeader].size.width, receiptDurationHeader.frame.size.height);
    receiptDuration = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptDurationHeader.frame.origin.y, [self screenWidth]-15-(15+100+5), receiptDurationHeader.frame.size.height)];
    receiptDuration.text = [NSString stringWithFormat:@"%li Minutes",selectedDuration*30];
    receiptDuration.font = receiptName.font;
    receiptDuration.textColor = [References primaryText];
    receiptDuration.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:receiptDuration];
    
    UIButton *sub30Min = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptDuration]+5, ([self screenWidth]-30-5)/2, 44)];
    [sub30Min addTarget:self action:@selector(calSubtract) forControlEvents:UIControlEventTouchUpInside];
    [sub30Min.titleLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightBold]];
//    [sub30Min setImage:[UIImage imageNamed:@"sub.png"] forState:UIControlStateNormal];
    [sub30Min setTitle:@"-30 MINUTES" forState:UIControlStateNormal];
    [sub30Min setBackgroundColor:[References background]];
    [sub30Min setTitleColor:[References primaryText] forState:UIControlStateNormal];
    [References cornerRadius:sub30Min radius:5];
//    [sub30Min setImageEdgeInsets:UIEdgeInsetsMake(2.5, 2.5, 2.5, 2.5)];
//    [References tintUIButton:sub30Min color:[References schoolMainColor]];
    [bookView addSubview:sub30Min];
    UIButton *add30Min = [[UIButton alloc] initWithFrame:CGRectMake(sub30Min.frame.origin.x+sub30Min.frame.size.width+5, sub30Min.frame.origin.y, ([self screenWidth]-30-5)/2, 44)];
    [add30Min.titleLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightBold]];
    [add30Min setBackgroundColor:[References background]];
    [add30Min setTitleColor:[References primaryText] forState:UIControlStateNormal];
    [add30Min setTitle:@"+30 MINUTES" forState:UIControlStateNormal];
    [References cornerRadius:add30Min radius:5];
    [add30Min setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
//    [add30Min setImage:[UIImage imageNamed:@"add.png"] forState:UIControlStateNormal];
    [add30Min addTarget:self action:@selector(calAdd) forControlEvents:UIControlEventTouchUpInside];
//    [References tintUIButton:add30Min color:[References schoolMainColor]];
    [bookView addSubview:add30Min];
    
    UILabel *durationDisclaimer = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:add30Min]+5, [self screenWidth]-30, 0)];
    durationDisclaimer.text = @"Use the buttons above to increase or decrase the duration of your session.";
    durationDisclaimer.numberOfLines = 0;
    durationDisclaimer.font = [UIFont systemFontOfSize:10 weight:UIFontWeightMedium];
    durationDisclaimer.textColor = [References secondaryText];
    durationDisclaimer.frame = CGRectMake(durationDisclaimer.frame.origin.x, durationDisclaimer.frame.origin.y, durationDisclaimer.frame.size.width, [References fixedWidthSizeWithFont:durationDisclaimer.font andString:durationDisclaimer.text andLabel:durationDisclaimer].size.height);
    [bookView addSubview:durationDisclaimer];
    [References createLinewithWidth:bookView xPos:15 yPos:[References getBottomYofView:durationDisclaimer]+10 inFront:YES andWidth:[self screenWidth]-30];
    UILabel *receiptDiscountHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:durationDisclaimer]+20, 100, 20)];
    receiptDiscountHeader.text = @"Discount";
    receiptDiscountHeader.font = receiptName.font;
    receiptDiscountHeader.textColor = [References secondaryText];
    [bookView addSubview:receiptDiscountHeader];
    finalReceiptDiscount = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptDiscountHeader.frame.origin.y, [self screenWidth]-15-(15+100+5), receiptDiscountHeader.frame.size.height)];
    finalReceiptDiscount.text = [NSString stringWithFormat:@"$0.00"];
    finalReceiptDiscount.font = receiptName.font;
    finalReceiptDiscount.textColor = [References primaryText];
    finalReceiptDiscount.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:finalReceiptDiscount];
    UILabel *receiptCostHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptDiscountHeader]+10, 100, 20)];
    receiptCostHeader.text = @"Total Cost";
    receiptCostHeader.font = receiptName.font;
    receiptCostHeader.textColor = [References primaryText];
    [bookView addSubview:receiptCostHeader];
    finalReceiptTotal = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptCostHeader.frame.origin.y, [self screenWidth]-15-(15+100+5), receiptCostHeader.frame.size.height)];
    finalReceiptTotal.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    finalReceiptTotal.font = receiptName.font;
    finalReceiptTotal.textColor = [References primaryText];
    finalReceiptTotal.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:finalReceiptTotal];
    apple = [[PKPaymentButton alloc] initWithPaymentButtonType:PKPaymentButtonTypePlain paymentButtonStyle:PKPaymentButtonStyleBlack];
    apple.enabled = NO;
    apple.alpha = 0.25;
    apple.frame = CGRectMake(15, [References getBottomYofView:receiptCostHeader]+10, [self screenWidth]-30, 50);
    venmo = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:apple]+5, ([self screenWidth]-30), 50)];
    [venmo setBackgroundColor:[UIColor whiteColor]];
    UIImageView *v = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, venmo.frame.size.width-40, venmo.frame.size.height-40)];
    [v setImage:[UIImage imageNamed:@"venmo.png"]];
    v.contentMode = UIViewContentModeScaleAspectFit;
    [venmo addSubview:v];
    [bookView addSubview:venmo];
    [bookView addSubview:apple];
    
    [References cornerRadius:apple radius:5.0];
    [References cornerRadius:venmo radius:5.0];
    UIButton *useBalance = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:venmo]+5, [self screenWidth]-30, 50)];
    [useBalance setBackgroundColor:[UIColor whiteColor]];
    UIImageView *balanceIcon = [[UIImageView alloc] initWithFrame:CGRectMake(useBalance.frame.size.width/2-(40/2), 10,40, 30)];
    [balanceIcon setImage:[UIImage imageNamed:@"icon.png"]];
    balanceIcon.contentMode = UIViewContentModeScaleAspectFit;
    [useBalance addSubview:balanceIcon];
    [References cornerRadius:useBalance radius:5];
    [bookView addSubview:useBalance];
//    UILabel *balancelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:balanceIcon],useBalance.frame.size.width,useBalance.frame.size.height-[References getBottomYofView:balanceIcon])];
//    balancelabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightThin];
//    balancelabel.textColor = [References schoolMainColor];
//    balancelabel.textAlignment = NSTextAlignmentCenter;
//    balancelabel.text = @"$0";
//    balancelabel.numberOfLines = 1;
//    [useBalance addSubview:balancelabel];
//    [bookView addSubview:useBalance];
    [venmo addTarget:self action:@selector(tappedVenmo) forControlEvents:UIControlEventTouchUpInside];
    [apple addTarget:self action:@selector(tappedApplePay) forControlEvents:UIControlEventTouchUpInside];
    venmo.enabled = NO;
    venmo.alpha = 0.25;
    bookView.alpha = 0;
    selectedbutton = sender;
    bookView.frame = CGRectMake(0, [self screenHeight], [self screenWidth], [References getBottomYofView:useBalance]+[References bottomMargin]+5);
    [References blurView:bookView];
    bookView.backgroundColor = [References cells];
    [References createLine:bookView xPos:0 yPos:bookView.frame.size.height-1 inFront:YES];
    overlay = [[UIButton alloc] initWithFrame:self.view.bounds];
    [References blurView:overlay];
    [overlay addTarget:self action:@selector(closeBookCancel) forControlEvents:UIControlEventTouchUpInside];
    overlay.backgroundColor = [[UIColor clearColor] colorWithAlphaComponent:1];
    overlay.alpha = 0;
    UIButton *cancel = [[UIButton alloc] initWithFrame:CGRectMake(15, 0, [self screenWidth]-30, (([self screenHeight]-bookView.frame.size.height)))];
//    [cancel addTarget:self action:@selector(closeBookview) forControlEvents:UIControlEventTouchUpInside];
    [cancel addTarget:self action:@selector(closeBookCancel) forControlEvents:UIControlEventTouchUpInside];
    cancel.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightBold]];
    [cancel setTitleColor:[[UIColor grayColor] colorWithAlphaComponent:0.4] forState:UIControlStateNormal];
    [References cornerRadius:cancel radius:5];
     [overlay addSubview:cancel];
    selectedTutor = [sender tutor];
    [self.view addSubview:overlay];
    [self.view addSubview:bookView];
    [self getBalanceWithCompletion:^(bool finished) {
        if (finished) {
            if (currentBalance > 0) {
                [useBalance addTarget:self action:@selector(applyBalance:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                discount = 0;
                useBalance.enabled = NO;
                useBalance.alpha = 0.25;
            }
            [self calculateReceipt];
            statusBarIsHidden = true;
            [UIView animateWithDuration:0.25 animations:^{
                [self setNeedsStatusBarAppearanceUpdate];
                self->bookView.frame = CGRectMake(0, [self screenHeight]-bookView.frame.size.height, bookView.frame.size.width, bookView.frame.size.height);
                bookView.alpha = 1;
                overlay.alpha =1;
            }];
            
            [self fetchClientToken];
            [self confirmAvailabilityWithCompletion:^(BOOL finished) {
                if (finished) {
                    NSLog(@"AVAILABLE!");
                } else {
                    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"update"]) {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Update Tutortree" message:@"Until you've updated to the latest minimum Tutortree version you will be unable to book Tutors." preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"Open App Store" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/us/app/tutortree/id1353273906?mt=8"]];
                        }]];
                        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self closeBookviewViewAsCancel:YES];
                        }]];
                        [self presentViewController:alert animated:YES completion:nil];
                    } else {
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Longer Available" message:@"This time slot has recently been booked or the tutor has already met their quota for the week. Sorry for the inconvenience" preferredStyle:UIAlertControllerStyleAlert];
                        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                            [self closeBookviewViewAsCancel:YES];
                        }]];
                        [self presentViewController:alert animated:YES completion:nil];
                    }
                    
                }
            }];
        }
        
    }];
    
}

-(void)closeBookCancel {
    [self closeBookviewViewAsCancel:YES];
}

-(void)calAdd {
    if (selectedDuration < selectedMaxDuration) {
        selectedDuration++;
        receiptDuration.text = [NSString stringWithFormat:@"%li MINUTES",selectedDuration*30];
    }
    [self calculateReceipt];
    [References selectionFeedback:UIImpactFeedbackStyleHeavy];
}

-(void)calSubtract {
    if (selectedDuration > 1) {
        selectedDuration--;
        receiptDuration.text = [NSString stringWithFormat:@"%li MINUTES",selectedDuration*30];
    }
    [self calculateReceipt];
    [References selectionFeedback:UIImpactFeedbackStyleHeavy];
}

-(void)applyBalance:(UIButton*)sender {
    NSString *title;
    if (currentBalance >= totalCost) {
        title = [NSString stringWithFormat:@"Checkout With Amount: $%.2f",self->totalCost];
    } else {
        title = [NSString stringWithFormat:@"Apply Balance: $%.2f",self->currentBalance];
    }
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Use Balance"] message:nil preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (self->currentBalance >= self->totalCost) {
            self->discount = self->totalCost;
            self->usedBalance = [NSNumber numberWithFloat:self->discount];
            [self checkoutFree];
        } else {
            self->discount = self->currentBalance;
            self->totalCost = self->totalCost - self->discount;
            self->usedBalance = [NSNumber numberWithFloat:self->discount];
            [self calculateReceipt];
        }
        
        sender.enabled = NO;
        sender.alpha = 0.25;
    }]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:controller animated:YES completion:^{
       
    }];
}

-(void)tappedVenmo {
    [self.venmoDriver authorizeAccountAndVault:NO completion:^(BTVenmoAccountNonce * _Nullable venmoAccount, NSError * _Nullable error) {
        if (venmoAccount) {
            NSLog(@"error: %@",error.localizedDescription);
            venmoNonce = venmoAccount.nonce;
            paymentVenmo = venmoAccount;
            [self returnedFromVenmo];
        }
    }];
}

-(void)returnedFromVenmo {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Confirm Booking"] message:[NSString stringWithFormat:@"The venmo account %@ will be charged %.2f",paymentVenmo.username,totalCost] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Pay $%.2f",totalCost] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self->paymentType = @"Venmo";
        [self updatedCheckout:self->venmoNonce andAmount:[NSString stringWithFormat:@"%.2f",totalCost]];
//        [self postNonceToServer:venmoNonce andAmount:[NSString stringWithFormat:@"%.2f",totalCost]];
    }]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:controller animated:YES completion:nil];
    
}

-(void)checkoutFree {
    paymentType = @"Balance";
    balanceUsed = totalCost;
    [self closeBookviewViewAsCancel:NO];
    __block UIView *processingPayment = [References loadingView:@"Creating Session..."];
    [self.view addSubview:processingPayment];
    [UIView animateWithDuration:0.25 animations:^{
        processingPayment.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            [self createSessionWithID:[References randomStringWithLength:8]];
        }
    }];
    
}

-(void)getBalanceWithCompletion:(void (^)(bool finished))complete {
    currentBalance = 0;
    NSDictionary *myAccount = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"accountDatabase"];
    NSDictionary *spending = myAccount[@"spending"];
    NSDictionary *income = myAccount[@"income"];
    
    if (income) {
        for (id key in income) {
            NSDictionary *td = income[key];
            NSNumber *n = td[@"amount"];
            currentBalance+=n.floatValue;
            
        }
    }
    if (spending) {
        for (id key in spending) {
            NSDictionary *td = spending[key];
            NSNumber *n = td[@"amount"];
            if (![(NSString*)td[@"type"] isEqualToString:@"Apple Pay"] && ![(NSString*)td[@"type"] isEqualToString:@"Venmo"]) {
                currentBalance-=n.floatValue;
            } else if ([(NSNumber*)td[@"usedBalance"] floatValue] > 0) {
                currentBalance-=[(NSNumber*)td[@"usedBalance"] floatValue];
            }
        }
    }
    complete(YES);
}

-(void)removeSessionWithID:(NSString*)transactionID {
    [[[FIRDatabase database] reference] updateChildValues:@{[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",[selectedTutor identifier],transactionID]: [NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",[FIRAuth auth].currentUser.uid,transactionID]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/income/%@",[selectedTutor identifier],transactionID]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/spending/%@",[FIRAuth auth].currentUser.uid,transactionID]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/invoices/%@",transactionID]:[NSNull null]} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        NSLog(@"DELETED");
    }];
}

-(void)createSessionWithID:(NSString*)transactionID {
    NSString *tID = [transactionID uppercaseString];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"MMM d, h:mm a";
    NSDictionary *spendingInvoice = @{@"amount":[NSNumber numberWithFloat:self->totalCost],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":paymentType,@"usedBalance":[NSNumber numberWithFloat:discount]};
    NSDictionary *incomeInvoice = @{@"amount":[NSNumber numberWithFloat:([[selectedTutor pph] floatValue] * selectedDuration)],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":paymentType,};
        NSMutableDictionary *myAccount = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"accountDatabase"]];
        NSMutableDictionary *spending = [[NSMutableDictionary alloc] initWithDictionary: myAccount[@"spending"]];
        [spending setObject:spendingInvoice forKey:transactionID];
        [myAccount setObject:spending forKey:@"spending"];
        [[NSUserDefaults standardUserDefaults] setObject:myAccount forKey:@"accountDatabase"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    NSDictionary *bookingTutor = @{@"start":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"end":[NSNumber numberWithDouble:[selectedEndDate timeIntervalSince1970]],@"course":course,@"other":[FIRAuth auth].currentUser.uid,@"status":@0,@"student":@0};
    NSDictionary *bookingStudent = @{@"start":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"end":[NSNumber numberWithDouble:[selectedEndDate timeIntervalSince1970]],@"course":course,@"other":[selectedTutor identifier],@"status":@0,@"student":@1};
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:selectedStartDate];
    NSDictionary *legacy = @{@"course":course,@"date":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"weekInt":[NSNumber numberWithInteger:[comp weekday]],@"value":@0};
    NSDictionary *globalInvoice = @{@"fromEmail":[FIRAuth auth].currentUser.uid,@"fromName":[FIRAuth auth].currentUser.uid,@"metadata":@{@"amount":[NSNumber numberWithFloat:([[selectedTutor pph] floatValue] * selectedDuration)+2.95],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]],@"type":paymentType},@"toName":[selectedTutor name],@"toEmail":[selectedTutor identifier]};
    NSArray *people = [@[[FIRAuth auth].currentUser.uid,[selectedTutor identifier]] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *connectionID = [NSString stringWithFormat:@"%@:%@",people[0],people[1]];
    NSDictionary *delivery = @{[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",[selectedTutor identifier],tID]: bookingTutor,[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",[FIRAuth auth].currentUser.uid,tID]:bookingStudent,[NSString stringWithFormat:@"updateDatabase/users/%@/income/%@",[selectedTutor identifier],tID]:incomeInvoice,[NSString stringWithFormat:@"updateDatabase/users/%@/spending/%@",[FIRAuth auth].currentUser.uid,tID]:spendingInvoice,[NSString stringWithFormat:@"updateDatabase/invoices/%@",tID]:globalInvoice,[NSString stringWithFormat:@"updateDatabase/connections/%@/sessions/%@",connectionID,tID]:legacy,[NSString stringWithFormat:@"updateDatabase/connections/%@/tutor",connectionID]:[selectedTutor identifier],[NSString stringWithFormat:@"updateDatabase/connections/%@/student",connectionID]:[FIRAuth auth].currentUser.uid};
    NSLog(@"%@",delivery);
    [[[FIRDatabase database] reference] updateChildValues:delivery withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
            fmt.dateFormat = @"EEEE, MMM d";
            NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
            tfmt.dateFormat = @"h:mm a";
            NSString *notification =[NSString stringWithFormat:@"%@ has booked you on %@ at %@. They have booked you for help with %@. This session is awaiting your confirmation.",myAccount[@"name"],[fmt stringFromDate:self->selectedStartDate],[tfmt stringFromDate:self->selectedStartDate],[self->course stringByReplacingOccurrencesOfString:@":" withString:@" "]];
            NSLog(@"%@",notification);
            [References sendNotification:[self->selectedTutor identifier] withSubject:@"New Booking" andMessage:notification forceEmail:YES andCompletion:^(bool result) {
                if (result) {
                    [self.navigationController popViewControllerAnimated:YES];
                    NSLog(@"COMPLETE");
                }
            }];
        } else {
            [self removeSessionWithID:transactionID];
        }
        
        
    }];
}

- (void)fetchClientToken {
//    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"clientToken"]) {
//        clientToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"clientToken"];
//        self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:clientToken];
//        self.venmoDriver = [[BTVenmoDriver alloc] initWithAPIClient:self.braintreeClient];
//        [UIView animateWithDuration:0.25 animations:^{
//            venmo.alpha = 1;
//            apple.alpha = 1;
//        } completion:^(BOOL finished) {
//            if (finished) {
//                venmo.enabled = YES;
//                apple.enabled = YES;
//            }
//        }];
//    } else {
        NSString *requestURL = @"https://tutortree-development.herokuapp.com/getClientToken";
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager POST:requestURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            self->clientToken = responseData;
            [self paymentReady];
            NSLog(@"apay: %@",self->clientToken);
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            //[References postError:error.slocalizedDescription];
        }];
//    }
    
}

-(void)paymentReady {
    self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:clientToken];
    self.venmoDriver = [[BTVenmoDriver alloc] initWithAPIClient:[[BTAPIClient alloc] initWithAuthorization:clientToken]];
    [[NSUserDefaults standardUserDefaults] setObject:clientToken forKey:@"clientToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"pulled from heroku");
    [UIView animateWithDuration:0.25 animations:^{
        venmo.alpha = 1;
        apple.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            venmo.enabled = YES;
            apple.enabled = YES;
        }
    }];
}

-(void)calculateReceipt {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEEE, MMM d, h:mm a";
//    NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
//    tfmt.dateFormat = @"EEEE, MMM d, h:mm a";
    receiptDuration.text = [NSString stringWithFormat:@"%li Minutes",selectedDuration*30];
    selectedEndDate = [selectedStartDate dateByAddingTimeInterval:selectedDuration*1800];
    finalReceiptDiscount.text = [NSString stringWithFormat:@"$%.2f",discount];
    totalCost = (([[selectedTutor pph] floatValue] * selectedDuration) + 2.95)- discount;
    finalReceiptTotal.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    [self confirmAvailabilityWithCompletion:^(BOOL finished) {
        if (finished) {
            NSLog(@"AVAILABLE!");
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Longer Available" message:@"This time slot has recently been booked and is no longer avaialable. Sorry for the inconvenience" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self closeBookviewViewAsCancel:YES];
            }]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}



// APPLE PAY

- (IBAction)tappedApplePay {
    PKPaymentRequest *paymentRequest = [self paymentRequest];
    PKPaymentAuthorizationViewController *vc = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:paymentRequest];
    vc.delegate = self;
    paymentType = @"Apple Pay";
    [self presentViewController:vc animated:YES completion:nil];
}

- (PKPaymentRequest *)paymentRequest {
    PKPaymentRequest *paymentRequest = [[PKPaymentRequest alloc] init];
    paymentRequest.merchantIdentifier = @"merchant.tutortree.ios";
    paymentRequest.supportedNetworks = @[PKPaymentNetworkAmex, PKPaymentNetworkVisa, PKPaymentNetworkMasterCard, PKPaymentNetworkDiscover];
    paymentRequest.merchantCapabilities = PKMerchantCapability3DS;
    paymentRequest.countryCode = @"US"; // e.g. US
    paymentRequest.currencyCode = @"USD"; // e.g. USD
    NSMutableArray *finalPriceBreakdown = [[NSMutableArray alloc] init];
    for (NSInteger a = 0; a < selectedDuration; a++) {
        [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"30 minutes" amount:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%li.00",[selectedTutor pph].integerValue]]]];
    }
    if (discount != 0) {
        [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"Applied Balance" amount:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"-%li.00",(NSInteger)discount]]]];
    }
    [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"Service Fee" amount:[NSDecimalNumber decimalNumberWithString:@"2.95"]]];
    [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"TUTORTREE" amount:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",totalCost]]]];
    paymentRequest.paymentSummaryItems = finalPriceBreakdown;
    return paymentRequest;
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    // Example: Tokenize the Apple Pay payment
    BTApplePayClient *applePayClient = [[BTApplePayClient alloc]
                                        initWithAPIClient:self.braintreeClient];
    [applePayClient tokenizeApplePayPayment:payment
                                 completion:^(BTApplePayCardNonce *tokenizedApplePayPayment,
                                              NSError *error) {
                                     if (tokenizedApplePayPayment) {
                                         NSString *totalCost = [NSString stringWithFormat:@"%.2f",self->totalCost];
                                         paymentType = @"Apple Pay";
//                                         [self postNonceToServer:tokenizedApplePayPayment.nonce andAmount:totalCost];
                                         [self updatedCheckout:tokenizedApplePayPayment.nonce andAmount:totalCost];
                                         completion(PKPaymentAuthorizationStatusSuccess);
                                         // Then indicate success or failure via the completion callback, e.g.
                                         
                                     } else {
                                         [References toastMessage:@"Braintree Dev Mode" andView:self andClose:NO];
                                         
                                         NSLog(@"%@",error.localizedDescription);
                                         // Tokenization failed. Check `error` for the cause of the failure.
                                         
                                         // Indicate failure via the completion callback:
                                         completion(PKPaymentAuthorizationStatusFailure);
                                     }
                                 }];
}

-(void)updatedCheckout:(NSString*)nonce andAmount:(NSString*)amount {
    __block UIView *processingPayment = [References loadingView:@"Processing Payment..."];
    [self.view addSubview:processingPayment];
    [UIView animateWithDuration:0.25 animations:^{
        processingPayment.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            NSString *requestURL = @"https://tutortree-development.herokuapp.com/payandbook";
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            manager.requestSerializer = [AFJSONRequestSerializer serializer];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
            fmt.dateFormat = @"MMM d, h:mm a";
            NSDictionary *spendingInvoice = @{@"amount":[NSNumber numberWithFloat:self->totalCost],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":paymentType,@"usedBalance":[NSNumber numberWithFloat:discount]};
            NSDictionary *incomeInvoice = @{@"amount":[NSNumber numberWithFloat:([[selectedTutor pph] floatValue] * selectedDuration)],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":paymentType,};
            NSDictionary *bookingTutor = @{@"start":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"end":[NSNumber numberWithDouble:[selectedEndDate timeIntervalSince1970]],@"course":course,@"other":[FIRAuth auth].currentUser.uid,@"status":@0,@"student":@0};
            NSDictionary *bookingStudent = @{@"start":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"end":[NSNumber numberWithDouble:[selectedEndDate timeIntervalSince1970]],@"course":course,@"other":[selectedTutor identifier],@"status":@0,@"student":@1};
            NSCalendar* cal = [NSCalendar currentCalendar];
            NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:selectedStartDate];
            NSDictionary *legacy = @{@"course":course,@"date":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"weekInt":[NSNumber numberWithInteger:[comp weekday]],@"value":@0};
            NSDictionary *globalInvoice = @{@"fromEmail":[FIRAuth auth].currentUser.uid,@"fromName":[FIRAuth auth].currentUser.uid,@"metadata":@{@"amount":[NSNumber numberWithFloat:([[selectedTutor pph] floatValue] * selectedDuration)+2.95],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]],@"type":paymentType},@"toName":[selectedTutor name],@"toEmail":[selectedTutor identifier]};
            NSArray *people = [@[[FIRAuth auth].currentUser.uid,[selectedTutor identifier]] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            NSString *connectionID = [NSString stringWithFormat:@"%@:%@",people[0],people[1]];
            NSMutableDictionary *myAccount = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"accountDatabase"]];
            NSDateFormatter *afmt = [[NSDateFormatter alloc] init];
            fmt.dateFormat = @"EEEE, MMM d";
            NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
            tfmt.dateFormat = @"h:mm a";
            NSString *notification =[NSString stringWithFormat:@"%@ has booked you on %@ at %@. They have booked you for help with %@. This session is awaiting your confirmation.",myAccount[@"name"],[afmt stringFromDate:self->selectedStartDate],[tfmt stringFromDate:self->selectedStartDate],[self->course stringByReplacingOccurrencesOfString:@":" withString:@" "]];
            NSDictionary *checkout = @{@"nonce":nonce,
                                       @"amount":[amount stringByReplacingOccurrencesOfString:@"." withString:@"decimal"],
                                       @"studentID":[FIRAuth auth].currentUser.uid,
                                       @"tutorID" : [self->selectedTutor identifier],
                                       @"connectionID" : connectionID,
                                       @"bookingTutor" : bookingTutor,
                                       @"bookingStudent" : bookingStudent,
                                       @"invoiceTutor" : incomeInvoice,
                                       @"invoiceStudent" : spendingInvoice,
                                       @"globalInvoice" : globalInvoice,
                                       @"legacy" : legacy,
                                       @"notification" : notification
                                       };
            [manager POST:requestURL parameters:checkout progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"RESPONSE DATA: %@",responseData);
                if (responseData.length == 8) {
                    NSMutableDictionary *spending = [[NSMutableDictionary alloc] initWithDictionary: myAccount[@"spending"]];
                    [spending setObject:spendingInvoice forKey:responseData];
                    [myAccount setObject:spending forKey:@"spending"];
                    [[NSUserDefaults standardUserDefaults] setObject:myAccount forKey:@"accountDatabase"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [self.navigationController popViewControllerAnimated:YES];
                    NSLog(@"COMPLETE");
                } else {
                    [UIView animateWithDuration:0.25 animations:^{
                        processingPayment.alpha = 0;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [processingPayment removeFromSuperview];
                            __block processingPayment = nil;
                            [References toastNotificationBottom:self.view andMessage:@"Something's not right."];
                        }
                        
                    }];
                }
                
                //NSLog(@"apay: %@",_clientToken);
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                
                NSLog(@"Error: %@", error);
                //[References postError:error.slocalizedDescription];
            }];
        }
    }];
//
}

- (void)postNonceToServer:(NSString *)paymentMethodNonce andAmount:(NSString*)amount{
    bool beta = false;
    if (beta) {
        __block UIView *processingPayment = [References loadingView:@"Processing Payment..."];
        [self.view addSubview:processingPayment];
        [UIView animateWithDuration:0.25 animations:^{
            processingPayment.alpha = 1;
        } completion:^(BOOL finished) {
            if (finished) {
                [self createSessionWithID:[References randomStringWithLength:8].uppercaseString];
                [UIView animateWithDuration:0.25 animations:^{
                    processingPayment.alpha = 0;
                } completion:^(BOOL finished) {
                    if (finished) {
                        [processingPayment removeFromSuperview];
                        __block processingPayment = nil;
                        [References toastNotificationBottom:self andMessage:@"Something's not right."];
                    }
                    
                }];
            }
        }];
    } else {
        [self confirmAvailabilityWithCompletion:^(BOOL finished) {
            if (finished) {
                [self closeBookviewViewAsCancel:NO];
                __block UIView *processingPayment = [References loadingView:@"Processing Payment..."];
                [self.view addSubview:processingPayment];
                [UIView animateWithDuration:0.25 animations:^{
                    processingPayment.alpha = 1;
                } completion:^(BOOL finished) {
                    if (finished) {
                        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                        NSString *url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/checkoutWithNonceAndAmount/%@/%@",paymentMethodNonce,[amount stringByReplacingOccurrencesOfString:@"." withString:@"decimal"]];
                        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
                        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                            NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                            NSLog(@"RESPONSE DATA: %@",responseData);
                            if (responseData.length == 8) {
                                [self createSessionWithID:responseData];
                            } else {
                                [UIView animateWithDuration:0.25 animations:^{
                                    processingPayment.alpha = 0;
                                } completion:^(BOOL finished) {
                                    if (finished) {
                                        [processingPayment removeFromSuperview];
                                        __block processingPayment = nil;
                                        [References toastNotificationBottom:self andMessage:@"Something's not right."];
                                    }
                                    
                                }];
                            }
                            //
                        } failure:^(NSURLSessionTask *operation, NSError *error) {
                            NSLog(@"%@",error.localizedDescription);
                            [UIView animateWithDuration:0.25 animations:^{
                                processingPayment.alpha = 0;
                            } completion:^(BOOL finished) {
                                if (finished) {
                                    [processingPayment removeFromSuperview];
                                    processingPayment = nil;
                                    [References toastNotificationBottom:self andMessage:@"Something's not working."];
                                }
                                
                            }];
                            
                            //[References postError:error.localizedDescription];
                        }];
                    }
                    
                }];
            } else {
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"No Longer Available" message:@"This time slot has recently been booked and is no longer avaialable. Sorry for the inconvenience" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self closeBookviewViewAsCancel:YES];
                }]];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }];
    }
    
    
    
    
}

-(void)sendNotification:(NSString*)notificationText withSubject:(NSString*)subject{
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[selectedTutor identifier]] child:@"badge"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSNumber *badge;
        NSString *email;
        if (snapshot.exists) {
            if (snapshot.value[@"badge"]) {
                badge = [NSNumber numberWithInteger:[(NSNumber*)snapshot.value integerValue] + 1];
            } else {
                badge = [NSNumber numberWithInteger:1];
            }
            if (snapshot.value[@"email"]) {
                email = snapshot.value[@"email"];
            } else {
                email = @"none";
            }
        }
            NSString *url;
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sandbox"] == true) {
                NSLog(@"sandbox");
                url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendSandboxNotification/%@/%@/%@/%@",subject,notificationText,[selectedTutor token],badge];
            } else {
                url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendNotification/%@/%@/%@/%@",subject,notificationText,[selectedTutor token],badge];
            }
            NSString *finalURL = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                if (badge.integerValue > 5) {
                    NSLog(@"send email");
                }
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"Error: %@", error);
                
            }];
    }];
}


-(void)closeBookviewViewAsCancel:(BOOL)cancel {
    statusBarIsHidden = false;
    [UIView animateWithDuration:0.25 animations:^{
        [self setNeedsStatusBarAppearanceUpdate];
        bookView.frame = CGRectMake(0, [self screenHeight], bookView.frame.size.width, bookView.frame.size.height);
        bookView.alpha = 0;
        overlay.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            if (cancel) {
                totalCost = 0;
                discount = 0;
            }
        }
    }];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 48;
}

-(void)confirmAvailabilityWithCompletion:(void (^)(BOOL))completion {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[selectedTutor identifier]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            completion([tutorAvailability verifyAvailability:(NSNumber*)[(NSArray*)snapshot.value[@"availability"] objectAtIndex:self->weekDayInt] andSessions:snapshot.value[@"sessions"] onWeekday:self->weekDayInt withTime:self->selectedRow andDurattion:self->selectedDuration withMaxWeekly:(snapshot.value[@"MAX"] ? snapshot.value[@"MAX"] : [NSNumber numberWithInt:168])]);
        }
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return [References statusBarColor];
}


@end
