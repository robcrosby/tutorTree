//
//  departmentReference.h
//  tutor
//
//  Created by Robert Crosby on 7/8/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "courseDatabase.h"
#import <UIKit/UIKit.h>
#import "References.h"

#import "scheduleViewController.h"
#import "nScheduleViewController.h"


@interface departmentReference : NSObject <UITableViewDataSource,UITableViewDelegate> {
    CGFloat cellHeight;
    BOOL departmentIsShowing;
    NSMutableArray *courses;
    UICollectionView *coursesCollection;
    UITableView *table;
    NSString *name;
    UIImageView *cardImage,*expandImage;
    UIView *cellView,*card;
    UILabel *cellTitle;
    UIScrollView *coursesScroll;
    CGFloat currentOffset;
    NSInteger tutorCount;
    UIViewController *parent;
}

-(id)initWithName:(NSString*)tname andCourses:(NSDictionary*)tcourses;
+(instancetype)withName:(NSString*)tname andCourses:(NSDictionary*)tcourses;

-(NSString*)name;
-(courseDatabase*)getCourseAtIndex:(int)index;
-(courseDatabase*)courseWithName:(NSString*)name;
-(NSInteger)numberOfCourses;

-(void)setParent:(UIViewController*)tparent;
-(CGFloat)cellHeight;
-(void)expandCell;
-(void)collapseCell;
-(void)addButtonToScroll:(UIButton*)button;
-(UIView*)cellView;
-(void)didTouch;

@end
