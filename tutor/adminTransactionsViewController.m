//
//  adminTransactionsViewController.m
//  tutor
//
//  Created by Robert Crosby on 1/3/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "adminTransactionsViewController.h"

@interface adminTransactionsViewController ()

@end

@implementation adminTransactionsViewController

- (void)viewDidLoad {
    expand = [[NSMutableArray alloc] initWithArray:@[@1,@1,@1]];
    fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMMM d, YYYY, h:mm a"];
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin])];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [References screenWidth]-30, 44)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = @"Transactions";
    
    header.textColor = [UIColor darkTextColor];
    header.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:header];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    [self.view sendSubviewToBack:menuBar];
    table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height, 0, [References bottomMargin], 0);
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    [self.view sendSubviewToBack:table];
    [self getTransactions];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)close {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (transactions.count > 0) {
        return transactions.count;
    }
    return 1;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0 && [expand[indexPath.section] isEqual:@1]) {
//        return 44;
//    } else {
//        return 0;
//    }
//}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 30)];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 7.5, [References screenWidth]-24, 15)];
    hheader.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    hheader.textColor = [UIColor lightGrayColor];
    if (section == 0) {
        hheader.text = @"ALL";
        UIButton *expand = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 7.5, 100, 15)];
        [expand.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightBold]];
        [expand setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [expand setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [expand setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand.tag = section;
        [expand addTarget:self action:@selector(expandRows:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:expand];
    }
    
    [view addSubview:hheader];
    return view;
}


-(void)expandRows:(UIButton*)sender {
    if ([expand[sender.tag] isEqual:@1]) {
        [sender setTitle:@"EXPAND" forState:UIControlStateNormal];
        expand[sender.tag] = @0;
    } else {
        [sender setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand[sender.tag] = @1;
    }
    [table beginUpdates];
    [table endUpdates];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 130;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        if (transactions.count > 0) {
            adminTransactionCell *cell = [transactions objectAtIndex:indexPath.row];

            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            return cell;
        } else {
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"notr"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"notr"];
                cell.textLabel.text = @"No Transactions";
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            return cell;
        }
}

-(void)getNameForUser:(NSString*)uid andCompletion:(void (^)(NSString *result))completionHandler{
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] child:@"name"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            completionHandler(snapshot.value);
        } else {
            completionHandler(@"unknown");
        }
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.removeFromSuperViewOnHide = YES;
    hud.margin = 10.f;
    adminTransactionCell *transaction = transactions[indexPath.row];
    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[transaction getTutorID]] child:@"sessions"] child:[transaction invoiceID]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [hud hideAnimated:true afterDelay:0];
        if (snapshot.exists) {
            NSDateFormatter *dfmt = [[NSDateFormatter alloc] init];
            dfmt.dateFormat = @"EEEE, MMMM d";
            NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
            tfmt.dateFormat = @"h:mm a";
            NSDate *start = [NSDate dateWithTimeIntervalSince1970:[(NSNumber*)snapshot.value[@"start"] floatValue]];
            NSDate *end = [NSDate dateWithTimeIntervalSince1970:[(NSNumber*)snapshot.value[@"end"] floatValue]];
            NSString *course = snapshot.value[@"course"];
//            NSNumber *confirmed = snapshot.value[@"status"];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[dfmt stringFromDate:start] message:[NSString stringWithFormat:@"%@\n%@ -> %@",course,[tfmt stringFromDate:start],[tfmt stringFromDate:end]] preferredStyle:UIAlertControllerStyleActionSheet];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Couldn't find a booking for this transaction" message:@"It may be a donation or a web payment." preferredStyle:UIAlertControllerStyleActionSheet];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
        }
    }];
}

-(void)getTransactions {
    FIRDatabaseQuery *query = [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"invoices"] queryOrderedByChild:@"metadata/numberDate"] queryLimitedToLast:40];
    [query observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->transactions = [[NSMutableArray alloc] init];
            for (id key in snapshot.value) {
                if (![[[snapshot.value[key] objectForKey:@"metadata"] objectForKey:@"type"] isEqualToString:@"Web"]) {
                    [self->transactions addObject:[[adminTransactionCell alloc] initWithID:(NSString*)key andData:snapshot.value[key]]];
                }
            }
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"numberDate"
                                                         ascending:NO];
            self->transactions = [[NSMutableArray alloc] initWithArray:[self->transactions sortedArrayUsingDescriptors:@[sortDescriptor]]];
            [self->table reloadData];
//            [self processTransactions];
        }
    }];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    if (result == MFMailComposeResultSent) {
        [References toastNotificationBottom:self.view andMessage:@"Sent"];
    }
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}
//


-(void)processTransactions {
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self dataFilePath]]) {
        [[NSFileManager defaultManager] createFileAtPath: [self dataFilePath] contents:nil attributes:nil];
        NSLog(@"Route creato");
    }
    [[[[FIRDatabase database] reference] child:@"updateDatabase"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSString *output = @"";
            int i = 0;
            for (adminTransactionCell *t in self->transactions) {
                NSString *studentName = [[[snapshot.value objectForKey:@"users"] objectForKey:[t getStudentID]] objectForKey:@"name"];
                NSString *tutorName = [[[snapshot.value objectForKey:@"users"] objectForKey:[t getTutorID]] objectForKey:@"name"];
                NSString *course = [[[[[snapshot.value objectForKey:@"users"] objectForKey:[t getTutorID]] objectForKey:@"sessions"] objectForKey:[t invoiceID]] objectForKey:@"course"];
                NSString *ta = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%ld,%@\n",[t invoiceID],tutorName,studentName,[t amount],course,(long)[t numberDate],[t dateT]];
                output = [output stringByAppendingString:ta];
//                [pp setCompletedUnitCount:i];
                NSLog(@"%i/%li",i,self->transactions.count);
                i = i + 1;
            }
//            [hud hideAnimated:true];
            NSFileHandle *handle;
            handle = [NSFileHandle fileHandleForWritingAtPath: [self dataFilePath] ];
            //say to handle where's the file fo write
            [handle truncateFileAtOffset:[handle seekToEndOfFile]];
            //position handle cursor to the end of file
            [handle writeData:[output dataUsingEncoding:NSUTF8StringEncoding]];
            
            NSLog(@"%@",output);
    }];
}

-(NSString *)dataFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"myfile.csv"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
