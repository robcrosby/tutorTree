//
//  homeViewController.h
//  tutor
//
//  Created by Robert Crosby on 5/11/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "courseDatabase.h"
#import "References.h"
#import "tutorOfficeViewController.h"
#import "adminViewController.h"
#import <MessageUI/MessageUI.h>
#import <UserNotifications/UserNotifications.h>
#import "dayAvailability.h"
#import "campusView.h"
#import "departmentReference.h"
#import <StoreKit/StoreKit.h>
#import "popoverView.h"
#import "connectionViewController.h"


#import "tutorContainerController.h"
#import "browseCourseViewController.h"
#import "tutorObject.h"
#import "nGettingStartedViewController.h"
#import "textFieldWithURL.h"
#import "accountTransactionsViewController.h"
#import "adminWithdrawalsViewController.h"
#import "adminTransactionsViewController.h"
#import "adminBalancesViewController.h"
#import "adminUsersViewController.h"
#import <MBProgressHUD.h>
#import "manualBookingViewController.h"
#import "paymentProcessingView.h"
@import SafariServices;
@import Firebase;
@import UserNotifications;


@interface homeViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,MFMailComposeViewControllerDelegate,UNUserNotificationCenterDelegate,UITabBarDelegate,UITextFieldDelegate,SFSafariViewControllerDelegate,UITextViewDelegate> {
    UIView *headerV;
    NSString *universityName;
    bool showFeaturedTutors;
    NSString *uid;
    NSMutableArray *departmentNames;
    NSMutableArray *searchCourseNames;
    UILabel *smartSearchSuggestion;
    UITextField *smartSearch;
    BOOL darkMode;
    UIImageView *profileImageView;
    MBProgressHUD *clearingBadges;
    CGFloat currentBalance;
    UIScrollView *withdrawalPage;
    NSString *myName;
    bool finishAccount;
    NSInteger reloadCount;
    UIView *bestTutorsView;
    NSMutableArray *upcomingSessions;
    NSMutableDictionary *connections;
    NSInteger connectionsLoaded;
    BOOL hideStatusBar;
    BOOL amTutor;
    NSInteger currentSection;
    NSString *currentHeader;
    UIView *loadingView;
    // math
    NSInteger marginTop,contentHeight,myCoursesHeight,departmentsHeight;
    bool animating;
    BOOL reloadFavoriteCourses;
    BOOL tutor;
    UITabBarItem *me;
    // data
    NSMutableArray *tutors;
    NSMutableArray *courses;
    NSMutableArray *departmentDatabases;
    NSMutableArray *myCourses,*myCoursesKeys;
    NSMutableArray *sessions,*pending,*completed,*completedStars,*moreArray,*past;
    
    NSMutableArray *communicationsSelection;
    NSInteger currentCommunication;
    UIRefreshControl *refreshControl;
    NSMutableArray *registeredNibs;
    // views
    UITableView *table,*connectionsTable,*more;
    NSMutableArray *morebuttons;
    UITabBar *tabbar;
    UIScrollView *ctrl,*mainScroll;
    CGFloat mainScrollContentSize;
    UICollectionView *connectionsCollection,*bestTutorsCollection;
    UILabel *noConnections,*noCourses,*subNoConnections,*subNoCourses,*tosDisclaimer;
    UIView *menuBar,*bottomBar;
    UILabel *header,*myCoursesHeading,*AllCoursesHeading;
    UIView *menuView,*tosView;
    NSString *lastHeader;
    UIButton *closeMenu,*closeTOS,*agreeTOS,*disagreeTOS,*dismissTOSButton,*contactTOS,*tutorPanelButton,*adminPortalButton;
    UIStatusBarStyle currentStatusBar;
    BOOL needAgreement;
    BOOL admin;
    campusView *campusMap;
    BOOL noUpcoming;
    popoverView *tosPopover,*bioPopover;
    BOOL bioChanged;
    UITextView *bioView;
    NSIndexPath *selectedIndex;
    UILabel *statusBarBlur;
}

@property (strong, nonatomic) courseDatabase *courseToOpen;
@property (strong, nonatomic) NSNumber *isReturningFromCheckout;


@end
