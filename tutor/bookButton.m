
//
//  bookButton.m
//  tutor
//
//  Created by Robert Crosby on 10/31/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "bookButton.h"

@implementation bookButton

-(id)initWithData:(NSDictionary*)tdata andTutor:(tutorAvailability*)ttutor {
    self = [super init];
    if (self) {
        data = tdata;
        date = data[@"date"];
        duration = [data[@"duration"] integerValue];
        tutor = ttutor;
//        date = tdate;
//        duration = tduration;
    }
    return self;
}

-(void)printData {
    NSLog(@"data: %@",data);
    }

-(tutorAvailability*)tutor {
    return tutor;
}
    
-(NSDate*)startDate {
    return date;
}

-(NSInteger)duration {
    return duration;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
