//
//  adminTutorViewController.h
//  tutor
//
//  Created by Robert Crosby on 8/27/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"

@import Firebase;

@interface adminTutorViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    UITableView *table;
    NSMutableArray *spending,*income;
}

@property (strong, nonatomic) NSDictionary *tutorDict;

@end
