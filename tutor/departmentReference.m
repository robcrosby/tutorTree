//
//  departmentReference.m
//  tutor
//
//  Created by Robert Crosby on 7/8/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "departmentReference.h"
#define scrollHeight 300
#define courseHeight 75

@implementation departmentReference

-(id)initWithName:(NSString *)tname andCourses:(NSDictionary*)tcourses {
    self = [super init];
    if (self) {
        name = tname;
        currentOffset = 0;
        courses = [[NSMutableArray alloc] init];
        tutorCount = 0;
        for (id key in tcourses) {
            if ([tcourses[key] objectForKey:@"tutors"]) {
                tutorCount+=[(NSArray*)[tcourses[key] objectForKey:@"tutors"] count];
            }
            courseDatabase *course = [[courseDatabase alloc] initWithDatabase:tcourses[key] andName:key andDeparment:name];
            [courses addObject:course];
        }
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"courseNumber"
                                                     ascending:YES];
        courses = [[NSMutableArray alloc] initWithArray:[courses sortedArrayUsingDescriptors:@[sortDescriptor]]];
        [self createCellView];
    }
    return self;
}

+(instancetype)withName:(NSString *)tname andCourses:(NSDictionary*)tcourses {
    return [[departmentReference alloc] initWithName:tname andCourses:tcourses];
}

-(NSString*)name {
    return name;
}

-(void)createCellView {
    cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 80)];
    card = [[UIView alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-30, 60)];
    card.backgroundColor = [UIColor blackColor];
    [References cornerRadius:card radius:5.0f];
    
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",[[[self name] componentsSeparatedByString:@" "] objectAtIndex:0].lowercaseString]];

    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, card.frame.size.width-15-24-5, 40)];
    name.numberOfLines = 1;
    name.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    name.textColor = [UIColor whiteColor];
    name.text = [self name];
    [card addSubview:name];
    CGFloat tHeight = courses.count > 7 ? 44*7 : courses.count*44;
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 60, card.frame.size.width, tHeight) style:UITableViewStylePlain];
    UIView *blur = [[UIView alloc] initWithFrame:table.frame];
    [References blurView:blur];
    table.alpha = 0;
    table.delegate = self;
    table.dataSource = self;
    table.backgroundColor = [UIColor clearColor];
    table.separatorColor = [[References primaryText] colorWithAlphaComponent:0.25];
    [card addSubview:blur];
    [card addSubview:table];

    cellHeight = [References getBottomYofView:table];
    if (img) {
        cardImage = [[UIImageView alloc] initWithFrame:card.bounds];
        cardImage.contentMode = UIViewContentModeScaleAspectFill;
        cardImage.alpha = 0.75;
        [cardImage setImage:img];
        [card addSubview:cardImage];
        [card sendSubviewToBack:cardImage];
    } else {
        CAGradientLayer *l = [References createGradient:[References accent] andColor:[[References accent] colorWithAlphaComponent:0.76] andFrame:card];
        l.frame = CGRectMake(0, 0, card.frame.size.width, cellHeight);
        [card.layer insertSublayer:l atIndex:0];
    }
//    coursesCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:name], card.frame.size.width-40, ((courses.count+3-1)/3) * 50) collectionViewLayout:flowLayout];
//
//    coursesCollection.alpha = 0;
//    coursesCollection.backgroundColor = [UIColor clearColor];
//    coursesCollection.delegate = self;
//    coursesCollection.dataSource = self;
//    for (courseDatabase *course in courses) {
//        [table registerClass:[UITableViewCell class] forCellReuseIdentifier:[course courseNumberString]];
////        [coursesCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:[course courseNumberString]];
//    }
//    coursesCollection.frame = CGRectMake(20, [References getBottomYofView:name]+5, card.frame.size.width-40, coursesCollection.collectionViewLayout.collectionViewContentSize.height);
    cardImage.frame = CGRectMake(0, 0, card.frame.size.width, cellHeight+10);
    [card addSubview:coursesCollection];
    UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(card.frame.origin.x+5, card.frame.origin.y+5, card.frame.size.width-10, card.frame.size.height-10)];
    shadow.backgroundColor = [UIColor whiteColor];
    [References bottomshadow:shadow];
    [cellView addSubview:shadow];
    expandImage = [[UIImageView alloc] initWithFrame:CGRectMake(card.frame.size.width-15-20, name.frame.origin.y+(name.frame.size.height/2-10), 20, 20)];
    [expandImage setImage:[UIImage imageNamed:@"down.png"]];
    expandImage.alpha = 0.75;
    [card addSubview:expandImage];
    [cellView addSubview:card];
}

-(CGFloat)cellHeight {
    return cellHeight+20;
}

-(void)setParent:(UIViewController *)tparent {
    parent = tparent;
}

-(void)expandCell {
    [UIView animateWithDuration:0.33 animations:^{
        expandImage.transform = CGAffineTransformMakeRotation(M_PI);
        table.alpha = 1;
        self->cellView.frame = CGRectMake(self->cellView.frame.origin.x, self->cellView.frame.origin.y, self->cellView.frame.size.width, self->cellHeight+40);
        self->card.frame = CGRectMake(self->card.frame.origin.x, self->card.frame.origin.y, self->card.frame.size.width, self->cellHeight);
    }];
}

-(void)collapseCell {
    [UIView animateWithDuration:0.33 animations:^{
        expandImage.transform = CGAffineTransformMakeRotation(0);
        coursesCollection.alpha = 0;
        self->cellView.frame = CGRectMake(self->cellView.frame.origin.x, self->cellView.frame.origin.y, self->cellView.frame.size.width, 80);
        self->card.frame = CGRectMake(self->card.frame.origin.x, self->card.frame.origin.y, self->card.frame.size.width, 60);
    }];
}

-(UIView*)cellView {
    return cellView;
}

-(courseDatabase*)getCourseAtIndex:(int)index {
    return (courseDatabase*)courses[index];
}

-(courseDatabase*)courseWithName:(NSString*)name {
    for (courseDatabase* course in courses) {
        NSLog(@"\n%@\n%@",course.courseDepartmentAndNumber,name);
        if ([[course courseDepartmentAndNumber] isEqualToString:name]) {
            NSLog(@"found match! %@",course.courseDepartmentAndNumber);
            return course;
        }
    }
    return nil;
}


-(NSInteger)numberOfCourses {
    return courses.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self numberOfCourses];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    courseDatabase *course = courses[indexPath.row];
    UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[course courseNumberString]];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[course courseNumberString]];
        cell.textLabel.text = [course courseNumberString];
        cell.textLabel.textColor = [References primaryText];
        if ([course numberOfTutors] == 1) {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%li Tutor",[course numberOfTutors]];
        } else {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%li Tutors",[course numberOfTutors]];
        }
        cell.detailTextLabel.font = [UIFont systemFontOfSize:8 weight:UIFontWeightRegular];
        cell.detailTextLabel.textColor = [References secondaryText];
        cell.backgroundColor = [UIColor clearColor];
        [References blurView:cell.backgroundView];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    scheduleViewController *calendarViewController = [[scheduleViewController alloc] initWithCourse:[(courseDatabase*)courses[indexPath.row] courseDepartmentAndNumber] andTutors:[(courseDatabase*)courses[indexPath.row] tutors] andCourse:(courseDatabase*)courses[indexPath.row]];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [calendarViewController setModalPresentationStyle:UIModalPresentationFormSheet];
        [calendarViewController setModalTransitionStyle: UIModalTransitionStyleCoverVertical];
        [parent presentViewController:calendarViewController animated:YES completion:nil];
        calendarViewController.view.superview.frame = CGRectMake(0, 0, [References screenWidth]/2, [References screenHeight]-200);//it's important to do this after
        calendarViewController.view.superview.center = parent.view.center;
        // ipad
    } else {
        [parent.navigationController pushViewController:calendarViewController animated:YES];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    browseCourseViewController *bvc = [[browseCourseViewController alloc] init];
//    bvc.course = (courseDatabase*)courses[indexPath.item];
    scheduleViewController *calendarViewController = [[scheduleViewController alloc] initWithCourse:[(courseDatabase*)courses[indexPath.item] courseDepartmentAndNumber] andTutors:[(courseDatabase*)courses[indexPath.item] tutors] andCourse:(courseDatabase*)courses[indexPath.item]];
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [calendarViewController setModalPresentationStyle:UIModalPresentationFormSheet];
        [calendarViewController setModalTransitionStyle: UIModalTransitionStyleCoverVertical];
        [parent presentViewController:calendarViewController animated:YES completion:nil];
        calendarViewController.view.superview.frame = CGRectMake(0, 0, [References screenWidth]/2, [References screenHeight]-200);//it's important to do this after
        calendarViewController.view.superview.center = parent.view.center;
        // ipad
    } else {
        [parent.navigationController pushViewController:calendarViewController animated:YES];
    }
    
}

-(void)addButtonToScroll:(UIButton*)button {
    button.frame = CGRectMake(currentOffset, button.frame.origin.y, button.frame.size.width, button.frame.size.height);
    [coursesScroll addSubview:button];
    currentOffset+=button.frame.size.width+10;
    coursesScroll.contentSize = CGSizeMake(currentOffset, coursesScroll.frame.size.height);
}
- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    courseDatabase *course = courses[indexPath.row];
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[course courseNumberString] forIndexPath:indexPath];
    if (cell.tag == 0) {
        cell.backgroundColor = [UIColor clearColor];
        UIView *card = [[UIView alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height)];
        [References blurView:card];
        [References cornerRadius:card radius:7.5];
        UILabel *cTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, card.frame.size.width-10, 25)];
        cTitle.textColor = [References primaryText];
        cTitle.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
        cTitle.text = [course courseNumberString];
        [card addSubview:cTitle];
        UILabel *tCount = [[UILabel alloc] initWithFrame:CGRectMake(5, [References getBottomYofView:cTitle], cTitle.frame.size.width, 12)];
        if ([course numberOfTutors] == 1) {
            tCount.text = [NSString stringWithFormat:@"%li TUTOR",[course numberOfTutors]];
        } else {
            tCount.text = [NSString stringWithFormat:@"%li TUTORS",[course numberOfTutors]];
        }
        
        tCount.font = [UIFont systemFontOfSize:8 weight:UIFontWeightBold];
        tCount.textColor = [References secondaryText];
        [card addSubview:tCount];
        [cell addSubview:card];
        cell.tag = 1;
    }
    return cell;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return courses.count;
}

@end
