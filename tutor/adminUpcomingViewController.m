
//
//  adminUpcomingViewController.m
//  tutor
//
//  Created by Robert Crosby on 10/29/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "adminUpcomingViewController.h"

@interface adminUpcomingViewController ()

@end

@implementation adminUpcomingViewController

- (void)viewDidLoad {
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 96+[References topMargin])];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+44, [References screenWidth]-24, 52)];
    header.text = @"Upcoming";
    header.textColor = [UIColor darkTextColor];
    header.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBold];
    [menuBar addSubview:header];
    UILabel *blur = [[UILabel alloc] initWithFrame:menuBar.bounds];
    [References blurView:blur];
    [menuBar addSubview:blur];
    [menuBar sendSubviewToBack:blur];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight]) style:UITableViewStyleGrouped];
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height-[References topMargin], 0, 44, 0);
    table.scrollIndicatorInsets = table.contentInset;
    table.dataSource = self;
    table.delegate = self;
    [self.view addSubview:table];
    [self.view addSubview:menuBar];
    [super viewDidLoad];
    [self getSessions];
    // Do any additional setup after loading the view.
}

-(void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getSessions {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:_uid] child:@"connections"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->sessions = [[NSMutableArray alloc] init];
            for (id key in snapshot.value) {
                [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:(NSString*)key] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    if (snapshot.exists) {
                        if ([snapshot.value objectForKey:@"sessions"]) {
                            NSString *other;
                            if ([[snapshot.value objectForKey:@"tutor"] isEqualToString:self->_uid]) {
                                other = [snapshot.value objectForKey:@"student"];
                            } else {
                                other = [snapshot.value objectForKey:@"tutor"];
                            }
                            NSDictionary *tSessions = [snapshot.value objectForKey:@"sessions"];
                            for (id sesh in tSessions) {
                                NSDictionary *d = @{@"other":other,@"date":[tSessions[sesh] objectForKey:@"date"]};
                                if (self->sessions.count > 0) {
                                    bool added = false;
                                    for (int a = 0; a < self->sessions.count; a++) {
                                        NSDictionary *object = self->sessions[a];
                                        if ([object[@"date"] doubleValue] < [d[@"date"] doubleValue]) {
                                            [self->sessions insertObject:d atIndex:a];
                                            added = true;
                                            break;
                                        }
                                    }
                                    if (!added) {
                                        [self->sessions addObject:d];
                                    }
                                } else {
                                    [self->sessions addObject:d];
                                }
                            }
                            [self->table reloadData];
                        }
                    }
                }];
            }
            
        }
    }];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *object = sessions[indexPath.row];
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@",object[@"date"]]];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"%@",object[@"date"]]];
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"EEEE, MMM d h:mm a";
        cell.textLabel.text = [fmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:[object[@"date"] doubleValue]]];
        cell.detailTextLabel.text = object[@"other"];
    }
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  sessions.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *d = sessions[indexPath.row];
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:d[@"other"]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:snapshot.value[@"name"] message:snapshot.value[@"email"] preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Back" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}

@end
