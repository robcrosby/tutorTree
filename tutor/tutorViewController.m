//
//  tutorViewController.m
//  tutor
//
//  Created by Robert Crosby on 8/13/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "tutorViewController.h"

@interface tutorViewController ()

@end

@implementation tutorViewController

@synthesize tutor;

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    self.view.clipsToBounds = YES;
    [self populateView];
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [super viewDidLoad];
    [FIRAnalytics logEventWithName:@"tutor_view" parameters:@{[[tutor tutorName] stringByReplacingOccurrencesOfString:@" " withString:@"_"]:@1}];
    // Do any additional setup after loading the view.
}

-(void)close {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)populateView {
    currentY = 5;
    menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 96+[References topMargin])];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back setTitleColor:[References accent] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    UIButton *book = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-120-15, [References topMargin], 120, 44)];
    [book setTitle:@"Book Now" forState:UIControlStateNormal];
    [book setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [book.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [book setTitleColor:[References accent] forState:UIControlStateNormal];
    [book addTarget:self action:@selector(selectCourse) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:book];
    UIImageView *profileImage = [[UIImageView alloc] initWithFrame:CGRectMake([References screenWidth]/2-((menuBar.frame.size.height-[References topMargin]-10)/2), [References topMargin], menuBar.frame.size.height-[References topMargin]-10, menuBar.frame.size.height-[References topMargin]-10)];
    [References cornerRadius:profileImage radius:profileImage.frame.size.width/2];
    profileImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    profileImage.layer.borderWidth = 1;
    [profileImage sd_setImageWithURL:[NSURL URLWithString:[tutor profileURL]]];
    [menuBar addSubview:profileImage];
    
//    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+44, [References screenWidth]-24, 52)];
//    header.textAlignment = NSTextAlignmentLeft;
//    header.text = tutor.tutorName;
//    header.textColor = [UIColor darkTextColor];
//    header.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBold];
//    [menuBar addSubview:header];
    UILabel *blur = [[UILabel alloc] initWithFrame:menuBar.bounds];
    [References blurView:blur];
    [menuBar addSubview:blur];
    [menuBar sendSubviewToBack:blur];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    mainScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight])];
    mainScroll.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height-[References topMargin], 0, [References bottomMargin], 0);
    mainScroll.clipsToBounds = NO;
    mainScroll.contentSize = mainScroll.bounds.size;
    mainScroll.alwaysBounceVertical = YES;
    mainScroll.showsVerticalScrollIndicator = NO;
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, currentY, [References screenWidth]-30, 30)];
    name.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    name.textColor = [UIColor darkTextColor];
    name.text = tutor.tutorName;
    [mainScroll addSubview:name];
    currentY+=name.frame.size.height+5;
    if ([tutor bio]) {
        bioField = [[UILabel alloc] initWithFrame:CGRectMake(name.frame.origin.x, currentY, [References screenWidth]-(name.frame.origin.x), 0)];
        self->bioField.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
        self->bioField.textColor = [UIColor darkGrayColor];
        self->bioField.numberOfLines = 0;
        if ([tutor bio].length ) {
            bioField.text = [tutor bio];
        } else {
            bioField.text = @"This tutor has not provided a bio.";
        }
        
        bioField.frame = CGRectMake(bioField.frame.origin.x, bioField.frame.origin.y, bioField.frame.size.width, [References fixedWidthSizeWithFont:bioField.font andString:bioField.text andLabel:bioField].size.height);
        [self->mainScroll addSubview:bioField];
        currentY+=bioField.frame.size.height+10;
    }
    [self.view addSubview:mainScroll];
    [self.view addSubview:menuBar];
    [self getTutorRatings];
}

-(void)getTutorRatings {
    ratingObjects = [[NSMutableArray alloc] init];
    courseObjects = [[NSMutableArray alloc] init];
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[tutor identifier]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            
                NSInteger count = [[snapshot.value objectForKey:@"income"] allKeys].count;
                UILabel *sessionCount = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:bioField]+5, [References screenWidth]-30, 25)];
            currentY+=25;
                sessionCount.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
                sessionCount.textColor = [UIColor darkTextColor];
            if ([snapshot.value objectForKey:@"income"]) {
                sessionCount.text = [NSString stringWithFormat:@"This tutor has held %li sessions.",count];
            } else {
                sessionCount.text = @"This is a newly approved tutor.";
            }
            [mainScroll addSubview:sessionCount];
            [References createLine:mainScroll xPos:0 yPos:currentY inFront:YES];
            
            if ([snapshot.value objectForKey:@"ratings"]) {
                UILabel *feedback = [[UILabel alloc] initWithFrame:CGRectMake(15, currentY, [References screenWidth]-30, 30)];
                feedback.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
                feedback.textColor = [UIColor darkTextColor];
                feedback.text = @"Feedback";
                [self->mainScroll addSubview:feedback];
                currentY+=feedback.frame.size.height;
                overallRating = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(15,currentY, (([References screenWidth]-30)/3), 50)];
                overallRating.allowsHalfStars = YES;
                overallRating.emptyStarColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
                overallRating.starBorderWidth = 0;
                overallRating.tintColor = [References schoolMainColor];
                overallRating.userInteractionEnabled = NO;
                overallRating.value = 5;
                overallRating.hidden = YES;
                [mainScroll addSubview:overallRating];
                UICollectionViewFlowLayout* feedbackLayout = [[UICollectionViewFlowLayout alloc] init];
                feedbackLayout.itemSize = CGSizeMake([References screenWidth], 125);
                [feedbackLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                [feedbackLayout setSectionInset:UIEdgeInsetsMake(0, 0, 0, 0)];
                [feedbackLayout setMinimumInteritemSpacing:0];
                ratingCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 125) collectionViewLayout:feedbackLayout];
                ratingCollection.pagingEnabled = YES;
                ratingCollection.showsHorizontalScrollIndicator = NO;
                ratingCollection.backgroundColor = [UIColor clearColor];
                ratingCollection.clipsToBounds = NO;
                ratingCollection.delegate = self;
                ratingCollection.dataSource = self;
                NSDictionary *ratings = snapshot.value[@"ratings"];
                CGFloat ratingSum = 0;
                for (id key in ratings) {
                    NSString *cID = [References randomStringWithLength:8];
                    if ([[ratings valueForKey:key] isKindOfClass:[NSDictionary class]]) {
                        NSNumber *number = [[ratings valueForKey:key] valueForKey:@"rating"];
                        ratingSum+=number.floatValue;
                        [self->ratingObjects addObject:@{@"id":cID,@"rating":number,@"text":[[ratings valueForKey:key] valueForKey:@"text"]}];
                        self->overallRatingValue+=number.floatValue;
                    } else {
                        NSNumber *number = [ratings valueForKey:key];
                        ratingSum+=number.floatValue;
                        [self->ratingObjects addObject:@{@"id":cID,@"rating":number,@"text":@"no feedback description provided."}];
                        self->overallRatingValue+=number.floatValue;
                    }
                    
                    [self->ratingCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:cID];
                }

                self->overallRating.value = (self->overallRatingValue/self->ratingObjects.count);
                UILabel *textRating = [[UILabel alloc] initWithFrame:CGRectMake(15, currentY, 25, 46)];
                currentY+=textRating.frame.size.height;
                textRating.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBlack];
                textRating.textColor = [UIColor darkTextColor];
                textRating.text = [NSString stringWithFormat:@"%.2f",ratingSum/self->ratingObjects.count];
                textRating.frame = CGRectMake(textRating.frame.origin.x, textRating.frame.origin.y, [References fixedHeightSizeWithFont:textRating.font andString:textRating.text andLabel:textRating].size.width, textRating.frame.size.height);
                textRating.textAlignment = NSTextAlignmentCenter;
                UILabel *outOf = [[UILabel alloc] initWithFrame:CGRectMake(15, currentY, textRating.frame.size.width, 20)];
                outOf.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBlack];
                outOf.textAlignment = NSTextAlignmentCenter;
                outOf.textColor = [UIColor darkTextColor];
                currentY+=25;
                outOf.text = [NSString stringWithFormat:@"out of 5"];
                [self->mainScroll addSubview:textRating];
                [self->mainScroll addSubview:outOf];
                UILabel *numOfSessions = [[UILabel alloc] initWithFrame:CGRectMake([References screenWidth]-15-textRating.frame.size.width, textRating.frame.origin.y, textRating.frame.size.width, textRating.frame.size.height)];
                numOfSessions.font = textRating.font;
                numOfSessions.textColor = textRating.textColor;
                numOfSessions.text = [NSString stringWithFormat:@"%li",self->ratingObjects.count];
                numOfSessions.textAlignment = NSTextAlignmentCenter;
                UILabel *sessionsTitle = [[UILabel alloc] initWithFrame:CGRectMake([References screenWidth]-15-outOf.frame.size.width, outOf.frame.origin.y, outOf.frame.size.width, outOf.frame.size.height)];
                sessionsTitle.font = outOf.font;
                sessionsTitle.textColor = outOf.textColor;
                sessionsTitle.textAlignment = NSTextAlignmentCenter;
                sessionsTitle.text = @"ratings";
                [self->mainScroll addSubview:numOfSessions];
                [self->mainScroll addSubview:sessionsTitle];
                self->overallRating.hidden = YES;
                self->ratingCollection.frame = CGRectMake(self->ratingCollection.frame.origin.x, currentY, self->ratingCollection.frame.size.width, self->ratingCollection.frame.size.height);
                currentY+=self->ratingCollection.frame.size.height+15;
                [References createLine:self->mainScroll xPos:0 yPos:currentY inFront:YES];
                currentY+=10;
                [self->mainScroll addSubview:self->ratingCollection];
                [self->ratingCollection reloadData];
                
            }
            
            if ([snapshot.value objectForKey:@"courses"]) {
                UILabel *courses = [[UILabel alloc] initWithFrame:CGRectMake(15, currentY, [References screenWidth]-30, 30)];
                currentY+=35;
                courses.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
                courses.textColor = [UIColor darkTextColor];
                courses.text = @"Courses";
                [mainScroll addSubview:courses];
                UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
                flowLayout.itemSize = CGSizeMake(120, 80);
                [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                courseCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(15, currentY, [References screenWidth]-30, 175) collectionViewLayout:flowLayout];
                courseCollection.showsHorizontalScrollIndicator = NO;
                courseCollection.backgroundColor = [UIColor clearColor];
                courseCollection.clipsToBounds = NO;
                courseCollection.delegate = self;
                courseCollection.dataSource = self;
                [mainScroll addSubview:courseCollection];
                courseCollection.frame = CGRectMake(15, currentY, [References screenWidth]-30, courseCollection.collectionViewLayout.collectionViewContentSize.height);
                currentY+=courseCollection.frame.size.height+15;
                [References createLine:self->mainScroll xPos:0 yPos:currentY inFront:YES];
                NSDictionary *coursesD = [snapshot.value objectForKey:@"courses"];
                for (id key in coursesD) {
                    [self->courseCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:(NSString*)key];
                    [self->courseObjects addObject:(NSString*)key];
                }
                [self->courseObjects sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                [self->courseCollection reloadData];
                currentY+=10;
            }
//
//            UILabel *bookHeading = [[UILabel alloc] initWithFrame:CGRectMake(15, currentY, [References screenWidth]-30, 30)];
//            bookHeading.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
//            bookHeading.textColor = [UIColor darkTextColor];
//            bookHeading.text = @"Book From Here";
//            [mainScroll addSubview:bookHeading];
//            currentY+=30;
            UIButton *bookButton = [[UIButton alloc] initWithFrame:CGRectMake(15, self->currentY+5, [References screenWidth]-30, 60)];
            [bookButton setBackgroundColor:[References schoolMainColor]];
            [bookButton setTitle:@"Book Now" forState:UIControlStateNormal];
            [bookButton addTarget:self action:@selector(selectCourse) forControlEvents:UIControlEventTouchUpInside];
            [References cornerRadius:bookButton radius:20];
            [bookButton.titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightMedium]];
            UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(bookButton.frame.origin.x+5, bookButton.frame.origin.y+5, bookButton.frame.size.width-10, bookButton.frame.size.height-10)];
            shadow.backgroundColor = [UIColor whiteColor];
            [References biggestbottomshadow:shadow];
            [self->mainScroll addSubview:shadow];
            [self->mainScroll addSubview:bookButton];
            self->currentY+=80;
           self->mainScroll.contentSize = CGSizeMake([References screenWidth], self->currentY);
            
        }
    }];
}
//
//-(void)showBook:(NSString*)course {
//    self->backdrop = [[UILabel alloc] initWithFrame:self.view.bounds];
//    self->backdrop.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.4];
//    self->backdrop.alpha = 0;
//    self->bookFromHere = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [References screenHeight], [References screenWidth], [References screenHeight])];
//    self->bookFromHere.alwaysBounceVertical = YES;
//    self->bookFromHere.showsVerticalScrollIndicator = NO;
//    self->bookFromHere.delegate = self;
//    self->bookFromHere.alpha = 1;
//    UIButton *downArrow = [[UIButton alloc] initWithFrame:CGRectMake((self->bookFromHere.frame.size.width/2)-20, 5, 40, 30)];
//    [downArrow setShowsTouchWhenHighlighted:NO];
//    [downArrow setAdjustsImageWhenHighlighted:NO];
//    [downArrow setImage:[UIImage imageNamed:@"down.png"] forState:UIControlStateNormal];
//    [downArrow addTarget:self action:@selector(hideHelp) forControlEvents:UIControlEventTouchUpInside];
//    [References tintUIButton:downArrow color:[UIColor lightGrayColor]];
//    [self->bookFromHere addSubview:downArrow];
//    UILabel *helpHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:downArrow]+5, [References screenWidth]-24, 52)];
//    helpHeader.textAlignment = NSTextAlignmentLeft;
//    NSString *tname = self->tutor.tutorName;
//    if ([tname containsString:@" "]) {
//        tname = [[tname componentsSeparatedByString:@" "] objectAtIndex:0];
//    }
//    helpHeader.text = [NSString stringWithFormat:@"Book %@", tname];
//    helpHeader.textColor = [UIColor darkTextColor];
//    helpHeader.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBold];
//    [self->bookFromHere addSubview:helpHeader];
//    embeddedCalendarView *embed = [[embeddedCalendarView alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:helpHeader], [References screenWidth], self->bookFromHere.frame.size.height-[References getBottomYofView:helpHeader]) andTutor:self->tutor andCourse:course andParent:self];
//    [self->bookFromHere addSubview:embed];
//    [self.view addSubview:self->backdrop];
//    UILabel *bgBlur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self->bookFromHere.frame.size.width, self->bookFromHere.frame.size.height*2)];
//    [References cornerRadius:bgBlur radius:5];
//    [References blurView:bgBlur];
//    [self->bookFromHere addSubview:bgBlur];
//    [self->bookFromHere sendSubviewToBack:bgBlur];
//    [self.view addSubview:self->bookFromHere];
//    [UIView animateWithDuration:0.25 animations:^{
//        self->backdrop.alpha = 1;
//        self->bookFromHere.frame = self.view.bounds;
//    }];
//}

-(void)hideHelp {
    [UIView animateWithDuration:0.25 animations:^{
        self->backdrop.alpha = 0;
        self->bookFromHere.frame = CGRectMake(0, [References screenHeight], [References screenWidth], [References screenHeight]);
    } completion:^(BOOL finished) {
        if (finished) {
            [self->backdrop removeFromSuperview];
            [self->bookFromHere removeFromSuperview];
            self->backdrop = nil;
            self->bookFromHere = nil;
        }
    }];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self->bookFromHere) {
        
        if (scrollView.contentOffset.y < -100) {
            [self hideHelp];
        }
    }
}


-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth]-30, 30)];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 30)];
    title.font = [UIFont systemFontOfSize:16 weight:UIFontWeightMedium];
    title.textColor = [UIColor lightGrayColor];
    NSDictionary *dates = availableDays[section];
    title.text = dates[@"header"];
    [header addSubview:title];
    UIButton *show = [[UIButton alloc] initWithFrame:CGRectMake(header.frame.size.width-30, 0, 30, 30)];
    [show.titleLabel setFont:[UIFont systemFontOfSize:9 weight:UIFontWeightBold]];
    [show setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    if ([showDate[section] isEqual:@0]) {
        [show setTitle:@"SHOW" forState:UIControlStateNormal];
    } else {
        [show setTitle:@"HIDE" forState:UIControlStateNormal];
    }
    show.tag = section;
    [show setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [show addTarget:self action:@selector(toggleSection:) forControlEvents:UIControlEventTouchUpInside];
    [header addSubview:show];
    
    return header;
}

-(void)toggleSection:(UIButton*)sender {
    CGFloat currentHeight = mainScroll.contentSize.height;
    if ([sender.titleLabel.text isEqualToString:@"SHOW"]) {
        showDate[sender.tag] = @1;
        currentHeight+=[sectionHeights[sender.tag] floatValue];
        mainScroll.contentSize = CGSizeMake(mainScroll.frame.size.width, currentHeight);
    } else {
        showDate[sender.tag] = @0;
        currentHeight-=[sectionHeights[sender.tag] floatValue];
        mainScroll.contentSize = CGSizeMake(mainScroll.frame.size.width, currentHeight);
    }
    [self->scheduleView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationFade];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NSDictionary *dict = availableDays[indexPath.section];
    NSString *header;
    NSMutableArray *tAv;
    header = dict[@"header"];
    tAv = dict[@"availability"];
    int foureighttime = [(NSNumber*)tAv[indexPath.row] intValue];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@-%i",header,foureighttime]];
    if (!cell) {
        NSInteger hour = foureighttime/2;
        NSInteger minute = 0;
        NSString *ampm = @"AM";
        if (foureighttime % 2 != 0) {
            minute = 30;
        }
        if (hour == 0) {
            hour = 12;
        } else if (hour == 12) {
            ampm = @"PM";
        } else if (hour > 12){
            hour-=12;
            ampm = @"PM";
        }
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"%@-%i",header,foureighttime]];
        cell.backgroundColor = [References colorFromHexString:@"#F5F5F5"];
        cell.textLabel.text = [NSString stringWithFormat:@"%li:%.2li %@",hour,minute,ampm];
        cell.tag = foureighttime;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.tintColor = [References schoolMainColor];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *selectedDayDictionary = availableDays[indexPath.section];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([(NSNumber*)selectedDayDictionary[@"int"] intValue] != selectedDate) {
        NSLog(@"different day");
        if (selectedCells.count != 0) {
            for (UITableViewCell *tCell in selectedCells) {
                tCell.accessoryType = UITableViewCellAccessoryNone;
            }
            [selectedCells removeAllObjects];
        }
        currentDateObject = selectedDayDictionary[@"date"];
        selectedDate = [(NSNumber*)selectedDayDictionary[@"int"] intValue];
        duration = 0;
        startTime = cell.tag;
        [selectedCells addObject:cell];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        selectedDateIndex = indexPath.section;
        [self toggleView:YES];
    } else {
        if (cell.tag - (1+duration) != startTime) {
            NSLog(@"%li:%li",cell.tag,startTime);
            NSLog(@"same day different but diff time");
            if (selectedCells.count != 0) {
                for (UITableViewCell *tCell in selectedCells) {
                    tCell.accessoryType = UITableViewCellAccessoryNone;
                }
                [selectedCells removeAllObjects];
            }
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            selectedDateIndex = indexPath.section;
            duration = 0;
            startTime = cell.tag;
            selectedDate = [(NSNumber*)selectedDayDictionary[@"int"] intValue];
            [selectedCells addObject:cell];
            [self toggleView:YES];
        } else if (cell.tag == startTime) {
            if (selectedCells.count != 0) {
                for (UITableViewCell *tCell in selectedCells) {
                    tCell.accessoryType = UITableViewCellAccessoryNone;
                }
                [selectedCells removeAllObjects];
            }
            selectedDate = -1;
            duration = 0;
            startTime = -1;
            cell.accessoryType = UITableViewCellAccessoryNone;
            selectedDateIndex = -1;
            [self toggleView:NO];
        }else {
            NSLog(@"same day and duration increased");
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            duration++;
            [selectedCells addObject:cell];
            selectedDateIndex = indexPath.section;
            [self toggleView:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return availableDays.count;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *dict = availableDays[section];
    if ([showDate[section] isEqual:@1]) {
        return [(NSMutableArray*)dict[@"availability"] count];
    }
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == ratingCollection) {
        return 0;
    } else {
        return 10;
    }
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (collectionView == courseCollection) {
        NSString *course = courseObjects[indexPath.row];
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:course forIndexPath:indexPath];
        cell.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.15];
        [References cornerRadius:cell radius:5];
        NSMutableArray *title = [[NSMutableArray alloc] initWithArray:[course componentsSeparatedByString:@":"]];
        UILabel *courselabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, cell.frame.size.width-20, cell.frame.size.height)];
        if ([title[0] isEqualToString:@"Business Administration"]) {
            title[0] = @"BA";
        }
        courselabel.text = [NSString stringWithFormat:@"%@\n%@",title[0],title[1]];
        courselabel.numberOfLines = 2;
        courselabel.textColor = [UIColor darkTextColor];
        [cell addSubview:courselabel];
        return cell;
    } else if (collectionView == ratingCollection) {
        NSDictionary *ratings = ratingObjects[indexPath.row];
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ratings[@"id"] forIndexPath:indexPath];
        if (cell.tag != 1) {
            UILabel *card = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, cell.frame.size.width-30, cell.frame.size.height)];
            card.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.15];
            [References cornerRadius:card radius:5];
            [cell addSubview:card];
            HCSStarRatingView *rating = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(cell.frame.size.width/2-((cell.frame.size.width/3)/2), 15, cell.frame.size.width/3, 25)];
            rating.backgroundColor = [UIColor clearColor];
            rating.tintColor = [UIColor grayColor];
            NSNumber *value =  [ratings objectForKey:@"rating"];
            rating.userInteractionEnabled = NO;
            rating.allowsHalfStars = YES;
            rating.starBorderWidth = 0;
            rating.emptyStarColor = [[UIColor grayColor] colorWithAlphaComponent:0.25];
            rating.value = value.floatValue;
            [cell addSubview:rating];
            UITextView *feedbackview = [[UITextView alloc] initWithFrame:CGRectMake(25, [References getBottomYofView:rating]+15, cell.frame.size.width-50, cell.frame.size.height-([References getBottomYofView:rating]+15)) textContainer:nil];
            feedbackview.backgroundColor = [UIColor clearColor];
            feedbackview.text =[ratings objectForKey:@"text"];
            feedbackview.textAlignment = NSTextAlignmentCenter;
            feedbackview.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
            feedbackview.textColor = [UIColor darkTextColor];
            if ([feedbackview.text isEqualToString:@"no feedback description provided."] || [feedbackview.text isEqualToString:@"Leave optional feedback here"]) {
                feedbackview.text = @"no feedback description provided.";
                feedbackview.font = [UIFont italicSystemFontOfSize:12];
                feedbackview.textColor = [UIColor lightGrayColor];
            }
            feedbackview.userInteractionEnabled = NO;
            [cell addSubview:feedbackview];
            cell.tag = 1;
        }
        
        return cell;
    } else {
        return nil;
    }
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == courseCollection) {
        return courseObjects.count;
    } else if (collectionView == ratingCollection) {
        return ratingObjects.count;
    } else {
        return 0;
    }
}

-(void)toggleView:(BOOL)toggle {
    if (!bottomButton) {
        bottomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, [References screenHeight], [References screenWidth], 44+[References bottomMargin])];
        bottomButton.backgroundColor = [References schoolMainColor];
        [bottomButton addTarget:self action:@selector(selectCourse) forControlEvents:UIControlEventTouchUpInside];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44)];
        label.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.text = @"Book Now";
        [bottomButton addSubview:label];
        [self.view addSubview:bottomButton];
        bottomButton.tag = 0;
    }
    if (toggle && (bottomButton.frame.origin.y == [References screenHeight])) {
        [UIView animateWithDuration:0.25 animations:^{
            self->bottomButton.frame = CGRectMake(0, self->bottomButton.frame.origin.y-self->bottomButton.frame.size.height, [References screenWidth], self->bottomButton.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                self->bottomButton.tag = 1;
            }
        }];
    } else if (!toggle) {
        [UIView animateWithDuration:0.25 animations:^{
            self->bottomButton.frame = CGRectMake(0, self->bottomButton.frame.origin.y+self->bottomButton.frame.size.height, [References screenWidth], self->bottomButton.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                self->bottomButton.tag = 0;
            }
        }];
    }
}

-(void)selectCourse {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Select a Course"
                                                                   message:@"This allows the tutor to better prepare for the course." preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [References schoolMainColor];
    for (NSString *course in courseObjects) {
        UIAlertAction* btn = [UIAlertAction actionWithTitle:[course stringByReplacingOccurrencesOfString:@":" withString:@" "] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//            [self showBook:[course stringByReplacingOccurrencesOfString:@":" withString:@" "]];
//            [self goToCheckout:[course stringByReplacingOccurrencesOfString:@":" withString:@" "]];
            alert.view.tintColor = [References schoolMainColor];
        }];
        
        [alert addAction:btn];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
//
//-(void)goToCheckout:(NSString*)course {
//    NSDictionary *selectedDictionary = availableDays[selectedDateIndex];
//    checkoutPageViewController *checkoutPageViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"checkoutPageViewController"];
//
//    checkoutPageViewController.durationString = [NSString stringWithFormat:@"%@ - %@",[self fourtyeighttoString:startTime],[self fourtyeighttoString:startTime+(1+duration)]];
//    checkoutPageViewController.tutor = tutor;
//    checkoutPageViewController.duration = [NSNumber numberWithInteger:duration+1];
//    checkoutPageViewController.calendarDate = currentDateObject;
//
//    checkoutPageViewController.calendarDateString = selectedDictionary[@"header"];
//    checkoutPageViewController.courseString = course;
//    checkoutPageViewController.date = selectedDictionary[@"int"];
//    checkoutPageViewController.time = [NSNumber numberWithInteger:startTime];
//    [self.navigationController pushViewController:checkoutPageViewController animated:YES];
//}
//
//-(NSString*)fourtyeighttoString:(NSInteger)time {
//    NSInteger hour = time/2;
//    NSInteger minute = 0;
//    NSString *ampm = @"AM";
//    if (time % 2 != 0) {
//        minute = 30;
//    }
//    if (hour == 0) {
//        hour = 12;
//    } else if (hour > 12) {
//        hour-=12;
//        ampm = @"PM";
//    }
//    return [NSString stringWithFormat:@"%li:%.2li %@",hour,minute,ampm];
//}
@end
