//
//  dayAvailability.h
//  tutor
//
//  Created by Robert Crosby on 7/3/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface dayAvailability : NSObject {
    uint64_t availability;
}

-(id)initWithValue:(uint64_t)value;
+(instancetype)withValue:(uint64_t)value;
+(instancetype)empty;

// CALLS
-(void)printAvailabilityBits;
-(void)printAvailabilityValue;
-(uint64_t)getValue;
-(BOOL)getBitAtIndex:(int)index;
-(BOOL)isAvailableAtHour:(int)hour andMinute:(int)minute;
-(int)getStartTime;
-(int)getDuration;
-(BOOL)checkIfAnd:(dayAvailability*)day;

// MODIFICATIONS
-(BOOL)setBitAtIndex:(int)index andDuration:(int)duration;
-(BOOL)flipBitAtIndex:(int)index;
-(BOOL)invert;
-(BOOL)xorWith:(dayAvailability*)other;
-(BOOL)andWith:(dayAvailability*)other;
-(BOOL)orWith:(dayAvailability*)other;
@end

NS_ASSUME_NONNULL_END
