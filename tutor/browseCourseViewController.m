//
//  browseCourseViewController.m
//  tutor
//
//  Created by Robert Crosby on 10/24/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "browseCourseViewController.h"

@interface browseCourseViewController ()

@end

@implementation browseCourseViewController

@synthesize course;

-(void)loaded {
    NSLog(@"READY");
}

-(CGFloat)screenWidth {
    return self.view.bounds.size.width;
}

-(CGFloat)screenHeight {
    return self.view.bounds.size.height;
}

-(void)viewWillAppear:(BOOL)animated {
    tutors = [[NSMutableArray alloc] init];
//    tList = [[NSMutableArray alloc] init];
    for (NSString* tutor in [course tutors]) {
        tutorObject *obj = [[tutorObject alloc] initWithID:tutor andCompletion:^(bool finished) {
            if (finished) {
                NSLog(@"finished");
            }
        }];
        [tutors addObject:obj];
//        [tList addObject:obj];
    }
//    days = [[NSMutableArray alloc] initWithArray:@[@1,@1,@1,@1,@1,@1,@1]];
    NSLog(@"%@",[course courseDepartmentAndNumber]);
    self.view.backgroundColor = [References lightGrey];
        [self menu];
}
- (void)viewDidLoad {
    selectedCalendarTutor = 0;
    table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.contentInset = UIEdgeInsetsMake(44+[References topMargin] +controller.frame.size.height+30
, 0, [References bottomMargin], 0);
    table.separatorInset = table.contentInset;
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.backgroundColor = [UIColor clearColor];
    table.delegate = self;
    table.dataSource = self;
//    calendarView = [[UIView alloc] initWithFrame:CGRectMake([self screenWidth], 44+[References topMargin]+controller.frame.size.height+30, [self screenWidth], [self screenHeight]-(44+[References topMargin]+controller.frame.size.height+30)-[References bottomMargin])];
//    tutorImages = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], 80)];
//    tutorImages.alwaysBounceHorizontal = YES;
//    UIView *blur = [[UIView alloc] initWithFrame:tutorImages.frame];
//    [References blurView:blur];
//    [calendarView addSubview:blur];
//    [References createLine:tutorImages xPos:0 yPos:tutorImages.frame.size.height-1 inFront:YES];
//    [calendarView addSubview:tutorImages];
//    [self getImagesForCalendarView];
//    [self.view addSubview:calendarView];
    [self.view addSubview:table];
//    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
//    [components setHour: 0];
//    [components setMinute: 0];
//    [components setSecond: 0];
//    NSDate *tDate = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components];
//    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
//
//    UIView *daysView = [[UIView alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:tutorImages], [self screenWidth], 50)];
//    [References blurView:daysView];
//    CGFloat x = 55;
//    for (int a = 0; a < 7; a++) {
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(x, 0, ([self screenWidth]-55)/7, 50)];
//        x+=([self screenWidth]-50)/7;
//        UILabel *day = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, view.frame.size.width-15, view.frame.size.width-15)];
//        day.backgroundColor = [UIColor darkTextColor];
//        [References cornerRadius:day radius:day.frame.size.width/2];
//        fmt.dateFormat = @"d";
//        day.text = [fmt stringFromDate:tDate];
//        day.font = [UIFont systemFontOfSize:16 weight:UIFontWeightLight];
//        day.textColor = [UIColor whiteColor];
//        [day setAdjustsFontSizeToFitWidth:YES];
//        day.textAlignment = NSTextAlignmentCenter;
//        [view addSubview:day];
//
//        UILabel *weekday = [[UILabel alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:day], view.frame.size.width, view.frame.size.height-[References getBottomYofView:day])];
//        weekday.textAlignment = NSTextAlignmentCenter;
//        weekday.font = [UIFont systemFontOfSize:6 weight:UIFontWeightSemibold];
//        fmt.dateFormat = @"EEEE";
//        weekday.text = [fmt stringFromDate:tDate];
//        weekday.textColor = [UIColor lightGrayColor];
//        [view addSubview:weekday];
//        [daysView addSubview:view];
//        tDate = [tDate dateByAddingTimeInterval:86400];
////        [References createVerticalLine:calendarView atX:view.frame.origin.x+view.frame.size.width andY:[References getBottomYofView:tutorImages] inFront:YES andHeight:calendarView.frame.size.height];
//    }
//    [calendarView addSubview:daysView];
//
    [super viewDidLoad];
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"averageRating"
                                                 ascending:NO];
    
   tutors = [[NSMutableArray alloc] initWithArray:[tutors sortedArrayUsingDescriptors:@[sortDescriptor]]];
    [table reloadData];
    // Do any additional setup after loading the view.
}

-(void)getImagesForCalendarView {
    __block CGFloat position = 15;
    __block int index = 0;
    for (NSString *ident in [course tutors]) {
        [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:ident] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.exists) {
                UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(position, 0, 70, 70)];
                [button addTarget:self action:@selector(changeCalendar:) forControlEvents:UIControlEventTouchUpInside];
                button.tag = index;
                index++;
                if (position == 15) {
                    self->indicator = [[UIView alloc] initWithFrame:CGRectMake(15,button.frame.origin.y+7.5, button.frame.size.width, button.frame.size.height)];
                    [self->indicator setBackgroundColor:[References lightGrey]];
                    [References cornerRadius:self->indicator radius:5];
                    [self->tutorImages addSubview:self->indicator];
                }
                position = position + 70;
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(button.frame.size.width/2-(25), 10, 50, 50)];
                [img sd_setImageWithURL:[NSURL URLWithString:snapshot.value[@"profileURL"]]];
                [References cornerRadius:img radius:img.frame.size.width/2];
                img.layer.borderColor = [UIColor lightGrayColor].CGColor;
                img.layer.borderWidth = 0.5;
                [button addSubview:img];
                UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:img]+2.5, button.frame.size.width, 10)];
                name.font = [UIFont systemFontOfSize:9 weight:UIFontWeightBold];
                name.textColor = [UIColor darkTextColor];
                NSString *tname = snapshot.value[@"name"];
                if ([tname containsString:@" "]) {
                    tname = [[tname componentsSeparatedByString:@" "] objectAtIndex:0];
                }
                name.textAlignment = NSTextAlignmentCenter;
                name.text = [tname uppercaseString];
                [button addSubview:name];
                [self->tutorImages addSubview:button];
                self->tutorImages.contentSize = CGSizeMake(position+15, button.frame.size.height);
            }
        }];
        
    }
}

-(void)changeCalendar:(UIButton*)sender {
    [UIView animateWithDuration:0.25 animations:^{
        indicator.frame = CGRectMake(sender.frame.origin.x, indicator.frame.origin.y, indicator.frame.size.width, indicator.frame.size.height);
    }];
    
    [currentTutor freeAvailableView];
    selectedCalendarTutor = sender.tag;
    currentTutor = tutors[selectedCalendarTutor];
    [currentTutor getAvailableViewInView:CGRectMake(0, [References getBottomYofView:tutorImages]+50, [self screenWidth], calendarView.frame.size.height-([References getBottomYofView:tutorImages]+25)) inView:calendarView andParentVC:self withCourse:[course courseDepartmentAndNumber]];
}

-(void)exchangeSubview:(UISegmentedControl*)sender {
    if (sender.selectedSegmentIndex == 1) {
        [UIView animateWithDuration:0.25 animations:^{
            table.frame = CGRectMake(-1*[self screenWidth], table.frame.origin.y, table.frame.size.width, table.frame.size.height);
            calendarView.frame = CGRectMake(0, calendarView.frame.origin.y, calendarView.frame.size.width, calendarView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                if (tutors.count > 0) {
                    tutorObject *tutor = tutors[selectedCalendarTutor];
                    currentTutor = tutor;
                    [tutor getAvailableViewInView:CGRectMake(0, [References getBottomYofView:tutorImages]+50, [self screenWidth], calendarView.frame.size.height-([References getBottomYofView:tutorImages]+25)) inView:calendarView andParentVC:self withCourse:[course courseDepartmentAndNumber]];
                }
                
            }
        }];
    } else {
        [UIView animateWithDuration:0.25 animations:^{
            table.frame = CGRectMake(0, table.frame.origin.y, table.frame.size.width, table.frame.size.height);
            calendarView.frame = CGRectMake([self screenWidth], calendarView.frame.origin.y, calendarView.frame.size.width, calendarView.frame.size.height);
        } completion:^(BOOL finished) {
            if (finished) {
                [currentTutor freeAvailableView];
            }
        }];
    }
}

-(void)menu {
//    controller = [[UISegmentedControl alloc] initWithItems:@[@"List",@"Calendar"]];
//    controller.frame = CGRectMake(15, [References topMargin]+44, [self screenWidth]-30, controller.frame.size.height);
//    controller.tintColor = [References schoolMainColor];
//    [controller setSelectedSegmentIndex:0];
//    [controller addTarget:self action:@selector(exchangeSubview:) forControlEvents:UIControlEventValueChanged];
//    controller.frame.size.height
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], 44+[References topMargin]+5)];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *mainHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [self screenWidth]-30, 44)];
    mainHeader.textAlignment = NSTextAlignmentCenter;
    mainHeader.text = [course printCourse];
    
    mainHeader.textColor = [UIColor darkTextColor];
    mainHeader.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:mainHeader];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    tutorCourse = [[UIButton alloc] initWithFrame:CGRectMake([self screenWidth]-15-100, [References topMargin], 100, 44)];
    [tutorCourse setTitle:@"Tutor Course" forState:UIControlStateNormal];
    [tutorCourse setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [tutorCourse.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [tutorCourse setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [tutorCourse addTarget:self action:@selector(tutorCourse) forControlEvents:UIControlEventTouchUpInside];
    tutorCourse.hidden = YES;
    [menuBar addSubview:tutorCourse];
//    [menuBar addSubview:controller];
    [self.view addSubview:menuBar];
//
//    NSArray *tD = @[@"M",@"T",@"W",@"T",@"F",@"S",@"S"];
//    CGFloat width = ([self screenWidth]-30-(6*5))/7;
//    for (int a = 0; a < tD.count; a++) {
//        UIButton *day = [[UIButton alloc] initWithFrame:CGRectMake(15+(a*width)+(a*5), [References topMargin]+5+42, width, 42)];
//        day.tag = a;
//        [day addTarget:self action:@selector(toggleDay:) forControlEvents:UIControlEventTouchUpInside];
//        [day setBackgroundColor:[References schoolMainColor]];
//        [References cornerRadius:day radius:5];
//        [day setTitle:tD[a] forState:UIControlStateNormal];
//        [menuBar addSubview:day];
//    }
    [self checkTutor];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
}

-(void)tutorCourse {
    if (amTutor) {
        __block UIView *processingPayment = [References loadingView:@"Removing..."];
        [self.view addSubview:processingPayment];
        [UIView animateWithDuration:0.25 animations:^{
            processingPayment.alpha = 1;
        } completion:^(BOOL finished) {
            
            NSDictionary *dict = @{[NSString stringWithFormat:@"updateDatabase/%@/%@/%@/tutors/%@",[References getSchoolDomain],course.courseDepartment,course.courseNumberString,[FIRAuth auth].currentUser.uid]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/courses/%@:%@",[FIRAuth auth].currentUser.uid,course.courseDepartment,course.courseNumberString]:[NSNull null]};
            [[[FIRDatabase database] reference] updateChildValues:dict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                
                for (tutorObject *tu in tutors) {
                    if ([[tu identifier] isEqualToString:[FIRAuth auth].currentUser.uid]) {
                        [tutors removeObject:tu];
                        break;
                    }
                }
                [course removeTutor];
                [table reloadData];
                [tutorCourse setTitle:@"Tutor Course" forState:UIControlStateNormal];
                amTutor = NO;
                [UIView animateWithDuration:0.25 animations:^{
                    processingPayment.alpha = 0;
                } completion:^(BOOL finished) {
                    [processingPayment removeFromSuperview];
                    processingPayment = nil;
                }];
            }];
        }];
    } else {
        __block UIView *processingPayment = [References loadingView:@"Tutoring..."];
        [self.view addSubview:processingPayment];
        [UIView animateWithDuration:0.25 animations:^{
            processingPayment.alpha = 1;
        } completion:^(BOOL finished) {
            NSDictionary *dict = @{[NSString stringWithFormat:@"updateDatabase/%@/%@/%@/tutors/%@",[References getSchoolDomain],course.courseDepartment,course.courseNumberString,[FIRAuth auth].currentUser.uid]:[FIRAuth auth].currentUser.uid,[NSString stringWithFormat:@"updateDatabase/users/%@/courses/%@:%@",[FIRAuth auth].currentUser.uid,course.courseDepartment,course.courseNumberString]:[NSString stringWithFormat:@"%@:%@",course.courseDepartment,self->course.courseNumberString]};
            [[[FIRDatabase database] reference] updateChildValues:dict withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                [tutors addObject:[[tutorObject alloc] initWithID:[FIRAuth auth].currentUser.uid andCompletion:^(bool finished) {
                    if (finished) {
                        NSLog(@"finished");
                    }
                }]];
                [course addTutor];
                [table reloadData];
                [tutorCourse setTitle:@"Stop Tutoring" forState:UIControlStateNormal];
                amTutor = YES;
                [UIView animateWithDuration:0.25 animations:^{
                    processingPayment.alpha = 0;
                } completion:^(BOOL finished) {
                    [processingPayment removeFromSuperview];
                    processingPayment = nil;
                }];
            }];
        }];
    }
}

-(void)checkTutor {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] child:@"tutor"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            [[[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[References getSchoolDomain]] child:course.courseDepartment] child:course.courseNumberString] child:@"tutors"] child:[FIRAuth auth].currentUser.uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if (snapshot.exists) {
                    amTutor = YES;
                    [tutorCourse setTitle:@"Stop Tutoring" forState:UIControlStateNormal];
                    tutorCourse.hidden = NO;
                } else {
                    tutorCourse.hidden = NO;
                }
            }];
        }
    }];
}

-(void)toggleDay:(UIButton*)sender {
    if (sender.alpha == 0.5) {
        sender.alpha = 1;
        days[sender.tag] = @1;
    } else {
        sender.alpha = 0.5;
        days[sender.tag] = @0;
    }
    [table reloadData];
}

-(void)close {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [self dismissViewControllerAnimated:true completion:nil];
        // ipad
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
    
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return tutors.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tutorObject *tutor = tutors[indexPath.row];
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:[tutor identifier]];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[tutor identifier]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        UIView *sView = [tutor smallView];
        UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(sView.frame.origin.x+5, sView.frame.origin.y+5, sView.frame.size.width-10, sView.frame.size.height-10)];
        shadow.backgroundColor = [UIColor whiteColor];
        [References bottomshadow:shadow];
        [cell addSubview:shadow];
        [cell addSubview:sView];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    tutorContainerController *tvc = [[tutorContainerController alloc] init];
    tvc.tutorID = [tutors[indexPath.item] identifier];
    [self.navigationController pushViewController:tvc animated:YES];
}

@end
