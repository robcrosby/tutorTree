//
//  bookingList.m
//  tutor
//
//  Created by Robert Crosby on 6/5/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "bookingList.h"

@implementation bookingList

-(id)initWithDictionary:(NSDictionary*)dictionary andID:(NSString*)tID{
    self = [super init];
    if (self) {
        transactionID = tID;
        NSNumber *dateSeconds = [dictionary valueForKey:@"calDate"];
        startTime = [dictionary valueForKey:@"time"];
        duration = [dictionary valueForKey:@"duration"];
        intDate = [dictionary valueForKey:@"intDate"];
        date = [NSDate dateWithTimeIntervalSince1970:dateSeconds.floatValue];
    }
    return self;
}

+(instancetype)WithDictionary:(NSDictionary*)dictionary andID:(NSString *)tID {
    return [[bookingList alloc] initWithDictionary:dictionary andID:tID];
}

-(NSString*)transactionID {
    return transactionID;
}

-(NSString*)dateAndTime {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMM d, h:mm a"];
    return [fmt stringFromDate:date];
}

-(float)timeSince1970 {
    return date.timeIntervalSince1970;
}

-(BOOL)hasPassed {
    if ([self timeSince1970] < [[NSDate date] timeIntervalSince1970]) {
        return YES;
    } else {
        return NO;
    }
}

-(NSNumber*)duration {
    return duration;
}
-(NSNumber*)intDate {
    return intDate;
}
-(NSNumber*)startTime {
    return startTime;
}
-(NSDate*)date {
    return date;
}
@end
