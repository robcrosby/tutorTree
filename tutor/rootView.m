//
//  rootView.m
//  tutor
//
//  Created by Robert Crosby on 7/26/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "rootView.h"

@interface rootView ()

@end

@implementation rootView

-(void)viewDidAppear {
    
}

- (void)viewDidLoad {
    if ([FIRAuth auth].currentUser) {
        homeViewController *tutorOfficeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"homeViewController"];
        [self.navigationController pushViewController:tutorOfficeViewController animated:NO];
    }
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(([References screenWidth]/2)-50, [References topMargin]+25, 100, 100)];
    UILabel *tutortree = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:logo]+15, [References screenWidth]-30, 30)];
    UIButton *continuebutton = [[UIButton alloc] initWithFrame:CGRectMake(15, [References screenHeight]-[References bottomMargin]-15-40, [References screenWidth]-30, 20)];
    NSMutableArray *imgs = [[NSMutableArray alloc] initWithArray:@[@"accounting.jpg",@"biology.jpg",@"business.jpg",@"chemistry.jpg",@"economics.jpg",@"finance.jpg",@"math.jpg",@"physics.jpg",@"statistics.jpg"]];
    int count = (int)[imgs count];
    for (int i = 0; i < count; ++i) {
        // Select a random element between i and end of array to swap with.
        int nElements = count - i;
        int n = (arc4random() % nElements) + i;
        [imgs exchangeObjectAtIndex:i withObjectAtIndex:n];
    }
    lastX = 0;
    CGFloat size = 250;
    for (int a = 0; a < imgs.count; a++) {
        UIView *card = [[UIView alloc] initWithFrame:CGRectMake([References screenWidth]+(a*size)+(a*25), [self randomValueBetween:[References getBottomYofView:tutortree] and:continuebutton.frame.origin.y-size], size, size)];
        UIImageView *img = [[UIImageView alloc] initWithFrame:card.bounds];
        img.contentMode = UIViewContentModeScaleAspectFill;
        [img setImage:[UIImage imageNamed:imgs[a]]];
        [References cornerRadius:img radius:10];
        UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(img.frame.origin.x+5, img.frame.origin.y+5, img.frame.size.width-10, img.frame.size.height-10)];
        shadow.backgroundColor = [UIColor whiteColor];
        [References bottomshadow:shadow];
        [card addSubview:shadow];
        [card addSubview:img];
        [self.view addSubview:card];
        [self shapeAnimation:card];
        if (card.frame.origin.x+card.frame.size.width > lastX) {
            lastX = card.frame.origin.x+card.frame.size.width+15;
        }
    }
    
        [logo setImage:[UIImage imageNamed:@"icon.png"]];
        [self.view addSubview:logo];
    
        tutortree.font = [UIFont systemFontOfSize:26 weight:UIFontWeightUltraLight];
        tutortree.textColor = [References colorFromHexString:@"#004800"];
        tutortree.textAlignment = NSTextAlignmentCenter;
        tutortree.text = @"tutortree";
        [self.view addSubview:tutortree];
    
    
        [continuebutton.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
        [continuebutton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [continuebutton setTitle:@"Get Started" forState:UIControlStateNormal];
        [continuebutton addTarget:self action:@selector(signin) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:continuebutton];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)signin {
    nGettingStartedViewController *gvc = [[nGettingStartedViewController alloc] init];
//            onboardingViewController *tutorOfficeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"onboardingViewController"];
    [UIView animateWithDuration:0.45 animations:^{
        for (UIView *v in self.view.subviews) {
            v.alpha = 0;
        }
        [self.navigationController pushViewController:gvc animated:YES];
    }];
    
}


-(void)shapeAnimation:(UIView*)card {
    if (card.frame.origin.x+card.frame.size.width < -10) {
//        card.frame = CGRectMake(lastX, card.frame.origin.y, card.frame.size.width, card.frame.size.height);
//        lastX = card.frame.origin.x+card.frame.size.width+15;
    }
    [UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        card.frame = CGRectMake(card.frame.origin.x-40, card.frame.origin.y, card.frame.size.width, card.frame.size.height);
    } completion:^(BOOL finished) {
        [self shapeAnimation:card];
    }];
}

- (CGFloat)randomValueBetween:(CGFloat)min and:(CGFloat)max {
    return (CGFloat)(min + arc4random_uniform(max - min + 1));
}

@end
