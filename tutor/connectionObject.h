//
//  connectionObject.h
//  tutor
//
//  Created by Robert Crosby on 10/25/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "messageReference.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AFHTTPSessionManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface connectionObject : NSObject {
    NSString *identification,*other,*otherName,*otherProfileURL,*otherToken,*meetingPoint;
    BOOL tutor;
    CGFloat messagesHeight;
    UIView *cellView;
    UILabel *cellName,*cellMessage,*notificationBadge;
    UIImageView *cellImage;
    NSMutableArray *messages,*messageIDs;
    NSMutableDictionary *sessions;
    UITableViewCell *preview;
    UITableView *table;
}

-(id)initWithID:(NSString*)tID;

-(void)addSession:(NSDictionary*)session;
-(UIView*)getCellView;
-(void)updateColors;

-(void)messageTable:(UITableView*)tTable;
-(messageReference*)messageAtIndex:(NSInteger)index;
-(NSString*)messageIdAtIndex:(NSInteger)index;
-(NSString*)otherID;
-(NSInteger)messageCount;
-(NSMutableArray*)messages;
-(void)sendMessage:(NSString*)message withCompletion:(void (^)(BOOL))complete;
-(void)updateMeetingPoint:(NSString*)place withCompletion:(void (^)(BOOL))complete;
-(void)updateRating:(NSNumber*)rating withCompletion:(void (^)(BOOL))complete;
-(void)sendNotification:(NSString*)notificationText withSubject:(NSString*)subject;

-(FIRDatabaseReference*)connectionURL;
-(FIRDatabaseReference*)profileURL;
-(NSURL*)profileImageURL;
-(NSString*)name;

@end

NS_ASSUME_NONNULL_END
