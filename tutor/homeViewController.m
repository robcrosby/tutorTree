//
//  homeViewController.m
//  tutor
//
//  Created by Robert Crosby on 5/11/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "homeViewController.h"

#define departmentHeight 80

@interface homeViewController ()

@end

@implementation homeViewController

-(void)transferSessions {
//    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:@"6fsexHWXsKPg9d8EEXUpnmHRYJj2"] child:@"sessions"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        if (snapshot.exists) {
//            NSDictionary *result = snapshot.value;
//            [self transferSessionsAction:result atIndex:0 andID:[[result allKeys] objectAtIndex:0]];
//        }
//    }];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Are you sure?" message:@"This is not reversible (yet)" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.exists) {
                NSDictionary *result = snapshot.value;
                [self transferSessionsAction:result atIndex:0 andID:[[result allKeys] objectAtIndex:0]];
            }
        }];
    }]];
     [alert addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil]];
     [self presentViewController:alert animated:YES completion:nil];
    
}

-(void)transferSessionsAction:(NSDictionary*)dictionary atIndex:(NSInteger)index andID:(NSString*)ident {
        NSString *key = [[dictionary allKeys] objectAtIndex:index];
        NSDictionary *dict = dictionary[key];
        NSString *tutor = [dict objectForKey:@"tutor"];
        NSString *student = [dict objectForKey:@"student"];
        NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] init];
        if ([dict objectForKey:@"sessions"]) {
            for (id bID in [dict objectForKey:@"sessions"]) {
                NSDictionary *session = [[dict objectForKey:@"sessions"] objectForKey:bID];
                if (session[@"date"] && session[@"course"] && session[@"date"] && session[@"value"]) {
//                    NSNumber *val = session[@"value"];
//                    NSInteger duration = [[[dayAvailability alloc] initWithValue:val.unsignedLongLongValue] getDuration];
                    CGFloat end = [(NSNumber*)session[@"date"] floatValue] + (1800);
                    [resultDictionary setObject:@{@"start":(NSNumber*)session[@"date"],@"end":[NSNumber numberWithFloat:end],@"other":student,@"course":(NSString*)session[@"course"]} forKey:[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",tutor,bID]];
                    [resultDictionary setObject:@{@"start":(NSNumber*)session[@"date"],@"end":[NSNumber numberWithFloat:end],@"other":tutor,@"course":(NSString*)session[@"course"]} forKey:[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",student,bID]];
                }
                
            }
        }
    [[[FIRDatabase database] reference] updateChildValues:resultDictionary withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (index+1 < [dictionary allKeys].count) {
            [self transferSessionsAction:dictionary atIndex:index+1 andID:[[dictionary allKeys] objectAtIndex:index]];
        }
    }];
}

-(void)batchNotify {
//
//    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        NSMutableArray *tokens = [[NSMutableArray alloc] init];
//        for (id key in snapshot.value) {
//            if ([snapshot.value[key] objectForKey:@"token"]) {
//                [tokens addObject:[snapshot.value[key] objectForKey:@"token"]];
//            }
//        }
//        for (NSString *token in tokens) {
//            NSString *url = [NSString stringWithFormat:@"https://tutortree-release.herokuapp.com/sendNotification/Update Available/Due to high volume Tutortree may be experiencing technical difficulties. Update to the latest version in the App Store to avoid errors/%@/%@",token,@1];
//            NSString *finalURL = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//            [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//                NSLog(@"SENT");
//            } failure:^(NSURLSessionTask *operation, NSError *error) {
//                NSLog(@"Error: %@", error);
//            }];
//
//        }
//    }];
}

-(void)checkVersion {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"minimumVersion"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            NSString *minimumVersion = [NSString stringWithFormat:@"%@",snapshot.value];
            
            NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
            NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"]; // example: 1.0.0
            if ([minimumVersion isEqualToString:appVersion]) {
                NSLog(@"Version up to date");
            } else {
                UIView *pleaseupdate = [[UIView alloc] initWithFrame:self.view.bounds];
                pleaseupdate.backgroundColor = [UIColor whiteColor];
                UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake([References screenWidth]/2-30, [References screenHeight]/4, 60, 60)];
                icon.image = [UIImage imageNamed:@"icon.png"];
                [pleaseupdate addSubview:icon];
                UILabel *why = [[UILabel alloc] initWithFrame:CGRectMake(50, [References getBottomYofView:icon]+10, [References screenWidth]-100, 0)];
                why.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
                why.text = @"A new version of Tutortree\nis available on the App Store";
                why.numberOfLines = 0;
                why.textColor = [UIColor lightGrayColor];
                why.textAlignment = NSTextAlignmentCenter;
                why.frame = CGRectMake(why.frame.origin.x, why.frame.origin.y, why.frame.size.width, [References fixedWidthSizeWithFont:why.font andString:why.text andLabel:why].size.height);
                [pleaseupdate addSubview:why];
                UIButton *update = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:why]+25, [References screenWidth]-30, 30)];
                update.titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
                [update setTitle:@"Update Now" forState:UIControlStateNormal];
                [update setTitleColor:[References schoolMainColor] forState:UIControlStateNormal];
                [update addTarget:self action:@selector(openAppStore:) forControlEvents:UIControlEventTouchUpInside];
                [pleaseupdate addSubview:update];
                [self.view addSubview:pleaseupdate];
                [self.view bringSubviewToFront:pleaseupdate];
            }
            
        }
    }];
}

-(void)openAppStore:(UIButton*)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/us/app/tutortree/id1353273906?mt=8"]];
    
}

-(void)viewWillAppear:(BOOL)animated {
    if (reloadCount < 1) {
        loadingView = [[UIView alloc] initWithFrame:self.view.bounds];
        loadingView.backgroundColor = [References background];
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake([References screenWidth]/2-30, [References screenHeight]/3, 60, 60)];
        icon.image = [UIImage imageNamed:[[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] ? @"invert-icon.png" : @"icon.png"];
        [loadingView addSubview:icon];
        [self spinView:icon];
//        UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        spinner.frame = CGRectMake([References screenWidth]/2-(spinner.frame.size.width/2), [References getBottomYofView:icon]+25, spinner.frame.size.width, spinner.frame.size.height);
//        [spinner startAnimating];
//        [loadingView addSubview:spinner];
        [self.view addSubview:loadingView];
        [self.view bringSubviewToFront:loadingView];
    }
    darkMode = [[NSUserDefaults standardUserDefaults] boolForKey:@"dark"];
}

-(void)spinView:(UIView*)v {
    [UIView animateWithDuration:0.75 animations:^{
        v.transform = CGAffineTransformMakeScale(-1, 1);
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.75 animations:^{
                v.transform = CGAffineTransformMakeScale(1, 1);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self spinView:v];
                }
            }];
        }
    }];
}

-(void)viewDidAppear:(BOOL)animated {
    self.view.tag = 5;
    if (reloadCount > 0) {
        NSLog(@"appear reload");
//        [self reloadConnections:nil];
//        [self reloadDepartments:nil];
        
    }
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = self;

}


-(UIStatusBarStyle)preferredStatusBarStyle {
    return [References statusBarColor];
}

- (void)viewDidLoad {
    self->uid = [FIRAuth auth].currentUser.uid;
    marginTop = [References topMargin];
    [self.view setBackgroundColor:[References background]];
    [self createStatic];
//    [self transferSessions];
    [super viewDidLoad];
//    [self analyzeConnections];
    [self registerForRemoteNotifications];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createStatic {
    ctrl = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight])];
    ctrl.backgroundColor = [UIColor clearColor];
    ctrl.alpha = 0;
    ctrl.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    ctrl.pagingEnabled = YES;
    ctrl.contentSize = CGSizeMake([References screenWidth]*2, ctrl.frame.size.height);
    ctrl.delegate = self;
    ctrl.alwaysBounceVertical = NO;
    [self.view addSubview:ctrl];
    [self.view sendSubviewToBack:ctrl];
    connectionsTable = [[UITableView alloc] initWithFrame:CGRectMake([References screenWidth], 0, [References screenWidth], [References screenHeight]) style:UITableViewStyleGrouped];
    connectionsTable.showsVerticalScrollIndicator = NO;
    connectionsTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    connectionsTable.backgroundColor = [References background];
    connectionsTable.delegate = self;
    connectionsTable.dataSource = self;
    connectionsTable.contentInset = UIEdgeInsetsMake(0, 0, tabbar.frame.size.height+15+50, 0);
    UIRefreshControl *connectionsTableReload = [[UIRefreshControl alloc]init];
        [connectionsTableReload addTarget:self action:@selector(reloadConnections:) forControlEvents:UIControlEventValueChanged];
//    UIImageView *loadimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    loadimage.image = [UIImage imageNamed:@"icon.png"];
//    [connectionsTableReload addSubview:loadimage];
    [connectionsTable addSubview:connectionsTableReload];
    [ctrl addSubview:connectionsTable];
    ctrl.scrollEnabled = NO;
    [self frames];
}

-(void)reloadDepartments:(UIRefreshControl*)sender {
    [UIView animateWithDuration:0.25 animations:^{
        table.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self->table removeFromSuperview];
            self->table = nil;
            selectedIndex = nil;
            [self populateDepartmentswithCompletion:true onComplete:^(BOOL finished) {
                [UIView animateWithDuration:0.25 animations:^{
                    table.alpha = 1;
                    if (sender) {
                        [sender endRefreshing];
                    }
                }];
            }];
        }
    }];
}

-(void)reloadConnections:(UIRefreshControl*)sender {
    
    [self populateConnectionswithCompletion:^(BOOL finished, NSMutableArray *courses) {
        if (finished) {
            if (sender) {
                [sender endRefreshing];
            }
        }
    }];
    
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == ctrl) {
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
    } else {
        if (scrollView.contentOffset.y > 0 && statusBarBlur.tag == 0) {
            statusBarBlur.tag = 1;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 1;
            }];
        } else if (scrollView.contentOffset.y <= 0 && statusBarBlur.tag == 1) {
            statusBarBlur.tag = 0;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 0;
            }];
        }
    }
}

-(void)queryUpdateAvailable {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"version"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            double versionDouble = [version doubleValue];
            double firebaseDouble = [(NSNumber*)snapshot.value doubleValue];
            if (versionDouble < firebaseDouble) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"update"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self showUpdateAvailable];
            }
        } else {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"update"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
    }];
}

-(void)showUpdateAvailable {
    UIButton *view = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 0)];
//    [References cornerRadius:view radius:5];
    [view addTarget:self action:@selector(openAppStore:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *updateText = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+10, view.frame.size.width-30, 0)];
    updateText.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
    updateText.numberOfLines = 0;
    updateText.text = @"A new version of Tutortree is available. Tap here to be taken to the App Store to manually update.";
    updateText.frame = CGRectMake(updateText.frame.origin.x, updateText.frame.origin.y, updateText.frame.size.width, [References fixedWidthSizeWithFont:updateText.font andString:updateText.text andLabel:updateText].size.height);
    updateText.textColor = [References primaryText];
    view.frame = CGRectMake(0, -1*(updateText.frame.size.height+25+[References topMargin]), [References screenWidth], updateText.frame.size.height+25+[References topMargin]);
//    [References blurView:view];
    view.backgroundColor = [[References accent] colorWithAlphaComponent:1];
    [view addSubview:updateText];
    [self.view addSubview:view];
    [UIView animateWithDuration:0.15 animations:^{
        view.frame = CGRectMake(view.frame.origin.x, 0, view.frame.size.width, view.frame.size.height);
    }];
}

-(void)frames {
    [UIView animateWithDuration:0.25 animations:^{
        self->mainScroll.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            self->mainScrollContentSize = 15;
            [self moreView];
            [self populateConnectionswithCompletion:^(BOOL finished, NSMutableArray *pastCourses) {
                if (finished) {
                    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"] isEqualToString:@"uoregon"]) {
                        universityName = @"University of Oregon";
                        showFeaturedTutors = true;
                    } else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"] isEqualToString:@"ucsb"]) {
                        universityName = @"University of California,Santa Barbara";
                        showFeaturedTutors = false;
                    } else if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"] isEqualToString:@"oregonstate"]) {
                        universityName = @"Oregon State University";
                        showFeaturedTutors = false;
                    }
                    headerV = [self createUniversityHeader:universityName];
                    [self getBestTutorsWithCompletion:^(BOOL finished) {
                        [self populateDepartmentswithCompletion:false onComplete:^(BOOL finished) {
                            if (finished) {
                                [UIView animateWithDuration:0.25 animations:^{
                                        self->ctrl.alpha = 1;
                                        loadingView.alpha = 0;
                                    } completion:^(BOOL finished) {
                                        [self->loadingView removeFromSuperview];
                                        loadingView = nil;
                                    }];
                                    if (!self->tabbar) {
                                        [self bottomBar];
                                    }
                                    self->reloadCount++;
                                [self queryUpdateAvailable];
//                            TODO:   THIS IS WHERE LOADING IS DONE!!!!
                                
                            }
                        }];
                    }];
                    
                }
            }];
        }
    }];
    
}

-(UIView*)createUniversityHeader:(NSString*)name {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 0)];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-80, 0)];
    hheader.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    hheader.textColor = [References primaryText];
    hheader.text = [universityName stringByReplacingOccurrencesOfString:@"," withString:@"\n"];
    hheader.numberOfLines = 0;
    CGFloat height = [References fixedWidthSizeWithFont:hheader.font andString:hheader.text andLabel:hheader].size.height;
    view.frame = CGRectMake(0, 0, [References screenWidth], height+25);
    hheader.frame = CGRectMake(hheader.frame.origin.x, (view.frame.size.height/2)-(height/2), hheader.frame.size.width, height);
    UIImageView *image = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"invert-icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    image.tintColor = [References primaryText];
    height+=10;
    image.frame = CGRectMake([References screenWidth]-15-height, (view.frame.size.height/2)-(height/2), height, height);
    [view addSubview:hheader];
    [view addSubview:image];
    return view;
}

-(void)toggleDarkMode:(UISwitch*)toggle {
    darkMode = toggle.isOn;
    if (!darkMode) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"dark"];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"dark"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.backgroundColor = [References background];
        self->more.backgroundColor = [References background];
        self->more.alpha = 0;
        self->tabbar.tintColor = [References accent];
        self->tabbar.unselectedItemTintColor = [References secondaryText];
        self->tabbar.barTintColor = [References background];
        self->connectionsTable.backgroundColor = [References background];
        [(UIVisualEffectView*)self->statusBarBlur.subviews[0] setEffect:[UIBlurEffect effectWithStyle:(self->darkMode == true) ? UIBlurEffectStyleDark : UIBlurEffectStyleProminent]];
        [self setNeedsStatusBarAppearanceUpdate];
        for (NSString *connectionID in [self->connections allKeys]) {
            [(connectionObject*)[self->connections objectForKey:connectionID] updateColors];
        }
    } completion:^(BOOL finished) {
        if (finished) {
            headerV = [self createUniversityHeader:universityName];
            [self reloadDepartments:nil];
            [self->bestTutorsCollection reloadData];
            [self->connectionsTable reloadData];
            [self->more reloadData];
            [UIView animateWithDuration:0.15 animations:^{
                self->more.alpha = 1;
            }];
        }
    }];
}

-(void)findUserByName {
    [[[[FIRDatabase database] reference] child:@"updateDatabase/invoices"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        for (id key in snapshot.value) {
            if ([(NSString*)[snapshot.value[key] objectForKey:@"fromEmail"] containsString:@"E2XFcPrCFJOexLHgASHSrgCckfM2"] && [(NSString*)[snapshot.value[key] objectForKey:@"toEmail"] containsString:@"R1swuHiIrnSwR7g9FDclK9ok6Bu1"]) {
                NSString *date = [[snapshot.value[key] objectForKey:@"metadata"] objectForKey:@"date"];
                NSLog(@"%@: %@",key,date);
            }
        }
    }];
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSLog(@"item %ld",(long)item.tag);
    if (item.tag == 0) {
        [self scrollPage:CGPointMake(0, 0)];
        [tabBar setSelectedItem:item];
        if (table.contentOffset.y > 0 && statusBarBlur.tag == 0) {
            statusBarBlur.tag = 1;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 1;
            }];
        } else if (table.contentOffset.y <= 0 && statusBarBlur.tag == 1) {
            statusBarBlur.tag = 0;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 0;
            }];
        }
    } else if (item.tag == 1) {
        [self scrollPage:CGPointMake([References screenWidth], 0)];
        [tabBar setSelectedItem:item];
        if (connectionsTable.contentOffset.y > 0 && statusBarBlur.tag == 0) {
            statusBarBlur.tag = 1;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 1;
            }];
        } else if (connectionsTable.contentOffset.y <= 0 && statusBarBlur.tag == 1) {
            statusBarBlur.tag = 0;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 0;
            }];
        }
    } else  if (item.tag == 2) {
        [self scrollPage:CGPointMake([References screenWidth]*2, 0)];
        [tabBar setSelectedItem:item];
        if (more.contentOffset.y > 0 && statusBarBlur.tag == 0) {
            statusBarBlur.tag = 1;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 1;
            }];
        } else if (more.contentOffset.y <= 0 && statusBarBlur.tag == 1) {
            statusBarBlur.tag = 0;
            [UIView animateWithDuration:0.33 animations:^{
                statusBarBlur.alpha = 0;
            }];
        }
    }
}

-(void)scrollPage:(CGPoint)position {
    [ctrl setContentOffset:position animated:NO];
}

-(void)bottomBar {
    tabbar = [[UITabBar alloc] init];
    tabbar.delegate = self;
    tabbar.frame = CGRectMake(0, [References screenHeight], [References screenWidth], tabbar.frame.size.height);
    tabbar.tintColor = [References accent];
    tabbar.unselectedItemTintColor = [References secondaryText];
    tabbar.barTintColor = [References background];
    UITabBarItem *discover = [[UITabBarItem alloc] initWithTitle:@"Discover" image:[UIImage imageNamed:@"feed.png"] tag:0];
    me = [[UITabBarItem alloc] initWithTitle:@"You" image:[UIImage imageNamed:@"user.png"] tag:1];
    [me setBadgeColor:[References colorFromHexString:@"#FF1744"]];
    if (pending.count > 0) {
        [me setBadgeValue:[NSString stringWithFormat:@"%li",pending.count]];
    } else {
        [me setBadgeValue:nil];
    }
    
    UITabBarItem *more = [[UITabBarItem alloc] initWithTitle:@"More" image:[UIImage imageNamed:@"menu.png"] tag:2];
    [tabbar setItems:@[discover,me,more]];
    [tabbar setSelectedItem:discover];
//    bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, [References screenHeight], [References screenWidth], [References bottomMargin]+tabbar.frame.size.height)];
//    [References blurView:bottomBar];
//    [References createLine:bottomBar xPos:0 yPos:0 inFront:YES];
//    [bottomBar addSubview:tabbar];
//    [self.view addSubview:bottomBar];
    [self.view addSubview:tabbar];
    [UIView animateWithDuration:0.33 animations:^{
        self->tabbar.frame =CGRectMake(0, [References screenHeight]-self->tabbar.frame.size.height, [References screenWidth],self->tabbar.frame.size.height);
    } completion:^(BOOL finished) {
        if (finished) {
            statusBarBlur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References topMargin])];
            [References blurView:statusBarBlur];
            statusBarBlur.alpha = 0;
            [self.view addSubview:statusBarBlur];
            if (needAgreement) {
                [self performSelector:@selector(openInfo) withObject:nil afterDelay:0.5];
            }
        }
    }];
}

-(void)analytics {
//    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"analytics"] child:@"opens"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        NSNumber *opens;
//        if (snapshot.exists) {
//            opens = snapshot.value;
//        } else {
//            opens = [NSNumber numberWithInteger:0];
//        }
//        opens = [NSNumber numberWithInteger:opens.integerValue+1];
//        NSLog(@"this is open: %@",opens);
//        [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"analytics"] updateChildValues:@{@"opens":opens} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
//            NSLog(@"analytics updated!");
//            if (opens.integerValue % 20 == 0) {
//                [SKStoreReviewController requestReview];
//            }
//        }];
//    }];
}

-(void)getBestTutorsWithCompletion:(void(^)(BOOL finished))complete {
    @try {
        if (showFeaturedTutors) {
            [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"featuredTutors"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if (snapshot.hasChildren) {
                    bestTutorsView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, [References screenWidth], 160)];
                    bestTutorsView.backgroundColor = [UIColor clearColor];
                    UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
                    
                    flowLayout.itemSize = CGSizeMake([References screenWidth], 50);
                    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
                    tutors = [[NSMutableArray alloc] init];
                    bestTutorsCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 160) collectionViewLayout:flowLayout];
                    bestTutorsCollection.backgroundColor = [UIColor clearColor];
                    bestTutorsCollection.delegate = self;
                    bestTutorsCollection.dataSource = self;
                    bestTutorsCollection.showsHorizontalScrollIndicator = NO;
                    bestTutorsCollection.pagingEnabled = YES;
                    [bestTutorsView addSubview:bestTutorsCollection];
                    for (id key in snapshot.value) {
                        [bestTutorsCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:[NSString stringWithFormat:@"%@",key]];
                        [tutors addObject:key];
                    }
                    [bestTutorsCollection reloadData];
                    [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:@"featuredTutors"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }];
            complete(YES);
        } else {
            complete(YES);
        }
       
    } @catch (NSException *exception) {
        [self signOut];
    }
    
//    }
    
}

- (void) pullDataAndRefresh:(bool)refresh{
    NSLog(@"domain: %@",[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"]);
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        if (![(NSString*)key isEqualToString:@"currentDomain"] && ![(NSString*)key isEqualToString:@"school"] && ![(NSString*)key isEqualToString:@"clientToken"] && ![(NSString*)key isEqualToString:@"currentVersion"] && ![(NSString*)key isEqualToString:@"agreement"] && ![(NSString*)key isEqualToString:@"sandbox"]) {
            [defs removeObjectForKey:key];
        }
        
    }
    connectionsCollection = nil;
    connections = nil;
    connections = [[NSMutableArray alloc] init];
    [self frames];
    [refreshControl endRefreshing];
}
//
//-(NSString*)AIText {
//    NSInteger numberOfSessions = 0;
//    NSDate *soonest = nil;
//    connectionReference *soonestConnection = nil;
//    for (connectionReference *connection in connections) {
//        if ([connection soonestSession]) {
//            if (soonest == nil) {
//                soonest = [connection soonestSession];
//                soonestConnection = connection;
//            } else if ([connection soonestSession].timeIntervalSinceNow < soonest.timeIntervalSinceNow) {
//                soonest = [connection soonestSession];
//                soonestConnection = connection;
//            }
//        }
//        numberOfSessions+=[connection numberOfSessions];
//    }
//    if (soonest != nil) {
//        noUpcoming = FALSE;
//        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
//        [fmt setDateFormat:@"EEEE, MMMM d"];
//        NSString *dateText = [fmt stringFromDate:soonest];
//        [fmt setDateFormat:@"h:mm a"];
//        NSString *timeText = [fmt stringFromDate:soonest];
//        if (numberOfSessions == 1) {
//            return [NSString stringWithFormat:@"You have one upcoming session on %@ at %@",dateText,timeText];
//        } else {
//            return [NSString stringWithFormat:@"You have %li upcoming sessions, the soonest is on %@ at %@",numberOfSessions,dateText,timeText];
//        }
//    }
//    noUpcoming = TRUE;
//    return @"You have no upcoming sessions.";
//}



-(void)populateDepartmentswithCompletion:(bool)forceRefresh onComplete:(void(^)(BOOL finished))complete {
    @try {
        NSLog(@"No offline departments...%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"currentDomain"]);
        [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[[NSUserDefaults standardUserDefaults] objectForKey:@"currentDomain"]]  observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.hasChildren) {
                NSLog(@"%@",snapshot.value);
                [self->departmentDatabases removeAllObjects];
                if (self->departmentDatabases) {
                    self->departmentDatabases = nil;
                }
                self->departmentDatabases = [[NSMutableArray alloc] init];
                for (id key in snapshot.value) {
                    departmentReference *department = [[departmentReference alloc] initWithName:key andCourses:[snapshot.value objectForKey:key]];
                    if ([(NSString*)key isEqualToString:@"Math"] || [(NSString*)key isEqualToString:@"Physics"] || [(NSString*)key isEqualToString:@"Statistics"]) {
                        [departmentDatabases insertObject:department atIndex:0];
                    } else {
                        [departmentDatabases addObject:department];
                    }
                }
                NSSortDescriptor *sortDescriptor;
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"tutorCount"
                                                             ascending:NO];
                self->departmentDatabases = [[NSMutableArray alloc] initWithArray:[departmentDatabases sortedArrayUsingDescriptors:@[sortDescriptor]]];
                if (!self->table) {
                    self->table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight]) style:UITableViewStyleGrouped];
                    self->table.contentInset = UIEdgeInsetsMake(0, 0, self->tabbar.frame.size.height+[References bottomMargin]+60, 0);
                    self->table.clipsToBounds = YES;
                    self->table.delegate = self;
                    self->table.dataSource = self;
                    self->table.separatorColor = [UIColor clearColor];
                    self->table.backgroundColor = [References background];
                    UIRefreshControl *departmentsTableReload = [[UIRefreshControl alloc]init];
                    [self->table addSubview:departmentsTableReload];
                    [departmentsTableReload addTarget:self action:@selector(reloadDepartments:) forControlEvents:UIControlEventValueChanged];
                    [self->ctrl addSubview:table];
                }
                [self->table reloadData];
                NSLog(@"Storing offline departments...");
                [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:@"departmentDatabases"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                complete(YES);
            } else {
                complete(YES);
            }
        }];
    } @catch (NSException *exception) {
        [self signOut];
    }
    
//    }
}

-(void)populateConnectionswithCompletion:(void(^)(BOOL finished, NSMutableArray* courses))complete {
    @try {
        [[[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"sessions"] queryOrderedByChild:@"start"] queryStartingAtValue:[NSNumber numberWithInt:1554102000]] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSMutableArray *pastCourses = [[NSMutableArray alloc] init];
            if (snapshot.exists) {
                //            NSLog(@"%@",snapshot.value);
                NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
                [components setHour: 0];
                [components setMinute: 0];
                [components setSecond: 0];
                double timeFrame = [[[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components] timeIntervalSince1970];
                self->sessions = nil;
                self->connections = nil;
                self->pending = nil;
                self->completed = nil;
                self->completed = [[NSMutableArray alloc] init];
                self->sessions = [[NSMutableArray alloc] init];
                self->pending = [[NSMutableArray alloc] init];
                self->connections = [[NSMutableDictionary alloc] init];
                for (NSInteger a = [[(NSDictionary*)snapshot.value allKeys] count]-1; a >= 0; a--) {
                    NSDictionary *dict = [snapshot.value objectForKey:[[(NSDictionary*)snapshot.value allKeys] objectAtIndex:a]];
                    NSArray *people = [@[self->uid,[dict valueForKey:@"other"]] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
                    NSString *connectionID = [NSString stringWithFormat:@"%@:%@",people[0],people[1]];
                    NSDate *start = [NSDate dateWithTimeIntervalSince1970:[(NSNumber*)[dict valueForKey:@"start"] doubleValue]];
                    NSDate *end = [NSDate dateWithTimeIntervalSince1970:[(NSNumber*)[dict valueForKey:@"end"] doubleValue]];
                    NSString *course;
                    if ((NSString*)[dict valueForKey:@"course"]) {
                        course = (NSString*)[dict valueForKey:@"course"];
                        [pastCourses addObject:course];
                    } else {
                        course = @"Not Provided";
                    }
                    NSNumber *status;
                    if ((NSNumber*)[dict valueForKey:@"status"]) {
                        status = (NSNumber*)[dict valueForKey:@"status"];
                    } else {
                        status = @1;
                    }
                    NSNumber *student;
                    if ((NSNumber*)[dict valueForKey:@"student"]) {
                        student = (NSNumber*)[dict valueForKey:@"student"];
                    } else {
                        student = @0;
                    }
                    NSDictionary *session = @{@"id":[[(NSDictionary*)snapshot.value allKeys] objectAtIndex:a],@"connection":connectionID,@"start":start,@"end":end,@"course":course,@"other":[dict valueForKey:@"other"],@"student":student,@"status":status,@"cellID": [NSString stringWithFormat:@"%@%@",status,[[(NSDictionary*)snapshot.value allKeys] objectAtIndex:a]]};
                    if ([(NSNumber*)[dict valueForKey:@"start"] doubleValue] > timeFrame) {
                        if ([status isEqualToNumber:@0]) {
                            [self->pending addObject:session];
                        } else {
                            [self->sessions addObject:session];
                        }
                    } else if (![dict valueForKey:@"rated"]) {
                        [self->completed addObject:session];
                    }
                    if ([connections objectForKey:connectionID]) {
                        [(connectionObject*)[self->connections objectForKey:connectionID] addSession:session];
                    } else {
                        connectionObject *connection = [[connectionObject alloc] initWithID:connectionID];
                        [connection addSession:session];
                        [connections setObject:connection forKey:connectionID];
                    }
                }
                if (pending.count > 0) {
                    [me setBadgeValue:[NSString stringWithFormat:@"%li",pending.count]];
                } else {
                    [me setBadgeValue:nil];
                }
                [self sortSessions];
                complete(YES, pastCourses);
            } else {
                [self sortSessions];
                complete(YES, pastCourses);
            }
        }];
    } @catch (NSException *exception) {
        [self signOut];
    }
    
}

-(void)getAllCoursesonComplete:(void(^)(BOOL finished))complete {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[[NSUserDefaults standardUserDefaults] objectForKey:@"currentDomain"]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        self->searchCourseNames = [[NSMutableArray alloc] init];
        for (id key in snapshot.value) {\
            for (id number in snapshot.value[key]) {
                [self->searchCourseNames addObject:[NSString stringWithFormat:@"%@ %@",key,number]];
            }
        }
        complete(true);
    }];
}

-(void)smartHomePage:(NSMutableArray*)pastCourses onComplete:(void(^)(BOOL finished))complete {
    
    if (!smartSearch) {
        smartSearch = [[UITextField alloc] initWithFrame:CGRectMake(15, [References topMargin]+15, [References screenWidth]-30, 44)];
        [smartSearch addTarget:self action:@selector(smartSearch:) forControlEvents:UIControlEventEditingChanged];
        
        [References cornerRadius:smartSearch radius:5];
        [smartSearch setTextColor:[References primaryText]];
        [smartSearch setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightBold]];
        [smartSearch setLeftView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, smartSearch.frame.size.height)]];
        smartSearchSuggestion.font = smartSearch.font;
        
        [smartSearch setLeftViewMode:UITextFieldViewModeAlways];
        smartSearch.delegate = self;
        [self->ctrl addSubview:smartSearch];
        smartSearchSuggestion = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, smartSearch.frame.size.width-10, smartSearch.frame.size.height)];
        smartSearchSuggestion.font = smartSearch.font;
        smartSearchSuggestion.textColor = [References secondaryText];
        smartSearchSuggestion.text = @"Find Your Course";
        [smartSearch addSubview:smartSearchSuggestion];
    }
    [smartSearch setBackgroundColor:[References cells]];
//    if (!self->table) {
//        self->table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight]) style:UITableViewStyleGrouped];
//        self->table.contentInset = UIEdgeInsetsMake(0, 0, tabbar.frame.size.height+[References bottomMargin]+60, 0);
//        self->table.clipsToBounds = YES;
//        self->table.delegate = self;
//        self->table.dataSource = self;
//        self->table.separatorColor = [UIColor clearColor];
//        self->table.backgroundColor = [References background];
//        UIRefreshControl *departmentsTableReload = [[UIRefreshControl alloc]init];
//        //                UIImageView *loadimage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//        //                loadimage.image = [UIImage imageNamed:@"icon.png"];
//        //                [departmentsTableReload addSubview:loadimage];
//        [self->table addSubview:departmentsTableReload];
//        [departmentsTableReload addTarget:self action:@selector(reloadDepartments:) forControlEvents:UIControlEventValueChanged];
//        [self->ctrl addSubview:table];
//    }
//    
//    [self->table reloadData];
    complete(YES);
}

-(void)smartSearch:(UITextField*)textfield {
    for (NSString* course in searchCourseNames) {
        if ([course containsString:textfield.text]) {
            smartSearchSuggestion.text = course;
            break;
        }
    }
    
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if ([textField isEqual:smartSearch]) {
        textField.text = @"";
        smartSearchSuggestion.text = @"Find Your Course";
    }
    return true;
}

-(NSString*)findMatch:(NSString*)current {
    for (NSString* course in searchCourseNames) {
        if ([course containsString:current]) {
            return course;
        }
    }
    return @"";
}

-(BOOL)textFieldShouldReturn:(textFieldWithURL *)textField {
    if ([textField isEqual:smartSearch]) {
        if (textField.text.length == 0) {
            smartSearchSuggestion.text = @"Find Your Course";
        } else {
            textField.text = smartSearchSuggestion.text;
        }
    } else {
        if ([[textField getURL] isEqualToString:@"PPH"]) {
            [self updateValueAt:[textField getURL] withValue:[NSNumber numberWithInt:[textField.text stringByReplacingOccurrencesOfString:@"$" withString:@""].intValue]];
        } else if ([[textField getURL] isEqualToString:@"MAX"]) {
            [self updateValueAt:[textField getURL] withValue:[NSNumber numberWithInt:textField.text.intValue]];
        } else if ([[textField getURL] isEqualToString:@"TUTORCODE"]) {
            [self verifyCode:textField.text];
            textField.text = @"";
            //        [self updateValueAt:[textField getURL] withValue:[NSNumber numberWithInt:textField.text.intValue]];
        } else {
            [self updateValueAt:[textField getURL] withValue:textField.text];
        }
    }
    
    [textField resignFirstResponder];
    return true;
}

-(void)sortSessions {
    NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"start"
                                                     ascending:YES];
        sessions = [[NSMutableArray alloc] initWithArray:[sessions sortedArrayUsingDescriptors:@[sortDescriptor]]];
    [connectionsTable reloadData];
}
//
//-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
//    connectionsLoaded = connectionsLoaded + 1;
//    if (connectionsLoaded >= connections.count) {
//        upcomingSessions = [[NSMutableArray alloc] init];
//        for (connectionReference* connect in connections) {
//            [upcomingSessions addObjectsFromArray:[connect upcomingSessions]];
//        }
//        connectionsLoaded = 0;
//        [object removeObserver:self forKeyPath:@"isLoaded"];
//    }
//    NSLog(@"upcomig: %@",upcomingSessions);
//    NSSortDescriptor *sortDescriptor;
//    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"time"
//                                                 ascending:YES];
//    upcomingSessions = [[NSMutableArray alloc] initWithArray:[upcomingSessions sortedArrayUsingDescriptors:@[sortDescriptor]]];
//    NSSortDescriptor *sortDescriptorD;
//    sortDescriptorD = [[NSSortDescriptor alloc] initWithKey:@"mostRecentSession"
//                                                 ascending:NO];
//    connections = [[NSMutableArray alloc] initWithArray:[connections sortedArrayUsingDescriptors:@[sortDescriptorD]]];
//    [connectionsTable reloadData];
//}

-(void)populateAccountDatabase {
    @try {
        FIRDatabaseReference *reference = [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid];
        [reference observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if ([snapshot hasChildren]) {
                self->myName = snapshot.value[@"name"];
                if (!snapshot.value[@"agreed"]) {
                    needAgreement = YES;
                }
                [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:@"accountDatabase"];
                NSNumber *adminV = [snapshot.value valueForKey:@"admin"];
                NSNumber *tutorV = [snapshot.value valueForKey:@"tutor"];
                if (tutorV.integerValue == 1) {
                    tutor = YES;
                } else {
                    tutor = NO;
                }
                if (adminV.integerValue == 1) {
                    admin = YES;
                } else {
                    admin = NO;
                }
                [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:@"accountDatabase"];
                
                
            }
        }];
    } @catch (NSException *exception) {
        [self signOut];
    }
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == connectionsCollection) {
        return connections.count;
    }else if (collectionView == bestTutorsCollection) {
        return tutors.count;
    } else {
        return 0;
    }
    
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//    if (collectionView == myCoursesCollection) {
//        return UIEdgeInsetsMake(0, 0, 0, 0);
//    } else {
//        return UIEdgeInsetsMake(0, 0, 0, 0);
//    }
//
//}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (collectionView == bestTutorsCollection) {
        NSString *ttr = tutors[indexPath.row];
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[NSString stringWithFormat:@"%@",ttr] forIndexPath:indexPath];
        if (cell.tag != 1) {
            cell.backgroundColor = [UIColor clearColor];
            UIImageView *profile = [[UIImageView alloc] initWithFrame:CGRectMake(15, 2, 40, 40)];
            profile.layer.borderColor = [UIColor lightGrayColor].CGColor;
            profile.layer.borderWidth = 1;
            [References cornerRadius:profile radius:profile.frame.size.width/2];
            [cell addSubview:profile];
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(65, 10, cell.frame.size.width-70, 25)];
            name.textColor = [References primaryText];
            NSDictionary *offline = [[NSUserDefaults standardUserDefaults] objectForKey:ttr];
            if (offline) {
                name.text = offline[@"name"];
                 [profile sd_setImageWithURL:[NSURL URLWithString:offline[@"profileURL"]]];
            } else {
                name.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
                [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:ttr] child:@"name"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    name.text = snapshot.value;
                }];
                [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:ttr] child:@"profileURL"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    [profile sd_setImageWithURL:[NSURL URLWithString:snapshot.value]];
                }];
            }
            
            [cell addSubview:name];
//                [References createLinewithWidth:cell xPos:15 yPos:cell.frame.size.height-1 inFront:YES andWidth:cell.frame.size.width-30];
            
            cell.clipsToBounds = YES;
            cell.tag = 1;
        }
        ((UILabel*)cell.subviews[2]).textColor = [References primaryText];
        return cell;
    }
        return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == bestTutorsCollection) {
        tutorContainerController *tvc = [[tutorContainerController alloc] init];
        tvc.tutorID = tutors[indexPath.item];
        [self.navigationController pushViewController:tvc animated:YES];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == connectionsTable) {
        NSInteger count = 2;
        if (completed.count > 0) {
            count++;
        }
        if (pending.count > 0) {
            count++;
        }
        return count;
    } else if (tableView == more) {
        if (morebuttons) {
            return morebuttons.count+1;
        } else {
            return 1;
        }
        
    } else {
        NSLog(@"%@",[References getSchoolDomain]);
        if (![[References getSchoolDomain] isEqualToString:@"uoregon"]) {
            return 2;
        } else {
            return 3;
        }
    }
    
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == connectionsTable) {
        if (connectionsTable.numberOfSections == 4) {
            if (section == 0) {
                return completed.count;
            } else if (section == 1) {
                return pending.count;
            } else if (section == 2) {
                return sessions.count > 0 ? sessions.count : 1;
            } else {
                return connections.count;
            }
        } else if (connectionsTable.numberOfSections == 3) {
            if (section == 0) {
                return completed.count > 0 ? completed.count : pending.count;
            } else if (section == 1) {
                return sessions.count > 0 ? sessions.count : 1;
            } else {
                return connections.count;
            }
        } else {
            if (section == 0) {
                return sessions.count > 0 ? sessions.count : 1;
            } else {
                return connections.count;
            }
        }
    } else if (tableView == more) {
        if (section == 0) {
            return 0;
        } else if (section > 0) {
            return [(NSArray*)[(NSDictionary*)morebuttons[section-1] objectForKey:@"cells"] count];
        } else {
            return 0;
        }
    } else {
        if (section == 0) {
            return 1;
        } else if (section == 1) {
            return [departmentDatabases count];
        } else if (section == 2) {
            return 1;
        }
        return 0;
    }
}

-(void)freeConnections {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        [self removeMessages:[snapshot.value allKeys] :0];
    }];
}

-(void)removeMessages:(NSArray*)connections :(int)index{
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:connections[index]] child:@"messages"] setValue:[NSNull null] withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (index < connections.count-1) {
            [self removeMessages:connections :index+1];
        }
    }];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == connectionsTable) {
        if (indexPath.section == connectionsTable.numberOfSections-1) {
            connectionObject *connection = [connections objectForKey:[[connections allKeys] objectAtIndex:indexPath.row]];
            connectionViewController *cv = [[connectionViewController alloc] init];
            cv.connection = connection;
            [self.navigationController pushViewController:cv animated:YES];
        } else if (indexPath.section == connectionsTable.numberOfSections-2) {
            NSDictionary *booking = sessions[indexPath.row];
            UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"More" message:[NSString stringWithFormat:@"Invoice %@",booking[@"id"]] preferredStyle:UIAlertControllerStyleAlert];
            if ([[booking objectForKey:@"student"] isEqualToNumber:@0]) {
                [controller addAction:[UIAlertAction actionWithTitle:@"Cancel Session" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self cancelSession:booking];
                }]];
            } else {
                controller.message = @"Only the tutor is able to modify this session.";
            }
            [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:controller animated:YES completion:nil];
        } else {
            if (((connectionsTable.numberOfSections == 4) && (indexPath.section == 1)) || ((pending.count > 0) && (indexPath.section == 0))) {
                NSDictionary *booking = pending[indexPath.row];
                UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"More" message:[NSString stringWithFormat:@"Invoice %@",booking[@"id"]] preferredStyle:UIAlertControllerStyleAlert];
                if ([[booking objectForKey:@"student"] isEqualToNumber:@0]) {
                    [controller addAction:[UIAlertAction actionWithTitle:@"Confirm Session" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self confirmSession:booking];
                    }]];
                    [controller addAction:[UIAlertAction actionWithTitle:@"Void Session" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                        [self voidSession:booking];
                    }]];
                } else {
                    controller.message = @"Until the tutor responds to your request this session will be marked as pending.";
                }
                [controller addAction:[UIAlertAction actionWithTitle:@"Reschedule Session" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [References toastMessage:@"This feature is not available yet" andView:self andClose:NO];
                    //                swapViewController *svc = [[swapViewController alloc] init];
                    //                svc.bookingDetails = self->upcomingSessions[indexPath.row];
                    //                [self.navigationController pushViewController:svc animated:YES];
                    
                }]];
                [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
                [self presentViewController:controller animated:YES completion:nil];
            }
        }
//        if (pending.count > 0) {
//            if ((indexPath.section == 0)) {
//
//            } else if (indexPath.section == 1) {
//
//            } else {
//                connectionObject *connection = [connections objectForKey:[[connections allKeys] objectAtIndex:indexPath.row]];
//                connectionViewController *cv = [[connectionViewController alloc] init];
//                cv.connection = connection;
//                [self.navigationController pushViewController:cv animated:YES];
//            }
//        } else {
//            if ((indexPath.section == 0)) {
//                NSDictionary *booking = sessions[indexPath.row];
//                UIAlertController *controller = [UIAlertController alertControllerWithTitle:@"More" message:[NSString stringWithFormat:@"Invoice %@",booking[@"id"]] preferredStyle:UIAlertControllerStyleAlert];
//
//                [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
//                [self presentViewController:controller animated:YES completion:nil];
//            } else if (indexPath.section == 1) {
//
//            }
//        }
    } else if (tableView == table && indexPath.section == 1) {
        if (indexPath == selectedIndex) {
            selectedIndex = nil;
            departmentReference *database = departmentDatabases[indexPath.row];
            [database collapseCell];
            [tableView beginUpdates];
            [tableView endUpdates];
        } else if (selectedIndex) {
            departmentReference *databaseOld = departmentDatabases[selectedIndex.row];
            departmentReference *databaseNew = departmentDatabases[indexPath.row];
            [databaseOld collapseCell];
            [databaseNew expandCell];
            selectedIndex = indexPath;
            [tableView beginUpdates];
            [tableView endUpdates];
            
        } else {
            departmentReference *database = departmentDatabases[indexPath.row];
            [database expandCell];
            selectedIndex = indexPath;
            [tableView beginUpdates];
            [tableView endUpdates];
            
        }
//        [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:true];
    } else if (tableView == more) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if ([cell.textLabel.text isEqualToString:@"Sign Out"]) {
            [self signOut];
        } else if ([cell.textLabel.text isEqualToString:@"Read Terms of Service"]) {
            [self openInfo];
        } else if ([cell.textLabel.text isEqualToString:@"Contact Support"]) {
            [self contactTutorTree];
        } else if ([cell.textLabel.text isEqualToString:@"Modify Availability"]) {
            [self openTutorPanel];
        }else if ([cell.textLabel.text isEqualToString:@"Transactions"]) {
            accountTransactionsViewController *atvc = [[accountTransactionsViewController alloc] init];
            [self presentViewController:atvc animated:YES completion:^{
                [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
            }];
        } else if ([cell.textLabel.text isEqualToString:@"Deposit"]) {
            [self shareDeposit];
        } else if ([cell.textLabel.text isEqualToString:@"Withdraw"]) {
            [self withdrawal];
        } else if ([cell.textLabel.text isEqualToString:@"All Withdrawals"]) {
            adminWithdrawalsViewController *atvc = [[adminWithdrawalsViewController alloc] init];
            [self presentViewController:atvc animated:YES completion:^{
                [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
            }];
        } else if ([cell.textLabel.text isEqualToString:@"All Transactions"]) {
            adminTransactionsViewController *atvc = [[adminTransactionsViewController alloc] init];
            [self presentViewController:atvc animated:YES completion:^{
                [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
            }];
        } else if ([cell.textLabel.text isEqualToString:@"All Users"]) {
            adminUsersViewController *atvc = [[adminUsersViewController alloc] init];
            [self presentViewController:atvc animated:YES completion:^{
                [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
            }];
        } else if ([cell.textLabel.text isEqualToString:@"Users With Balances"]) {
            adminBalancesViewController *atvc = [[adminBalancesViewController alloc] init];
            [self presentViewController:atvc animated:YES completion:^{
                [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
            }];
        } else if ([cell.textLabel.text isEqualToString:@"Send Tutor Code"]) {
            [self generateTutorCode];
        } else if ([cell.textLabel.text isEqualToString:@"Add Course"]) {
            [self addCourse];
        } else if ([cell.textLabel.text isEqualToString:@"Add Department"]) {
            [self addDepartment];
        } else if ([cell.textLabel.text isEqualToString:@"Update/View Your Bio"]) {
            [self setBio];
        } else if ([cell.textLabel.text isEqualToString:@"Update Profile Picture"]) {
            [self getPhoto];
        } else if ([cell.textLabel.text isEqualToString:@"Manual Session Creation"]) {
            manualBookingViewController *manual = [[manualBookingViewController alloc] init];
            [self presentViewController:manual animated:YES completion:^{
                [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
            }];
        }
        
        
        
//        if (indexPath.row == 0) {
//
//        } else if (indexPath.row == 1) {
//            [self openTransactions];
//        } else if (indexPath.row == 2) {
//
//        } else if (indexPath.row == 3) {
//
//        } else if (indexPath.row == 4) {
//
//        } else if (indexPath.row == 5) {
//            [self openAdmin];
//        } else if (indexPath.row == 6) {
//            [self transferSessions];
//        }
    }
}

-(void)createSession {
    __block NSString *studentEmail,*tutorEmail,*startTime,*endTime,*course;
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Start with the students email" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Student Email";
    }];
    [alert addAction:cancelAction];
    [alert addAction:[UIAlertAction actionWithTitle:@"Next" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        studentEmail = alert.textFields[0].text;
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Now the tutor email" message:nil preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"Tutor Email";
        }];
        [alert addAction:cancelAction];
        [alert addAction:[UIAlertAction actionWithTitle:@"Next" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            tutorEmail = alert.textFields[0].text;
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Start with the students email" message:nil preferredStyle:UIAlertControllerStyleAlert];
            UIDatePicker *startPicker = [[UIDatePicker alloc] init];
            [alert.view addSubview:startPicker];
            [alert addAction:cancelAction];
            [alert addAction:[UIAlertAction actionWithTitle:@"Next" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
             [self presentViewController:alert animated:true completion:nil];
        }]];
         [self presentViewController:alert animated:true completion:nil];
    }]];
    [self presentViewController:alert animated:true completion:nil];
}


-(void)getPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Profile Picture"
                                                                   message:@"Please find a good picture that will make it easy for others to recognize you."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* select = [UIAlertAction actionWithTitle:@"Select a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* take = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {}];
    
    [alert addAction:select];
    [alert addAction:take];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [profileImageView setImage:chosenImage];
    [self uploadChosenImage:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)uploadChosenImage:(UIImage*)image {
    __block UIView *uppingimage = [References loadingView:@"Uploading Image..."];
    [self.view addSubview:uppingimage];
    [UIView animateWithDuration:0.25 animations:^{
        uppingimage.alpha = 1;
    } completion:^(BOOL finished) {
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *storageRef = [storage reference];
        NSString *imgName = [References randomStringWithLength:16];
        FIRStorageReference *profileRep = [storageRef child:[NSString stringWithFormat:@"images/%@.jpg",imgName]];
        
        NSData *data = UIImageJPEGRepresentation(image, 1);
        [profileRep putData:data
                   metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error) {
                       if (error != nil) {
                           [References toastNotificationBottom:self.view andMessage:error.localizedDescription];
                       } else {
                           [profileRep downloadURLWithCompletion:^(NSURL * _Nullable URL, NSError * _Nullable error) {
                               [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"profileURL"] setValue:URL.absoluteString withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                                   if (!error) {
                                       [UIView animateWithDuration:0.25 animations:^{
                                           uppingimage.alpha = 0;
                                       } completion:^(BOOL finished) {
                                           if (finished) {
                                               [uppingimage removeFromSuperview];
                                               uppingimage = nil;
                                           }
                                       }];
                                   }
                               }];
                               
                           }];
                           
                           
                       }
                   }];
    }];
    
}


-(void)generateTutorCode {
    NSString *code = [[References randomStringWithLength:5] uppercaseString];
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"tutorCodes"] child:code] setValue:@0 withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:code message:@"Share this code with instructions to go to more -> become a tutor and enter the code and they will become a tutor." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Copy %@ to clipboard",code] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                pasteboard.string = code;
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:alert animated:true completion:nil];
        }
    }];
}


-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == connectionsTable) {
        if (indexPath.section == 1) {
            return 100;
        } else if (indexPath.section == 0) {
            if (sessions.count == 0) {
                return 20;
            } else {
                return 80;
            }
        }
        return 0;
    } else {
        if (indexPath.section == 0) {
            return 160;
        } else if (indexPath.section == 1) {
            if (indexPath == selectedIndex) {
                departmentReference *dept = departmentDatabases[indexPath.row];
                return [dept cellHeight];
            } else {
                return departmentHeight;
            }
        }  else {
            return 280;
        }
    }
}

-(void)confirmSession:(NSDictionary*)session {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] updateChildValues:@{[NSString stringWithFormat:@"%@/sessions/%@/status",self->uid,session[@"id"]]: @1,[NSString stringWithFormat:@"%@/sessions/%@/status",session[@"other"],session[@"id"]]: @1,} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            NSDateFormatter *dfmt,*tfmt;
            dfmt = [[NSDateFormatter alloc] init];
            dfmt.dateFormat = @"EEEE, MMMM d";
            tfmt = [[NSDateFormatter alloc] init];
            tfmt.dateFormat = @"h:mm a";
            [References sendNotification:session[@"other"] withSubject:@"Booking Confirmed" andMessage:[NSString stringWithFormat:@"%@ has confirmed your upcoming session on %@ at %@",self->myName,[dfmt stringFromDate:session[@"start"]],[tfmt stringFromDate:session[@"start"]]] forceEmail:TRUE andCompletion:^(bool result) {
                [self->pending removeObject:session];
                [self reloadConnections:nil];
            }];
            
        }
    }];
}

-(void)voidSession:(NSDictionary*)session {
    NSString *requestURL = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/refund/%@",[session[@"id"] lowercaseString]];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:requestURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        if (([[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] isEqualToString:@"Success"]) || ([[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding] isEqualToString:@"Not Found"])) {
            [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] updateChildValues:@{[NSString stringWithFormat:@"%@/sessions/%@",self->uid,session[@"id"]]:[NSNull null],[NSString stringWithFormat:@"%@/sessions/%@",session[@"other"],session[@"id"]]:[NSNull null],[NSString stringWithFormat:@"%@/income/%@",self->uid,session[@"id"]]:[NSNull null],[NSString stringWithFormat:@"%@/spending/%@",session[@"other"],session[@"id"]]:[NSNull null]} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    NSDateFormatter *dfmt,*tfmt;
                    dfmt = [[NSDateFormatter alloc] init];
                    dfmt.dateFormat = @"EEEE, MMMM d";
                    tfmt = [[NSDateFormatter alloc] init];
                    tfmt.dateFormat = @"h:mm a";
                    [References sendNotification:session[@"other"] withSubject:@"Booking Voided" andMessage:[NSString stringWithFormat:@"%@ is unable to meet for your upcoming session on %@ at %@. You have been refunded.",self->myName,[dfmt stringFromDate:session[@"start"]],[tfmt stringFromDate:session[@"start"]]] forceEmail:TRUE andCompletion:^(bool result) {
                        [self->pending removeObject:session];
                        [self reloadConnections:nil];
                    }];
                }
            }];
        } else {
            NSLog(@"%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        }
        //NSLog(@"apay: %@",_clientToken);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        //[References postError:error.slocalizedDescription];
    }];
}

-(void)cancelSession:(NSDictionary*)session {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] updateChildValues:@{[NSString stringWithFormat:@"%@/sessions/%@",self->uid,session[@"id"]]:[NSNull null],[NSString stringWithFormat:@"%@/sessions/%@",session[@"other"],session[@"id"]]:[NSNull null],[NSString stringWithFormat:@"%@/income/%@",self->uid,session[@"id"]]:[NSNull null],[NSString stringWithFormat:@"%@/spending/%@",session[@"other"],session[@"id"]]:[NSNull null]} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            NSDateFormatter *dfmt,*tfmt;
            dfmt = [[NSDateFormatter alloc] init];
            dfmt.dateFormat = @"EEEE, MMMM d";
            tfmt = [[NSDateFormatter alloc] init];
            tfmt.dateFormat = @"h:mm a";
            [References sendNotification:session[@"other"] withSubject:@"Booking Cancelled" andMessage:[NSString stringWithFormat:@"%@ is unable to meet for your upcoming session on %@ at %@. You have been refunded.",self->myName,[dfmt stringFromDate:session[@"start"]],[tfmt stringFromDate:session[@"start"]]] forceEmail:TRUE andCompletion:^(bool result) {
                [self->pending removeObject:session];
                [self reloadConnections:nil];
            }];
        }
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == table) {
        if (indexPath.section == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"FEATURED"]];
            cell.clipsToBounds = true;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"FEATURED"]];
                [cell addSubview:bestTutorsView];
            }
            cell.backgroundColor = [References cells];
            return cell;
        } else if (indexPath.section == 1) {
            departmentReference *database = departmentDatabases[indexPath.row];

            UITableViewCell *cell;
            cell = [tableView dequeueReusableCellWithIdentifier:[database name]];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[database name]];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell addSubview:[database cellView]];
                [database setParent:self];
                cell.clipsToBounds = NO;
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                cell.backgroundColor = [UIColor clearColor];
            }
            return cell;
        } else if (indexPath.section == 2) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@CAMPUS",[NSNumber numberWithBool:darkMode]]];

            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:[NSString stringWithFormat:@"%@CAMPUS",[NSNumber numberWithBool:darkMode]]];
                campusMap = [[campusView alloc] initWithParent:self];
                [campusMap setYCoordinate:15];
                [cell addSubview:[campusMap campusMap]];
                [campusMap zoomToLocation];
                cell.clipsToBounds = NO;
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                cell.backgroundColor = [UIColor clearColor];
            }

            return cell;

        }
        return nil;
    } else if (tableView == connectionsTable) {
        if (indexPath.section == connectionsTable.numberOfSections-1) {
            return [self connectionCell:indexPath];
        } else if (indexPath.section == connectionsTable.numberOfSections-2) {
            return [self upcomingCell:indexPath];
        } else if (connectionsTable.numberOfSections == 4) {
            return indexPath.section == 0 ? [self ratingCell:indexPath] : [self pendingCell:indexPath];
        } else {
            return completed.count > 0 ? [self ratingCell:indexPath] : [self pendingCell:indexPath];
        }
    } else if (tableView == more) {
        // TODO: ALL THESE
        NSDictionary *dict = morebuttons[indexPath.section-1];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@%@",[NSNumber numberWithBool:darkMode],dict[@"title"]]];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%@%@",[NSNumber numberWithBool:darkMode],dict[@"title"]]];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            if (indexPath.section > 0) {
                NSDictionary *cellDict = [(NSArray*)dict[@"cells"] objectAtIndex:indexPath.row];
                if ([cellDict[@"type"] isEqualToString:@"text"]) {
                    
                    textFieldWithURL *tf = [[textFieldWithURL alloc]initWithURL:cellDict[@"url"]];
                    tf.frame = CGRectMake(15, 5, [References screenWidth]-30, 43-10);
                    tf.font = [UIFont systemFontOfSize:17 weight:UIFontWeightRegular];
                    tf.borderStyle = UITextBorderStyleNone;
                    tf.tag = indexPath.section-1;
                    tf.delegate = self;
                    tf.textColor = [References primaryText];
                    tf.text = cellDict[@"value"];
                    if ([(NSString*)cellDict[@"url"] isEqualToString:@"email"] || [(NSString*)dict[@"title"] isEqualToString:@"name"]) {
                        tf.userInteractionEnabled = false;
                    }
                    [cell addSubview:tf];
                } else if ([cellDict[@"type"] isEqualToString:@"switch"]) {
                    cell.textLabel.text = cellDict[@"title"];
                    UISwitch *switchview = [[UISwitch alloc] initWithFrame:CGRectZero];
                    
                    if ([cellDict[@"url"] isEqualToString:@"dark"]) {
                        [switchview addTarget:self action:@selector(toggleDarkMode:) forControlEvents:UIControlEventValueChanged];
                        switchview.tag = 5;
                    } else {
                        switchview.tag = indexPath.section-1;
                        [switchview addTarget:self action:@selector(toggleSwitch:) forControlEvents:UIControlEventValueChanged];
                        [switchview setOn:[cellDict[@"value"] boolValue]];
                    }
                    switchview.onTintColor = [References accent];
                    cell.accessoryView = switchview;
                } else if ([cellDict[@"type"] isEqualToString:@"number"]) {
                    cell.textLabel.text = cellDict[@"title"];
                    textFieldWithURL *tf = [[textFieldWithURL alloc]initWithURL:cellDict[@"url"]];
                    tf.frame = CGRectMake([References screenWidth]-15-51, 6, 51, 43-12);
                    tf.font = [UIFont systemFontOfSize:15 weight:UIFontWeightBold];
                    tf.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.15];
                    [References cornerRadius:tf radius:5];
                    tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                    tf.textAlignment = NSTextAlignmentCenter;
                    tf.borderStyle = UITextBorderStyleNone;
                    tf.tag = indexPath.section-1;
                    tf.delegate = self;
                    if ([cellDict[@"url"] isEqualToString:@"PPH"]) {
                        tf.text = [NSString stringWithFormat:@"$%@",cellDict[@"value"]];
                    } else if ([cellDict[@"url"] isEqualToString:@"TUTORCODE"]) {
                        tf.text = @"";
                        tf.placeholder = @"•••••";
                        tf.frame = CGRectMake(tf.frame.origin.x-20, tf.frame.origin.y, tf.frame.size.width+20, tf.frame.size.height);
                    } else {
                        tf.text = [NSString stringWithFormat:@"%@",cellDict[@"value"]];
                    }
                    tf.textColor = [References primaryText];
                    [cell addSubview:tf];
                } else if ([cellDict[@"type"] isEqualToString:@"button"]) {
                    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
                    cell.textLabel.text = cellDict[@"title"];
                    UIView *bgColorView = [[UIView alloc] init];
                    bgColorView.backgroundColor = [References highlight];
                    [cell setSelectedBackgroundView:bgColorView];
                    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                    if ([cell.textLabel.text isEqualToString:@"Withdraw"] && currentBalance == 0) {
                        cell.textLabel.textColor = [UIColor lightGrayColor];
                        cell.accessoryType = UITableViewCellAccessoryNone;
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    }
                }
            }
            

        }
        if ([cell.accessoryView.class isEqual:UISwitch.class] && (cell.accessoryView.tag == 5)) {
            [(UISwitch*)cell.accessoryView setOn:darkMode];
        }
        cell.backgroundColor = [References cells];
        cell.textLabel.textColor = [References primaryText];
        return cell;
    } else {
        return nil;
    }
}

-(UITableViewCell*)ratingCell:(NSIndexPath*)indexPath {
    @try {
        NSDictionary *rating = completed[indexPath.row];
        UITableViewCell *cell;
        cell = [connectionsTable dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@rating:%@",[NSNumber numberWithBool:darkMode],rating[@"id"]]];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%@rating:%@",[NSNumber numberWithBool:darkMode],rating[@"id"]]];
            UIView *card = [[UIView alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-30, 250-20)];
            card.backgroundColor = [References cells];
            UIView *shadow = [[UIView alloc] initWithFrame:CGRectMake(card.frame.origin.x+5, card.frame.origin.y+5, card.frame.size.width-10, card.frame.size.height-10)];
            shadow.backgroundColor = [UIColor whiteColor];
            [References bottomshadow:shadow];
            [References cornerRadius:card radius:10];
            [cell addSubview:shadow];
            [cell addSubview:card];
            NSDictionary *offline = [[NSUserDefaults standardUserDefaults] objectForKey:rating[@"other"]];
            UILabel *tip = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, card.frame.size.width-40, 0)];
            tip.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
            tip.numberOfLines = 0;
            tip.textAlignment = NSTextAlignmentCenter;
            tip.textColor = [References primaryText];
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake((card.frame.size.width/2)-40, [References getBottomYofView:tip]+5, 80, 80)];
            [References cornerRadius:image radius:image.frame.size.width/2];
            image.layer.borderColor = [References colorFromHexString:@"#E0E0E0"].CGColor;
            image.layer.borderWidth = 3;
            HCSStarRatingView *stars = [[HCSStarRatingView alloc] init];
            stars.tag = 3;
            stars.backgroundColor = [UIColor clearColor];
            stars.emptyStarColor = [References highlight];
            stars.tintColor = [References secondaryText];
            stars.starBorderColor = [UIColor clearColor];
            [stars addTarget:self action:@selector(changeRating:) forControlEvents:UIControlEventValueChanged];
            UIButton *submitRating = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.frame.size.width, image.frame.size.height)];
            [References cornerRadius:submitRating radius:submitRating.frame.size.width/2];
            [submitRating addTarget:self action:@selector(leaveRating:) forControlEvents:UIControlEventTouchUpInside];
            [submitRating setBackgroundColor:[[References schoolMainColor] colorWithAlphaComponent:1]];
            submitRating.alpha = 0;
            submitRating.tag = 4;
            submitRating.layer.borderColor = [References colorFromHexString:@"#E0E0E0"].CGColor;
            submitRating.layer.borderWidth = 3;
            UIImageView *submitImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, submitRating.frame.size.width-20, submitRating.frame.size.height-20)];
            submitImage.image = [UIImage imageNamed:@"send.png"];
            submitImage.transform = CGAffineTransformMakeRotation(270 * M_PI/180);
            [submitRating addSubview:submitImage];
            if (offline) {
                tip.text = [NSString stringWithFormat:@"How was your session with %@?",[offline objectForKey:@"name"]];
                [image sd_setImageWithURL:[offline objectForKey:@"profileURL"]];
                tip.frame = CGRectMake(tip.frame.origin.x, tip.frame.origin.y, tip.frame.size.width, [References fixedWidthSizeWithFont:tip.font andString:tip.text andLabel:tip].size.height);
                image.frame = CGRectMake((card.frame.size.width/2)-40, [References getBottomYofView:tip]+10, 80, 80);
                stars.frame = CGRectMake(35, [References getBottomYofView:image]+10, card.frame.size.width-70, 40);
                submitRating.frame = image.frame;
            } else {
                [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:rating[@"other"]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    if (snapshot.exists) {
                        tip.text = [NSString stringWithFormat:@"How was your session with %@?",[snapshot.value objectForKey:@"name"]];
                        [image sd_setImageWithURL:[snapshot.value objectForKey:@"profileURL"]];
                        tip.frame = CGRectMake(tip.frame.origin.x, tip.frame.origin.y, tip.frame.size.width, [References fixedWidthSizeWithFont:tip.font andString:tip.text andLabel:tip].size.height);
                        image.frame = CGRectMake((card.frame.size.width/2)-40, [References getBottomYofView:tip]+10, 80, 80);
                        stars.frame = CGRectMake(35, [References getBottomYofView:image]+10, card.frame.size.width-70,40);
                        submitRating.frame = image.frame;
                    }
                }];
            }
            
            [card addSubview:image];
            [card addSubview:tip];
            [card addSubview:stars];
            [card addSubview:submitRating];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.clipsToBounds = NO;
            cell.backgroundColor = [UIColor clearColor];
            cell.tag = indexPath.row;
            card.tag = indexPath.row;
        }
        
        return cell;
    } @catch (NSException *exception) {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"error"];
        cell.textLabel.text = @"Something's not right.";
        return cell;
    }
    
}

-(void)changeRating:(HCSStarRatingView*)sender {
    if ([sender.superview viewWithTag:4].alpha != 1) {
        [UIView animateWithDuration:0.2 animations:^{
            [sender.superview viewWithTag:4].alpha = 1;
        }];
    }
}

-(void)leaveRating:(UIButton*)sender {
    NSInteger row = sender.superview.tag;
    CGFloat rating = [(HCSStarRatingView*)[sender.superview viewWithTag:3] value];
    NSLog(@"%f",rating);
    NSDictionary *session = completed[row];
    [[[FIRDatabase database] reference] updateChildValues:@{[NSString stringWithFormat:@"updateDatabase/users/%@/ratings/%@",session[@"other"],session[@"id"]]:[NSNumber numberWithFloat:rating],[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@/rated",self->uid,session[@"id"]]:[NSNumber numberWithBool:true]} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
//            [UIView animateWithDuration:0.15 animations:^{
//                self->connectionsTable.alpha = 0;
//            } completion:^(BOOL finished) {
//                if (finished) {
//                    [self->connectionsTable reloadData];
//                    [UIView animateWithDuration:0.15 animations:^{
//                        self->connectionsTable.alpha = 1;
//                    } completion:^(BOOL finished) {
//
//                    }];
//                }
//            }];
        }
    }];
}


-(UITableViewCell*)pendingCell:(NSIndexPath*)indexPath {
    NSDictionary *upcoming = pending[indexPath.row];
    UITableViewCell *cell;
    cell = [connectionsTable dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@pending:%@",[NSNumber numberWithBool:darkMode],upcoming[@"id"]]];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%@pending:%@",[NSNumber numberWithBool:darkMode],upcoming[@"id"]]];
        NSDateFormatter *dfmt = [[NSDateFormatter alloc] init];
        dfmt.dateFormat = @"EEEE";
        NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
        tfmt.dateFormat = @"h:mm a";
        UIView *smallView = [[UIView alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-30, 80)];
        smallView.backgroundColor = [References cells];
        [References cornerRadius:smallView radius:5];
        UILabel *course = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, 0, 20)];
        course.backgroundColor = [References colorFromHexString:@"#FF1744"];
        [References cornerRadius:course radius:5];
        course.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
        course.text = @"PENDING";
        course.textColor = [UIColor whiteColor];
        course.textAlignment = NSTextAlignmentCenter;
        course.frame = CGRectMake(10, course.frame.origin.y, [References fixedHeightSizeWithFont:course.font andString:course.text andLabel:course].size.width+15, 20);
        [smallView addSubview:course];
        UILabel *other = [[UILabel alloc] initWithFrame:CGRectMake(course.frame.origin.x, 5, smallView.frame.size.width-course.frame.origin.x, 20)];
        other.font = course.font;
        other.textColor = [References primaryText];
        [smallView addSubview:other];
        NSDictionary *offline = [[NSUserDefaults standardUserDefaults] objectForKey:upcoming[@"other"]];
        if (offline) {
            other.text = [offline objectForKey:@"name"];
        } else {
            [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:upcoming[@"other"]] child:@"name"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if (snapshot.exists) {
                    other.text = snapshot.value;
                }
            }];
        }

        UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(course.frame.origin.x, [References getBottomYofView:course]+5, smallView.frame.size.width-course.frame.origin.x, 20)];
        time.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
        time.text = [NSString stringWithFormat:@"%@ at %@ until %@",[dfmt stringFromDate:upcoming[@"start"]],[tfmt stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:round([(NSDate*)upcoming[@"start"] timeIntervalSinceReferenceDate]/300.0)*300.0]],[tfmt stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:round([(NSDate*)upcoming[@"end"] timeIntervalSinceReferenceDate]/300.0)*300.0]]];
        time.textColor = [References primaryText];
        [smallView addSubview:time];
        
        UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(smallView.frame.origin.x+5, smallView.frame.origin.y+5, smallView.frame.size.width-10, smallView.frame.size.height-10)];
        shadow.backgroundColor = [UIColor whiteColor];
        [References bottomshadow:shadow];
        [cell addSubview:shadow];
        [cell addSubview:smallView];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.clipsToBounds = NO;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(UITableViewCell*)upcomingCell:(NSIndexPath*)indexPath {
    UITableViewCell *cell;
    if ([sessions count] == 0) {
        
        cell = [connectionsTable dequeueReusableCellWithIdentifier:@"NONE"];
        
        if (cell.tag == 0)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:@"NONE"];
            UILabel *none = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, cell.frame.size.height)];
            none.text = @"No Upcoming Sessions";
            none.textAlignment = NSTextAlignmentCenter;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            none.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
            none.textColor = [UIColor lightGrayColor];
            [cell addSubview:none];
            cell.clipsToBounds = NO;
            cell.backgroundColor = [UIColor clearColor];
            cell.tag = 1;
        }
        return cell;
    } else {
        NSDictionary *upcoming = sessions[indexPath.row];
        UITableViewCell *cell;
        cell = [connectionsTable dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%@upcoming:%@",[NSNumber numberWithBool:darkMode],upcoming[@"id"]]];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[NSString stringWithFormat:@"%@upcoming:%@",[NSNumber numberWithBool:darkMode],upcoming[@"id"]]];
            NSDateFormatter *dfmt = [[NSDateFormatter alloc] init];
            dfmt.dateFormat = @"EEEE";
            NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
            tfmt.dateFormat = @"h:mm a";
            UIView *smallView = [[UIView alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-30, 80)];
            smallView.backgroundColor = [References cells];
            [References cornerRadius:smallView radius:5];
            UILabel *course = [[UILabel alloc] initWithFrame:CGRectMake(10, 30, 0, 20)];
            course.backgroundColor = [References accent];
            [References cornerRadius:course radius:5];
            course.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
            course.text = [upcoming[@"course"] uppercaseString];
            course.textColor = [UIColor whiteColor];
            course.textAlignment = NSTextAlignmentCenter;
            course.frame = CGRectMake(10, course.frame.origin.y, [References fixedHeightSizeWithFont:course.font andString:course.text andLabel:course].size.width+15, 20);
            [smallView addSubview:course];
            UILabel *other = [[UILabel alloc] initWithFrame:CGRectMake(course.frame.origin.x, 5, smallView.frame.size.width-course.frame.origin.x, 20)];
            other.font = course.font;
            other.textColor = [References primaryText];
            [smallView addSubview:other];
            NSDictionary *offline = [[NSUserDefaults standardUserDefaults] objectForKey:upcoming[@"other"]];
            if (offline) {
                other.text = [offline objectForKey:@"name"];
            } else {
                [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:upcoming[@"other"]] child:@"name"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                    if (snapshot.exists) {
                        other.text = snapshot.value;
                    }
                }];
            }
            
            UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(course.frame.origin.x, [References getBottomYofView:course]+5, smallView.frame.size.width-course.frame.origin.x, 20)];
            time.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
            time.text = [NSString stringWithFormat:@"%@ at %@ until %@",[dfmt stringFromDate:upcoming[@"start"]],[tfmt stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:round([(NSDate*)upcoming[@"start"] timeIntervalSinceReferenceDate]/300.0)*300.0]],[tfmt stringFromDate:[NSDate dateWithTimeIntervalSinceReferenceDate:round([(NSDate*)upcoming[@"end"] timeIntervalSinceReferenceDate]/300.0)*300.0]]];
            time.textColor = [References primaryText];
            [smallView addSubview:time];
            
            UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(smallView.frame.origin.x+5, smallView.frame.origin.y+5, smallView.frame.size.width-10, smallView.frame.size.height-10)];
            shadow.backgroundColor = [UIColor whiteColor];
            [References bottomshadow:shadow];
            [cell addSubview:shadow];
            [cell addSubview:smallView];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.clipsToBounds = NO;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
}

-(UITableViewCell*)connectionCell:(NSIndexPath*)indexPath {
    NSString *connectionID = [[connections allKeys] objectAtIndex:indexPath.row];
    connectionObject *connection = [connections objectForKey:connectionID];
    UITableViewCell *cell;
    cell = [connectionsTable dequeueReusableCellWithIdentifier:connectionID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell.tag == 0)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:connectionID];
        [cell addSubview:[connection getCellView]];
        cell.clipsToBounds = NO;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.backgroundColor = [UIColor clearColor];
        cell.tag = 1;
    }
    [cell setTintColor:[References secondaryText]];
    [connection updateColors];
    return cell;
}

-(void)fillName {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Something's Missing..." message:@"Please re-enter your name to continue" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (alert.textFields[0].text.length > 0) {
            [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"name"] setValue:alert.textFields[0].text withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    [References fullScreenToast:@"Thank you!" inView:self withSuccess:TRUE andClose:NO];
                }
            }];
        } else {
            [self fillName];
        }
        
    }]];
    [self presentViewController:alert animated:TRUE completion:nil];
}

-(void)fillPhone {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Something's Missing..." message:@"Please re-enter your name to continue" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (alert.textFields[0].text.length > 0) {
            [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] updateChildValues:@{@"phone":alert.textFields[0].text,@"smsNotifications":[NSNumber numberWithBool:true]} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    [References fullScreenToast:@"Thank you!" inView:self withSuccess:TRUE andClose:NO];
                }
            }];
        } else {
            [self fillPhone];
        }
        
    }]];
    [self presentViewController:alert animated:TRUE completion:nil];
}


-(void)fillEmail {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Something's Missing..." message:@"Please re-enter your email address to continue" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Continue" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (alert.textFields[0].text.length > 0) {
            [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] updateChildValues:@{@"email":alert.textFields[0].text,@"emailNotifications":[NSNumber numberWithBool:true]} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    [References fullScreenToast:@"Thank you!" inView:self withSuccess:TRUE andClose:NO];
                }
            }];
        } else {
            [self fillEmail];
        }
        
    }]];
    [self presentViewController:alert animated:TRUE completion:nil];
}

-(void)resetImage {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"profileURL"] setValue:@"https://s3.amazonaws.com/spoonflower/public/design_thumbnails/0213/1542/4solidlightgrey_shop_preview.png" withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
        }
    }];
}

-(void)moreView {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            if (!self->more) {
                self->more = [[UITableView alloc] initWithFrame:CGRectMake([References screenWidth]*2, 0, [References screenWidth], [References screenHeight]) style:UITableViewStyleGrouped];
                self->more.backgroundColor = [References background];
                self->more.separatorStyle = UITableViewCellSeparatorStyleNone;
                [self->more setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
                self->more.delegate = self;
                self->more.dataSource = self;
                self->more.contentInset = UIEdgeInsetsMake([References topMargin], 0, self->tabbar.frame.size.height+35+[References bottomMargin]+115, 0);
//                self->more.scrollIndicatorInsets = self->more.contentInset;
                
                [self->ctrl addSubview:self->more];
                self->morebuttons = [[NSMutableArray alloc] init];
            }
            [self->morebuttons removeAllObjects];
            @try {
                [self->morebuttons addObject:@{@"title":@"how people see you",@"cells":@[@{@"value":snapshot.value[@"name"],@"type":@"text",@"url":@"name"},@{@"title":@"Update Profile Picture",@"value":@1,@"type":@"button"}]}];
            } @catch (NSException *exception) {
                [self fillName];
            }
            @try {
                [self->morebuttons addObject:@{@"title":@"push notifciations",@"cells":@[@{@"title":@"Push Notifications",@"value":[NSNumber numberWithBool:UIApplication.sharedApplication.isRegisteredForRemoteNotifications],@"type":@"switch",@"url":@"push notifications"}]}];
            } @catch (NSException *exception) {
                
            }
            @try {
                [self->morebuttons addObject:@{@"title":@"email",@"cells":@[@{@"value":snapshot.value[@"email"],@"type":@"text",@"url":@"email"},@{@"title":@"Email Notifications",@"value":snapshot.value[@"emailNotifications"],@"type":@"switch",@"url":@"emailNotifications"}]}];
            } @catch (NSException *exception) {
                [self fillEmail];
            }
            @try {
                [self->morebuttons addObject:@{@"title":@"phone",@"cells":@[@{@"value":snapshot.value[@"phone"],@"type":@"text",@"url":@"phone"},@{@"title":@"SMS Notifications",@"value":snapshot.value[@"smsNotifications"],@"type":@"switch",@"url":@"smsNotifications"}]}];
            } @catch (NSException *exception) {
                [self fillPhone];
            }
            self->currentBalance = [self calculateBalance:snapshot.value[@"income"] andSpending:snapshot.value[@"spending"]];
            [self->morebuttons addObject:@{@"title":[NSString stringWithFormat:@"current balance: $%.2f",self->currentBalance],@"cells":@[@{@"title":@"Transactions",@"value":@1,@"type":@"button"},@{@"title":@"Deposit",@"value":@1,@"type":@"button"},@{@"title":@"Withdraw",@"value":@1,@"type":@"button"}]}];
            if (snapshot.value[@"tutor"]) {
                self->tutor = YES;
                if (snapshot.value[@"MAX"]) {
                    [self->morebuttons addObject:@{@"title":@"tutoring",@"cells":@[@{@"title":@"Price Per Half Hour",@"value":snapshot.value[@"PPH"],@"type":@"number",@"url":@"PPH"},@{@"title":@"Maximum Hours Per Week",@"value":snapshot.value[@"MAX"],@"type":@"number",@"url":@"MAX"},@{@"title":@"Modify Availability",@"value":@1,@"type":@"button"},@{@"title":@"Update/View Your Bio",@"value":@1,@"type":@"button"}]}];
                } else {
                    [self->morebuttons addObject:@{@"title":@"tutoring",@"cells":@[@{@"title":@"Price Per Half Hour",@"value":snapshot.value[@"PPH"],@"type":@"number",@"url":@"PPH"},@{@"title":@"Maximum Hours Per Week",@"value":@0,@"type":@"number",@"url":@"MAX"},@{@"title":@"Modify Availability",@"value":@1,@"type":@"button"},@{@"title":@"Update/View Your Bio",@"value":@1,@"type":@"button"}]}];
                }
                self->bioView = [[UITextView alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 250) textContainer:nil];
                self->bioView.text = snapshot.value[@"bio"] ? snapshot.value[@"bio"] : @"";
            } else {
                self->tutor = NO;
                [self->morebuttons addObject:@{@"title":@"become a tutor",@"cells":@[@{@"title":@"Referral Code",@"value":@"00000",@"type":@"number",@"url":@"TUTORCODE"}]}];
            }
            [self->morebuttons addObject:@{@"title":@"more",@"cells":@[@{@"title":@"Sign Out",@"value":@1,@"type":@"button"},@{@"title":@"Read Terms of Service",@"value":@1,@"type":@"button"},@{@"title":@"Contact Support",@"value":@1,@"type":@"button"},@{@"title":@"Dark Mode",@"type":@"switch",@"url":@"dark"}]}];
            @try {
                self->myName = snapshot.value[@"name"];
            } @catch (NSException *exception) {
                NSLog(@"Missing Name");
            }
            
            [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:@"accountDatabase"];
            NSNumber *adminV = [snapshot.value valueForKey:@"admin"];
            if (adminV.integerValue == 1) {
                admin = YES;
                [self->morebuttons addObject:@{@"title":@"admin",@"cells":@[@{@"title":@"All Transactions",@"value":@1,@"type":@"button"},@{@"title":@"All Withdrawals",@"value":@1,@"type":@"button"},@{@"title":@"All Users",@"value":@1,@"type":@"button"},@{@"title":@"Users With Balances",@"value":@1,@"type":@"button"},@{@"title":@"Send Tutor Code",@"value":@1,@"type":@"button"},@{@"title":@"Add Course",@"value":@1,@"type":@"button"},@{@"title":@"Add Department",@"value":@1,@"type":@"button"},@{@"title":@"Manual Session Creation",@"value":@1,@"type":@"button"}]}];
            } else {
                admin = NO;
            }
            self->profileImageView = [[UIImageView alloc] initWithFrame:CGRectMake([References screenWidth]-15-40, 5, 40, 40)];
            [References cornerRadius:self->profileImageView radius:self->profileImageView.frame.size.width/2];
            @try {
                [profileImageView sd_setImageWithURL:[NSURL URLWithString:snapshot.value[@"profileURL"]]];
            } @catch (NSException *exception) {
                [self fillEmail];
            }
            [self->more reloadData];
        } else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Uh oh..." message:@"Something isn't right with your account and you need to re-fill in your account details. Sorry for the inconvenience." preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[FIRAuth auth].currentUser deleteWithCompletion:^(NSError * _Nullable error) {
                    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
                    NSDictionary * dict = [defs dictionaryRepresentation];
                    for (id key in dict) {
                        if (![(NSString*)key isEqualToString:@"accountDatabase"] && ![(NSString*)key isEqualToString:@"clientToken"] && ![(NSString*)key isEqualToString:@"currentVersion"]) {
                            [defs removeObjectForKey:key];
                            
                        }
                        [defs synchronize];
                        
                    }
                    [self.navigationController pushViewController:[[nGettingStartedViewController alloc] init] animated:YES];
                }];
            }]];
            [self presentViewController:alert animated:true completion:nil];
        }
    }];
}


-(CGFloat)calculateBalance:(NSDictionary*)income andSpending:(NSDictionary*)spending {
    CGFloat currentBalance = 0;
    if (income) {
        for (id key in income) {
            NSDictionary *td = income[key];
            NSNumber *n = td[@"amount"];
            currentBalance+=n.floatValue;
            
        }
    }
    if (spending) {
        for (id key in spending) {
            NSDictionary *td = spending[key];
            NSNumber *n = td[@"amount"];
            if (![(NSString*)td[@"type"] isEqualToString:@"Apple Pay"] && ![(NSString*)td[@"type"] isEqualToString:@"Venmo"]) {
                currentBalance-=n.floatValue;
            } else if ([(NSNumber*)td[@"usedBalance"] floatValue] > 0) {
                currentBalance-=[(NSNumber*)td[@"usedBalance"] floatValue];
            }
        }
    }
    return currentBalance;
}


-(void)toggleSwitch:(UISwitch*)toggle {
    NSString *url = @"Error";
    for (NSDictionary *d in (NSArray*)[(NSDictionary*)morebuttons[toggle.tag] objectForKey:@"cells"]) {
        if ([d[@"type"] isEqualToString:@"switch"]) {
            url = d[@"url"];
            break;
        }
    }
    if ([url isEqualToString:@"push notifications"]) {
        if ([toggle isOn]) {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if (!error && granted) {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
                } else {
                    if (!granted) {
                        [toggle setOn:false animated:false];
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:nil completionHandler:nil];
                        }
                    }
                }];
        } else {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [UIApplication.sharedApplication unregisterForRemoteNotifications];
            [[[[[[[FIRDatabase database]reference]child:@"updateDatabase"] child:@"users"]child:self->uid] child:@"token"] removeValue];
        }
    } else {
        [self updateValueAt:url withValue:[NSNumber numberWithBool:toggle.isOn]];
    }
    
}



-(void)verifyCode:(NSString*)tutorCode {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.detailsLabel.text = @"Trying Code...";
    [References blurView:hud.backgroundView];
    hud.removeFromSuperViewOnHide = YES;
    hud.margin = 10.f;
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"tutorCodes"] child:tutorCode] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            if ([snapshot.value isEqual:@1]) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [hud hideAnimated:true afterDelay:0];
                    [References toastMessage:@"Already used" andView:self andClose:NO];
                });
            } else {
                [[[FIRDatabase database] reference] updateChildValues:@{[NSString stringWithFormat:@"updateDatabase/tutorCodes/%@",tutorCode]:@1,[NSString stringWithFormat:@"updateDatabase/users/%@/availability",self->uid]:@[@0,@0,@0,@0,@0,@0,@0],[NSString stringWithFormat:@"updateDatabase/users/%@/tutor",self->uid]:@1,[NSString stringWithFormat:@"updateDatabase/users/%@/PPH",self->uid]:@10,[NSString stringWithFormat:@"updateDatabase/users/%@/MAX",self->uid]:@40} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                    if (!error) {
                        dispatch_async(dispatch_get_main_queue(), ^(){
                            [hud hideAnimated:true afterDelay:0];
                            [References toastMessage:@"Success" andView:self andClose:NO];
                        });
                    }
                }];
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(){
                [hud hideAnimated:true afterDelay:0];
                [References toastMessage:@"Invalid code" andView:self andClose:NO];
            });
        }
    }];
}

-(void)updateValueAt:(NSString*)url withValue:(id)value {
    __block UIView *uppingimage = [References loadingView:@"Updating..."];
    [self.view addSubview:uppingimage];
    [UIView animateWithDuration:0.25 animations:^{
        uppingimage.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] updateChildValues:@{url:value} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    [UIView animateWithDuration:0.25 animations:^{
                        uppingimage.alpha = 0;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [uppingimage removeFromSuperview];
                        }
                    }];
                }
            }];
        }
        }];
    
}

-(void)openCourse:(UIButton*)sender {
//    courseDatabase *cd = [sender.layer valueForKey:@"course"];
//    scheduleViewController *scd =
//    [self openCourseDatabase:cd asMine:[NSNumber numberWithBool:tutor]];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == connectionsTable) {
         if (indexPath.section == connectionsTable.numberOfSections-2) {
            return (pending.count > 0 || sessions.count > 0) ? 100 : 30;
         } else {
             if ((completed.count > 0) && (((connectionsTable.numberOfSections == 4) || (connectionsTable.numberOfSections == 3)) && (indexPath.section == 0))) {
                     return 250;
             }
             return 100;
         }
    } else if (tableView == more) {
        return 43;
    } else {
        if (indexPath.section == 0) {
            if (showFeaturedTutors) {
                return 170;
            }
            return 0;
            
        } else if (indexPath.section == 1) {
            if (indexPath == selectedIndex) {
                departmentReference *dept = departmentDatabases[indexPath.row];
                return [dept cellHeight];
            } else {
                return departmentHeight;
            }
            
        }  else {
            return 280;
    }
    }
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 50)];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-24, 30)];
    hheader.textColor = [References primaryText];
    hheader.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    hheader.textAlignment = NSTextAlignmentLeft;
    if (tableView == connectionsTable) {
        if (connectionsTable.numberOfSections == 4) {
            if (section == 0) {
                hheader.text = @"Rate Your Experience";
            } else if (section == 1) {
                hheader.text = @"Awaiting Confirmation";
            } else if (section == 2) {
                hheader.text = @"Upcoming";
            } else {
                hheader.text = @"Messages";
            }
        } else if (connectionsTable.numberOfSections == 3) {
            if (section == 0) {
                hheader.text = completed.count > 0 ? @"Rate Your Experience" : @"Awaiting Confirmation";
            } else if (section == 1) {
                hheader.text = @"Upcoming";
            } else {
                hheader.text = @"Messages";
            }
            
        } else {
            if (section == 0) {
                hheader.text = @"Upcoming";
            } else {
                hheader.text = @"Messages";
            }
        }
    } else if (tableView == more) {
        if (section == 0) {

            hheader.text = @"Your Profile";
            [view addSubview:profileImageView];
        } else {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 30)];
            UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 7.5, [References screenWidth]-24, 15)];
            hheader.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
            hheader.textColor = [References primaryText];
            NSDictionary *dict = morebuttons[section-1];
            
            hheader.text = [[dict valueForKey:@"title"] uppercaseString];
            if ([hheader.text containsString:@"CURRENT BALANCE"]) {
                hheader.text = @"CURRENT BALANCE: ";
                hheader.frame = CGRectMake(hheader.frame.origin.x, hheader.frame.origin.y, [References fixedHeightSizeWithFont:hheader.font andString:hheader.text andLabel:hheader].size.width, hheader.frame.size.height);
                UILabel *balance = [[UILabel alloc] initWithFrame:CGRectMake(hheader.frame.origin.x+hheader.frame.size.width+2.5, hheader.frame.origin.y-2.5, 0, 15+5)];
                balance.backgroundColor = [References accent];
                balance.font = [UIFont systemFontOfSize:13 weight:UIFontWeightBold];
                balance.textColor = [UIColor whiteColor];
                balance.text = [[[[dict valueForKey:@"title"] uppercaseString] componentsSeparatedByString:@": "] objectAtIndex:1];
                [References cornerRadius:balance radius:5];
                balance.frame = CGRectMake(balance.frame.origin.x, balance.frame.origin.y, [References fixedHeightSizeWithFont:balance.font andString:balance.text andLabel:balance].size.width+5, balance.frame.size.height);
                balance.textAlignment = NSTextAlignmentCenter;
                [view addSubview:balance];
                
            }
            [view addSubview:hheader];
            return view;
        }
    } else {
        if (section == 0) {
            return headerV;
        }
    }

    [view addSubview:hheader];
    return view;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == more) {
        if (section > 0) {
            return 30;
        } else {
            return 40;
        }
    } else if (tableView == table) {
        if (section == 0) {
            return headerV.frame.size.height;
        }
        return 0;
    }
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (tableView == more) {
//        return 24;
//    }
    return -10;
}


//
//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 24)];
//    UILabel *version = [[UILabel alloc] initWithFrame:view.bounds];
//        NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
//        NSString *appVersion = [infoDict objectForKey:@"CFBundleShortVersionString"]; // example: 1.0.0
//        NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"]; // example: 42
//        version.text = [NSString stringWithFormat:@"TutorTree Version %@.%@",appVersion,buildNumber];
//        version.font = [UIFont systemFontOfSize:8 weight:UIFontWeightSemibold];
//        version.textColor = [UIColor lightGrayColor];
//        version.textAlignment = NSTextAlignmentCenter;
//        [view addSubview:version];
//    return view;
//}

//-(void)verifyEmail {
//    NSString *requestURL = @"https://tutortree-development.herokuapp.com/verify/rncrosby@ucsc.edu/1234";
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    [manager GET:requestURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
//        //NSLog(@"apay: %@",_clientToken);
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//
//        NSLog(@"Error: %@", error);
//        //[References postError:error.slocalizedDescription];
//    }];
//
//}

-(void)openCourseDatabase:(courseDatabase*)sender asMine:(NSNumber*)myCourse{
    browseCourseViewController *bvc = [[browseCourseViewController alloc] init];
    bvc.course = sender;
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [bvc setModalPresentationStyle:UIModalPresentationFormSheet];
        [bvc setModalTransitionStyle: UIModalTransitionStyleCoverVertical];
        [self presentViewController:bvc animated:YES completion:nil];
        // ipad
    } else {
        [self.navigationController pushViewController:bvc animated:YES];
    }
    
    //[self presentViewController:calendarViewController animated:YES completion:nil];
}

-(void)openMyCourseAtIndex:(UIButton*)sender {
    [self openCourseDatabase:[self findCourse:myCourses[sender.tag]] asMine:[NSNumber numberWithBool:YES]];
}

-(courseDatabase*)findCourse:(NSString*)course {
    for (courseDatabase* tCourse in courses) {
        if ([[tCourse courseDepartmentAndNumber] isEqualToString:course]) {
            return tCourse;
        }
    }
    return nil;
}


-(UIButton*)moreButton:(CGFloat)y {
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, y, [References screenWidth]-15, 20)];
    [button.titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightBold]];
    [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    return button;
}



-(void)openAdmin {
    adminViewController *adminViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"adminViewController"];
    [self presentViewController:adminViewController animated:YES completion:^{
        [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
    }];
}



-(void)contactTutorTree {
    MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
    composeVC.mailComposeDelegate = self;
    
    // Configure the fields of the interface.
    [composeVC setToRecipients:@[@"info@jointutortree.com"]];
    [composeVC setSubject:@"TutorTree Comment/Concern"];
    
    // Present the view controller modally.
    if ([MFMailComposeViewController canSendMail]) {
        // device is configured to send mail
        [self presentViewController:composeVC animated:YES completion:^{
            [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
        }];
    } else {
        [References toastNotificationBottom:self.view andMessage:@"Can't send mail."];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    if (result == MFMailComposeResultSent) {
        [References toastMessage:@"Thank you for contacting Tutortree." andView:self andClose:NO];
    }
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)disagreeToTOS {
    [self signOut];
}

-(void)agreeToTOS {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"agreement"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)signOut {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        if (![(NSString*)key isEqualToString:@"accountDatabase"] && ![(NSString*)key isEqualToString:@"clientToken"] && ![(NSString*)key isEqualToString:@"currentVersion"]) {
            [defs removeObjectForKey:key];
        
    }
    [defs synchronize];
        
}
    [[FIRAuth auth] signOut:nil];
    [self.navigationController pushViewController:[[nGettingStartedViewController alloc] init] animated:YES];
}

-(void)openTutorPanel {
    tutorOfficeViewController *tutorOfficeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"tutorOfficeViewController"];
    [self presentViewController:tutorOfficeViewController animated:YES completion:^{
        [self->ctrl setContentOffset:CGPointMake([References screenWidth]*2, 0)];
    }];
}

- (void)registerForRemoteNotifications {
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
        if(!error){
            dispatch_async(dispatch_get_main_queue(), ^(){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            });
        }
    }];
}

-(void)openInfo {
    tosPopover = [[popoverView alloc] initWithSuperview:self];
    [tosPopover addTitle:@"Terms"];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"tos"
                                                     ofType:@"txt"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    UITextView *tosView = [[UITextView alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 0) textContainer:nil];
    tosView.backgroundColor = [UIColor clearColor];
    tosView.editable = NO;
    tosView.textColor = [References primaryText];
    tosView.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    tosView.text = content;
    tosView.scrollEnabled = NO;
    tosView.frame = CGRectMake(tosView.frame.origin.x, tosView.frame.origin.y, tosView.frame.size.width, [References fixedWidthSizeWithFont:tosView.font andString:tosView.text andLabel:tosView].size.height);
    [tosPopover addView:tosView];
    if (needAgreement) {
            [tosPopover setCantHide];
        [tosPopover setAcceptFunction:@selector(acceptTOS)];
        [tosPopover setDeclineFunction:@selector(declineTOS)];
    } else {
        [tosPopover setOkFunction:@selector(closeTOS)];
    }
    [tosPopover showView];
}

-(void)acceptTOS {
    needAgreement = NO;
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"agreed"] setValue:@1];
    [tosPopover hideView];
}
    
-(void)closeTOS {
    [tosPopover hideView];
}

-(void)declineTOS {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)analyzeBalances {
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self dataFilePath:@"balances"]]) {
        [[NSFileManager defaultManager] createFileAtPath: [self dataFilePath:@"balances"] contents:nil attributes:nil];
        NSLog(@"BALANCES");
    }
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSString *output = @"";
        for (id key in snapshot.value) {
            NSDictionary *dict = snapshot.value[key];
            CGFloat balance = [self calculateBalance:[dict objectForKey:@"income"] andSpending:[dict objectForKey:@"spending"]];
            if (balance > 0) {
                output = [output stringByAppendingString:[NSString stringWithFormat:@"%@,$%.2f\n",dict[@"email"],balance]];
            }
        }
        NSFileHandle *handle;
        handle = [NSFileHandle fileHandleForWritingAtPath: [self dataFilePath:@"balances"] ];
        //say to handle where's the file fo write
        [handle truncateFileAtOffset:[handle seekToEndOfFile]];
        //position handle cursor to the end of file
        [handle writeData:[output dataUsingEncoding:NSUTF8StringEncoding]];
        NSLog(@"DONE WITH BALANCES");
    }];
}

-(NSString *)dataFilePath:(NSString*)title {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.csv",title]];
}

-(void)shareDeposit {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Deposit" message:@"Deposited money will be available immediately for use within Tutortree. All major credit cards are accepted." preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Deposit Now" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self openDeposit];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Make a Guest Payment" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSArray *items = @[[NSString stringWithFormat:@"https://www.jointutortree.com/wp-content/uploads/2018/07/pay.html?%@",self->uid]];
        
        // build an activity view controller
        UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:items applicationActivities:nil];
        [controller.popoverPresentationController setSourceView:self.view];
        // and present it
        [self presentViewController:controller animated:YES completion:^{
            // executes after the user selects something
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
    // grab an item we want to share
    
}

-(void)openDeposit {
    SFSafariViewController *safariVC = [[SFSafariViewController alloc]initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.jointutortree.com/wp-content/uploads/2018/07/pay.html?%@",self->uid]] entersReaderIfAvailable:NO];
    safariVC.delegate = self;
    safariVC.preferredControlTintColor = [References schoolMainColor];
    [self presentViewController:safariVC animated:YES completion:nil];
}

-(void)withdrawal {
    if (currentBalance > 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Available Balance: $%.2f",currentBalance] message:@"Enter your requested withdrawal amount." preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.textAlignment = NSTextAlignmentCenter;
            textField.text = [NSString stringWithFormat:@"%.2f",self->currentBalance];
            textField.keyboardType = UIKeyboardTypeDecimalPad;
            textField.keyboardAppearance = [References keyboard];
        }];
        [alert addAction:[UIAlertAction actionWithTitle:@"Proceed to Venmo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (![self verifyBalance:alert.textFields[0]]) {
                [self withdrawal];
            } else {
                [self venmoWithdrawal:alert.textFields[0].text.floatValue];
            }
        }]];
//        [alert addAction:[UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            if (![self verifyBalance:alert.textFields[0]]) {
//                [self withdrawal];
//            } else {
//                [self emailWithdrawal:alert.textFields[0].text.floatValue];
//            }
//        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }
    
}

-(BOOL)verifyBalance:(UITextField*)sender {
    CGFloat entered = sender.text.floatValue;
    if (entered <= currentBalance) {
        return true;
    } else {
        return false;
    }
    
}

-(void)venmoWithdrawal:(CGFloat)amount {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Withdraw $%.2f",currentBalance] message:@"Now enter your Venmo username or phone number." preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.textAlignment = NSTextAlignmentCenter;
        textField.placeholder = @"Venmo Username or Phone";
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.keyboardAppearance = [References keyboard];
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (alert.textFields[0].text.length > 1) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.detailsLabel.text = @"Processing";
            [References blurView:hud.backgroundView];
            hud.removeFromSuperViewOnHide = YES;
            hud.margin = 10.f;
            NSDictionary *withdrawal = @{@"amount":[NSNumber numberWithFloat:amount],@"venmo":alert.textFields[0].text,@"status":@0,@"uid":self->uid,@"type":@"Withdrawal",@"numberDate":[NSNumber numberWithDouble:[NSDate date].timeIntervalSince1970]};
            NSString *refID = [References randomStringWithLength:8].uppercaseString;
            [[[[FIRDatabase database] reference] child:@"updateDatabase"] updateChildValues:@{[NSString stringWithFormat:@"users/%@/spending/%@",self->uid,refID]:withdrawal,[NSString stringWithFormat:@"withdrawals/%@",refID]:withdrawal} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    dispatch_after(0, dispatch_get_main_queue(), ^(void){
                        [hud hideAnimated:true afterDelay:1];
                    });
                    
                }
            }];
        }
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)emailWithdrawal:(CGFloat)amount {
    NSLog(@"soon");
}



-(void)clearAllBadges {
    clearingBadges = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    clearingBadges.mode = MBProgressHUDModeText;
    clearingBadges.detailsLabel.text = @"Running";
    [References blurView:clearingBadges.backgroundView];
    clearingBadges.removeFromSuperViewOnHide = YES;
    clearingBadges.margin = 10.f;
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            [self removeBadges:[snapshot.value allKeys] andIndex:0];
        }
    }];
}

-(void)removeBadges:(NSArray*)uids andIndex:(NSInteger)index {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uids[index]] child:@"badge"] removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (index < uids.count-1) {
            [self removeBadges:uids andIndex:index+1];
        } else {
            [self->clearingBadges hideAnimated:true afterDelay:0];
        }
    }];
}

-(void)addCourse {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose a department" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        for (id key in snapshot.value) {
            [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",key] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                NSString *dept = action.title;
                UIAlertController *tAlert = [UIAlertController alertControllerWithTitle:dept message:@"Enter the # or title" preferredStyle:UIAlertControllerStyleAlert];
                [tAlert addTextFieldWithConfigurationHandler:nil];
                [tAlert addAction:[UIAlertAction actionWithTitle:@"Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    NSString *course = tAlert.textFields[0].text;
                    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"]] child:dept] child:course] setValue:@{@"info":@"nil"} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                        if (!error) {
                            [References toastMessage:@"Created!" andView:self andClose:NO];
                        }
                    }
                     ];
                }]];
                [tAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:tAlert animated:true completion:nil];
            }]];
        }
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }];
}

-(void)addDepartment {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"New Department" message:@"Enter a department and a number, you'll be able to add more numbers later just start with the first one." preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Department";
    }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"101";
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSString *dept = alert.textFields[0].text;
        NSString *numb = alert.textFields[1].text;
        NSDictionary *data = @{[NSString stringWithFormat:@"%@/%@/%@",[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"],dept,numb]:@{@"info":@"nil"}};
        [[[[FIRDatabase database] reference] child:@"updateDatabase"] updateChildValues:data withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            if (!error) {
                [References toastMessage:@"Department Created" andView:self andClose:false];
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
    
//    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//    }];
    
//        for (id key in snapshot.value) {
//            [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@",key] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                NSString *dept = action.title;
//                UIAlertController *tAlert = [UIAlertController alertControllerWithTitle:dept message:@"Enter the # or title" preferredStyle:UIAlertControllerStyleAlert];
//                [tAlert addTextFieldWithConfigurationHandler:nil];
//                [tAlert addAction:[UIAlertAction actionWithTitle:@"Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                    NSString *course = tAlert.textFields[0].text;
//                    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:[[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"]] child:dept] child:course] setValue:@{@"info":@"nil"} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
//                        if (!error) {
//                            [References toastMessage:@"Created!" andView:self andClose:NO];
//                        }
//                    }
//                     ];
//                }]];
//                [tAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
//                [self presentViewController:tAlert animated:true completion:nil];
//            }]];
//        }
    
}

-(void)getAllTutors {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSString *emails = @"";
        for (id key in snapshot.value) {
            if ([(NSDictionary*)snapshot.value[key] objectForKey:@"availability"]) {
                if ([(NSDictionary*)snapshot.value[key] objectForKey:@"email"]) {
                    emails = [[emails stringByAppendingString:[(NSDictionary*)snapshot.value[key] objectForKey:@"email"]] stringByAppendingString:@", "];
                }
            }
        }
        NSLog(@"%@",emails);
    }];
}

-(void)setBio {
    bioPopover = [[popoverView alloc] initWithSuperview:self];
    [bioPopover addTitle:@"Update Bio"];

    
    bioView.backgroundColor = [References cells];
    [bioView setTextContainerInset:UIEdgeInsetsMake(10, 10, 10, 10)];
    [References cornerRadius:bioView radius:5];
    bioView.editable = YES;
    bioView.delegate = self;
    bioView.returnKeyType = UIReturnKeyDone;
    bioView.textColor = [References primaryText];
    bioView.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    bioView.scrollEnabled = NO;
    [bioPopover addView:bioView];
    [bioPopover setCantHide];
    [bioPopover setOkFunction:@selector(updateBio)];
    [bioPopover showView];
}

-(void)updateBio {
    if (bioChanged) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:self->uid] child:@"bio"] setValue:bioView.text withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            if (!error) {
                [MBProgressHUD hideHUDForView:self.view animated:true];
                [self->bioPopover hideView];
                self->bioChanged = false;
            }
        }];
    } else {
        [self->bioPopover hideView];
    }
}

-(bool)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([textView isEqual:bioView]) {
        
        if ([text isEqualToString:@"\n"]) {
            [textView resignFirstResponder];
            return false;
        } else {
            if (!bioChanged) {
                bioChanged = true;
            }
        }
    }
    return true;
}

-(void)analyzeSessions {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
            for (id key in snapshot.value) {
                if ([snapshot.value[key] objectForKey:@"sessions"]) {
                    for (id seshID in (NSDictionary*)[snapshot.value[key] objectForKey:@"sessions"]) {
                        if ([[[snapshot.value[key] objectForKey:@"sessions"] objectForKey:seshID] objectForKey:@"course"]) {
                            NSString *course = [[[snapshot.value[key] objectForKey:@"sessions"] objectForKey:seshID] objectForKey:@"course"];
                            NSNumber *count;
                            if (dict[course]) {
                                count = [NSNumber numberWithInt:[(NSNumber*)dict[course] intValue]+1];
                            } else {
                                count = [NSNumber numberWithInt:1];
                            }
                            dict[course] = count;
                        }
                    }
                }
            }
            NSLog(@"%@",dict);
        }
    }];
}

-(void)allEmails {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            NSString *emails = @"";
            for (id key in snapshot.value) {
                emails = [emails stringByAppendingString:[NSString stringWithFormat:@"%@,",[snapshot.value[key] objectForKey:@"email"]]];
            }
            NSLog(@"%@",emails);
        }
    }];
}


-(void)convertTimesToInt {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"invoices"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (id key in snapshot.value) {
                [array addObject:@{@"id":key,@"date":[[snapshot.value[key] objectForKey:@"metadata"] objectForKey:@"numberDate"]}];
            }
            [self iterateOver:array atIndex:0];
        }
    }];
}

-(void)iterateOver:(NSMutableArray*)array atIndex:(int)index {
    NSNumber *date = (NSNumber*)[array[index] objectForKey:@"date"];
    if (date.integerValue > 1300000000000) {
        NSInteger i = date.integerValue / 1000;
        date = [NSNumber numberWithInteger:i];
        NSLog(@"updated %@",[array[index] objectForKey:@"id"]);
    }
    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"invoices"] child:[array[index] objectForKey:@"id"]] child:@"metadata"] child:@"numberDate"] setValue:[NSNumber numberWithInt:date.intValue] withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            if (index < array.count-1) {
                [self iterateOver:array atIndex:index+1];
            }
        }
    }];

}


-(void)getAllEmails {
    if (![[NSFileManager defaultManager] fileExistsAtPath:[self dataFilePath:@"tutoremails"]]) {
        [[NSFileManager defaultManager] createFileAtPath: [self dataFilePath:@"tutoremails"] contents:nil attributes:nil];
        NSLog(@"Route creato");
    }
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSString *output = @"";
        for (id key in snapshot.value) {
            if ([snapshot.value[key] objectForKey:@"tutor"]) {
                output = [output stringByAppendingString:[NSString stringWithFormat:@"%@\n",[[snapshot.value objectForKey:key] objectForKey:@"email"]]];
            }
            
        }
        
        //            [hud hideAnimated:true];
        NSFileHandle *handle;
        handle = [NSFileHandle fileHandleForWritingAtPath: [self dataFilePath:@"tutoremails"] ];
        //say to handle where's the file fo write
        [handle truncateFileAtOffset:[handle seekToEndOfFile]];
        //position handle cursor to the end of file
        [handle writeData:[output dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"done");
    }];
}


@end
