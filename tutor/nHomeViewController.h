//
//  nHomeViewController.h
//  tutor
//
//  Created by Robert Crosby on 4/23/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "nUpcomingView.h"
@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface nHomeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource> {
    nUpcomingView *upcoming;
    UITableView *table;
    NSArray<NSString*> *sections;
}

@end

NS_ASSUME_NONNULL_END
