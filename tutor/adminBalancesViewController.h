//
//  adminBalancesViewController.h
//  tutor
//
//  Created by Robert Crosby on 4/16/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import <MBProgressHUD.h>

@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface adminBalancesViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

{
    NSMutableArray<NSDictionary*> *balances;
    UITableView *table;
}

@end

NS_ASSUME_NONNULL_END
