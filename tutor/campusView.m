//
//  campusView.m
//  tutor
//
//  Created by Robert Crosby on 7/8/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "campusView.h"

#define locationWidth 190
#define locationHeight 50

@implementation campusView

-(id)initWithParent:(UIViewController*)tparent {
    self = [super init];
    if (self) {
        parentView = tparent;
        [self createView];
        
    }
    return self;
}

+(instancetype)withParent:(UIViewController*)tparent {
    return [[campusView alloc] initWithParent:tparent];
}

-(UIView*)campusMap {
    return view;
}

-(void)initializeLocations {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"uoregonCampus"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            campusLocations = [[NSMutableArray alloc] init];
            UICollectionViewFlowLayout* flowLayout = [[UICollectionViewFlowLayout alloc] init];
            flowLayout.itemSize = CGSizeMake(locationWidth, locationHeight);
            [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
            campusLocationCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:map]+15, [References screenWidth]-15, locationHeight) collectionViewLayout:flowLayout];
            campusLocationCollection.bounces = YES;
            campusLocationCollection.clipsToBounds = NO;
            campusLocationCollection.alwaysBounceHorizontal = YES;
            campusLocationCollection.backgroundColor = [UIColor clearColor];
            campusLocationCollection.showsHorizontalScrollIndicator = NO;
            campusLocationCollection.dataSource = self;
            campusLocationCollection.delegate = self;
            [view addSubview:campusLocationCollection];
            for (id key in snapshot.value) {
                NSDictionary *info = snapshot.value[key];
                 [campusLocations addObject:@{@"name":key,@"info":snapshot.value[key]}];
                 [campusLocationCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:key];
                MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
                pin.coordinate = CLLocationCoordinate2DMake([(NSNumber*)info[@"lat"] floatValue], [(NSNumber*)info[@"lon"] floatValue]);
                pin.title = key;
                [map addAnnotation:pin];
            }
            [self zoomToFitMapAnnotations:map];
            [campusLocationCollection reloadData];
        }
    }];
}

-(void)createView {
    if (view == nil) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 200+15+locationHeight)];
        map = [[MKMapView alloc] initWithFrame:CGRectMake(15, 0, view.frame.size.width-30, view.frame.size.height-15-locationHeight)];
        map.showsPointsOfInterest = NO;
        map.showsBuildings = NO;
        map.scrollEnabled = NO;
        map.delegate = self;
        currentLocation = [[CLLocation alloc] initWithLatitude:44.044807 longitude: -123.072369];
        [References cornerRadius:map radius:5];
        UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(map.frame.origin.x+5, map.frame.origin.y+5, map.frame.size.width-10, map.frame.size.height-10)];
        shadow.backgroundColor = [UIColor whiteColor];
        [References bottomshadow:shadow];
        [view addSubview:shadow];
        [view addSubview:map];
        [self initializeLocations];
        preview = [[UIView alloc] initWithFrame:CGRectMake(0, map.frame.size.height, map.frame.size.width, 50)];
        UILabel *blur = [[UILabel alloc] initWithFrame:preview.bounds];
        [References blurView:blur];
        [preview addSubview:blur];
        
        currentTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 12, preview.frame.size.width-10, 15)];
        currentTitle.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
        currentTitle.textColor = [UIColor darkTextColor];
        [preview addSubview:currentTitle];
        
        currentHours = [[UILabel alloc] initWithFrame:CGRectMake(10, [References getBottomYofView:currentTitle],preview.frame.size.width-10 , 15)];
        currentHours.font = [UIFont systemFontOfSize:8 weight:UIFontWeightBold];
        currentHours.textColor = [UIColor lightGrayColor];
        [preview addSubview:currentHours];
        
        UIButton *open = [[UIButton alloc] initWithFrame:CGRectMake(preview.frame.size.width-70, currentTitle.frame.origin.y, 60, [References getBottomYofView:currentHours]-currentTitle.frame.origin.y)];
        [open.titleLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightMedium]];
        [open setTitle:@"MORE" forState:UIControlStateNormal];
        [open setTitleColor:[References accentColor] forState:UIControlStateNormal];
        open.layer.borderColor = [References accentColor].CGColor;
        open.layer.borderWidth = 1;
        [open addTarget:self action:@selector(more) forControlEvents:UIControlEventTouchUpInside];
        open.layer.cornerRadius = 8;
        [preview addSubview:open];
        [References createLine:preview xPos:0 yPos:0 inFront:YES];
        [map addSubview:preview];
        
    }
    
}


-(void)setYCoordinate:(CGFloat)y {
    view.frame = CGRectMake(view.frame.origin.x, y, view.frame.size.width, view.frame.size.height);
}

//-(void)locationManager:(CLLocationManager *)manager didFailWithError: (NSError *)error {
//    NSLog(@"didFailWithError: %@", error);
//}
//
//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
//    if (locations.count > 0) {
//        currentLocation = [[CLLocation alloc] initWithLatitude:44.044807 longitude: -123.072369];
//        //currentLocation = [locations firstObject];
//        NSLog(@"%@",currentLocation);
//        [self zoomToLocation];
//    }
//}

-(CGFloat)height {
    return view.frame.size.height;
}

-(void)zoomToLocation {
//    MKMapRect zoomRect = MKMapRectNull;
//    for (id <MKAnnotation> annotation in map.annotations)
//    {
//        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.6, 0.6);
//        zoomRect = MKMapRectUnion(zoomRect, pointRect);
//    }
   // [map setVisibleMapRect:zoomRect animated:YES];
    MKCoordinateRegion mapRegion;
    mapRegion.center = currentLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.001;
    mapRegion.span.longitudeDelta = 0.001;

    [map setRegion:mapRegion animated:NO];
}

-(void)zoomToFitMapAnnotations:(MKMapView*)mapView
{
    if([mapView.annotations count] == 0)
        return;
    
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
    
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
    
    for(MKPointAnnotation* annotation in mapView.annotations)
    {
        topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
        topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
        bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
        bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
    
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude) * 2; // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude) * 2; // Add a little extra space on the sides
    
    region = [mapView regionThatFits:region];
    [mapView setRegion:region animated:YES];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
        return campusLocations.count;
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5.0;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
        NSDictionary *location = campusLocations[indexPath.row];
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:location[@"name"] forIndexPath:indexPath];
        if (cell.tag == 0) {
            cell.clipsToBounds = NO;
            UIView *cv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, locationWidth, locationHeight)];
            UIView *card = [[UIView alloc] initWithFrame:CGRectMake(15, 0, locationWidth-15, locationHeight)];
            card.backgroundColor = [References accentColor];
            [References cornerRadius:card radius:5.0f];
            UILabel *place = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, card.frame.size.width-40, card.frame.size.height)];
            place.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
            [place setAdjustsFontSizeToFitWidth:YES];
            place.numberOfLines = 0;
            place.textColor = [UIColor whiteColor];
            place.text = location[@"name"];
            [card addSubview:place];
            UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(card.frame.origin.x+5, card.frame.origin.y+5, card.frame.size.width-10, card.frame.size.height-10)];
            shadow.backgroundColor = [UIColor whiteColor];
            [References bottomshadow:shadow];
            [cv addSubview:shadow];
            [cv addSubview:card];
            [cell addSubview:cv];
            cell.tag = 1;
        }
        return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *location = campusLocations[indexPath.row];
    currentPlace = location;
    [self showPreview:location];
    
}

-(void)showPreview:(NSDictionary*)dt {
    
    NSDictionary *location = dt[@"info"];
    MKCoordinateRegion mapRegion;
    mapRegion.center = CLLocationCoordinate2DMake([(NSNumber*)location[@"lat"] floatValue], [(NSNumber*)location[@"lon"] floatValue]);
    mapRegion.span.latitudeDelta = 0.005;
    mapRegion.span.longitudeDelta = 0.005;
    if (preview.frame.origin.y < map.frame.size.height) {
        [UIView animateWithDuration:0.15 animations:^{
            currentTitle.text = dt[@"name"];
            currentHours.text = [self openText:location[@"hours"] onDay:[[NSCalendar currentCalendar] component:NSCalendarUnitWeekday fromDate:[NSDate date]]];
            [map setRegion:mapRegion];
        }];
    } else {
        [References topshadow:preview];
        [UIView animateWithDuration:0.15 animations:^{
            currentTitle.text = dt[@"name"];
            currentHours.text = [self openText:location[@"hours"] onDay:[[NSCalendar currentCalendar] component:NSCalendarUnitWeekday fromDate:[NSDate date]]];
            preview.frame = CGRectMake(preview.frame.origin.x, preview.frame.origin.y-preview.frame.size.height, preview.frame.size.width, preview.frame.size.height);
            [map setRegion:mapRegion];
        }];
    }
}

-(NSString*)openText:(NSArray*)value onDay:(NSInteger)weekday {
    NSArray *days = @[@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday",@"Sunday"];
    if (weekday == 1) {
        weekday = 6;
    } else {
        weekday-=2;
    }
    NSInteger today = [[NSCalendar currentCalendar] component:NSCalendarUnitWeekday
                                                     fromDate:[NSDate date]];
    if (today == 1) {
        today = 6;
    } else {
        today-=2;
    }
    NSString *todayString;
    if (weekday == today) {
        todayString = @"Today";
    } else {
        NSLog(@"BOUTA CRASH, %li",weekday);
        todayString = days[weekday];
        NSLog(@"CRASHED");
    }
    NSString *todayHours = (NSString*)value[weekday];
    if ([todayHours containsString:@"-"]) {
        NSArray *todayHoursArray = [todayHours componentsSeparatedByString:@"-"];
        NSInteger open = [todayHoursArray[0] integerValue];
        NSInteger close = [todayHoursArray[1] integerValue];
        NSLog(@"%li:%li",open,close);
        NSString *openText,*closeText;
        if (open > 12) {
            openText = [NSString stringWithFormat:@"%li PM to ",open-12];
        } else {
            openText = [NSString stringWithFormat:@"%li AM to ",open];
        }
        if (close > 12) {
            closeText = [NSString stringWithFormat:@"%li PM",close-12];
        } else {
            closeText = [NSString stringWithFormat:@"%li AM",close];
        }
        return [NSString stringWithFormat:@"Open %@ from %@%@",todayString,openText,closeText];
    } else {
        return [NSString stringWithFormat:@"Closed %@",todayString];
    }
}

-(void)more {
    NSString *allHours = @"";
    NSDictionary *dt = currentPlace[@"info"];
    for (NSInteger a = 1; a < 8; a++) {
        allHours = [allHours stringByAppendingString:[NSString stringWithFormat:@"%@\n",[self openText:dt[@"hours"] onDay:a]]];
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:currentPlace[@"name"] message:allHours preferredStyle:UIAlertControllerStyleActionSheet];
//    UIAlertAction *website = [UIAlertAction actionWithTitle:@"Go To Website" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
//        // Ok action example
//    }];
    UIAlertAction *apple = [UIAlertAction actionWithTitle:@"Open In Apple Maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        NSNumber *lat = [currentPlace[@"info"] valueForKey:@"lat"];
        NSNumber *lon = [currentPlace[@"info"] valueForKey:@"lon"];
        MKMapItem *dest = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(lat.floatValue, lon.floatValue)]];
        dest.name = currentPlace[@"name"];
        [dest openInMapsWithLaunchOptions:nil];
    }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        // Other action
    }];
    [alert addAction:apple];
    [alert addAction:cancel];
    [parentView presentViewController:alert animated:YES completion:nil];
}
@end
