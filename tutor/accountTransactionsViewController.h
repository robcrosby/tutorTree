//
//  accountTransactionsViewController.h
//  tutor
//
//  Created by Robert Crosby on 1/2/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD.h>
#import "References.h"
@import SafariServices;
@import Firebase;


NS_ASSUME_NONNULL_BEGIN

@interface accountTransactionsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,SFSafariViewControllerDelegate> {
    MBProgressHUD *loading;
    NSInteger populated;
    CGFloat currentBalance;
    NSMutableArray *expand;
    NSDateFormatter *fmt;
    NSMutableArray *spending,*income,*withdrawals;
    UITableView *table;
}


@end

NS_ASSUME_NONNULL_END
