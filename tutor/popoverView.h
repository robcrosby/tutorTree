//
//  popoverView.h
//  tutor
//
//  Created by Robert Crosby on 10/10/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"

NS_ASSUME_NONNULL_BEGIN

@interface popoverView : UIView <UIScrollViewDelegate> {
    BOOL cantHide;
    UILabel *parentDarken;
    UIView *bottomBar;
    UIViewController *parent;
    UIScrollView *scrollview;
    UIButton *accept,*decline,*ok;
}

-(id)initWithSuperview:(UIViewController*)controller;
-(void)setCantHide;
-(void)showView;
-(void)addTitle:(NSString*)title;
-(void)addView:(UIView*)view;
-(BOOL)containsView:(UIView*)view;
-(void)hideView;

-(void)setAcceptFunction:(SEL)function;
-(void)setDeclineFunction:(SEL)function;
-(void)setOkFunction:(SEL)function;

@end

NS_ASSUME_NONNULL_END
