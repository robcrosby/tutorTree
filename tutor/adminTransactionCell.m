//
//  adminTransactionCell.m
//  tutor
//
//  Created by Robert Crosby on 1/23/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "adminTransactionCell.h"

@implementation adminTransactionCell


-(id)initWithID:(NSString*)tID andData:(NSDictionary*)data {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:invoiceID];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    invoiceID = tID;
    numberDate = [(NSNumber*)[data[@"metadata"] objectForKey:@"numberDate"] integerValue];
    invoice = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, [References screenWidth]-5, 20)];
    invoice.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    invoice.text = invoiceID;
    invoice.textColor = [UIColor lightGrayColor];
    fromName = [[UILabel alloc] initWithFrame:CGRectMake(15, 25, [References screenWidth]-5, 20)];
    fromName.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    toName = [[UILabel alloc] initWithFrame:CGRectMake(15, 45, [References screenWidth]-5, 20)];
    toName.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    course = [[UILabel alloc] initWithFrame:CGRectMake(15, 65, [References screenWidth]-5, 20)];
    course.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    date = [[UILabel alloc] initWithFrame:CGRectMake(15, 85, [References screenWidth]-5, 20)];
    date.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    date.text = [data[@"metadata"] objectForKey:@"date"];
    price = [[UILabel alloc] initWithFrame:CGRectMake(15, 105, [References screenWidth]-5, 20)];
    price.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    price.text = [NSString stringWithFormat:@"$%.2f",[(NSNumber*)[data[@"metadata"] objectForKey:@"amount"] floatValue]];
    if (data[@"fromEmail"]) {
        studentID = data[@"fromEmail"];
        [self getNameForUser:data[@"fromEmail"] andUpdateLabel:fromName];
    }
    if (data[@"toEmail"]) {
        tutorID = data[@"toEmail"];
        [self getNameForUser:data[@"toEmail"] andUpdateLabel:toName];
    }
    if (!([(NSString*)data[@"fromEmail"] containsString:@"@"]) && !([(NSString*)data[@"toEmail"] containsString:@"@"])) {
        NSArray *people = [@[data[@"fromEmail"],data[@"toEmail"]] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        [self getCourse:[NSString stringWithFormat:@"%@:%@",people[0],people[1]]];
    } else {
        course.text = @"Unknown (Web, Donation, Old Transaction)";
    }
    [self addSubview:invoice];
    [self addSubview:course];
    [self addSubview:fromName];
    [self addSubview:toName];
    [self addSubview:price];
    [self addSubview:date];
    return self;
}

-(void)getCourse:(NSString*)connectionID {
    [[[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:connectionID] child:@"sessions"] child:invoiceID] child:@"course"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->course.text = snapshot.value;
        } else {
            self->course.text = @"Unknown (Web, Donation, Old Transaction)";
        }
    }];
}

-(void)getNameForUser:(NSString*)uid andUpdateLabel:(UILabel*)label {
    if ([uid containsString:@"@"]) {
        [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] queryEqualToValue:uid childKey:@"email"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.exists) {
                label.text = snapshot.value[@"name"];
            } else {
                label.text = uid;
            }
        }];
    } else {
        [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] child:@"name"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.exists) {
                label.text = snapshot.value;
            }
        }];
    }
}

-(NSString*)invoiceID {
    return invoiceID;
}

-(NSString*)getTutorID {
    return tutorID;
}

-(NSString*)getStudentID {
    return studentID;
}

-(NSString*)amount {
    return price.text;
}

-(NSString*)dateT {
    return date.text;
}

-(NSInteger)numberDate {
    return numberDate;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
