//
//  adminUsersViewController.m
//  tutor
//
//  Created by Robert Crosby on 1/3/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "adminUsersViewController.h"

@interface adminUsersViewController ()

@end

@implementation adminUsersViewController

- (void)viewDidLoad {
    expand = [[NSMutableArray alloc] initWithArray:@[@1,@1,@1]];
    fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMMM d, YYYY, h:mm a"];
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin])];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [References screenWidth]-30, 44)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = @"Users";
    
    header.textColor = [UIColor darkTextColor];
    header.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:header];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    UIButton *find = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-44, [References topMargin], 50, 44)];
    [find setTitle:@"Find" forState:UIControlStateNormal];
    [find setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [find.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [find setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [find addTarget:self action:@selector(find) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:find];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    [self.view sendSubviewToBack:menuBar];
    table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height, 0, [References bottomMargin], 0);
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    [self.view sendSubviewToBack:table];
//    [self getUsers];
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    UIAlertController *options = [UIAlertController alertControllerWithTitle:@"Users" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [options addAction:[UIAlertAction actionWithTitle:@"Get All Users" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self getUsers];
    }]];
    [options addAction:[UIAlertAction actionWithTitle:@"Find By Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self find];
    }]];
    [options addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:true completion:nil];
    }]];
    [self presentViewController:options animated:true completion:nil];
}

-(void)find {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Find User" message:@"Type the users email" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Email Address";
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Find" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self queryEmail:alert.textFields[0].text];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)queryEmail:(NSString*)value {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *list = snapshot.value;
        bool found = false;
        for (id key in list) {
            if ([[list[key] objectForKey:@"email"] isEqualToString:value]) {
                dispatch_after(0, dispatch_get_main_queue(), ^(void){
                    [MBProgressHUD hideHUDForView:self.view animated:true];
                    NSDictionary *dict = list[key];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:dict[@"email"] message:dict[@"name"] preferredStyle:UIAlertControllerStyleActionSheet];
                    [alert addAction:[UIAlertAction actionWithTitle:@"Add Money to Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self addBalanceToUser:key andName:dict[@"email"]];
                    }]];
                    [alert addAction:[UIAlertAction actionWithTitle:@"Deduct Money from Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self deductBalanceFromUser:key andName:dict[@"email"]];
                    }]];
                    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
                    [self presentViewController:alert animated:true completion:nil];
                });
                found = true;
                break;
            }
        }
        if (!found) {
            dispatch_after(0, dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:true];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.detailsLabel.text = @"Unable to find a user with that information.";
                hud.removeFromSuperViewOnHide = YES;
                hud.margin = 10.f;
                [hud hideAnimated:true afterDelay:1.5];
            });
        }
    }];
}

-(void)close {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return users.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && [expand[indexPath.section] isEqual:@1]) {
        return 44;
    }
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 30)];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 7.5, [References screenWidth]-24, 15)];
    hheader.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    hheader.textColor = [UIColor lightGrayColor];
//    if (section == 0) {
//        hheader.text = @"PENDING";
//        UIButton *expand = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 7.5, 100, 15)];
//        [expand.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightBold]];
//        [expand setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//        [expand setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
//        [expand setTitle:@"COLLAPSE" forState:UIControlStateNormal];
//        expand.tag = section;
//        [expand addTarget:self action:@selector(expandRows:) forControlEvents:UIControlEventTouchUpInside];
//        [view addSubview:expand];
//    }
    [view addSubview:hheader];
    return view;
}


-(void)expandRows:(UIButton*)sender {
    if ([expand[sender.tag] isEqual:@1]) {
        [sender setTitle:@"EXPAND" forState:UIControlStateNormal];
        expand[sender.tag] = @0;
    } else {
        [sender setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand[sender.tag] = @1;
    }
    [table beginUpdates];
    [table endUpdates];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
            NSDictionary *dict = [users objectAtIndex:indexPath.row];
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"U%@",dict[@"email"]]];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"U%@",dict[@"email"]]];
//                UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 0, 100, 44)];
//                status.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
//                status.textAlignment = NSTextAlignmentRight;
//                status.text = @"PENDING";
//                status.textColor = [References colorFromHexString:@"#FFEA00"];
//                [cell addSubview:status];
                
                cell.textLabel.text = [NSString stringWithFormat:@"%@",dict[@"email"]];
                cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",dict[@"name"]];
                cell.accessoryType = UITableViewCellAccessoryDetailButton;
            }
            cell.clipsToBounds = true;
            return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = users[indexPath.row];
    NSLog(@"%@",dict);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:dict[@"email"] message:dict[@"name"] preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Add Money to Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self addBalanceToUser:dict[@"id"] andName:dict[@"email"]];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Deduct Money from Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self deductBalanceFromUser:dict[@"id"] andName:dict[@"email"]];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)addBalanceToUser:(NSString*)uid andName:(NSString*)email {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Send money to %@",email] message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.textAlignment = NSTextAlignmentCenter;
        textField.text = [NSString stringWithFormat:@"0"];
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.detailsLabel.text = @"Processing";
        [References blurView:hud.backgroundView];
        hud.removeFromSuperViewOnHide = YES;
        hud.margin = 10.f;
        [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] child:@"income"] child:[References randomStringWithLength:8].uppercaseString] setValue:@{@"amount":[NSNumber numberWithFloat:alert.textFields[0].text.floatValue],@"numberDate":[NSNumber numberWithFloat:[NSDate date].timeIntervalSince1970],@"type":@"Web"} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            if (!error) {
                [References sendNotification:uid withSubject:@"Balance Updated" andMessage:@"Your balance in Tutortree has been corrected." forceEmail:NO andCompletion:^(bool result) {
                    NSLog(@"Done");
                }];
                
                dispatch_after(0, dispatch_get_main_queue(), ^(void){
                    [hud hideAnimated:true afterDelay:1];
                });
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)deductBalanceFromUser:(NSString*)uid andName:(NSString*)email {
    
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Deduct money from %@",email] message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.textAlignment = NSTextAlignmentCenter;
        textField.text = [NSString stringWithFormat:@"0"];
        textField.keyboardType = UIKeyboardTypeDecimalPad;
    }];
    [alert addAction:[UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.detailsLabel.text = @"Processing";
        [References blurView:hud.backgroundView];
        hud.removeFromSuperViewOnHide = YES;
        hud.margin = 10.f;
        [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] child:@"spending"] child:[References randomStringWithLength:8].uppercaseString] setValue:@{@"amount":[NSNumber numberWithFloat:alert.textFields[0].text.floatValue],@"numberDate":[NSNumber numberWithFloat:[NSDate date].timeIntervalSince1970],@"type":@"Web"} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
            if (!error) {
                [References sendNotification:uid withSubject:@"Balance Updated" andMessage:@"Your balance in Tutortree has been corrected." forceEmail:NO andCompletion:^(bool result) {
                    NSLog(@"Done");
                }];
                dispatch_after(0, dispatch_get_main_queue(), ^(void){
                    [hud hideAnimated:true afterDelay:1];
                });
            }
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)getUsers {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.detailsLabel.text = @"Gathering";
    [References blurView:hud.backgroundView];
    hud.removeFromSuperViewOnHide = YES;
    hud.margin = 10.f;
    self->users = [[NSMutableArray alloc] init];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            [self->users removeAllObjects];
            for (id key in snapshot.value) {
                NSString *email = [snapshot.value[key] objectForKey:@"email"] ? [snapshot.value[key] objectForKey:@"email"] : @"ZZZ";
                NSString *name = [snapshot.value[key] objectForKey:@"name"] ? [snapshot.value[key] objectForKey:@"name"] : @"ZZZ";
                [self->users addObject:@{@"id":key,@"email":email,@"name":name}];
            }
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"email"
                                                         ascending:YES];
            self->users = [[NSMutableArray alloc] initWithArray:[self->users sortedArrayUsingDescriptors:@[sortDescriptor]]];
            [self->table reloadData];
            dispatch_after(0, dispatch_get_main_queue(), ^(void){
                [hud hideAnimated:true afterDelay:1];
            });
            
        }
    }];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    if (result == MFMailComposeResultSent) {
        [References toastNotificationBottom:self.view andMessage:@"Sent"];
    }
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
