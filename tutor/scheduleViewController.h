//
//  scheduleViewController.h
//  tutor
//
//  Created by Robert Crosby on 11/4/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "tutorAvailability.h"
#import "References.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "bookButton.h"
#import "BraintreeVenmo.h"
#import "BraintreeCore.h"
#import <AFHTTPSessionManager.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "tutorContainerController.h"
#import "BraintreeApplePay.h"
#import "courseDatabase.h"
#import "paymentProcessingView.h"

@import PassKit;

NS_ASSUME_NONNULL_BEGIN

@interface scheduleViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    bool statusBarIsHidden;
    UIView *noAvailableTimes;
    NSString *tutorID;
    UILabel *selectedEndDateLabel;
    NSDate *currentDateObject,*selectedStartDate,*selectedEndDate;
    NSInteger selectedDuration,selectedMaxDuration;
    NSInteger currentDateInt,weekDayInt;
    NSMutableArray *currentTutors,*tutors,*dates,*dateInts;
    NSString *course;
    UITableView *table;
    UIScrollView *controller;
    NSInteger earliestRow;
    UIView *bottomBar,*menuBar,*dateIndicator,*bottomTutors,*loadingView;
    NSInteger selectedRow;
    UIScrollView *bookView;
    UILabel *finalReceiptDiscount,*finalReceiptTotal;
    UIButton *overlay;
    NSNumber *usedBalance;
    PKPaymentButton *apple;
    BTVenmoAccountNonce *paymentVenmo;
    CGFloat currentBalance,cost,discount,balanceUsed;
    UIButton *subtractTime,*addTime,*venmo,*selectCourse,*tutorCourse;
    bool amTutor;
    NSString *clientToken,*paymentType,*venmoNonce;
    CGFloat totalCost;
    UILabel *receiptDuration;
    bookButton *selectedbutton;
    tutorAvailability *selectedTutor;
    courseDatabase *courseDa;
    NSInteger selectedStartTime;
}

@property (nonatomic, strong) BTVenmoDriver *venmoDriver;
@property (nonatomic, strong) BTAPIClient *braintreeClient;

-(id)initWithCourse:(NSString *)tcourse andTutors:(NSArray *)ttutors andCourse:(courseDatabase*)tCourseD;

@end

NS_ASSUME_NONNULL_END
