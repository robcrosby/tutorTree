//
//  adminInvoiceObject.m
//  tutor
//
//  Created by Robert Crosby on 5/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "adminInvoiceObject.h"

@implementation adminInvoiceObject

-(id)initWithKey:(NSString*)tKey andDictionary:(NSDictionary*)dictionary {
    self = [super init];
    if (self) {
        key = tKey;
        fromEmail = dictionary[@"fromEmail"];
        fromName = dictionary[@"fromName"];
        toEmail = dictionary[@"toEmail"];
        toName = dictionary[@"toName"];
        NSDictionary *meta = dictionary[@"metadata"];
        amount = meta[@"amount"];
        date = meta[@"date"];
        NSNumber *numberDate = meta[@"numberDate"];
        dateObject = [NSDate dateWithTimeIntervalSince1970:numberDate.integerValue];
        NSString *type = meta[@"type"];
        if ([type isEqualToString:@"Venmo"]) {
            isApple = NO;
        } else {
            isApple = YES;
        }
        isWithdrawal = NO;
    }
    return self;
}

-(id)initWithdrawalWithKey:(NSString*)tkey andDictionary:(NSDictionary*)dictionary {
    self = [super init];
    if (self) {
        key = tkey;
        fromEmail = @"US";
        fromName = @"tutortree";
        toEmail = dictionary[@"uid"];
        toName = dictionary[@"venmo"];
        amount = dictionary[@"amount"];
        NSNumber *numberDate = dictionary[@"numberDate"];
        dateObject = [NSDate dateWithTimeIntervalSince1970:numberDate.integerValue];
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        [fmt setDateFormat:@"MMM d, YYYY"];
        date = [fmt stringFromDate:dateObject];
        status = dictionary[@"status"];
        isWithdrawal = YES;
        NSLog(@"%@",dictionary);
    }
    return self;
}

+(instancetype)WithKey:(NSString*)tKey andDictionary:(NSDictionary*)dictionary {
    return [[adminInvoiceObject alloc] initWithKey:tKey andDictionary:dictionary];
}

-(BOOL)isWithdrawal {
    return isWithdrawal;
}

-(BOOL)isPending {
    if ([status isEqualToString:@"pending"]) {
        return YES;
    }
    return NO;
}

-(NSString*)date {
    return date;
}
-(NSString*)amount {
    return [NSString stringWithFormat:@"$%@",amount];
}
-(BOOL)isApple {
    return isApple;
}
-(NSString*)transactionID {
    return key;
}

-(NSString*)toTwoLines {
    return [NSString stringWithFormat:@"%@\n%@",toName,toEmail];
}
-(NSString*)fromTwoLines {
    return [NSString stringWithFormat:@"%@\n%@",fromName,fromEmail];
}

@end
