//
//  References.m
//  Hitch for iOS
//
//  Created by Robert Crosby on 11/15/16.
//  Copyright © 2016 Robert Crosby. All rights reserved.
//

#import "References.h"

@interface References ()

@end

@implementation References

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(void)cornerRadius:(UIView *)view radius:(float)radius{
    view.layer.cornerRadius = radius;
    view.layer.masksToBounds = YES;
}

+(void)borderColor:(UIView*)view color:(UIColor*)color{
    view.layer.borderWidth = 1;
    
    view.layer.borderColor = color.CGColor;
}

+(CGFloat)screenWidth {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat wid = screenSize.width;
    return wid;
}

+(CGFloat)screenHeight {
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat high = screenSize.height;
    return high;
}

+(void)bottomshadow:(UIView*)view {
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 16;
    view.layer.shadowColor = ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor clearColor].CGColor : [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.2;
}

+(void)biggerbottomshadow:(UIView*)view {
    view.layer.shadowOffset = CGSizeMake(0, 3);
    view.layer.shadowRadius = 10;
    view.layer.shadowColor = ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor clearColor].CGColor : [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.5;
}

+(void)biggestbottomshadow:(UIView*)view {
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 13;
    view.layer.shadowColor = ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor clearColor].CGColor : [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.5;
}

+(void)tighterBottomShadow:(UIView*)view {
    view.layer.shadowOffset = CGSizeMake(0,0 );
    view.layer.shadowRadius = 7;
    view.layer.shadowColor = ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor clearColor].CGColor : [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.16;
}
+(void)topshadow:(UIView*)view {
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 10;
    view.layer.shadowColor = ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor clearColor].CGColor : [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = .15;
}
+(void)textShadow:(UILabel*)view {
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = 16;
    view.layer.shadowColor = ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor clearColor].CGColor : [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.2;
}
+(void)cardButton:(UIView*)view {
    // border radius
    [view.layer setCornerRadius:8.0f];
    
    // drop shadow
    [view.layer setShadowColor:([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor clearColor].CGColor : [UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.25];
    [view.layer setShadowRadius:6.0];
    [view.layer setShadowOffset:CGSizeMake(0,0)];
}

+(void)fromoffscreen:(UIView*)view where:(NSString*)where{
    CGRect location = view.frame;
    if ([where isEqualToString:@"BOTTOM"]) {
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y+[self screenHeight], view.frame.size.width, view.frame.size.height);
    }
    if ([where isEqualToString:@"TOP"]) {
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y-[self screenHeight], view.frame.size.width, view.frame.size.height);
    }
    view.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        view.frame = location;
    }];
}

+(void)fromonscreen:(UIView *)view where:(NSString *)where{
    if ([where isEqualToString:@"BOTTOM"]) {
        [UIView animateWithDuration:0.5 animations:^{
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y+[self screenHeight], view.frame.size.width, view.frame.size.height);
        }];
    }
    if ([where isEqualToString:@"TOP"]) {
        [UIView animateWithDuration:0.5 animations:^{
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y-[self screenHeight], view.frame.size.width, view.frame.size.height);
            
        }];
        
    }
}

+(void)justMoveOffScreen:(UIView *)view where:(NSString *)where{
    if ([where isEqualToString:@"BOTTOM"]) {
        [UIView animateWithDuration:0.5 animations:^{
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y+[self screenHeight], view.frame.size.width, view.frame.size.height);
        }];
    }
    if ([where isEqualToString:@"TOP"]) {
        [UIView animateWithDuration:0.5 animations:^{
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y-[self screenHeight], view.frame.size.width, view.frame.size.height);
        }];
    }
}

+(void)justMoveOnScreen:(UIView *)view where:(NSString *)where{
    if ([where isEqualToString:@"BOTTOM"]) {
        view.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^{
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y-[self screenHeight], view.frame.size.width, view.frame.size.height);
        }];
    }
    if ([where isEqualToString:@"TOP"]) {
        view.hidden = NO;
        [UIView animateWithDuration:0.5 animations:^{
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y+[self screenHeight], view.frame.size.width, view.frame.size.height);
        }];
    }
    
    
}

+(void)fadeThenMove:(UIView *)view where:(NSString *)where{
    [UIView animateWithDuration:0.5 animations:^{
        view.alpha = 0;
    } completion:^(BOOL finished) {
        view.hidden = YES;
        if ([where isEqualToString:@"BOTTOM"]) {
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y+[self screenHeight], view.frame.size.width, view.frame.size.height);
            view.alpha = 1;
        }
        if ([where isEqualToString:@"TOP"]) {
            view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y-[self screenHeight], view.frame.size.width, view.frame.size.height);
            view.alpha = 1;
        }
    }];
}

+(void)fadeIn:(UIView *)view{
    view.hidden = NO;
    view.alpha = 0;
    [UIView animateWithDuration:0.5 animations:^{
        view.alpha = 1;
    }];
}
+(void)fadeOut:(UIView *)view{
    [UIView animateWithDuration:1 animations:^{
        view.alpha = 0;
    } completion:^(BOOL complete){
        view.hidden = YES;
    }];
}

+(void)shift:(UIView*)view X:(float)X Y:(float)Y W:(float)W H:(float)H{
    [UIView animateWithDuration:0.5 animations:^{
        view.frame = CGRectMake(X, Y, W, H);
    }];
}

+(void)adjustHeight:(UIView*)view H:(float)H{
    [UIView animateWithDuration:0.5 animations:^{
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y , view.frame.size.width, view.frame.size.height+H);
    }];
}

+(void)moveUp:(UIView*)view yChange:(float)yChange{
    [UIView animateWithDuration:.3 animations:^{
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y-yChange, view.frame.size.width, view.frame.size.height);
    }];
}
+(void)moveDown:(UIView*)view yChange:(float)yChange{
    [UIView animateWithDuration:.3 animations:^{
        view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y+yChange, view.frame.size.width, view.frame.size.height);
    }];
}

+(void)moveHorizontal:(UIView*)view where:(NSString*)where{
    [UIView animateWithDuration:.3 animations:^{
        if ([where isEqualToString:@"RIGHT"]) {
            view.frame = CGRectMake(view.frame.origin.x+[self screenWidth], view.frame.origin.y, view.frame.size.width, view.frame.size.height);
        }
        if ([where isEqualToString:@"LEFT"]) {
            view.frame = CGRectMake(view.frame.origin.x-[self screenWidth], view.frame.origin.y, view.frame.size.width, view.frame.size.height);
        }
        
    } completion:^(BOOL finished) {
    }];
    
}

+(void)moveBackDown:(UIView*)view frame:(CGRect)frame{
    [UIView animateWithDuration:.3 animations:^{
        view.frame = CGRectMake(frame.origin.x, frame.origin.y-350, frame.size.width, frame.size.height);
    }];
}

+(void)textFieldInset:(UITextField *)text{
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, text.frame.size.height)];
    text.leftView = leftView;
    text.leftViewMode = UITextFieldViewModeAlways;
}

+(void)fade:(UIView*)view alpha:(float)alpha {
    [UIView animateWithDuration:.5 animations:^{
        view.alpha = alpha;
    }];
    
}

+(void)blurView:(UIView *)view {
    
    [view setBackgroundColor:[UIColor clearColor]];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? UIBlurEffectStyleDark : UIBlurEffectStyleProminent];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [view addSubview:blurEffectView];
    [view sendSubviewToBack:blurEffectView];
}

+(void)darkBlurView:(UIView*)view {
    [view setBackgroundColor:[UIColor clearColor]];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:blurEffectView];
    [view sendSubviewToBack:blurEffectView];
}

+(void)lightblurView:(UIView *)view {
    [view setBackgroundColor:[UIColor clearColor]];
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurEffectView.frame = view.bounds;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [view addSubview:blurEffectView];
    [view sendSubviewToBack:blurEffectView];
}

+(void)fadeColor:(UIView*)view color:(UIColor*)color{
    UIColor *temp = view.backgroundColor;
    view.layer.backgroundColor = temp.CGColor;
    [view setBackgroundColor:[UIColor clearColor]];
    
    [UIView animateWithDuration:0.5 animations:^{
        view.layer.backgroundColor = color.CGColor;
    } completion:NULL];
}

+(void)fadeButtonTextColor:(UIButton*)view color:(UIColor*)color{
    [UIView animateWithDuration:0.5 animations:^{
        [view setTitleColor:color forState:UIControlStateNormal];
    } completion:NULL];
}

+(void)fadeLabelTextColor:(UILabel*)view color:(UIColor*)color {
    [UIView animateWithDuration:0.5 animations:^{
        [view setTextColor:color];
    } completion:NULL];
}

+(void)fadeButtonText:(UIButton*)view text:(NSString*)text{
    [UIView animateWithDuration:0.5 animations:^{
        [view setTitle:text forState:UIControlStateNormal];
    } completion:NULL];
}

+(void)fadeButtonColor:(UIButton*)view color:(UIColor*)color{
    [UIView animateWithDuration:0.5 animations:^{
        [view setBackgroundColor:color];
    } completion:NULL];
}

+(void)tintUIButton:(UIButton*)button color:(UIColor*)color{
    UIImage *image = [button.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [button setImage:image forState:UIControlStateNormal];
    button.tintColor = color;
}

+(void)tintUIImage:(UIImageView*)image color:(UIColor*)color {
    UIImage *timage = [image.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [image setImage:timage];
    image.tintColor = color;
}

+(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyz";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    
    return randomString;
}

+(NSString *) randomNumberLetterStringWithLength: (int) len {
    NSString *letters = @"ABCDEFGHIJKLMNPQRSTUVWXYZ1234567890";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    
    return randomString;
}

+(NSString *) randomIntWithLength: (int) len {
    NSString *letters = @"0123456789";
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    
    return randomString;
}

+(void)createLinewithWidth:(UIView*)superview xPos:(int)xPos yPos:(int)yPos inFront:(bool)inFront andWidth:(int)width {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, width, 1)];
    [view setBackgroundColor:[[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] ? [self cells] : [self colorFromHexString:@"#E0E0E0"]];
    [superview addSubview:view];
    if (inFront == TRUE) {
        [superview bringSubviewToFront:view];
    } else {
        [superview sendSubviewToBack:view];
    }
}

+(void)createVerticalLine:(UIView*)superview atX:(int)x andY:(int)y inFront:(bool)inFront andHeight:(int)height{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(x, y, 1, height)];
    [view setBackgroundColor:[[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] ? [self cells] : [self colorFromHexString:@"#E0E0E0"]];
    [superview addSubview:view];
    if (inFront == TRUE) {
        [superview bringSubviewToFront:view];
    } else {
        [superview sendSubviewToBack:view];
    }
}

+(void)createLine:(UIView*)superview  xPos:(int)xPos yPos:(int)yPos inFront:(bool)inFront{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, 10000, 1)];
    [view setBackgroundColor:[[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] ? [self cells] : [self colorFromHexString:@"#E0E0E0"]];
    [superview addSubview:view];
    if (inFront == TRUE) {
        [superview bringSubviewToFront:view];
    } else {
        [superview sendSubviewToBack:view];
    }
}

+(void)createDarkLine:(UIView*)superview  xPos:(int)xPos yPos:(int)yPos inFront:(bool)inFront {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, 10000, 1)];
    [view setBackgroundColor:[UIColor darkTextColor]];
    view.alpha = 0.10;
    [superview addSubview:view];
    if (inFront == TRUE) {
        [superview bringSubviewToFront:view];
    } else {
        [superview sendSubviewToBack:view];
    }
}

+(void)ViewToLine:(UIView*)superview withView:(UIView*)view xPos:(int)xPos yPos:(int)yPos{
    view = [[UIView alloc] initWithFrame:CGRectMake(xPos, yPos, 1000, 1)];
    [view setBackgroundColor:[UIColor blackColor]];
    view.alpha = 0.1;
    [superview addSubview:view];
    [superview bringSubviewToFront:view];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(NSString *)backendAddress {
    return @"http://138.197.217.29:5000/";
}

+(void)fadeLabelText:(UILabel*)view newText:(NSString*)newText {
    [UIView transitionWithView:view
                      duration:0.4f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        view.text = newText;
                        
                    } completion:nil];
}

+(void)fadePlaceholderText:(UITextField*)view newText:(NSString*)newText {
    [UIView transitionWithView:view
                      duration:0.4f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        
                        view.placeholder = newText;
                        
                    } completion:nil];
}

+(UIView*)createGradient:(UIColor *)colorA andColor:(UIColor *)colorB withFrame:(CGRect)frame {
    UIView *view = [[UIView alloc] initWithFrame:frame];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    gradient.colors = @[(id)colorA.CGColor, (id)colorB.CGColor];
    
    [view.layer insertSublayer:gradient atIndex:0];
    return view;
}

+(UIColor*)returnColorWithPreset:(NSString*)preset {
    if ([preset isEqualToString:@"GREEN"]) {
        return [self colorFromHexString:@"#69F0AE"];
    } else if ([preset isEqualToString:@"BLUE"]) {
        return [self colorFromHexString:@"#40C4FF"];
    } else if ([preset isEqualToString:@"LIGHTBLUE"]) {
        return [self colorFromHexString:@"#18FFFF"];
    } else if ([preset isEqualToString:@"LIGHTORANGE"]) {
        return [self colorFromHexString:@"#FFAB40"];
    } else if ([preset isEqualToString:@"ORANGE"]) {
        return [self colorFromHexString:@"#FF6E40"];
    } else if ([preset isEqualToString:@"PURPLE"]) {
        return [self colorFromHexString:@"#7C4DFF"];
    } else if ([preset isEqualToString:@"INDIGO"]) {
        return [self colorFromHexString:@"#536DFE"];
    } else {
        return [self colorFromHexString:@"#616161"];
    }
    
}

+(UIView*)createGradientwithFrame:(CGRect)frame andPreset:(NSString*)preset {
    UIView *view = [[UIView alloc] initWithFrame:frame];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    
    gradient.frame = view.bounds;
    if ([preset isEqualToString:@"GREEN"]) {
        gradient.colors = @[(id)[self colorFromHexString:@"#69F0AE"].CGColor, (id)[self colorFromHexString:@"#00E676"].CGColor];
    } else if ([preset isEqualToString:@"BLUE"]) {
        gradient.colors = @[(id)[self colorFromHexString:@"#40C4FF"].CGColor, (id)[self colorFromHexString:@"#00B0FF"].CGColor];
    } else if ([preset isEqualToString:@"LIGHTBLUE"]) {
        gradient.colors = @[(id)[self colorFromHexString:@"#18FFFF"].CGColor, (id)[self colorFromHexString:@"#00E5FF"].CGColor];
    } else if ([preset isEqualToString:@"LIGHTORANGE"]) {
        gradient.colors = @[(id)[self colorFromHexString:@"#FFAB40"].CGColor, (id)[self colorFromHexString:@"#FF9100"].CGColor];
    } else if ([preset isEqualToString:@"ORANGE"]) {
        gradient.colors = @[(id)[self colorFromHexString:@"#FF6E40"].CGColor, (id)[self colorFromHexString:@"#FF3D00"].CGColor];
    } else if ([preset isEqualToString:@"PURPLE"]) {
        gradient.colors = @[(id)[self colorFromHexString:@"#7C4DFF"].CGColor, (id)[self colorFromHexString:@"#651FFF"].CGColor];
    } else if ([preset isEqualToString:@"INDIGO"]) {
        gradient.colors = @[(id)[self colorFromHexString:@"#536DFE"].CGColor, (id)[self colorFromHexString:@"#3D5AFE"].CGColor];
    } else {
        gradient.colors = @[(id)[self colorFromHexString:@"#616161"].CGColor, (id)[self colorFromHexString:@"#424242"].CGColor];
    }
    
    
    [view.layer insertSublayer:gradient atIndex:0];
    return view;
}

+(CAGradientLayer*)createGradient:(UIColor*)colorA andColor:(UIColor*)colorB andFrame:(UIView*)frame {
    CAGradientLayer *gradient = [CAGradientLayer layer];

    gradient.frame = frame.bounds;
    gradient.colors = @[(id)colorA.CGColor, (id)colorB.CGColor];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1,1);
    return gradient;
}

- (UIColor *)getUIColorObjectFromHexString:(NSString *)hexStr alpha:(CGFloat)alpha
{
    // Convert hex string to an integer
    unsigned int hexint = [self intFromHexString:hexStr];
    
    // Create color object, specifying alpha as well
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexint & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexint & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexint & 0xFF))/255
                    alpha:alpha];
    
    return color;
}

- (unsigned int)intFromHexString:(NSString *)hexStr
{
    unsigned int hexInt = 0;
    
    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}

+(void)titleToastMessage:(NSString *)message andTitle:(NSString*)title andView:(UIViewController *)view andClose:(bool)close{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [view presentViewController:alert animated:YES completion:nil];
    
    int duration = 1.5; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^() {
            if (close == TRUE) {
                [view dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    });
}

+(void)toastMessage:(NSString *)message andView:(UIViewController *)view andClose:(bool)close{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [view presentViewController:alert animated:YES completion:nil];
    
    int duration = 1.5; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^() {
            if (close == TRUE) {
                [view dismissViewControllerAnimated:YES completion:nil];
            }
        }];
    });
}

+(void)successFeedback {
    UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
    [feedback prepare];
    [feedback notificationOccurred:UINotificationFeedbackTypeSuccess];
}

+(void)warningFeedback {
    UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
    [feedback prepare];
    [feedback notificationOccurred:UINotificationFeedbackTypeWarning];
}

+(void)createNotification:(UIViewController*)parentView andMessage:(NSString*)message andColor:(UIColor*)color andHeight:(int)height  andTextColor:(UIColor*)textColor andCompletion:(void (^)(int result))completionHandler {
    UINotificationFeedbackGenerator *feedback = [[UINotificationFeedbackGenerator alloc] init];
    [feedback prepare];
    [feedback notificationOccurred:UINotificationFeedbackTypeError];
    int sHeight = 0;
    int yOrigin = 0;
    if ([References screenHeight] == 812) {
        sHeight = 50;
        yOrigin = 30;
    } else {
        sHeight = 20;
        yOrigin = 0;
    }
    UIView *notificationView = [[UIView alloc] initWithFrame:CGRectMake(0, -1*sHeight, [self screenWidth], sHeight)];
    UILabel *notification = [[UILabel alloc] initWithFrame:CGRectMake(0, yOrigin, [self screenWidth], 20)];
    [notification setFont:[UIFont systemFontOfSize:12.0f weight:UIFontWeightBold]];
    [notificationView setBackgroundColor:color];
    notification.backgroundColor = color;
    notification.textColor = textColor;
    notification.text = message;
    notification.textAlignment = NSTextAlignmentCenter;
    [notificationView addSubview:notification];
    [parentView.view addSubview:notificationView];
    [UIView animateWithDuration:0.25 animations:^(void){
        notificationView.frame = CGRectMake(0, 0, [self screenWidth], sHeight);
    } completion:^(bool finished){
        if (finished) {
            int duration = 3; // duration in seconds
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.25 animations:^(void){
                    notificationView.frame = CGRectMake(0, -1*sHeight, [self screenWidth], sHeight);
                } completion:^(bool finished){
                    if (finished) {
                        [notificationView removeFromSuperview];
                        completionHandler(0);
                    }
                }];
            });
        }
    }];
}


+(void)parallax:(UIView *)view {
    int parallaxEffect = 100;
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-1*parallaxEffect);
    verticalMotionEffect.maximumRelativeValue = @(parallaxEffect);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-1*parallaxEffect);
    horizontalMotionEffect.maximumRelativeValue = @(parallaxEffect);
    
    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    // Add both effects to your view
    [view addMotionEffect:group];
}

+(UIColor*)systemColor:(NSString*)color {
    if ([color isEqualToString:@"RED"]) {
        return [self colorFromHexString:@"#FF3B30"];
    }
    if ([color isEqualToString:@"BLUE"]) {
        return [self colorFromHexString:@"#007AFF"];
    }
    if ([color isEqualToString:@"YELLOW"]) {
        return [self colorFromHexString:@"#FFCC00"];
    }
    if ([color isEqualToString:@"ORANGE"]) {
        return [self colorFromHexString:@"#FF9500"];
    }
    if ([color isEqualToString:@"LRED"]) {
        return [self colorFromHexString:@"#FF2D55"];
    }
    if ([color isEqualToString:@"LBLUE"]) {
        return [self colorFromHexString:@"#5AC8FA"];
    } else {
        return [UIColor blackColor];
    }
}

+(void)fullScreenToast:(NSString*)text inView:(UIViewController*)view withSuccess:(BOOL)success andClose:(BOOL)close{
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], [self screenHeight])];
    [self blurView:blur];
    [blur setAlpha:0];
    [view.view addSubview:blur];
    UITextView *message = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, [self screenWidth], [self screenHeight])];
    [message setEditable:NO];
    message.textContainerInset = UIEdgeInsetsMake(([self screenHeight]/2)-40, 0, [self screenHeight]/2, 0);
    message.textAlignment = NSTextAlignmentCenter;
    [message setText:text];
    [message setTextColor:[UIColor lightGrayColor]];
    [message setFont:[UIFont systemFontOfSize:16.0f]];
    [message setBackgroundColor:[UIColor clearColor]];
    [view.view  addSubview:message];
    [message setAlpha:0];
    [view.view bringSubviewToFront:blur];
    [view.view  bringSubviewToFront:message];
    UIImage *image;
    UIImageView *imageView;
    if (success == YES) {
        image = [[UIImage imageNamed:@"success.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(([self screenWidth]/2)-25, ([self screenHeight]/2)-100, 50, 50)];
         [imageView setImage:image];
    } else {
        image = [[UIImage imageNamed:@"failure.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(([self screenWidth]/2)-25, ([self screenHeight]/2)-100, 50, 50)];
        [imageView setImage:image];
    }
    imageView.tintColor = [UIColor lightGrayColor];
    [imageView setAlpha:0];
    [view.view  addSubview:imageView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [blur setAlpha:1.0];
    [message setAlpha:1.0];
    [imageView setAlpha:1.0];
    [UIView commitAnimations];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //Here your non-main thread.
        [NSThread sleepForTimeInterval:1.0f];
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:0.5];
            [blur setAlpha:0.0];
            [imageView setAlpha:0];
            [message setAlpha:0];
            [UIView commitAnimations];
            if (close == YES) {
                [view dismissViewControllerAnimated:YES completion:nil];
            }
        });
    });
}

+(NSString*)randomizeString:(NSString*)string {
    return string;
}
+(NSString*)normalizeString:(NSString*)string {
    return string;
}

+(int)getBottomYofView:(UIView*)view {
    return (int)view.frame.origin.y+(int)view.frame.size.height;
}


+(void)postError:(NSString*)error {
    [[[[[FIRDatabase database] reference] child:@"errors"] childByAutoId] setValue:@{@"error": error, @"date": @([[NSDate date] timeIntervalSince1970])}];
}

+(UIColor*)lightGrey {
    return [self colorFromHexString:@"#EEEEEE"];
}
+(UIColor*)darkText {
    return [UIColor darkTextColor];
}
+(UIColor*)dominantColor {
    return [self colorFromHexString:@"#004D40"];
}

+(CGFloat)topMargin {
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 20;
        // ipad
    } else {
        if ([self screenHeight] >= 812) {
            return 44;
        } else {
            return 20;
        }
    }
    
}

+(CGFloat)bottomMargin {
    if ([self screenHeight] >= 812) {
        return 34;
    } else {
        return 0;
    }
}

+(UIColor*)colorAtIndex:(int)index {
    NSArray *colors = [[NSArray alloc] initWithObjects:@"#FF1744",@"#2979FF",@"#00E676",@"#FFEA00",@"#FF3D00",@"#651FFF", nil];
    return [References colorFromHexString:colors[index%6]];
}

+(UIColor*)altColorAtIndex:(int)index {
    NSArray *colors = [[NSArray alloc] initWithObjects:@"#FFEBEE",@"#E3F2FD",@"#E8F5E9",@"#FFFDE7",@"#FBE9E7",@"#EDE7F6", nil];
    return [References colorFromHexString:colors[index%6]];
}


+(void)roundTopCorners:(UIView*)view withRadius:(CGFloat)radius {
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: view.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){radius, radius}].CGPath;
    
    view.layer.mask = maskLayer;
}
+(void)roundBottomCorners:(UIView*)view withRadius:(CGFloat)radius {
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: view.bounds byRoundingCorners: UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii: (CGSize){radius, radius}].CGPath;
    
    view.layer.mask = maskLayer;
}

+(void)roundLeftCorners:(UIView*)view withRadius:(CGFloat)radius {
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: view.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft cornerRadii: (CGSize){radius, radius}].CGPath;
    
    view.layer.mask = maskLayer;
}
+(void)roundRightCorners:(UIView*)view withRadius:(CGFloat)radius {
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: view.bounds byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii: (CGSize){radius, radius}].CGPath;
    
    view.layer.mask = maskLayer;
}

+(CGRect)fixedWidthSizeWithFont:(UIFont*)font andString:(NSString*)string andLabel:(UIView*)label {
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(label.frame.size.width, CGFLOAT_MAX)
                                             options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                          attributes:@{NSFontAttributeName:font}
                                             context:nil];
    return textRect;
}

+(void)adjustLabelHeight:(UILabel*)label {
    label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, label.frame.size.width, [label.text boundingRectWithSize:CGSizeMake(label.frame.size.width, CGFLOAT_MAX)
                                                                                                                          options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                                                                                       attributes:@{NSFontAttributeName:label.font}
                                                                                                                          context:nil].size.height);
}

+(void)adjustLabelWidth:(UILabel*)label {
    
}

+(CGRect)fixedHeightSizeWithFont:(UIFont*)font andString:(NSString*)string andLabel:(UIView*)label {
    CGRect textRect = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, label.frame.size.height)
                                           options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                        attributes:@{NSFontAttributeName:font}
                                           context:nil];
    return textRect;
}

+(void)toastNotificationBottom:(UIView*)view andMessage:(NSString*)message {
    __block UIView *notificationView = [[UIView alloc] initWithFrame:CGRectMake(16, [References screenHeight], [References screenWidth]-32, 44)];
    notificationView.backgroundColor = [self colorFromHexString:@"#424242"];
    [self cornerRadius:notificationView radius:8.0f];
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, notificationView.frame.size.width-32, 44)];
    messageLabel.text = message;
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightSemibold];
    [notificationView addSubview:messageLabel];
    notificationView.alpha = 0;
    
    [view addSubview:notificationView];
    [UIView animateWithDuration:0.25 animations:^{
        notificationView.alpha = 1;
        notificationView.frame = CGRectMake(16, [self screenHeight]-[self bottomMargin]-notificationView.frame.size.height-16, notificationView.frame.size.width, notificationView.frame.size.height);
    } completion:^(BOOL finished) {
        if (finished) {
            [NSThread sleepForTimeInterval:1.0f];
            [UIView animateWithDuration:0.25 animations:^{
                notificationView.alpha = 0;
                notificationView.frame = CGRectMake(16, [self screenHeight]+16, notificationView.frame.size.width, notificationView.frame.size.height);
            } completion:^(BOOL finished) {
                if (finished) {
                    notificationView = nil;
                }
            }];
        }
    }];
}

+(NSString*)departmentAbbreviation:(NSString*)department {
    if ([department.lowercaseString isEqualToString:@"statistics"]) {
        return @"STAT";
    }
    if ([department.lowercaseString isEqualToString:@"physics"]) {
        return @"PHYS";
    }
    if ([department.lowercaseString isEqualToString:@"math"]) {
        return @"MATH";
    }
    if ([department.lowercaseString isEqualToString:@"human physiology"]) {
        return @"H. PHY";
    }
    if ([department.lowercaseString isEqualToString:@"finance"]) {
        return @"FIN";
    }
    if ([department.lowercaseString isEqualToString:@"economics"]) {
        return @"ECON";
    }
    if ([department.lowercaseString isEqualToString:@"chemistry"]) {
        return @"CHEM";
    }
    if ([department.lowercaseString isEqualToString:@"business administration"]) {
        return @"B. AD";
    }
    if ([department.lowercaseString isEqualToString:@"biology"]) {
        return @"BIO";
    }
    if ([department.lowercaseString isEqualToString:@"accounting"]) {
        return @"AACT";
    }
    return department;
}
+(UIView*)noData:(NSString*)title andMessage:(NSString*)message {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 250)];
    UIImageView *frown = [[UIImageView alloc] initWithFrame:CGRectMake((view.frame.size.width/2)-40, 0, 80, 80)];
    UIImage *image = [[UIImage imageNamed:@"sad.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [frown setImage:image];
    [frown setTintColor:[UIColor darkTextColor]];
    [view addSubview:frown];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:frown]+8, view.frame.size.width-40, 0)];
    titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    titleLabel.textColor = [UIColor lightGrayColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, titleLabel.frame.size.width, [self fixedWidthSizeWithFont:titleLabel.font andString:titleLabel.text andLabel:titleLabel].size.height);
    [view addSubview:titleLabel];
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:titleLabel]+8, view.frame.size.width, 0)];
    messageLabel.numberOfLines = 5;
    messageLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightRegular];
    messageLabel.textColor = [UIColor lightGrayColor];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.text = message;
    messageLabel.frame = CGRectMake(messageLabel.frame.origin.x, messageLabel.frame.origin.y, messageLabel.frame.size.width, [self fixedWidthSizeWithFont:messageLabel.font andString:messageLabel.text andLabel:messageLabel].size.height);
    [view addSubview:messageLabel];
    return view;
}

+(UIView*)aboutAndTOS:(UIView*)belowView {
    CGFloat bottom = 44;
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"agreement"]) {
        bottom+=100;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:belowView]+10, [References screenWidth]-30, [References screenHeight]-[References bottomMargin]-[References getBottomYofView:belowView]-100-bottom)];
    UIView *blur = [[UILabel alloc] initWithFrame:view.bounds];
    [References darkBlurView:blur];
    [view addSubview:blur];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"tos"
                                                     ofType:@"txt"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    UITextView *tosView = [[UITextView alloc] initWithFrame:CGRectMake(5, 0, view.frame.size.width-10, view.frame.size.height) textContainer:nil];
    tosView.backgroundColor = [UIColor clearColor];
    tosView.editable = NO;
    tosView.textColor = [UIColor whiteColor];
    tosView.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    tosView.text = content;
    [view addSubview:tosView];
    [References cornerRadius:view radius:16.0f];
    view.alpha = 0;
    return view;
}

+(UIView*)loadingView:(NSString*)message {
    UIView *loading = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight])];
    UILabel *blur = [[UILabel alloc] initWithFrame:loading.bounds];
    [References blurView:blur];
    [loading addSubview:blur];
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, [References topMargin]+88, loading.frame.size.width-40, 0)];
    titleLabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    titleLabel.textColor = [UIColor lightGrayColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = message;
    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, titleLabel.frame.size.width, [self fixedWidthSizeWithFont:titleLabel.font andString:titleLabel.text andLabel:titleLabel].size.height);
    [loading addSubview:titleLabel];
    indicator.frame = CGRectMake(([References screenWidth]/2)-(indicator.frame.size.width/2), [References getBottomYofView:titleLabel]+8, indicator.frame.size.width, indicator.frame.size.height);
    [loading addSubview:indicator];
    loading.alpha = 0;
    return loading;
}

+(NSDate *)dateWithOutTime:(NSDate *)datDate {
    if( datDate == nil ) {
        datDate = [NSDate date];
    }
    NSDateComponents* comps = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:datDate];
    NSDate *date =[[NSCalendar currentCalendar] dateFromComponents:comps];
    return date;
}

+(UIColor*)schoolMainColor {
    return [self accentColor];
}
+(UIColor*)schoolAccentColor {
    return [self colorFromHexString:@"#F9E610"];
}

+(UIColor*)schoolInputColor {
    NSString *school = [[NSUserDefaults standardUserDefaults] stringForKey:@"school"];
    if ([school isEqualToString:@"osu"]) {
        return [[UIColor blackColor] colorWithAlphaComponent:.80];
    } else if ([school isEqualToString:@"uo"]) {
         return [[UIColor whiteColor] colorWithAlphaComponent:.90];
    } else if ([school isEqualToString:@"psu"]) {
        return [[UIColor whiteColor] colorWithAlphaComponent:.90];
    } else {
        return [[UIColor whiteColor] colorWithAlphaComponent:.90];
    }
}

+(UIColor*)schoolInputTextColor {
    NSString *school = [[NSUserDefaults standardUserDefaults] stringForKey:@"school"];
    if ([school isEqualToString:@"osu"]) {
        return [UIColor whiteColor];
    } else if ([school isEqualToString:@"uo"]) {
        return [self schoolTextColor];
    } else if ([school isEqualToString:@"psu"]) {
        return [self schoolTextColor];
    } else {
        return [self schoolTextColor];
    }
}

+(UIColor*)schoolTextColor {
    NSString *school = [[NSUserDefaults standardUserDefaults] stringForKey:@"school"];
    if ([school isEqualToString:@"osu"]) {
        return [UIColor whiteColor];
    } else if ([school isEqualToString:@"uo"]) {
        return [UIColor whiteColor];
    } else if ([school isEqualToString:@"psu"]) {
        return [UIColor whiteColor];
    } else {
        return [UIColor lightGrayColor];
    }
}

+(UIStatusBarStyle)schoolStatusBar {
    NSString *school = [[NSUserDefaults standardUserDefaults] stringForKey:@"school"];
    if ([school isEqualToString:@"osu"]) {
        return UIStatusBarStyleLightContent;
    } else if ([school isEqualToString:@"uo"]) {
        return UIStatusBarStyleLightContent;
    } else if ([school isEqualToString:@"psu"]) {
        return UIStatusBarStyleLightContent;
    } else {
        return UIStatusBarStyleDefault;
    }
}

+(void)postActivity:(NSString *)tag andDescription:(NSString *)description andCompletion:(void (^)(bool result))completionHandler {
    [[[[[FIRDatabase database] reference] child:@"adminFeed"] child:[References randomNumberLetterStringWithLength:16]] setValue:@{@"tag":tag,@"description":description,@"date":[NSNumber numberWithFloat:[NSDate date].timeIntervalSince1970],@"sender":[FIRAuth auth].currentUser.uid} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        NSLog(@"HEY");
        completionHandler(TRUE);
    }];
}

+(void)sendNotification:(NSString*)uid withSubject:(NSString*)subject andMessage:(NSString*)message forceEmail:(BOOL)force andCompletion:(void (^)(bool result))completionHandler {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            NSNumber *smsNotifications = (snapshot.value[@"smsNotifications"] ? snapshot.value[@"smsNotifications"] : [NSNumber numberWithBool:false]);
            NSNumber *emailNotifications = (snapshot.value[@"emailNotifications"] ? snapshot.value[@"emailNotifications"] : [NSNumber numberWithBool:false]);
            NSNumber *badge = (snapshot.value[@"badge"] ? snapshot.value[@"badge"] : [NSNumber numberWithInteger:0]);
            NSString *token = (snapshot.value[@"token"] ? snapshot.value[@"token"] : @"");
            NSString *email = (snapshot.value[@"email"] ? snapshot.value[@"email"] : @"");
            NSString *phone = (snapshot.value[@"phone"] ? snapshot.value[@"phone"] : @"");
            badge = [NSNumber numberWithInteger:badge.integerValue+1];
            [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] child:@"badge"] setValue:badge withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                NSString *url;
                if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sandbox"] == true) {
                    NSLog(@"sandbox");
                    url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendSandboxNotification/%@/%@/%@/%@",subject,message,token,badge];
                } else {
                    url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendNotification/%@/%@/%@/%@",subject,message,token,badge];
                }
                NSString *finalURL = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                manager.requestSerializer = [AFHTTPRequestSerializer serializer];
                manager.responseSerializer = [AFHTTPResponseSerializer serializer];
                manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
                [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                    if (((badge.integerValue > 4) || force == true) || (emailNotifications.boolValue == true)) {
                        NSString *tSub,*tMes;
                        if ([subject isEqualToString:@"New Booking"] || [subject isEqualToString:@"Location Update"] || [subject isEqualToString:@"Booking Confirmed"] || [subject isEqualToString:@"Booking Voided"]) {
                            tSub = subject;
                            tMes = message;
                        } else {
                            NSLog(@"is message!");
                            tSub = @"You have notifications in Tutortree";
                            tMes = [NSString stringWithFormat:@"%@: %@",subject,message];
                        }
                        [self sendEmail:email withSubject:tSub andMessage:tMes andCompletion:^(bool result) {
                            if (result) {
                                if (smsNotifications.boolValue == true) {
                                    [self sendSMS:phone withSubject:tSub andMessage:tMes andCompletion:^(bool result) {
                                        if (result) {
                                            completionHandler(YES);
                                        }
                                    }];
                                }
                            } else {
                                if (smsNotifications.boolValue == true) {
                                    [self sendSMS:phone withSubject:tSub andMessage:tMes andCompletion:^(bool result) {
                                        if (result) {
                                            completionHandler(YES);
                                        }
                                    }];
                                } else {
                                    completionHandler(YES);
                                }
                            }
                        }];
                    } else {
                        completionHandler(YES);
                    }
                } failure:^(NSURLSessionTask *operation, NSError *error) {
                    NSLog(@"Error: %@", error);
                    if (((badge.integerValue > 4) || force == true) || (emailNotifications.boolValue == true)) {
                        NSString *tSub,*tMes;
                        if ([subject isEqualToString:@"New Booking"] || [subject isEqualToString:@"Location Update"] || [subject isEqualToString:@"Booking Confirmed"] || [subject isEqualToString:@"Booking Voided"]) {
                            tSub = subject;
                            tMes = message;
                        } else {
                            NSLog(@"is message!");
                            tSub = @"You have notifications in Tutortree";
                            tMes = [NSString stringWithFormat:@"%@: %@",subject,message];
                        }
                        [self sendEmail:email withSubject:tSub andMessage:tMes andCompletion:^(bool result) {
                            if (result) {
                                if (smsNotifications.boolValue == true) {
                                    [self sendSMS:phone withSubject:tSub andMessage:tMes andCompletion:^(bool result) {
                                        if (result) {
                                            completionHandler(YES);
                                        }
                                    }];
                                } else {
                                    completionHandler(YES);
                                }
                            } else {
                                if (smsNotifications.boolValue == true) {
                                    [self sendSMS:phone withSubject:tSub andMessage:tMes andCompletion:^(bool result) {
                                        if (result) {
                                            completionHandler(YES);
                                        }
                                    }];
                                } else {
                                    completionHandler(YES);
                                }
                            }
                        }];
                    } else {
                        if (smsNotifications.boolValue == true) {
                            NSString *tSub,*tMes;
                            if ([subject isEqualToString:@"New Booking"] || [subject isEqualToString:@"Location Update"] || [subject isEqualToString:@"Booking Confirmed"] || [subject isEqualToString:@"Booking Voided"]) {
                                tSub = subject;
                                tMes = message;
                            } else {
                                NSLog(@"is message!");
                                tSub = @"You have notifications in Tutortree";
                                tMes = [NSString stringWithFormat:@"%@: %@",subject,message];
                            }
                            [self sendSMS:phone withSubject:tSub andMessage:tMes andCompletion:^(bool result) {
                                if (result) {
                                    completionHandler(YES);
                                }
                            }];
                    }
                    }
            }];
            
            
            }];
    }
    }];
}

+(void)sendTestNotification {
    NSString *finalURL;
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sandbox"] == true) {
        NSLog(@"sandbox");
        finalURL = [[[NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendSandboxNotification/%@/%@/%@/%@",@"Test Notification",@"Notifications are working!",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"],@"1"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    } else {
        finalURL = [[[NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendNotification/%@/%@/%@/%@",@"Test Notification",@"Notifications are working!",[[NSUserDefaults standardUserDefaults] objectForKey:@"token"],@"1"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"%@",finalURL);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error");
    }];
}

+(void)sendEmail:(NSString*)email withSubject:(NSString*)subject andMessage:(NSString*)message andCompletion:(void (^)(bool result))completionHandler {
    NSString *url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendEmailTo/%@/%@/%@",email,subject,message];
    NSString *finalURL = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        completionHandler(YES);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionHandler(YES);
    }];
    
}

+(void)sendSMS:(NSString*)number withSubject:(NSString*)subject andMessage:(NSString*)message andCompletion:(void (^)(bool result))completionHandler {
    NSString *url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendSMSTo/%@/%@",[NSString stringWithFormat:@"+1%@",number],[NSString stringWithFormat:@"%@\n%@",subject,message]];
    NSString *finalURL = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        completionHandler(YES);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        completionHandler(YES);
    }];
    
}

+(NSString*)getSchoolDomain {
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"currentDomain"] : @"uoregon";
}

+(void)selectionFeedback:(UIImpactFeedbackStyle)style {
    UIImpactFeedbackGenerator *feedback = [[UIImpactFeedbackGenerator alloc] initWithStyle:style];
    [feedback impactOccurred];
}

+(BOOL)showUser:(NSString*)uid {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sandbox"] == YES) {
        NSLog(@"is sandbox, so getting all tutors");
        return true;
    } else {
        NSString *developer = @"ECW5jdzYRZYNTsyP15A9YnHPHAZ2";
        NSString *rob = @"Y2XF39UWwIZtwpg40AJG8Fuut0E2";
        if (developer.hash == uid.hash || rob.hash == uid.hash) {
            NSLog(@"not sandbox, not getting all tutors");
            return false;
        }
        NSLog(@"not sandbox, so getting all tutors");
        return true;
    }
    
}

+(void)adjustLabelCharacterSpacing:(UILabel *)label withSpacing:(CGFloat)spacing {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:label.text];

    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [label.text length])];
    label.text = @"";
    label.attributedText = attributedString;
}

// DARK MODE

+(UIColor*)accentColor {
    return [self colorFromHexString:@"#4CAF50"];
}

+(UIColor*)background {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [self colorFromHexString:@"#232528"] : [UIColor whiteColor];
}
+(UIColor*)cells {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [self colorFromHexString:@"#212121"] : [self colorFromHexString:@"#EEEEEE"];
}

+(UIColor*)highlight {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [self colorFromHexString:@"#424242"] : [self colorFromHexString:@"#E0E0E0"];
}

+(UIColor*)accent {
    return [self colorFromHexString:@"#4CAF50"];
}
+(UIColor*)primaryText {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor whiteColor] : [UIColor darkTextColor];
}
+(UIColor*)secondaryText {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [self colorFromHexString:@"#E0E0E0"] : [self colorFromHexString:@"#9E9E9E"];
}


+(UIStatusBarStyle)statusBarColor {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? UIStatusBarStyleLightContent : UIStatusBarStyleDefault;
}

+(UIColor*)chatOther {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [self colorFromHexString:@"#424242"] : [self colorFromHexString:@"#F5F5F5"];
}

+(UIColor*)chatSender {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [self colorFromHexString:@"#1B5E20"] : [self colorFromHexString:@"#4CAF50"];
}

+(UIColor*)chatTextOther {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor whiteColor] : [UIColor darkTextColor];
}

+(UIColor*)chatTextSender {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? [UIColor whiteColor] : [UIColor whiteColor];
}

+(UIKeyboardAppearance)keyboard {
    return ([[NSUserDefaults standardUserDefaults] boolForKey:@"dark"] == true) ? UIKeyboardAppearanceDark : UIKeyboardAppearanceLight;
}
@end

