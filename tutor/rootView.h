//
//  rootView.h
//  tutor
//
//  Created by Robert Crosby on 7/26/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import "homeViewController.h"

//#import "getStartedViewController.h"
#import "nGettingStartedViewController.h"
#import <AVFoundation/AVFoundation.h>

@import Firebase;
@import MediaPlayer;
NS_ASSUME_NONNULL_BEGIN

@interface rootView : UIViewController {
    NSMutableArray *shapes;
    CGFloat lastX;
}


@end

NS_ASSUME_NONNULL_END
