//
//  tutorOfficeViewController.m
//  tutor
//
//  Created by Robert Crosby on 5/25/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "tutorOfficeViewController.h"

@interface tutorOfficeViewController ()

@end

@implementation tutorOfficeViewController

-(void)viewWillAppear:(BOOL)animated {
    self.view.backgroundColor = [References background];
}


- (void)viewDidLoad {
//    self.navigationController.interactivePopGestureRecognizer.delegate = self;
//    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    changeMade = NO;
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"]child:@"users"]child:[FIRAuth auth].currentUser.uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            myAccount = snapshot.value;
            NSDictionary *av = myAccount[@"availability"];
            times = [[NSMutableArray alloc] init];
            for (NSNumber *key in av) {
                dayAvailability *tt = [[dayAvailability alloc] initWithValue:key.unsignedLongLongValue];
                [tt printAvailabilityValue];
                [tt printAvailabilityBits];
                [times addObject:tt];
            }
            
        }
        [self createStatic];
        [self getBio];
    }];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)getBio {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"]child:@"users"]child:[FIRAuth auth].currentUser.uid] child:@"bio"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->aboutText = snapshot.value;
            self->bioField.text = self->aboutText;
        } else {
            self->aboutText = @"Describe yourself for students to better understand who you are.";
            self->bioField.text = self->aboutText;
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createStatic {
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin])];
    [References blurView:menuBar];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [References screenWidth]-30, 44)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = @"Your Availability";
    
    header.textColor = [References primaryText];
    header.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:header];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accent] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    [self.view sendSubviewToBack:menuBar];
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, menuBar.frame.size.height, [References screenWidth], [References screenHeight]-menuBar.frame.size.height-(([References screenWidth]-50)/7)-[References bottomMargin]) style:UITableViewStylePlain];
    table.clipsToBounds = false;
    table.backgroundColor = [UIColor clearColor];
    table.allowsSelection = NO;
    table.separatorColor = [UIColor clearColor];
    table.showsHorizontalScrollIndicator = YES;
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    dayWidth = ([References screenWidth]-50)/7;
    UIView *bottomBarDates = [[UIView alloc] initWithFrame:CGRectMake(0, [References screenHeight]-[References bottomMargin]-dayWidth, [References screenHeight], dayWidth+[References bottomMargin])];
    UILabel *bottomBarBlur = [[UILabel alloc] initWithFrame:bottomBarDates.bounds];
    [References blurView:bottomBarBlur];
    [bottomBarDates addSubview:bottomBarBlur];
    NSArray *days = [[NSArray alloc] initWithObjects:@"M",@"T",@"W",@"T",@"F",@"S",@"S", nil];
    for (int a = 0; a < 7; a++) {
        UILabel *day = [[UILabel alloc] initWithFrame:CGRectMake(50+(a*dayWidth), 0, dayWidth, dayWidth)];
        day.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
        day.textColor = [References primaryText];
        day.textAlignment = NSTextAlignmentCenter;
        day.text = days[a];
        [bottomBarDates addSubview:day];
    }
    [References createLine:bottomBarDates xPos:0 yPos:0 inFront:YES];
    [self.view addSubview:bottomBarDates];
    if (myAccount[@"availability"]) {
    } else {
        [self tutorView];
    }
    [self.view bringSubviewToFront:menuBar];
}
-(void)updateBio:(NSString*)bio {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"]child:@"users"]child:[FIRAuth auth].currentUser.uid] child:@"bio"] setValue:bio];
}

-(bool)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        changeMade = YES;
        [UIView animateWithDuration:0.15 animations:^{
            self->saveButton.alpha = 1;
        }];
        return false;
    }
    return true;
}

-(void)updatePPHH:(UIStepper*)stepper {
    if (!changeMade) {
        changeMade = true;
        if (saveButton.alpha == 0) {
            saveButton.alpha = 1;
        }
    }
    PPH = stepper.value;
    pphLabel.text = [NSString stringWithFormat:@"$%li",PPH];
}

-(void)tutorView {
    becomeTutor = [[UIView alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:disclaimer]+4, [References screenWidth], [References screenHeight]-([References getBottomYofView:disclaimer]+17))];
    UILabel *blur = [[UILabel alloc] initWithFrame:becomeTutor.bounds];
    [References blurView:blur];
    [becomeTutor addSubview:blur];
    sad = [[UIImageView alloc] initWithFrame:CGRectMake((becomeTutor.frame.size.width/2)-40, 16, 80,80)];
    [References cornerRadius:sad radius:sad.frame.size.width/2];
    [sad setImage:[[UIImage imageNamed:@"sad.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
    [sad setTintColor:[UIColor darkTextColor]];
    [becomeTutor addSubview:sad];
    codeEntryTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:sad]+8, becomeTutor.frame.size.width-40, 0)];
    codeEntryTitle.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    codeEntryTitle.textColor = [UIColor lightGrayColor];
    codeEntryTitle.textAlignment = NSTextAlignmentCenter;
    codeEntryTitle.text = @"Enter a Tutor Code";
    codeEntryTitle.frame = CGRectMake(codeEntryTitle.frame.origin.x, codeEntryTitle.frame.origin.y, codeEntryTitle.frame.size.width, [References fixedWidthSizeWithFont:codeEntryTitle.font andString:codeEntryTitle.text andLabel:codeEntryTitle].size.height);
    [becomeTutor addSubview:codeEntryTitle];
    codeEntry = [[UITextField alloc] initWithFrame:CGRectMake((becomeTutor.frame.size.width/2)-60, [References getBottomYofView:codeEntryTitle]+16, 120, 44)];
    [codeEntry setAutocorrectionType:UITextAutocorrectionTypeNo];
    [codeEntry setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [codeEntry setBackgroundColor:[UIColor whiteColor]];
    codeEntry.textAlignment = NSTextAlignmentCenter;
    [References cornerRadius:codeEntry radius:codeEntry.frame.size.height/3];
    codeEntry.placeholder = @"ABCDE";
    [codeEntry addTarget:self
                     action:@selector(textFieldDidChange:)
           forControlEvents:UIControlEventEditingChanged];
    [becomeTutor addSubview:codeEntry];
    
    becomeTutor.alpha = 0;
    [self.view addSubview:becomeTutor];
    UIButton *bookButton = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:codeEntry]+15, [References screenWidth]-30, 60)];
    [bookButton setBackgroundColor:[References schoolMainColor]];
    [bookButton setTitle:@"Apply Today" forState:UIControlStateNormal];
    [bookButton addTarget:self action:@selector(apply) forControlEvents:UIControlEventTouchUpInside];
    [References cornerRadius:bookButton radius:20];
    [bookButton.titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightMedium]];
    UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(bookButton.frame.origin.x+5, bookButton.frame.origin.y+5, bookButton.frame.size.width-10, bookButton.frame.size.height-10)];
    shadow.backgroundColor = [UIColor whiteColor];
    [References biggestbottomshadow:shadow];
    [becomeTutor addSubview:shadow];
    [becomeTutor addSubview:bookButton];
    mainScroll.scrollEnabled = NO;
    [UIView animateWithDuration:0.2 animations:^{
        self->becomeTutor.alpha = 1;
    }];
}

-(void)apply {
    SFSafariViewController *safariVC = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.jointutortree.com/tutor-with-us/"]]];
    safariVC.delegate = self;
    safariVC.preferredControlTintColor = [References schoolMainColor];
    [self presentViewController:safariVC animated:YES completion:nil];
}

-(void)safariViewControllerDidFinish:(SFSafariViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)textFieldDidChange:(UITextField*)textField {
    if (textField == name) {
        
        if (saveButton.alpha == 0) {
            updateName = YES;
            changeMade = YES;
            [UIView animateWithDuration:0.15 animations:^{
                saveButton.alpha = 1;
            }];
        }
    } else {
        if (textField.text.length == 5) {
            [self checkCode:textField.text];
        }
    }
    
}

-(void)checkCode:(NSString*)code {
    enteredCode = code;
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"tutorCodes"] child:code] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot *snapshot) {
        if (snapshot.value) {
            if ([snapshot.value isEqual:@0]) {
                [self codeWorked];
            } else {
                [self codeDidntWork];
            }
        }
        
    }];
}

-(void)codeWorked {
    [codeEntry resignFirstResponder];
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:codeEntryTitle]+3, becomeTutor.frame.size.width-30, 0)];
    messageLabel.numberOfLines = 5;
    messageLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightRegular];
    messageLabel.textColor = [UIColor lightGrayColor];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.text = @"Once ready, you'll be able to set your weekly availability and cost per half hour from here. When you're on a course page, you will be able to make yourself available as a tutor for the course. This means that students will be able to schedule you on your time, for courses you choose.";
    messageLabel.frame = CGRectMake(messageLabel.frame.origin.x, messageLabel.frame.origin.y+8, messageLabel.frame.size.width, [References fixedWidthSizeWithFont:messageLabel.font andString:messageLabel.text andLabel:messageLabel].size.height);
    messageLabel.alpha = 0;
    [becomeTutor addSubview:messageLabel];
    [UIView animateWithDuration:0.15 animations:^{
        sad.alpha = 0;
        sad.frame = CGRectMake(sad.frame.origin.x, sad.frame.origin.y-10, sad.frame.size.width, sad.frame.size.height);
        codeEntryTitle.text = @"Filling out some paperwork...";
        codeEntryTitle.frame = CGRectMake(codeEntryTitle.frame.origin.x, codeEntryTitle.frame.origin.y-8, codeEntryTitle.frame.size.width, codeEntryTitle.frame.size.height);
        messageLabel.alpha = 1;
        messageLabel.frame = CGRectMake(messageLabel.frame.origin.x, messageLabel.frame.origin.y-8, messageLabel.frame.size.width, messageLabel.frame.size.height);
        codeEntry.alpha = 0;
        codeEntry.frame = CGRectMake(codeEntry.frame.origin.x, codeEntry.frame.origin.y-10, codeEntry.frame.size.width, codeEntry.frame.size.height);
        
    } completion:^(BOOL finished) {
        if (finished) {
            [self createFirebaseTutor];
        }
    }];
}

-(void)createFirebaseTutor {
    FIRDatabaseReference *ref = [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"]  child:[FIRAuth auth].currentUser.uid];
    NSDictionary *tutorDictionary = @{@"availability":@{@"0":@0,@"1":@0,@"2":@0,@"3":@0,@"4":@0,@"5":@0,@"6":@0},@"PPH": @15, @"tutor":@1};
    [ref updateChildValues:tutorDictionary withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            times = [[NSMutableArray alloc] init];
            for (int a = 0; a < 7; a++) {
                dayAvailability *tt = [[dayAvailability alloc] initWithValue:0];
                [tt printAvailabilityBits];
                [times addObject:tt];
            }
            [UIView animateWithDuration:0.15 animations:^{
                becomeTutor.alpha = 0;
            } completion:^(BOOL finished) {
                if (finished) {
                    mainScroll.scrollEnabled = YES;
                    becomeTutor = nil;
                    [becomeTutor removeFromSuperview];
                    [self useCode];
                }
            }];
        }
    }];
}

-(void)updateLocalDatabase {
    FIRDatabaseReference *reference = [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid];
    [reference observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if ([snapshot hasChildren]) {
            myAccount = snapshot.value;
            [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:@"accountDatabase"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            name.text = snapshot.value[@"name"];
            if ([name.text isEqualToString:@"EMPTY"]) {
                name.text = @"";
                updateName = YES;
            }
            if (myAccount[@"profileURL"]) {
                UIImageView *imageView = [[UIImageView alloc] init];
                [imageView sd_setImageWithURL:[NSURL URLWithString:myAccount[@"profileURL"]] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                    [imageButton setImage:image forState:UIControlStateNormal];
                    chosenImage = image;
                }];
            } else {
                [imageButton setTitle:@"+" forState:UIControlStateNormal];
                chosenImage = nil;
            }
            [table reloadData];
            
            }
        }
    ];
}

-(void)useCode {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"tutorCodes"] child:enteredCode] setValue:nil];
}

-(void)codeDidntWork {
    NSLog(@"code bad");
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)close {
//    if ((name.text.length == 0 || !chosenImage) && myAccount[@"tutor"]) {
//        [References titleToastMessage:@"Make sure you have your name and profile picture set." andTitle:@"Need Information" andView:self andClose:NO];
//    } else {
//        if (changeMade || uploadImage) {
//            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Changes Made"
//               message:@"You have unsaved changes. Would you like to leave without saving or save them now?" preferredStyle:UIAlertControllerStyleAlert];
//            UIAlertAction* save = [UIAlertAction actionWithTitle:@"Save and Exit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                [self saveChanges:nil andExit:YES];
//            }];
//            UIAlertAction* dontsave = [UIAlertAction actionWithTitle:@"Exit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//                self->mainScroll.clipsToBounds = YES;
//                [self dismissViewControllerAnimated:YES completion:nil];
////                [self.navigationController popViewControllerAnimated:YES];
//            }];
//            UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {}];
//            [alert addAction:save];
//            [alert addAction:dontsave];
//            [alert addAction:cancel];
//            [self presentViewController:alert animated:YES completion:nil];
//        } else {
            mainScroll.clipsToBounds = YES;
//            [self.navigationController popViewControllerAnimated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
//        }

    
}

-(void)getPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Profile Picture"
                                                                   message:@"Please find a good picture that will make it easy for others to recognize you."
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* select = [UIAlertAction actionWithTitle:@"Select a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* take = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {}];
    
    [alert addAction:select];
    [alert addAction:take];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    uploadImage = YES;
    chosenImage = info[UIImagePickerControllerEditedImage];
    [imageButton setBackgroundImage:chosenImage forState:UIControlStateNormal];
    [self uploadChosenImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%licell",indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    cell.clipsToBounds = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:[NSString stringWithFormat:@"%licell",indexPath.row]];
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 30)];
    view.backgroundColor = [References background];
    view.clipsToBounds = NO;
    UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(0,0, 50, 8)];
    time.textAlignment = NSTextAlignmentRight;
    time.font = [UIFont systemFontOfSize:10 weight:UIFontWeightSemibold];
    time.textColor = [References colorFromHexString:@"#E0E0E0"];
    [References createLine:view xPos:54 yPos:4 inFront:NO];
    if (indexPath.row % 2 == 0) {
        NSInteger currentTime = (30 * indexPath.row);
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setMinute:currentTime];
        NSDate* date = [[NSCalendar currentCalendar] dateFromComponents:comps];
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"h a"];
        time.text = [NSString stringWithFormat:@"%@",[format stringFromDate:date]];
    }
    [view addSubview:time];
    view.tag = indexPath.row;
    for (int a = 0; a < 7; a++) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake((54+(dayWidth*a))+2, 4, dayWidth-4, view.frame.size.height-4)];
        [button addTarget:self action:@selector(setTime:) forControlEvents:UIControlEventTouchUpInside];
        [button setTag:a];
        dayAvailability *time = times[a];
        if ([time getBitAtIndex:(int)indexPath.row]) {
            button.backgroundColor = [References accent];
//            if ([(NSString*)times[a] characterAtIndex:indexPath.row] == '1') {
//
//            } else {
//                button.backgroundColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.4];
//            }
//
        } else {
            button.backgroundColor = [References cells];
        }
        [view addSubview:button];
    }
    
    [cell addSubview:view];
    return cell;
}

-(void)setTime:(UIButton*)sender {
    NSInteger halfHour = sender.superview.tag;
    NSString *newNumber;
    dayAvailability *time = times[sender.tag];
    if (![time getBitAtIndex:(int)halfHour]) {
        sender.backgroundColor = [References accent];
    } else {
        sender.backgroundColor = [References cells];
        newNumber = @"0";
    }
    [time flipBitAtIndex:(int)halfHour];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] child:@"availability"] child:[NSString stringWithFormat:@"%i",(int)sender.tag]] setValue:[NSNumber numberWithUnsignedLongLong:[time getValue]] withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            [MBProgressHUD hideHUDForView:self.view animated:true];
            self->times[sender.tag] = time;
        }
    }];
//    NSMutableArray *newAvailability = [[NSMutableArray alloc] init];
//    for (dayAvailability *day in times) {
//        NSLog(@"VAL:%llu",[day getValue]);
//        [newAvailability addObject:[NSNumber numberWithUnsignedLongLong:[day getValue]]];
//    }
//    NSMutableDictionary *dic = [myAccount mutableCopy];
//    [dic setValue:newAvailability forKey:@"availability"];
//    myAccount = dic;
//    if (!cantChange) {
//        times[day] = [(NSString*)times[day] stringByReplacingCharactersInRange:NSMakeRange(halfHour, 1) withString:newNumber];
//        NSMutableDictionary *dic = [myAccount mutableCopy];
//        [dic setValue:times forKey:@"availability"];
//        myAccount = dic;
//        times = nil;
//    } else {
//        [References toastNotificationBottom:self.view andMessage:@"This time slot is already booked."];
//    }

}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 48;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

-(void)uploadChosenImage {
    __block UIView *uppingimage = [References loadingView:@"Uploading Image..."];
    [self.view addSubview:uppingimage];
    [UIView animateWithDuration:0.25 animations:^{
        uppingimage.alpha = 1;
    } completion:^(BOOL finished) {
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *storageRef = [storage reference];
        NSString *imgName = [References randomStringWithLength:16];
        FIRStorageReference *profileRep = [storageRef child:[NSString stringWithFormat:@"images/%@.jpg",imgName]];
        
        NSData *data = UIImageJPEGRepresentation(chosenImage, 1);
        [profileRep putData:data
                   metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error) {
                       if (error != nil) {
                           [References toastNotificationBottom:self.view andMessage:error.localizedDescription];
                       } else {
                           [UIView animateWithDuration:0.25 animations:^{
                               uppingimage.alpha = 0;
                           } completion:^(BOOL finished) {
                               if (finished) {
                                   [uppingimage removeFromSuperview];
                                   uppingimage = nil;
                                   [profileRep downloadURLWithCompletion:^(NSURL * _Nullable URL, NSError * _Nullable error) {
                                       [self saveChanges:URL.absoluteString andExit:NO];
                                   }];
                                   
                               }
                           }];
                           
                       }
                   }];
    }];
    
}

-(void)saveAccount {
    if (updateName == YES || uploadImage == YES) {
        if (name.text.length == 0) {
            [References toastNotificationBottom:self.view andMessage:@"Please fill out your name"];
        } else {
            [self uploadChosenImage];
        }
    } else {
        [self saveChanges:nil andExit:NO];
    }
    
}

-(void)saveChanges:(NSString*)downloadURL andExit:(BOOL)exit{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    FIRDatabaseReference *ref = [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"]  child:[FIRAuth auth].currentUser.uid] child:@"availability"];
    [ref updateChildValues:myAccount[@"availability"] withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            FIRDatabaseReference *reference = [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid];
            [reference observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if ([snapshot hasChildren]) {
                    [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:@"accountDatabase"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    [UIView animateWithDuration:0.25 animations:^{
                        saveButton.alpha = 0;
                        changeMade = NO;
                        uploadImage = NO;
                        updateName = NO;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                            if (exit) {
                                mainScroll.clipsToBounds = YES;
//                                [self.navigationController popViewControllerAnimated:YES];
                                [self dismissViewControllerAnimated:YES completion:nil];
                            }
                        }
                    }];
                }
            }
             ];
        }
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return [References statusBarColor];
}
@end


