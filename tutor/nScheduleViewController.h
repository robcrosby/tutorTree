//
//  nScheduleViewController.h
//  tutor
//
//  Created by Robert Crosby on 5/8/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.

#import <UIKit/UIKit.h>
#import "tutorAvailability.h"
#import "courseDatabase.h"

NS_ASSUME_NONNULL_BEGIN

@interface nScheduleViewController : UIViewController {
    NSMutableArray *tutors;
}

-(id)initWithCourse:(NSString *)tcourse andTutors:(NSArray *)ttutors andCourse:(courseDatabase*)tCourseD;

@end

NS_ASSUME_NONNULL_END
