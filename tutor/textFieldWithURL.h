//
//  textFieldWithURL.h
//  tutor
//
//  Created by Robert Crosby on 12/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface textFieldWithURL : UITextField {
    NSString *url;
}

-(id)initWithURL:(NSString*)tUrl;
-(NSString*)getURL;

@end

NS_ASSUME_NONNULL_END
