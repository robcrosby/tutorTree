//
//  tutorAvailability.m
//  tutor
//
//  Created by Robert Crosby on 11/4/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#define MSB 0x8000000000000000
#import "tutorAvailability.h"

@implementation tutorAvailability

-(id)initWithID:(NSString*)ident andAvailability:(NSArray*)avail andSessions:(NSDictionary*)sessions andName:(NSString*)tname andURL:(NSString*)turl andPPH:(NSNumber*)number andToken:(NSString*)ttoken{
    self = [super init];
    if (self) {
        cost = number;
        token = ttoken;
        name = tname;
        url = turl;
        identifier = ident;
        numberAvailability = [[NSMutableArray alloc] init];
        for (NSNumber* ava in avail) {
            [numberAvailability addObject:ava];
        }
        NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
        [components setHour: 0];
        [components setMinute: 0];
        [components setSecond: 0];
        double timeFrame = [[[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components] timeIntervalSince1970];
        @try {
            for (NSInteger a = [[(NSDictionary*)sessions allKeys] count]-1; a >= 0; a--) {
                NSDictionary *dict = [sessions objectForKey:[[(NSDictionary*)sessions allKeys] objectAtIndex:a]];
                if ([(NSNumber*)[dict valueForKey:@"start"] doubleValue] > timeFrame) {
                    if ((!([dict objectForKey:@"status"])) || (([dict objectForKey:@"status"]) && ([[dict objectForKey:@"status"] isEqualToNumber:@1]))) {
                        double start = [(NSNumber*)[dict valueForKey:@"start"] doubleValue];
                        double finish = [(NSNumber*)[dict valueForKey:@"end"] doubleValue];
                        NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:start];
                        double duration = ([[NSDate dateWithTimeIntervalSince1970:finish] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:start]]/60)/60;
                        NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: startDate];
                        NSInteger hour = [components hour]*2;
                        NSCalendar* cal = [NSCalendar currentCalendar];
                        NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:startDate];
                        NSInteger weekday = [comp weekday];
                        if (weekday == 1) {
                            weekday = 6;
                        } else {
                            weekday-=2;
                        }
                        if ([components minute] == 30) {
                            hour+=1;
                        }
                        NSNumber *tAv = numberAvailability[weekday];
                        uint64_t og = tAv.unsignedLongLongValue;
                        while (duration > 0) {
                            uint64_t msbAtIndex = MSB;
                            msbAtIndex = msbAtIndex >> hour;
                            msbAtIndex = ~msbAtIndex;
                            og = og & msbAtIndex;
                            hour+=1;
                            duration = duration - 0.5;
                        }
                        numberAvailability[weekday] = [NSNumber numberWithUnsignedLongLong:og];
                    }
                    
                }
            }
        
        } @catch (NSException *exception) {
            NSLog(@"No Sessions");
        } @finally {
            [self process];
        }
    }
    return self;
}

-(void)process {
    dictionaryAvailability = [[NSMutableArray alloc] init];
    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
    [components setHour: 0];
    [components setMinute: 0];
    [components setSecond: 0];
    NSDate *tDate = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components];
//    NSLog(@"%@",tDate);
    NSMutableArray *days = [[NSMutableArray alloc] init];
    for (int a = 0; a < 7; a++) {
        [days addObject:tDate];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 1;
        tDate = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:tDate options:0];
    }
    for (int a = 0; a < 7; a++) {
        NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: (NSDate*)days[a]];
        NSInteger weekday = [components weekday];
        if (weekday == 1) {
            weekday = 6;
        } else {
            weekday-=2;
        }
        NSInteger earliestAvailability = 48;
        if ([(NSNumber*)numberAvailability[weekday] unsignedLongLongValue] != 0) {
            NSDate *base = [NSDate dateWithTimeInterval:0 sinceDate:(NSDate*)days[a]];
            NSMutableArray *tAvailable = [[NSMutableArray alloc] init];
            for (int b = 0; b < 48; b++) {
                if ([self getAvailabilityForDay:weekday atIndex:b]) {
                    if (b < earliestAvailability) {
                        earliestAvailability = b;
                    }
                    NSInteger maxDuration = 1;
                    for (int c = b+1; c < 48; c++) {
                        if ([self getAvailabilityForDay:weekday atIndex:c]) {
                            maxDuration = maxDuration + 1;
                        } else {
                            break;
                        }
                    }
                    [tAvailable addObject:@{@"date":base,@"duration":[NSNumber numberWithInteger:maxDuration]}];
                } else {
                    [tAvailable addObject:@{@"date":base,@"duration":[NSNumber numberWithInteger:0]}];
                }
                base = [base dateByAddingTimeInterval:1800];
            }
            [dictionaryAvailability addObject:@{@"date":(NSDate*)days[a],@"available":tAvailable}];
        } else {
            [dictionaryAvailability addObject:@{@"date":(NSDate*)days[a],@"available":@"NO"}];
        }
    }
}

-(BOOL)getAvailabilityForDay:(NSInteger)weekDay atIndex:(NSInteger)index {
    NSNumber *tAv = numberAvailability[weekDay];
    uint64_t tempAvailability = tAv.unsignedLongLongValue;
    uint64_t msbAtIndex = MSB;
    msbAtIndex = msbAtIndex >> index;
    uint64_t result = msbAtIndex & tempAvailability;
    if (result != 0) {
        return YES;
    }
    return NO;
}

-(BOOL)checkAvailabilityonWeekday:(NSInteger)weekday andTime:(NSInteger)time {
    NSDictionary *dict = dictionaryAvailability[weekday];
    if (time == 48) {
        if ([[dict objectForKey:@"available"] isKindOfClass:[NSString class]]) {
            return NO;
        } else {
            return YES;
        }
    } else {
        if ([[dict objectForKey:@"available"] isKindOfClass:[NSString class]]) {
            return NO;
        } else {
            NSArray *times = [dict objectForKey:@"available"];
            if ([[times[time] objectForKey:@"duration"] isEqual:@0]) {
                return NO;
            } else {
                return YES;
            }
        }
    }
    return NO;
}

-(NSInteger)earliestAvailabilityOn:(NSInteger)weekday {
    NSDictionary *dict = dictionaryAvailability[weekday];
    for (int a = 0; a < 48; a++) {
        if ([[dict objectForKey:@"available"] isKindOfClass:[NSString class]]) {
            return 48;
        } else {
            NSArray *times = [dict objectForKey:@"available"];
            if (![[times[a] objectForKey:@"duration"] isEqual:@0]) {
                return a;
            }
        }
    }
    return 48;
}

-(NSDictionary*)availabilityOnWeekday:(NSInteger)weekday andTime:(NSInteger)time {
    return [[dictionaryAvailability[weekday] objectForKey:@"available"] objectAtIndex:time];
}

-(NSString*)identifier {
    return identifier;
}
-(NSString*)name {
    return name;
}
-(NSURL*)url {
    return [NSURL URLWithString:url];
}

-(NSNumber*)pph {
    return cost;
}

-(NSString*)token {
    return token;
}

+(BOOL)verifyAvailability:(NSNumber*)availability andSessions:(NSDictionary*)sessions onWeekday:(NSInteger)tweekday withTime:(NSInteger)time andDurattion:(NSInteger)duration withMaxWeekly:(NSNumber*)maxWeekly {
    double maxWeeklyDouble = maxWeekly.doubleValue * 60;
    NSNumber *tAvailability = availability;
        NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
        [components setHour: 0];
        [components setMinute: 0];
        [components setSecond: 0];
        double timeFrame = [[[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components] timeIntervalSince1970];
        double scheduledAlready = 0;
        for (NSInteger a = [[(NSDictionary*)sessions allKeys] count]-1; a >= 0; a--) {
            NSDictionary *dict = [sessions objectForKey:[[(NSDictionary*)sessions allKeys] objectAtIndex:a]];
            if ([(NSNumber*)[dict valueForKey:@"start"] doubleValue] > timeFrame) {
                if ((!([dict objectForKey:@"status"])) || (([dict objectForKey:@"status"]) && ([[dict objectForKey:@"status"] isEqualToNumber:@1]))) {
                double start = [(NSNumber*)[dict valueForKey:@"start"] doubleValue];
                double finish = [(NSNumber*)[dict valueForKey:@"end"] doubleValue];
                NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:start];
                double duration = ([[NSDate dateWithTimeIntervalSince1970:finish] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:start]]/60)/60;
                scheduledAlready = scheduledAlready + (duration * 60);
                    NSLog(@"ALREADY SCHEDULED: %f",scheduledAlready);
                NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: startDate];
                NSInteger hour = [components hour]*2;
                NSCalendar* cal = [NSCalendar currentCalendar];
                NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:startDate];
                NSInteger weekday = [comp weekday];
                if (weekday == 1) {
                    weekday = 6;
                } else {
                    weekday-=2;
                }
                if ([components minute] == 30) {
                    hour+=1;
                }
                if (weekday == tweekday) {
                    NSNumber *tAv = tAvailability;
                    uint64_t og = tAv.unsignedLongLongValue;
                    while (duration > 0) {
                        uint64_t msbAtIndex = MSB;
                        msbAtIndex = msbAtIndex >> hour;
                        msbAtIndex = ~msbAtIndex;
                        og = og & msbAtIndex;
                        hour+=1;
                        duration = duration - 0.5;
                    }
                    tAvailability = [NSNumber numberWithUnsignedLongLong:og];
                }
            }
            }
        }
        if (scheduledAlready >= maxWeeklyDouble) {
            return NO;
        }
        if ([tAvailability unsignedLongLongValue] != 0) {
            for (NSInteger a = time; a < time + duration; a++) {
                NSLog(@"CHECKING TIME: %li",a);
                NSNumber *tAv = tAvailability;
                uint64_t tempAvailability = tAv.unsignedLongLongValue;
                uint64_t msbAtIndex = MSB;
                msbAtIndex = msbAtIndex >> a;
                uint64_t result = msbAtIndex & tempAvailability;
                if (result == 0) {
                    return NO;
                }
            }
            return YES;
        }
        return NO;
}

@end
