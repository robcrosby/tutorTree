//
//  connectionViewController.h
//  tutor
//
//  Created by Robert Crosby on 10/11/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import "popoverView.h"
#import <HCSStarRatingView/HCSStarRatingView.h>

#import <MBProgressHUD.h>
#import "connectionObject.h"

@import MapKit;

NS_ASSUME_NONNULL_BEGIN

@interface connectionViewController : UIViewController <UITabBarDelegate,UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource> {
    UITableView *table;
    UIView *menubar;
    UIView *bottomBar;
    UITabBar *tabbar;
    UITextField *message;
    UIButton *send,*more;
    CGFloat keyboardHeight;
    popoverView *detailsView;
    UITextField *location;
    UIView *ratingView;
    HCSStarRatingView *ratingStars;
    NSString *meetingPoint;
    CGFloat interval;
    BOOL amTutor;
    UIView *meetingBar;
    UILabel *meetingLabel;
}

@property (strong, nonatomic) connectionObject *connection;

@end

NS_ASSUME_NONNULL_END
