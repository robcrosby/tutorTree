//
//  courseDatabase.h
//  tutor
//
//  Created by Robert Crosby on 5/11/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "References.h"


@import Firebase;

@interface courseDatabase : NSObject {
    bool isMyCourse;
    NSInteger tutorCount;
    NSString *courseNumber;
    UILabel *notificationLabel;
    NSString *courseDepartment;
    NSString *courseInfo;
    NSMutableArray *tutors;
    NSMutableArray *studentIDs;
    BOOL isHead;
}

@property (strong, nonatomic) courseDatabase *next;
@property (strong, nonatomic) courseDatabase *parent;

-(id)initAsHead;
-(id)initWithDatabase:(NSDictionary*)course andName:(NSString*)name andDeparment:(NSString*)department;
+(instancetype)databaseWithDictionary:(NSDictionary*)course andName:(NSString*)name andDeparment:(NSString*)department;
-(bool)insertCourse:(courseDatabase*)course;
-(bool)amTutor;
-(BOOL)addTutor;
-(BOOL)removeTutor;
-(BOOL)isHead;
-(UIView*)notificationLabel:(UIView*)inView;
-(NSArray*)tutors;
-(NSInteger)numberOfTutors;
-(NSString*)tutorAtIndex:(int)index;
-(NSInteger)courseNumber;
-(NSString*)courseNumberString;
-(NSString*)courseDepartment;
-(NSString*)courseDepartmentAndNumber;
-(NSString*)printCourse;
@end
