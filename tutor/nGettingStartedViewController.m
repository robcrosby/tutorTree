//
//  nGettingStartedViewController.m
//  tutor
//
//  Created by Robert Crosby on 1/31/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "nGettingStartedViewController.h"

@interface nGettingStartedViewController ()

@end

@implementation nGettingStartedViewController

-(void)supportedDomains {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"supported"] child:@"description"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Supported Domains" message:snapshot.value preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
    }];
}

-(UIView*)signInView {
    UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+50, [References screenWidth]-30, 0)];
    title.font = [UIFont systemFontOfSize:44 weight:UIFontWeightHeavy];
    title.textColor = [UIColor darkTextColor];
    title.text = @"Sign In";
    title.textAlignment = NSTextAlignmentCenter;
    title.frame = CGRectMake(title.frame.origin.x, title.frame.origin.y, title.frame.size.width, [References fixedWidthSizeWithFont:title.font andString:title.text andLabel:title].size.height);
    [view addSubview:title];
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:title]+5, [References screenWidth]-40, 0)];
    description.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    description.textColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.45];
    description.text = @"Sign in with a supported University email.";
    description.textAlignment = NSTextAlignmentCenter;
    description.numberOfLines = 0;
    description.frame = CGRectMake(description.frame.origin.x, description.frame.origin.y, description.frame.size.width, [References fixedWidthSizeWithFont:description.font andString:description.text andLabel:description].size.height);
    emailReturning = [[UITextField alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:description]+10, [References screenWidth]-30, 54)];
    emailReturning.autocorrectionType = UITextAutocorrectionTypeNo;
    emailReturning.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailReturning.font = [UIFont systemFontOfSize:20 weight:UIFontWeightSemibold];
    emailReturning.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    emailReturning.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, emailReturning.frame.size.height)];
    emailReturning.leftViewMode = UITextFieldViewModeAlways;
    emailReturning.delegate = self;
    [References cornerRadius:emailReturning radius:emailReturning.frame.size.height/2];
    emailReturning.keyboardType = UIKeyboardTypeEmailAddress;
    emailReturning.placeholder = @"ex. jappleseed@uoregon.edu";
    [view addSubview:emailReturning];
    passwordReturning = [[UITextField alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:emailReturning]+10, [References screenWidth]-30, 54)];
    passwordReturning.autocorrectionType = UITextAutocorrectionTypeNo;
    passwordReturning.font = [UIFont systemFontOfSize:20 weight:UIFontWeightSemibold];
    passwordReturning.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    passwordReturning.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, emailReturning.frame.size.height)];
    passwordReturning.leftViewMode = UITextFieldViewModeAlways;
    passwordReturning.delegate = self;
    [References cornerRadius:passwordReturning radius:passwordReturning.frame.size.height/2];
    passwordReturning.keyboardType = UIKeyboardTypeEmailAddress;
    passwordReturning.placeholder = @"•••••••";
    passwordReturning.secureTextEntry = true;
    [view addSubview:passwordReturning];
    UIButton *completeSignUp = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:passwordReturning]+10, [References screenWidth]-30, 60)];
    [completeSignUp.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightHeavy]];
    [completeSignUp setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [completeSignUp setTitle:@"Sign In" forState:UIControlStateNormal];
    [completeSignUp setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [completeSignUp addTarget:self action:@selector(completeSignIn) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:completeSignUp];
    view.frame = CGRectMake(view.frame.origin.x, [References screenHeight]*1, view.frame.size.width, view.frame.size.height);
    scroll.contentSize = CGSizeMake([References screenWidth], [References screenHeight]*2);
    return view;
}


- (void)viewDidLoad {
    pushSwitch = [[UISwitch alloc] init];
    smsSwitch = [[UISwitch alloc] init];
    emailSwitch = [[UISwitch alloc] init];
    tosSwitch = [[UISwitch alloc] init];
    currentPage = 0;
    self.view.backgroundColor = [UIColor whiteColor];
    scroll = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [scroll setContentInsetAdjustmentBehavior:UIScrollViewContentInsetAdjustmentNever];
    scroll.showsVerticalScrollIndicator = false;
    scroll.pagingEnabled = true;
    scroll.scrollEnabled = false;
    [self.view addSubview:scroll];
    [super viewDidLoad];
    [scroll addSubview:[self createIntroScreen]];
    // Do any additional setup after loading the view.
}

-(void)returningUser {
    [scroll addSubview:[self signInView]];
    [self makeBottomBar:true];
}


-(void)newUser {
    email = [self inputTextFieldKeyboard:UIKeyboardTypeEmailAddress withPlaceholder:@"jappleseed"];
    email.autocorrectionType = UITextAutocorrectionTypeNo;
    email.autocapitalizationType = UITextAutocapitalizationTypeNone;
    email.enabled = false;
    email.alpha = 0.5;
    [scroll addSubview:[self createTextInputScreen:email andPage:1 withTitleDescription:@{@"Your Email":@"Please select your university and enter your university username to get started."} isEmail:true]];
    name = [self inputTextFieldKeyboard:UIKeyboardTypeDefault withPlaceholder:@"ex. John Appleseed"];
    [scroll addSubview:[self createTextInputScreen:name andPage:2 withTitleDescription:@{@"Your Name":@"Please enter your preferred name for interacting with other users."} isEmail:false]];
    phone = [self inputTextFieldKeyboard:UIKeyboardTypeNumbersAndPunctuation withPlaceholder:@"ex. 5418675309"];
    [scroll addSubview:[self createTextInputScreen:phone andPage:3 withTitleDescription:@{@"Your Phone":@"Please enter your phone number for interacting with other users. You will not recieve SMS notifications unless you opt-in to recieve them."} isEmail:false]];
    picture = [[UIButton alloc] init];
    [picture setTitle:@"Select Photo" forState:UIControlStateNormal];
    [picture addTarget:self action:@selector(getPhoto) forControlEvents:UIControlEventTouchUpInside];
    [scroll addSubview:[self createButtonInputScreen:picture OnPage:4 withTitleDescription:@{@"Your Look":@"Please select a good picture of yourself so your peers can recognize you when you meet for tutoring."}]];
    [self createNotificationsScreenAsPage:5];
//    emailNotifications = [[UIButton alloc] init];
//    [emailNotifications setTitle:@"Important Emails" forState:UIControlStateNormal];
//    [emailNotifications addTarget:self action:@selector(optForEmail) forControlEvents:UIControlEventTouchUpInside];
//    [scroll addSubview:[self createButtonInputScreen:emailNotifications OnPage:5 withTitleDescription:@{@"More Emails?":@"You will recieve emails for information regarding bookings, opting in for more emails will notifiy you when you recieve other notification types such as location updates and messages from users. This can be toggled later."}]];
//    smsNotifications = [[UIButton alloc] init];
//    [smsNotifications setTitle:@"No SMS" forState:UIControlStateNormal];
//    [smsNotifications addTarget:self action:@selector(optForSMS) forControlEvents:UIControlEventTouchUpInside];
//    [scroll addSubview:[self createButtonInputScreen:smsNotifications OnPage:6 withTitleDescription:@{@"SMS?":@"Opting in for SMS notifications will send you notifcations for bookings, session updates and messages. This can be toggled later."}]];
//    pushNotifications = [[UIButton alloc] init];
//    [pushNotifications setTitle:@"Recieve Notifications" forState:UIControlStateNormal];
//    [pushNotifications addTarget:self action:@selector(optForPush) forControlEvents:UIControlEventTouchUpInside];
//    [scroll addSubview:[self createButtonInputScreen:pushNotifications OnPage:7 withTitleDescription:@{@"Pushes?":@"Your device will recieve native push notfications bookings, session updates and messages. This can be toggled later."}]];
    [self makeBottomBar:false];
    scroll.contentSize = CGSizeMake([References screenWidth], [References screenHeight]*5);
}

-(void)goBack:(UIButton*)sender {
    if (activeTF) {
        [activeTF resignFirstResponder];
    }
    if (completeSignUpView) {
        [self backFromSignUp];
        sender.enabled = true;
    } else {
        sender.enabled = false;
        [UIView animateWithDuration:0.25 animations:^{
            [self->scroll setContentOffset:CGPointMake(0, self->scroll.contentOffset.y > 0 ? self->scroll.contentOffset.y-[References screenHeight] : 0)];
        } completion:^(BOOL finished) {
            if (finished) {
                if (self->scroll.contentOffset.y == 0) {
                    [self resetView];
                }
                sender.enabled = true;
            }
        }];
    }
    
}

-(void)resetView {
    if (activeTF) {
        [activeTF resignFirstResponder];
    }
    for (UIView* view in scroll.subviews) {
        if (![view isEqual:introView]) {
            for (UIView *subview in view.subviews) {
                [subview removeFromSuperview];
            }
            [view removeFromSuperview];
        }
    }
    [UIView animateWithDuration:0.25 animations:^{
        self->bottomBar.frame = CGRectMake(0, [References screenHeight], self->bottomBar.frame.size.width, self->bottomBar.frame.size.height);
    } completion:^(BOOL finished) {
        if (finished) {
            self->bottomBar = nil;
        }
    }];
}

-(void)goToPage:(int)page button:(UIButton*)sender {
    if (activeTF) {
        [activeTF resignFirstResponder];
    }
    [UIView animateWithDuration:0.25 animations:^{
        [self->scroll setContentOffset:CGPointMake(0, [References screenHeight]*page)];
    } completion:^(BOOL finished) {
        if (finished) {
            sender.enabled = true;
        }
    }];
}

-(void)goNext:(UIButton*)sender {
    if (activeTF) {
        [activeTF resignFirstResponder];
    }
    if ([emailString containsString:@"@uoregon.edu"] || [emailString containsString:@"oregonstate.edu"] || [emailString containsString:@"@ucsb.edu"]) {
        sender.enabled = false;
        if (self->scroll.contentOffset.y < self->scroll.contentSize.height) {
            [UIView animateWithDuration:0.25 animations:^{
                [self->scroll setContentOffset:CGPointMake(0, self->scroll.contentOffset.y+[References screenHeight])];
            } completion:^(BOOL finished) {
                if (finished) {
                    sender.enabled = true;
                }
            }];
        } else {
            if (emailString.length == 0) {
                [self goToPage:1 button:sender];
            } else if (name.text.length == 0) {
                [self goToPage:2 button:sender];
            } else if (phone.text.length == 0) {
                [self goToPage:3 button:sender];
            } else if (pictureURL.length == 0) {
                [self goToPage:4 button:sender];
            } else if (!tosSwitch.isOn) {
                [self goToPage:5 button:sender];
                [References fullScreenToast:@"You must accept the Terms of Service" inView:self withSuccess:false andClose:false];
            } else if ([emailString containsString:@"@uoregon.edu"] || [emailString containsString:@"@my.lanecc.edu"] || [emailString containsString:@"@ucsc.edu"] || [emailString containsString:@"oregonstate.edu"] || [emailString containsString:@"@ucsb.edu"]) {
                [self completeSignUpView];
            } else {
                [self goToPage:1 button:sender];
            }
        }

    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unsupported University" message:@"Currently Tutortree is only available at The University of Oregon and Oregon State University" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
        
    }
    
    
}

-(void)makeBottomBar:(bool)returning {
    bottomBar = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [References screenHeight], [References screenWidth], [References bottomMargin]+44+10)];
    bottomBar.clipsToBounds = false;
    bottomBar.backgroundColor = [UIColor whiteColor];
    [References createLine:bottomBar xPos:0 yPos:0 inFront:true];
//    UIView *card = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight])];
//    card.backgroundColor = [References accent];
//    [References topshadow:card];
//    [bottomBar addSubview:card];
    if (!returning) {
        backPage = [[UIButton alloc] initWithFrame:CGRectMake(15, 5, (([References screenWidth]-30-10)/2), 44)];
        backPage.backgroundColor = [References colorFromHexString:@"#F5F5F5"];
        [References cornerRadius:backPage radius:5];
        [backPage setTitle:@"Back" forState:UIControlStateNormal];
        [backPage setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [backPage addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
//        UIImageView *upV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"down.png"]];
//        upV.frame = CGRectMake(20, 20, backPage.frame.size.width-40, backPage.frame.size.height-40);
//        [References tintUIImage:upV color:[References colorFromHexString:@"#9E9E9E"]];
//        upV.transform = CGAffineTransformMakeRotation(M_PI);
//        [backPage addSubview:upV];
        [bottomBar addSubview:backPage];
        nextPage = [[UIButton alloc] initWithFrame:CGRectMake(15+(([References screenWidth]-30-10)/2)+5, 5, (([References screenWidth]-30-10)/2), 44)];
        nextPage.backgroundColor = [References colorFromHexString:@"#F5F5F5"];
        [nextPage setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [nextPage setTitle:@"Next" forState:UIControlStateNormal];
        [References cornerRadius:nextPage radius:5];
        [nextPage addTarget:self action:@selector(goNext:) forControlEvents:UIControlEventTouchUpInside];
//        UIImageView *downV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"down.png"]];
//        downV.frame = CGRectMake(20, 20, nextPage.frame.size.width-40, nextPage.frame.size.height-40);
//        [References tintUIImage:downV color:[References colorFromHexString:@"#9E9E9E"]];
//        [nextPage addSubview:downV];
        [bottomBar addSubview:nextPage];
    } else {
        backPage = [[UIButton alloc] initWithFrame:CGRectMake(15, 5, ([References screenWidth]-30), 44)];
        backPage.backgroundColor = [References colorFromHexString:@"#F5F5F5"];
        [References cornerRadius:backPage radius:5];
        [backPage setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
        [backPage setTitle:@"Back" forState:UIControlStateNormal];
        [backPage addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
        [bottomBar addSubview:backPage];
    }
    
    [self.view addSubview:bottomBar];
    [UIView animateWithDuration:0.35 animations:^{
        [self->scroll setContentOffset:CGPointMake(0, [References screenHeight])];
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.25 delay:0.15 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self->bottomBar.frame = CGRectMake(0, [References screenHeight]-self->bottomBar.frame.size.height, self->bottomBar.frame.size.width, self->bottomBar.frame.size.height);
            } completion:^(BOOL finished) {
                if (finished) {
                    
                }
            }];
        }
    }];
    
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([string isEqualToString:@"@"] && [textField isEqual:email]) {
        [textField resignFirstResponder];
        return false;
    }
    return true;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == email) {
        emailString = [NSString stringWithFormat:@"%@%@",textField.text,emailRightField.text];
    }
    activeTF = nil;
    [textField resignFirstResponder];
    return true;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    activeTF = textField;
    return true;
}

-(void)setActiveButton:(UIButton*)sender {
    activeBT = sender;
}

-(void)dismissActiveKeyboard {
    [activeTF resignFirstResponder];
    activeTF = nil;
}

-(UITextField*)inputTextFieldKeyboard:(UIKeyboardType)kType withPlaceholder:(NSString*)placeholder {
    UITextField *tf = [[UITextField alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 54)];
    tf.font = [UIFont systemFontOfSize:20 weight:UIFontWeightSemibold];
    tf.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    tf.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, tf.frame.size.height)];
    tf.leftViewMode = UITextFieldViewModeAlways;
    tf.delegate = self;
    [References cornerRadius:tf radius:tf.frame.size.height/2];
    tf.keyboardType = kType;
    tf.placeholder = placeholder;
    return tf;
}

-(UIView*)createIntroScreen {
    introView = [[UIView alloc] initWithFrame:self.view.bounds];
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"invert-icon.png"]];
    [References tintUIImage:logo color:[References darkText]];
    logo.frame = CGRectMake([References screenWidth]/2-(30), [References topMargin]+30, 60, 60);
    [introView addSubview:logo];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+100, [References screenWidth]-30, 0)];
    title.font = [UIFont systemFontOfSize:44 weight:UIFontWeightHeavy];
    title.textColor = [UIColor darkTextColor];
    title.text = @"Tutortree";
    title.textAlignment = NSTextAlignmentCenter;
    title.frame = CGRectMake(title.frame.origin.x, title.frame.origin.y, title.frame.size.width, [References fixedWidthSizeWithFont:title.font andString:title.text andLabel:title].size.height);
    [introView addSubview:title];
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:title]+5, [References screenWidth]-40, 0)];
    description.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    description.textColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.45];
    description.textAlignment = NSTextAlignmentCenter;
    description.text = @"The easiest way to connect with Tutors on college campuses.";
    description.numberOfLines = 0;
    description.frame = CGRectMake(description.frame.origin.x, description.frame.origin.y, description.frame.size.width, [References fixedWidthSizeWithFont:description.font andString:description.text andLabel:description].size.height);
    [introView addSubview:description];
    introView.frame = CGRectMake(introView.frame.origin.x, 0, introView.frame.size.width, introView.frame.size.height);
    UIButton *newUser = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:description]+10, [References screenWidth]-30, 54)];
    [newUser setTitle:@"First Time?" forState:UIControlStateNormal];
    [newUser setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [newUser.titleLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightSemibold]];
    newUser.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    [newUser addTarget:self action:@selector(newUser) forControlEvents:UIControlEventTouchUpInside];
    [References cornerRadius:newUser radius:newUser.frame.size.height/2];
    [introView addSubview:newUser];
    UIButton *returningUser = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:newUser]+10, [References screenWidth]-30, 54)];
    [returningUser setTitle:@"Returning User" forState:UIControlStateNormal];
    [returningUser setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [returningUser.titleLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightSemibold]];
    returningUser.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    [returningUser addTarget:self action:@selector(returningUser) forControlEvents:UIControlEventTouchUpInside];
    [References cornerRadius:returningUser radius:returningUser.frame.size.height/2];
    [introView addSubview:returningUser];
    return introView;
}

-(void)toggleUniversity:(UIButton*)sender {
    if (sender.tag == 0) {
        emailString = @"%@uoregon.edu";
        emailRightField.text = @"@uoregon.edu";
    } else if (sender.tag == 1) {
        emailString = @"%@oregonstate.edu";
        emailRightField.text = @"@oregonstate.edu";
    } else if (sender.tag == 2) {
        emailString = @"%@ucsb.edu";
        emailRightField.text = @"@ucsb.edu";
    }
    [UIView animateWithDuration:0.2 animations:^{
        if (sender.tag == 0) {
            [uo setBackgroundColor:[[References secondaryText] colorWithAlphaComponent:0.5]];
            [osu setBackgroundColor:[UIColor clearColor]];
            [ucsb setBackgroundColor:[UIColor clearColor]];
        } else if (sender.tag == 1) {
            [osu setBackgroundColor:[[References secondaryText] colorWithAlphaComponent:0.5]];
            [uo setBackgroundColor:[UIColor clearColor]];
            [ucsb setBackgroundColor:[UIColor clearColor]];
        } else if (sender.tag == 2) {
            [ucsb setBackgroundColor:[[References secondaryText] colorWithAlphaComponent:0.5]];
            [osu setBackgroundColor:[UIColor clearColor]];
            [uo setBackgroundColor:[UIColor clearColor]];
        }
        self->emailRightField.frame = CGRectMake(0, 0, [References fixedHeightSizeWithFont:emailRightField.font andString:emailRightField.text andLabel:emailRightField].size.width+20, emailRightField.frame.size.height);
        self->email.alpha = 1;
    }];
    email.enabled = true;
}


-(UIView*)createTextInputScreen:(UITextField*)tf andPage:(int)page withTitleDescription:(NSDictionary*)details isEmail:(BOOL)email {
    UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+50, [References screenWidth]-30, 0)];
    title.font = [UIFont systemFontOfSize:44 weight:UIFontWeightHeavy];
    title.textColor = [UIColor darkTextColor];
    title.text = details.allKeys[0];
    title.textAlignment = NSTextAlignmentCenter;
    title.frame = CGRectMake(title.frame.origin.x, title.frame.origin.y, title.frame.size.width, [References fixedWidthSizeWithFont:title.font andString:title.text andLabel:title].size.height);
    [view addSubview:title];
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:title]+5, [References screenWidth]-40, 0)];
    description.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    description.textColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.45];
    description.text = details.allValues[0];
    description.textAlignment = NSTextAlignmentCenter;
    description.numberOfLines = 0;
    description.frame = CGRectMake(description.frame.origin.x, description.frame.origin.y, description.frame.size.width, [References fixedWidthSizeWithFont:description.font andString:description.text andLabel:description].size.height);
    [view addSubview:description];
    if (email) {
        [tf setTextContentType:UITextContentTypeEmailAddress];
        emailRightField = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, tf.frame.size.height)];
        emailRightField.textAlignment = NSTextAlignmentLeft;
        emailRightField.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
        emailRightField.textColor = [UIColor lightGrayColor];
        emailRightField.text = @"";
        tf.rightView = emailRightField;
        tf.rightViewMode = UITextFieldViewModeAlways;
        uo = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]/2-70-35-5, [References getBottomYofView:description]+15, 70, 50)];
        [uo setImage:[UIImage imageNamed:@"uo.png"] forState:UIControlStateNormal];
        [uo setContentEdgeInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
        [uo.imageView setContentMode:UIViewContentModeScaleAspectFit];
        uo.tag = 0;
        [References cornerRadius:uo radius:5];
        [uo addTarget:self action:@selector(toggleUniversity:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:uo];
        osu = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]/2-35, [References getBottomYofView:description]+15, 70, 50)];
        [References cornerRadius:osu radius:5];
        [osu setImage:[UIImage imageNamed:@"osu.png"] forState:UIControlStateNormal];
        [osu.imageView setContentMode:UIViewContentModeScaleAspectFit];
        osu.tag = 1;
        [osu setContentEdgeInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
        [osu addTarget:self action:@selector(toggleUniversity:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:osu];
        ucsb = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]/2+35+5, [References getBottomYofView:description]+15, 70, 50)];
        [References cornerRadius:ucsb radius:5];
        [ucsb setImage:[UIImage imageNamed:@"ucsb.png"] forState:UIControlStateNormal];
        [ucsb.imageView setContentMode:UIViewContentModeScaleAspectFit];
        ucsb.tag = 2;
        [ucsb setContentEdgeInsets:UIEdgeInsetsMake(5, 10, 5, 10)];
        [ucsb addTarget:self action:@selector(toggleUniversity:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:ucsb];
//        UIButton *info = [UIButton buttonWithType:UIButtonTypeInfoDark];
//        info.tintColor = [References schoolMainColor];
//        info.frame = CGRectMake(([References screenWidth]/2)-(info.frame.size.width/2), [References getBottomYofView:description]+10, info.frame.size.width, info.frame.size.height);
//        [info addTarget:self action:@selector(supportedDomains) forControlEvents:UIControlEventTouchUpInside];
//        [view addSubview:info];
        tf.frame = CGRectMake(15, [References getBottomYofView:uo]+15, tf.frame.size.width, tf.frame.size.height);
    } else {
        tf.frame = CGRectMake(15, [References getBottomYofView:description]+10, tf.frame.size.width, tf.frame.size.height);
    }
    
    [view addSubview:tf];
    view.frame = CGRectMake(view.frame.origin.x, [References screenHeight]*page, view.frame.size.width, view.frame.size.height);
    return view;
}

-(UIView*)createButtonInputScreen:(UIButton*)button OnPage:(int)page withTitleDescription:(NSDictionary*)details {
    UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+50, [References screenWidth]-30, 0)];
    title.font = [UIFont systemFontOfSize:44 weight:UIFontWeightHeavy];
    title.textColor = [UIColor darkTextColor];
    title.text = details.allKeys[0];
    title.textAlignment = NSTextAlignmentCenter;
    title.frame = CGRectMake(title.frame.origin.x, title.frame.origin.y, title.frame.size.width, [References fixedWidthSizeWithFont:title.font andString:title.text andLabel:title].size.height);
    [view addSubview:title];
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:title]+5, [References screenWidth]-40, 0)];
    description.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    description.textColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.45];
    description.text = details.allValues[0];
    description.textAlignment = NSTextAlignmentCenter;
    description.numberOfLines = 0;
    description.frame = CGRectMake(description.frame.origin.x, description.frame.origin.y, description.frame.size.width, [References fixedWidthSizeWithFont:description.font andString:description.text andLabel:description].size.height);
    [view addSubview:description];
    button.frame = CGRectMake(15, [References getBottomYofView:description]+10, [References screenWidth]-30, 54);
    [button setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:20 weight:UIFontWeightSemibold]];
    button.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    [button addTarget:self action:@selector(setActiveButton:) forControlEvents:UIControlEventTouchUpInside];
    [References cornerRadius:button radius:button.frame.size.height/2];
    [view addSubview:button];
    view.frame = CGRectMake(view.frame.origin.x, [References screenHeight]*page, view.frame.size.width, view.frame.size.height);
    return view;
}

-(void)createNotificationsScreenAsPage:(int)page {
    UIScrollView *toggles = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [References screenHeight]*page, [References screenWidth], [References screenHeight])];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+50, [References screenWidth]-30, 0)];
    title.font = [UIFont systemFontOfSize:44 weight:UIFontWeightHeavy];
    title.textColor = [UIColor darkTextColor];
    title.text = @"Notifications";
    title.textAlignment = NSTextAlignmentCenter;
    title.frame = CGRectMake(title.frame.origin.x, title.frame.origin.y, title.frame.size.width, [References fixedWidthSizeWithFont:title.font andString:title.text andLabel:title].size.height);
    [toggles addSubview:title];
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:title]+5, [References screenWidth]-40, 0)];
    description.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    description.textColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.45];
    description.text = @"Control the locations you'd like to recieve notifications.";
    description.textAlignment = NSTextAlignmentCenter;
    description.numberOfLines = 0;
    description.frame = CGRectMake(description.frame.origin.x, description.frame.origin.y, description.frame.size.width, [References fixedWidthSizeWithFont:description.font andString:description.text andLabel:description].size.height);
    [toggles addSubview:description];
    pushSwitch.frame = CGRectMake([References screenWidth]-15-pushSwitch.frame.size.width, [References getBottomYofView:description]+10, pushSwitch.frame.size.width, pushSwitch.frame.size.height);
    pushSwitch.tintColor = [References accent];
    [pushSwitch setOnTintColor:[References accent]];
    [pushSwitch addTarget:self action:@selector(toggle:) forControlEvents:UIControlEventTouchUpInside];
    [toggles addSubview:pushSwitch];
    
    UILabel *pushHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:description]+5, [References screenWidth]-pushSwitch.frame.size.width-20, pushSwitch.frame.size.height+15)];
    pushHeader.text = @"Push Notifications";
    pushHeader.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    [toggles addSubview:pushHeader];
    [References createLinewithWidth:toggles xPos:15 yPos:[References getBottomYofView:pushHeader] inFront:YES andWidth:[References screenWidth]-30];
    
    smsSwitch.frame = CGRectMake([References screenWidth]-15-smsSwitch.frame.size.width, [References getBottomYofView:pushHeader]+10, smsSwitch.frame.size.width, smsSwitch.frame.size.height);
    smsSwitch.tintColor = [References accent];
    [smsSwitch setOnTintColor:[References accent]];
    [smsSwitch addTarget:self action:@selector(toggle:) forControlEvents:UIControlEventTouchUpInside];
    [toggles addSubview:smsSwitch];
    UILabel *smsHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:pushHeader]+5, [References screenWidth]-smsSwitch.frame.size.width-20, smsSwitch.frame.size.height+15)];
    smsHeader.text = @"SMS Notifications";
    smsHeader.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    [toggles addSubview:smsHeader];
    [References createLinewithWidth:toggles xPos:15 yPos:[References getBottomYofView:smsHeader] inFront:YES andWidth:[References screenWidth]-30];
    
    [emailSwitch addTarget:self action:@selector(toggle:) forControlEvents:UIControlEventTouchUpInside];
    emailSwitch.frame = CGRectMake([References screenWidth]-15-emailSwitch.frame.size.width, [References getBottomYofView:smsHeader]+10, emailSwitch.frame.size.width, emailSwitch.frame.size.height);
    emailSwitch.tintColor = [References accent];
    [emailSwitch setOnTintColor:[References accent]];
    [toggles addSubview:emailSwitch];
    UILabel *emailHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:smsHeader]+5, [References screenWidth]-smsSwitch.frame.size.width-20, smsSwitch.frame.size.height+15)];
    emailHeader.text = @"Email Notifications";
    emailHeader.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    [toggles addSubview:emailHeader];
    [References createLinewithWidth:toggles xPos:15 yPos:[References getBottomYofView:emailHeader] inFront:YES andWidth:[References screenWidth]-30];
    
    [tosSwitch addTarget:self action:@selector(toggle:) forControlEvents:UIControlEventTouchUpInside];
    tosSwitch.frame = CGRectMake([References screenWidth]-15-tosSwitch.frame.size.width, [References getBottomYofView:emailHeader]+10, tosSwitch.frame.size.width, tosSwitch.frame.size.height);
    tosSwitch.tintColor = [References accent];
    [tosSwitch setOnTintColor:[References accent]];
    [toggles addSubview:tosSwitch];
    UIButton *tosHeader = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:emailHeader]+5, [References screenWidth]-tosSwitch.frame.size.width-20, tosSwitch.frame.size.height+15)];
    [tosHeader setTitle:@"Read Terms of Service" forState:UIControlStateNormal];
    [tosHeader setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    tosHeader.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [tosHeader addTarget:self action:@selector(openInfo) forControlEvents:UIControlEventTouchUpInside];
    [tosHeader.titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightRegular]];
    [toggles addSubview:tosHeader];
    [References createLinewithWidth:toggles xPos:15 yPos:[References getBottomYofView:tosHeader] inFront:YES andWidth:[References screenWidth]-30];
    [scroll addSubview:toggles];
}

-(void)openInfo {
    tosPopover = [[popoverView alloc] initWithSuperview:self];
    [tosPopover addTitle:@"Terms"];
    NSString* path = [[NSBundle mainBundle] pathForResource:@"tos"
                                                     ofType:@"txt"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    UITextView *tosView = [[UITextView alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 0) textContainer:nil];
    tosView.backgroundColor = [UIColor clearColor];
    tosView.editable = NO;
    tosView.textColor = [UIColor darkTextColor];
    tosView.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    tosView.text = content;
    tosView.scrollEnabled = NO;
    tosView.frame = CGRectMake(tosView.frame.origin.x, tosView.frame.origin.y, tosView.frame.size.width, [References fixedWidthSizeWithFont:tosView.font andString:tosView.text andLabel:tosView].size.height);
    [tosPopover addView:tosView];
    [tosPopover setAcceptFunction:@selector(acceptTOS)];
    [tosPopover setDeclineFunction:@selector(closeTOS)];
    [tosPopover showView];
}

-(void)acceptTOS {
    [tosPopover hideView];
    [tosSwitch setOn:YES];
}

-(void)closeTOS {
    [tosPopover hideView];
}

-(void)declineTOS {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)toggle:(UISwitch*)sender {
    if (sender == pushSwitch) {
        if (pushSwitch.isOn) {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
                NSLog(@"ERROR %@\n%@",error.localizedDescription,error.debugDescription);
                NSLog(@"granted: %i",granted);
                if (!error && granted) {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
                } else {
                    if (!granted) {
                        
                        [sender setOn:false animated:false];
                        if (self->triedNotifications) {
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:nil completionHandler:^(BOOL success) {
                                if (success) {
                                    
                                }
                            }];
                        } else {
                            self->triedNotifications = true;
                        }
                        
                    }
                }
            }];
        }
    }
}

-(void)getPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Profile Picture"
                                                                   message:@"Please find a good picture that will make it easy for others to recognize you."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* select = [UIAlertAction actionWithTitle:@"Select a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* take = [UIAlertAction actionWithTitle:@"Take a photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        picker.sourceType = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {}];
    
    [alert addAction:select];
    [alert addAction:take];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [self uploadChosenImage:chosenImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)uploadChosenImage:(UIImage*)image {
    __block UIView *uppingimage = [References loadingView:@"Uploading Image..."];
    [self.view addSubview:uppingimage];
    [UIView animateWithDuration:0.25 animations:^{
        uppingimage.alpha = 1;
    } completion:^(BOOL finished) {
        FIRStorage *storage = [FIRStorage storage];
        FIRStorageReference *storageRef = [storage reference];
        NSString *imgName = [References randomStringWithLength:16];
        FIRStorageReference *profileRep = [storageRef child:[NSString stringWithFormat:@"images/%@.jpg",imgName]];
        
        NSData *data = UIImageJPEGRepresentation(image, 1);
        [profileRep putData:data
                   metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error) {
                       if (error != nil) {
                           [References toastNotificationBottom:self.view andMessage:error.localizedDescription];
                       } else {
                           [profileRep downloadURLWithCompletion:^(NSURL * _Nullable URL, NSError * _Nullable error) {
                               self->pictureURL = URL.absoluteString;
                               [self->picture setBackgroundColor:[References accent]];
                               [self->picture setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                               [self->picture setTitle:@"Picture Ready" forState:UIControlStateNormal];
                               [UIView animateWithDuration:0.25 animations:^{
                                   uppingimage.alpha = 0;
                               } completion:^(BOOL finished) {
                                   if (finished) {
                                       [uppingimage removeFromSuperview];
                                       uppingimage = nil;
                                   }
                               }];
                           }];
                           
                           
                       }
                   }];
    }];
    
}

-(void)optForEmail {
    if (emailNotifications.tag == 0) {
        [self->emailNotifications setBackgroundColor:[References accent]];
        [self->emailNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self->emailNotifications setTitle:@"All Emails" forState:UIControlStateNormal];
        self->emailNotifications.tag = 1;
    } else {
        [self->emailNotifications setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.25]];
        [self->emailNotifications setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self->emailNotifications setTitle:@"Important Emails" forState:UIControlStateNormal];
        self->emailNotifications.tag = 0;
    }
}

-(void)optForSMS {
    if (smsNotifications.tag == 0) {
        [self->smsNotifications setBackgroundColor:[References accent]];
        [self->smsNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self->smsNotifications setTitle:@"Recieving SMS" forState:UIControlStateNormal];
        self->smsNotifications.tag = 1;
    } else {
        [self->smsNotifications setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.25]];
        [self->smsNotifications setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self->smsNotifications setTitle:@"No SMS" forState:UIControlStateNormal];
        self->smsNotifications.tag = 0;
    }
}

-(void)optForPush {
    if (pushNotifications.tag == 0) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            NSLog(@"ERROR %@\n%@",error.localizedDescription,error.debugDescription);
            NSLog(@"granted: %i",granted);
            if (!error && granted) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                    [self->pushNotifications setBackgroundColor:[References accent]];
                    [self->pushNotifications setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [self->pushNotifications setTitle:@"Push Notifications On" forState:UIControlStateNormal];
                    self->pushNotifications.tag = 1;
                });
            } else {
                if (!granted) {
                    [self->pushNotifications setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.25]];
                    [self->pushNotifications setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
                    [self->pushNotifications setTitle:@"Push Notifciations Off" forState:UIControlStateNormal];
                    self->pushNotifications.tag = 0;
                    if (self->triedNotifications) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:nil completionHandler:^(BOOL success) {
                            if (success) {
                                
                            }
                        }];
                    } else {
                        self->triedNotifications = true;
                    }
                    
                }
            }
        }];
    } else {
        [self->pushNotifications setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.25]];
        [self->pushNotifications setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [self->pushNotifications setTitle:@"Push Notifciations Off" forState:UIControlStateNormal];
        self->pushNotifications.tag = 0;
    }
}

-(void)completeSignUpView {
    completeSignUpView = [[UIView alloc] initWithFrame:self.view.bounds];
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+50, [References screenWidth]-30, 0)];
    title.font = [UIFont systemFontOfSize:44 weight:UIFontWeightHeavy];
    title.textColor = [UIColor darkTextColor];
    title.text = @"Almost...";
    title.textAlignment = NSTextAlignmentCenter;
    title.frame = CGRectMake(title.frame.origin.x, title.frame.origin.y, title.frame.size.width, [References fixedWidthSizeWithFont:title.font andString:title.text andLabel:title].size.height);
    [completeSignUpView addSubview:title];
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(20, [References getBottomYofView:title]+5, [References screenWidth]-40, 0)];
    description.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    description.textColor = [[UIColor darkTextColor] colorWithAlphaComponent:0.45];
    description.text = @"Finish up by creating a good password.";
    description.textAlignment = NSTextAlignmentCenter;
    description.numberOfLines = 0;
    description.frame = CGRectMake(description.frame.origin.x, description.frame.origin.y, description.frame.size.width, [References fixedWidthSizeWithFont:description.font andString:description.text andLabel:description].size.height);
    [completeSignUpView addSubview:description];
    passwordReturning = [[UITextField alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:description]+10, [References screenWidth]-30, 54)];
    passwordReturning.font = [UIFont systemFontOfSize:20 weight:UIFontWeightSemibold];
    passwordReturning.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    passwordReturning.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, passwordReturning.frame.size.height)];
    passwordReturning.leftViewMode = UITextFieldViewModeAlways;
    passwordReturning.delegate = self;
    [References cornerRadius:passwordReturning radius:passwordReturning.frame.size.height/2];
    passwordReturning.keyboardType = UIKeyboardTypeEmailAddress;
    passwordReturning.placeholder = @"•••••••";
    passwordReturning.secureTextEntry = true;
    [completeSignUpView addSubview:passwordReturning];
    UIButton *completeSignUp = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:passwordReturning]+10, [References screenWidth]-30, 60)];
    [completeSignUp.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightHeavy]];
    [completeSignUp setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [completeSignUp setTitle:@"Finish Sign Up" forState:UIControlStateNormal];
    [completeSignUp setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [completeSignUp addTarget:self action:@selector(completeSignUp) forControlEvents:UIControlEventTouchUpInside];
    [completeSignUpView addSubview:completeSignUp];
    completeSignUpView.frame = CGRectMake(completeSignUpView.frame.origin.x, scroll.contentSize.height+[References screenHeight], completeSignUpView.frame.size.width, completeSignUpView.frame.size.height);
    scroll.contentSize = CGSizeMake(scroll.contentSize.width, scroll.contentSize.height+[References screenHeight]);
    [scroll addSubview:completeSignUpView];
    [UIView animateWithDuration:0.25 animations:^{
        self->bottomBar.alpha = 0;
        [self->scroll setContentOffset:CGPointMake(0, self->scroll.contentOffset.y+[References screenHeight])];
    }];

}

-(void)completeSignIn {
    if ([emailReturning.text containsString:@"@uoregon.edu"] || [emailReturning.text containsString:@"@my.lanecc.edu"] || [emailReturning.text containsString:@"@ucsc.edu"] || [emailReturning.text containsString:@"oregonstate.edu"] || [emailReturning.text containsString:@"ucsb.edu"]) {
        __block UIView *uppingimage = [References loadingView:@"Signing you in..."];
        [self.view addSubview:uppingimage];
        [UIView animateWithDuration:0.25 animations:^{
            uppingimage.alpha = 1;
        } completion:^(BOOL finished) {
            if (finished) {
                [[FIRAuth auth] signInWithEmail:self->emailReturning.text password:self->passwordReturning.text completion:^(FIRAuthDataResult * _Nullable authResult, NSError * _Nullable error) {
                    if (!error) {
                        self->emailString = emailReturning.text;
                        [self openApp];
                    } else {
                        [UIView animateWithDuration:0.25 animations:^{
                            uppingimage.alpha = 0;
                        } completion:^(BOOL tfinished) {
                            if (tfinished) {
                                [uppingimage removeFromSuperview];
                                uppingimage = nil;
                                if ([error.userInfo[@"error_name"] isEqualToString:@"ERROR_WRONG_PASSWORD"]) {
                                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Incorrect Password" message:@"Try again or reset your password" preferredStyle:UIAlertControllerStyleActionSheet];
                                    [alert addAction:[UIAlertAction actionWithTitle:@"Reset Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [[FIRAuth auth] sendPasswordResetWithEmail:self->emailReturning.text completion:^(NSError * _Nullable error) {
                                            if (error) {
                                                NSLog(@"%@",error.localizedDescription);
                                            }
                                            [References toastMessage:@"Check your email for a link to reset your password." andView:self andClose:false];
                                        }];
                                    }]];
                                    [alert addAction:[UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault handler:nil]];
                                    [self presentViewController:alert animated:true completion:nil];
                                } else {
                                    [References toastMessage:error.localizedDescription andView:self andClose:NO];
                                }
                                
                            }
                        }];
                        
                    }
                }];
            }
        }];
    } else {
        [References fullScreenToast:@"Currently only Unviersity of Oregon and Lane Community College emails are accepted." inView:self withSuccess:NO andClose:NO];
    }
    
}

-(void)completeSignUp {
    
    __block UIView *uppingimage = [References loadingView:@"Wrapping up..."];
    [self.view addSubview:uppingimage];
    [UIView animateWithDuration:0.25 animations:^{
        uppingimage.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            if (self->passwordReturning.text.length == 0) {
                [UIView animateWithDuration:0.25 animations:^{
                    uppingimage.alpha = 0;
                } completion:^(BOOL tfinished) {
                    if (tfinished) {
                        [uppingimage removeFromSuperview];
                        uppingimage = nil;
                        [References toastMessage:@"Enter a password" andView:self andClose:NO];
                    }
                }];
            } else {
                NSDictionary *account = @{@"email":self->emailString,@"name":self->name.text,@"phone":self->phone.text,@"profileURL":self->pictureURL,@"emailNotifications":[NSNumber numberWithBool:self->emailSwitch.isOn],@"smsNotifications":[NSNumber numberWithBool:self->smsSwitch.isOn],@"token":[[NSUserDefaults standardUserDefaults] stringForKey:@"token"] ? [[NSUserDefaults standardUserDefaults] stringForKey:@"token"] : @"0"};
                [[FIRAuth auth] createUserWithEmail:self->emailString password:self->passwordReturning.text completion:^(FIRAuthDataResult * _Nullable authResult, NSError * _Nullable error) {
                    if (!error) {
                        [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:authResult.user.uid] setValue:account withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                            if (error) {
                                [[FIRAuth auth].currentUser deleteWithCompletion:^(NSError * _Nullable error) {
                                    if (!error) {
                                        [UIView animateWithDuration:0.25 animations:^{
                                            uppingimage.alpha = 0;
                                        } completion:^(BOOL tfinished) {
                                            if (tfinished) {
                                                [uppingimage removeFromSuperview];
                                                uppingimage = nil;
                                                [References toastMessage:@"Something went wrong..." andView:self andClose:NO];
                                            }
                                        }];
                                        
                                    }
                                }];
                            } else {
                                [self openApp];
                            }
                        }];
                    } else {
                        [UIView animateWithDuration:0.25 animations:^{
                            uppingimage.alpha = 0;
                        } completion:^(BOOL tfinished) {
                            if (tfinished) {
                                [uppingimage removeFromSuperview];
                                uppingimage = nil;
                                [References toastMessage:error.localizedDescription andView:self andClose:NO];
                            }
                        }];
                    }
                }];
            }
        }
    }];
    
}

-(void)backFromSignUp {
    [UIView animateWithDuration:0.25 animations:^{
        [self->scroll setContentOffset:CGPointMake(0, self->scroll.contentOffset.y-[References screenHeight])];
    } completion:^(BOOL finished) {
        if (finished) {
            [self->completeSignUpView removeFromSuperview];
            self->completeSignUpView = nil;
            self->scroll.contentSize = CGSizeMake([References screenWidth], self->scroll.contentSize.height-[References screenHeight]);
            self->nextPage.enabled = true;
        }
    }];
}

-(void)openApp {
    if ([self->emailString containsString:@"oregonstate"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"oregonstate" forKey:@"currentDomain"];
        [[NSUserDefaults standardUserDefaults] setObject:@"oregonstate" forKey:@"school"];
    } else if ([self->emailString containsString:@"ucsb"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"ucsb" forKey:@"currentDomain"];
        [[NSUserDefaults standardUserDefaults] setObject:@"ucsb" forKey:@"school"];
    } else {
        [[NSUserDefaults standardUserDefaults] setObject:@"uoregon" forKey:@"currentDomain"];
        [[NSUserDefaults standardUserDefaults] setObject:@"uo" forKey:@"school"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    homeViewController *homeViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"homeViewController"];
    [self.navigationController pushViewController:homeViewController animated:YES];
}

@end
