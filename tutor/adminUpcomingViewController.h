//
//  adminUpcomingViewController.h
//  tutor
//
//  Created by Robert Crosby on 10/29/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
@import Firebase;


NS_ASSUME_NONNULL_BEGIN

@interface adminUpcomingViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    NSMutableArray *sessions;
    UITableView *table;
}

@property (strong, nonatomic) NSString *uid;

@end

NS_ASSUME_NONNULL_END
