//
//  paymentProcessingView.h
//  tutor
//
//  Created by Robert Crosby on 5/7/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface paymentProcessingView : UIView {
    NSString *nonce;
    NSString *amount;
    UIProgressView *statusProgress;
    UILabel *statusLabel;
    NSDictionary *invoices;
    int step;
}

- (instancetype)init;
-(id)initWithNonce:(NSString*)_nonce andAmount:(NSString*)_amount;
-(void)passNonce;

@end

NS_ASSUME_NONNULL_END
