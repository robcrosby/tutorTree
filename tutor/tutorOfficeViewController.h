//
//  tutorOfficeViewController.h
//  tutor
//
//  Created by Robert Crosby on 5/25/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "dayAvailability.h"
#import <MBProgressHUD.h>

@import SafariServices;

@import Firebase;

@interface tutorOfficeViewController : UIViewController <UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,SFSafariViewControllerDelegate> {
    NSMutableArray *times;
    NSInteger PPH;
    BOOL changeMade;
    NSInteger dayWidth;
    NSString *enteredCode,*aboutText;
    UITableView *table;
    UIScrollView *mainScroll;
    CGFloat mainScrollContentHeight;
    UIImage *chosenImage;
    NSDictionary *myAccount;
    UIButton *imageButton,*saveButton;
    UITextField *name,*codeEntry;
    UIView *becomeTutor;
    UITextView *bioField;
    UILabel *disclaimer,*codeEntryTitle,*pphLabel;
    UIImageView *sad;
    BOOL uploadImage;
    BOOL updateName;
}

@end
