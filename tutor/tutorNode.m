//
//  tutorObject.m
//  tutor
//
//  Created by Robert Crosby on 5/18/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "tutorNode.h"

@implementation tutorNode

@synthesize next,previous,tutorPreview,image,starRating;

-(id)initAsHead {
    self = [super init];
    if (self) {
        next = NULL;
        previous = NULL;
    }
    return self;
}

-(id)initWithIdentifier:(NSString*)identifier andOptionalButton:(UIButton *)button andImage:(UIImageView*)image {
    self = [super init];
    if (self) {
        hasRatings = FALSE;
        ratingObjects = [[NSMutableArray alloc] init];
        tutorPreview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth]-75, [References screenWidth]-75)];
        starRating = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
        uid = identifier;
        populated = NO;
        next = NULL;
        previous = NULL;
        timeButtons = [[NSMutableArray alloc] initWithCapacity:48];
        NSDictionary *dictionary = [[NSUserDefaults standardUserDefaults] dictionaryForKey:identifier];
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, button.frame.size.width-55, button.frame.size.height-20)];
        nameLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
        nameLabel.textColor = [UIColor darkTextColor];
        nameLabel.textAlignment = NSTextAlignmentLeft;
        nameLabel.numberOfLines = 2;
        if (dictionary) {
            NSLog(@"found local");
            name = dictionary[@"name"];
            bio = dictionary[@"bio"];
            nameLabel.text = [name stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
            pphh = dictionary[@"PPH"];
            email = dictionary[@"email"];
            token = dictionary[@"token"];
            profileURL = dictionary[@"profileURL"];
            availability = [[NSMutableArray alloc] init];
            if (dictionary[@"availability"]) {
                NSArray *av = dictionary[@"availability"];
                NSString *av1 = [NSString stringWithFormat:@"%@",av[0]];
                if (av1.length == 48) {
                    for (int a = 0; a < 7; a++) {
                        dayAvailability *tt = [[dayAvailability alloc] initWithValue:0];
                        [availability addObject:tt];
                    }
                } else {
                    NSArray *tAvailability = dictionary[@"availability"];
                    for (NSNumber *t in tAvailability) {
                        dayAvailability *tt = [[dayAvailability alloc] initWithValue:t.unsignedLongLongValue];
                        [availability addObject:tt];
                    }
                }
            }
            CGFloat tempRating = 0;
            CGFloat count = 0;
            for (id key in dictionary[@"ratings"]) {
                if (!hasRatings) {
                    hasRatings = TRUE;
                }
                if ([[dictionary[@"ratings"] valueForKey:key] isKindOfClass:[NSDictionary class]]) {
                    NSNumber *number = [[dictionary[@"ratings"] valueForKey:key] valueForKey:@"rating"];
                    [ratingObjects addObject:@{@"rating":number,@"text":[[dictionary[@"ratings"] valueForKey:key] valueForKey:@"text"]}];
                    tempRating+=number.integerValue;
                } else {
                    NSNumber *number = [dictionary[@"ratings"] valueForKey:key];
                    [ratingObjects addObject:@{@"rating":number,@"text":@"text"}];
                    tempRating+=number.integerValue;
                }
                count++;
            }
            if (hasRatings) {
                rating = tempRating/count;
                starRating.value = rating;
                label.text = [NSString stringWithFormat:@"%@",name];
            }
            populated = YES;
            label.text = [NSString stringWithFormat:@"%@",name];
            [image sd_setImageWithURL:[NSURL URLWithString:profileURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                if (!error) {
                    self.image = image;
                }
            }];
        } else {
            [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:identifier] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if ([snapshot hasChildren]) {
                    name = snapshot.value[@"name"];
                    nameLabel.text = [name stringByReplacingOccurrencesOfString:@" " withString:@"\n"];
                    bio = snapshot.value[@"bio"];
                    pphh = snapshot.value[@"PPH"];
                    email = snapshot.value[@"email"];
                    profileURL = snapshot.value[@"profileURL"];
                    token = snapshot.value[@"token"];
                    populated = YES;
                    availability = [[NSMutableArray alloc] init];
                    if (snapshot.value[@"availability"]) {
                        NSArray *av = snapshot.value[@"availability"];
                        NSString *av1 = [NSString stringWithFormat:@"%@",av[0]];
                        if (av1.length == 48) {
                            dayAvailability *tt = [[dayAvailability alloc] initWithValue:0];
                            [availability addObject:tt];
                        } else {
                            for (NSNumber *t in av) {
                                dayAvailability *tt = [[dayAvailability alloc] initWithValue:t.unsignedLongLongValue];
                                [availability addObject:tt];
                            }
                        }
                    }
                    CGFloat tempRating = 0;
                    CGFloat count = 0;
                    for (id key in snapshot.value[@"ratings"]) {
                        if (!hasRatings) {
                            hasRatings = TRUE;
                        }
                        if ([[snapshot.value[@"ratings"] valueForKey:key] isKindOfClass:[NSDictionary class]]) {
                            NSNumber *number = [[snapshot.value[@"ratings"] valueForKey:key] valueForKey:@"rating"];
                            [ratingObjects addObject:@{@"rating":number,@"text":[[snapshot.value[@"ratings"] valueForKey:key] valueForKey:@"text"]}];
                            tempRating+=number.floatValue;
                        } else {
                            NSNumber *number = [snapshot.value[@"ratings"] valueForKey:key];
                            [ratingObjects addObject:@{@"rating":number,@"text":@"text"}];
                            tempRating+=number.floatValue;
                        }
                        count++;
                    }
                    if (hasRatings) {
                        rating = tempRating/count;
                        starRating.value = rating;
                        label.text = [NSString stringWithFormat:@"%@",name];
                    }
                    
                    [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:identifier];
                    [image sd_setImageWithURL:[NSURL URLWithString:profileURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (!error) {
                            self.image = image;
                        }
                    }];
                    NSMutableArray *cache = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"cache"] mutableCopy];
                    [cache addObject:identifier];
                    [[NSUserDefaults standardUserDefaults] setObject:cache forKey:@"cache"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }];
        }
        [button addSubview:nameLabel];
    }
    return self;
}

-(tutorNode*)navigateToTutor:(NSInteger)index {
    NSInteger tPath = index;
    tutorNode *dummy = self;
    while (tPath > 0) {
        dummy = dummy.next;
        tPath--;
    }
    return dummy;
}

+(instancetype)tutorWithIdentifier:(NSString*)identifier andOptionalButton:(UIButton *)label{
    return [[tutorNode alloc] initWithIdentifier:identifier andOptionalButton:label];
}

-(bool)insertTutor:(tutorNode*)tutor {
        tutorNode *dummy = self;
        while (dummy.next != NULL) {
            dummy = dummy.next;
        }
        dummy.next = tutor;
        tutor.previous = dummy;
        return true;

}

-(void)updateFeaturedLabel:(UILabel*)label {
    label.text = self.tutorName;
    label.textColor = [UIColor darkTextColor];
}

-(void)updatePPH:(NSNumber*)newPPH {
    pphh = newPPH;
}

-(void)printTutors {
    tutorNode *head = next;
    while (head != NULL) {
        NSLog(@"%@",[head tutorName]);
        head = head.next;
    }
}

-(NSString*)tutorName {
    return name;
}

-(NSString*)profileURL {
    return profileURL;
}
-(NSInteger)tutorPPHH {
    return pphh.integerValue;
}

-(NSString*)bio {
    return bio;
}
-(NSInteger)isAvailableOn:(NSInteger)weekDay atTime:(NSInteger)time {
    dayAvailability *av = availability[weekDay];
    if ([av getBitAtIndex:(int)time]) {
        return 1;
    }
    return 0;
}

-(NSInteger)earliestAvailabilityOn:(NSInteger)weekDay {
    dayAvailability *av = availability[weekDay];
    for (int a = 0; a < 48; a++) {
        if ([av getBitAtIndex:a]) {
            return a;
        }
    }
    return 0;
}

-(BOOL)isAvailableBefore:(NSInteger)weekDay atTime:(NSInteger)time {
    dayAvailability *av = availability[weekDay];
    if (time - 1 >= 0) {
        return [av getBitAtIndex:(int)time-1];
    }
    return false;
}

-(BOOL)isAvailableAfter:(NSInteger)weekDay atTime:(NSInteger)time {
    dayAvailability *av = availability[weekDay];
    if (time + 1 < 48) {
        return [av getBitAtIndex:(int)time+1];
    }
    return false;
}

-(BOOL)isAvailableOn:(NSInteger)weekDay {
    dayAvailability *av = availability[weekDay];
    if ([av getValue] != 0) {
        return TRUE;
    }
    return FALSE;
}

-(void)setSkip {
    skip = YES;
}

-(void)clearSkip {
    skip = NO;
}

-(BOOL)returnSkip {
    return skip;
}

-(NSInteger)maxDuration:(NSInteger)weekDay atTime:(NSInteger)time {
    dayAvailability *av = availability[weekDay];
    NSInteger duration = 1;
    for (int a = (int)time+1; a < 48; a++) {
        if ([av getBitAtIndex:a]) {
            duration++;
        } else {
            break;
        }
    }
    return duration;
}


-(void)setProfileImage:(UIImageView*)view {
    [view sd_setImageWithURL:[NSURL URLWithString:profileURL] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (!error) {
            self.image = image;
        }
    }];
}

-(NSString*)identifier {
    return uid;
}

-(NSString*)email {
    return email;
}

-(void)createInvoices:(NSString*)transactionID andDuration:(NSInteger)duration andType:(NSString*)type studentPaid:(CGFloat)paid andBalance:(NSInteger)usedBalance withCompletion:(void(^)(BOOL finished))complete {
    NSDictionary *myAccount = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"accountDatabase"];
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMM d, YYYY"];
    FIRDatabaseReference *meRef = [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] child:@"spending"] child:transactionID];
    FIRDatabaseReference *tutorRef = [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid ] child:@"income"] child:transactionID];
    FIRDatabaseReference *invoice = [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"invoices"] child:transactionID];
    NSDictionary *tutorDictionary = @{@"date":[fmt stringFromDate:[NSDate date]],@"amount":[NSNumber numberWithInteger:duration*[self tutorPPHH]],@"type":type,@"numberDate":[NSNumber numberWithInteger:[[NSDate date] timeIntervalSince1970]]};
    NSDictionary *studentDictionary = @{@"date":[fmt stringFromDate:[NSDate date]],@"amount":[NSNumber numberWithFloat:paid],@"type":type,@"numberDate":[NSNumber numberWithInteger:[[NSDate date] timeIntervalSince1970]],@"usedBalance":[NSNumber numberWithInteger:usedBalance]};
    NSDictionary *globaldictionary = @{@"fromEmail":myAccount[@"email"],@"fromName":myAccount[@"name"],@"toName":[self tutorName],@"toEmail":[self email],@"metadata":tutorDictionary};
    [tutorRef  setValue:tutorDictionary withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            [meRef setValue:studentDictionary withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    [invoice setValue:globaldictionary withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                        if (!error) {
                            complete(TRUE);
                        }
                    }];
                }
            }];
        }
    }];
}

-(UIView*)getTutorPreview {
    for (UIView *view in tutorPreview.subviews) {
        if (view != starRating) {
            [view removeFromSuperview];
        }
    }
    [References cornerRadius:tutorPreview radius:16.0f];
    UILabel *blur = [[UILabel alloc] initWithFrame:tutorPreview.bounds];
    [References blurView:blur];
    [tutorPreview addSubview:blur];
    UIImageView *profileImage = [[UIImageView alloc] initWithFrame:CGRectMake((tutorPreview.frame.size.width/2)-40, 15, 80, 80)];
    [profileImage sd_setImageWithURL:[NSURL URLWithString:profileURL]];
    [References cornerRadius:profileImage radius:profileImage.frame.size.width/2];
    [tutorPreview addSubview:profileImage];
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(5, [References getBottomYofView:profileImage]+8, tutorPreview.frame.size.width-10, 10)];
    name.font = [UIFont systemFontOfSize:24 weight:UIFontWeightRegular];
    name.textColor = [UIColor darkTextColor];
    name.text = [self tutorName];
    name.textAlignment = NSTextAlignmentCenter;
    name.frame = CGRectMake(name.frame.origin.x, name.frame.origin.y, name.frame.size.width, [References fixedWidthSizeWithFont:name.font andString:name.text andLabel:name].size.height);
    [tutorPreview addSubview:name];
    starRating.backgroundColor = [UIColor clearColor];
    starRating.accurateHalfStars = YES;
    starRating.maximumValue = 5;
    starRating.minimumValue = 0;
    starRating.value = rating;
    
    starRating.tintColor = [References accent];
    [tutorPreview addSubview:starRating];
    [tutorPreview.layer setValue:[NSNumber numberWithFloat:[References getBottomYofView:name]+16] forKey:@"starRatingYValue"];
    return tutorPreview;
}

-(BOOL)hasRatings {
    return hasRatings;
}

-(CGFloat)returnRating {
    return rating;
}

-(void)reloadAvailability {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] child:@"availability"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if ([snapshot hasChildren]) {
            NSMutableDictionary *new = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:uid]];
            availability = nil;
            availability = [[NSMutableArray alloc] init];
            NSArray *tAvailability = snapshot.value;
            for (NSNumber *t in tAvailability) {
                dayAvailability *tt = [[dayAvailability alloc] initWithValue:t.unsignedLongLongValue];
                [availability addObject:tt];
            }
            [new setObject:availability forKey:@"availability"];
            [[NSUserDefaults standardUserDefaults] setObject:new forKey:uid];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"reloaded tutor");
        }
    }];
}

-(void)reloadAvailabilityWithCompletion:(void(^)(BOOL finished))complete {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] child:@"availability"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if ([snapshot exists]) {
            NSArray *av = snapshot.value;
            self->availability = nil;
            self->availability = [[NSMutableArray alloc] init];
            if ([NSString stringWithFormat:@"%@",av[0]].length == 48) {
                dayAvailability *tt = [[dayAvailability alloc] initWithValue:0];
                [self->availability addObject:tt];
            } else {
                for (NSNumber *t in av) {
                    dayAvailability *tt = [[dayAvailability alloc] initWithValue:t.unsignedLongLongValue];
                    [self->availability addObject:tt];
                }
            }
            complete(YES);
        }
    }];
}

-(void)createAtTimeSecs:(NSNumber*)timeSecs onWeekDate:(int)date withAvailability:(dayAvailability*)availability {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"EEEE, MMMM d"];
    NSString *dateText = [fmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeSecs.floatValue]];
    NSInteger myHash = [FIRAuth auth].currentUser.uid.hash;
    NSInteger tutorHash = uid.hash;
    NSString *connectionID;
    if (myHash < tutorHash) {
        connectionID = [NSString stringWithFormat:@"%li:%li",myHash,tutorHash];
    } else {
        connectionID = [NSString stringWithFormat:@"%li:%li",tutorHash,myHash];
    }
    NSDictionary *myAccount = [[NSUserDefaults standardUserDefaults] objectForKey:@"accountDatabase"];
    
}

-(void)addPreviewToView:(UIViewController*)parent {
    if (!previewView) {
        previewView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [References screenHeight], [References screenWidth], [References screenHeight])];
        previewView.delegate = self;
        previewView.contentSize = previewView.frame.size;
        previewView.clipsToBounds = NO;
        previewView.alwaysBounceVertical = YES;
        previewView.showsVerticalScrollIndicator = NO;
        UILabel *card = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, previewView.frame.size.width, previewView.frame.size.height)];
        card.backgroundColor = [UIColor whiteColor];
        [References cornerRadius:card radius:5];
        [previewView addSubview:card];
    }
    previewParentView = parent;
    [previewParentView.view addSubview:previewView];
    [UIView animateWithDuration:0.25 animations:^{
        self->previewView.frame = CGRectMake(0, [References topMargin], self->previewView.frame.size.width, self->previewView.frame.size.height-[References bottomMargin]);
    }];
}


@end
