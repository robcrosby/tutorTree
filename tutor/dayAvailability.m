//
//  dayAvailability.m
//  tutor
//
//  Created by Robert Crosby on 7/3/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#define MSB 0x8000000000000000
#import "dayAvailability.h"

@implementation dayAvailability

-(id)initWithValue:(uint64_t)value {
    self = [super init];
    if (self) {
        availability = value;
    }
    return self;
}

+(instancetype)withValue:(uint64_t)value {
    return [[dayAvailability alloc] initWithValue:value];
}

+(instancetype)empty {
    return [[dayAvailability alloc] initWithValue:0];
}

-(BOOL)getBitAtIndex:(int)index {
    uint64_t og = MSB;
    og = og >> index;
    uint64_t result = og & availability;
    if (result > 0) {
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)setBitAtIndex:(int)index {
    uint64_t og = MSB;
    og = og >> index;
    availability = og | availability;
    return YES;
}

-(BOOL)setBitAtIndex:(int)index andDuration:(int)duration {
    uint64_t og = MSB;
    og = og >> index;
    availability = og | availability;
    if (duration > 1) {
        int decrementDuration = duration;
        int newShiftIndex = index + 1;
        while (decrementDuration > 1) {
            uint64_t tOG = MSB;
            tOG = tOG >> newShiftIndex;
            availability = tOG | availability;
            decrementDuration--;
            newShiftIndex++;
        }
    }
    return YES;
}

-(BOOL)flipBitAtIndex:(int)index {
    uint64_t og = MSB;
    og = og >> index;
    uint64_t result = og ^ availability;
    availability = result;
    if (result > 0) {
        return YES;
    } else {
        return NO;
    }
}

-(uint64_t)getValue {
    return availability;
}

-(void)printAvailabilityValue {
    printf("%llu\n",availability);
}

-(void)printAvailabilityBits {
    uint64_t tempAvailability = availability;
    for (int c = 0; c < 48; c++) {
        uint64_t d = tempAvailability & MSB;
        d = d >> 63;
        printf("%llu",d);
        tempAvailability = tempAvailability << 1;
    }
    printf("\n");
}

-(BOOL)isAvailableAtHour:(int)hour andMinute:(int)minute {
    int shift = 0;
    shift = (hour * 2) + shift;
    return YES;
}

-(BOOL)invert {
    availability = ~availability;
    return TRUE;
}

-(BOOL)andWith:(dayAvailability*)other {
    availability = availability & [other getValue];
    return YES;
}

-(BOOL)xorWith:(dayAvailability*)other {
    availability = availability ^ [other getValue];
    return YES;
}

-(BOOL)orWith:(dayAvailability*)other {
    availability = availability | [other getValue];
    return YES;
}

-(int)getStartTime {
    uint64_t og = MSB;
    int counter = 0;
    while (counter < 64) {
        uint64_t result = og & availability;
        if (result > 0) {
            return counter;
        }
        og = og >> 1;
        counter++;
    }
    return 0;
}

-(int)getDuration {
    uint64_t og = MSB;
    int duration = 0;
    int counter = 0;
    while (counter < 64) {
        uint64_t result = og & availability;
        if (result != 0) {
            duration++;
        }
        og = og >> 1;
        counter++;
    }
    return duration;
}

-(BOOL)checkIfAnd:(dayAvailability*)day {
    uint64_t result = availability & [day getValue];
    NSLog(@"%llu",result);
    if (result != 0) {
        return YES;
    } else {
        return NO;
    }
}

@end
