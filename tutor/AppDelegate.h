//
//  AppDelegate.h
//  tutor
//
//  Created by Robert Crosby on 1/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BraintreeCore.h>
#import <UserNotifications/UserNotifications.h>
#import "References.h"
#import "nGettingStartedViewController.h"
#import "homeViewController.h"
#import "nHomeViewController.h"
@import Firebase;

@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;


@end

