//
//  connectionViewController.m
//  tutor
//
//  Created by Robert Crosby on 10/11/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "connectionViewController.h"

@interface connectionViewController ()

@end

@implementation connectionViewController

@synthesize connection;

-(UIStatusBarStyle)preferredStatusBarStyle {
    return [References statusBarColor];
}

-(void)viewWillAppear:(BOOL)animated {
    interval = 0;
    self.view.backgroundColor = [References background];
    [self borders];
}

- (void)viewDidLoad {
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
//    NSTimer *time = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(handleTimer:) userInfo:nil repeats: YES];
//    [time fire];
//    BOOL timerState = [time isValid];
//    NSLog(@"Timer Validity is: %@", timerState?@"YES":@"NO");
    // Do any additional setup after loading the view.
}

//-(void)handleTimer:(NSTimer*)sender {
//    interval = interval + 1;
//    NSLog(@"%f",interval);
//}

-(void)borders {
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight]) style:UITableViewStylePlain];
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.delegate = self;
    table.dataSource = self;
    for (messageReference *msg in [connection messages]) {
        [msg updateColors];
        [table registerClass:[UITableViewCell class] forCellReuseIdentifier:[msg messageID]];
    }
    [connection messageTable:table];
    [table reloadData];
    [self.view addSubview:table];
    UIView *menubar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References topMargin]+10+44)];
    [References blurView:menubar];
    UIImageView *profileimage = [[UIImageView alloc] initWithFrame:CGRectMake(40, [References topMargin]+5, 44, 44)];
    profileimage.backgroundColor = [References lightGrey];
    [References cornerRadius:profileimage radius:profileimage.frame.size.width/2];
    if ([connection profileURL]) {
        [profileimage sd_setImageWithURL:[connection profileImageURL]];
    } else {
        [[[connection profileURL] child:@"profileURL"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            [profileimage sd_setImageWithURL:[NSURL URLWithString:snapshot.value]];
        }];
    }
    [menubar addSubview:profileimage];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(40+44+7.5, [References topMargin]+5, [References screenWidth]-40-75-7.5, 44)];
    hheader.textAlignment = NSTextAlignmentLeft;
    if ([connection name]) {
        hheader.text = [connection name];
    } else {
        [[[connection profileURL] child:@"name"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            hheader.text = snapshot.value;
        }];
    }
    hheader.textColor = [References primaryText];
    hheader.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    [menubar addSubview:hheader];
//    more = [UIButton buttonWithType:UIButtonTypeInfoDark];
//    more.frame = CGRectMake([References screenWidth]-15-more.frame.size.width, hheader.frame.origin.y+(hheader.frame.size.height/2)-(more.frame.size.height/2), more.frame.size.width, more.frame.size.height);
//    [more addTarget:self action:@selector(morePanel) forControlEvents:UIControlEventTouchUpInside];
//    [menubar addSubview:more];
    [References createLine:menubar xPos:0 yPos:menubar.frame.size.height inFront:YES];
    [self.view addSubview:menubar];
    bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, [References screenHeight]-[References bottomMargin]-61, [References screenWidth], 61+[References bottomMargin])];
    [References blurView:bottomBar];
    message = [[UITextField alloc] initWithFrame:CGRectMake(10+5+(bottomBar.frame.size.height-16-[References bottomMargin]), 10, bottomBar.frame.size.width-(10+5+(bottomBar.frame.size.height-16-[References bottomMargin]))-10, bottomBar.frame.size.height-20-[References bottomMargin])];
    [message addTarget:self action:@selector(textfieldMessageChanged:) forControlEvents:UIControlEventEditingChanged];
    UIButton *attach = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, message.frame.size.height, message.frame.size.height)];
    [attach setImageEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [attach setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
    [attach setBackgroundColor:[References cells]];
    [attach addTarget:self action:@selector(openAttach) forControlEvents:UIControlEventTouchUpInside];
    [References tintUIButton:attach color:[References primaryText]];
    [References cornerRadius:attach radius:attach.frame.size.width/2];
    [bottomBar addSubview:attach];
    message.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    message.delegate = self;
    [References cornerRadius:message radius:5];
    [message setBackgroundColor:[References cells]];
    [References textFieldInset:message];
    [message setPlaceholder:@"Message"];
    [bottomBar addSubview:message];
    [message setValue:[References secondaryText] forKeyPath:@"_placeholderLabel.textColor"];
    [message setTextColor:[References primaryText]];
    [self.view addSubview:bottomBar];
    send = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, message.frame.size.height)];
    [send addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
    [send setTitle:@"Send" forState:UIControlStateNormal];
    [send setTitleColor:[References primaryText] forState:UIControlStateNormal];
    [send setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    send.enabled = NO;
    [message setRightView:send];
    [message setRightViewMode:UITextFieldViewModeAlways];
    
    [References createLine:bottomBar xPos:0 yPos:0 inFront:YES];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15,profileimage.frame.origin.y+(profileimage.frame.size.height/2)-10,20,20)];
    [back addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [back setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [References tintUIButton:back color:[References primaryText]];
    [self.view addSubview:back];
    table.contentInset = UIEdgeInsetsMake(menubar.frame.size.height+10, 0, bottomBar.frame.size.height, 0);
    table.backgroundColor = [UIColor clearColor];
    table.scrollIndicatorInsets = table.contentInset;
    if ([connection messageCount] > 1) {
        [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[connection messageCount]-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    message.keyboardAppearance = [References keyboard];
    [self getMeetingPoint];
}

-(void)textfieldMessageChanged:(UITextField*)sender {
    if (sender.text.length == 0) {
        send.enabled = false;
    } else {
        send.enabled = true;
    }
}

-(void)openAttach {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"More" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Update Meeting Point" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *popup = [UIAlertController alertControllerWithTitle:@"New Meeting Point" message:@"Enter a library, a room, something recognizable." preferredStyle:UIAlertControllerStyleAlert];
        [popup addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"Location";
        }];
        [popup addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
        [popup addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self updateMeetingPoint:popup.textFields[0].text];
        }]];
        [self presentViewController:popup animated:true completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [message resignFirstResponder];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)getMeetingPoint {
    [[[connection connectionURL] child:@"meetingPoint"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            [self addMeetingToView:(NSString*)snapshot.value];
        }
    }];
}

//-(void)morePanel {
//        detailsView = [[popoverView alloc] initWithSuperview:self];
//        [detailsView addTitle:@"Details"];
//        UIView *locationView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 0)];
//        locationView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.25];
//        [References cornerRadius:locationView radius:5];
//        location = [[UITextField alloc] initWithFrame:CGRectMake(7.5, 7.5, locationView.frame.size.width-15, 44)];
//        location.delegate = self;
//        [location setBackgroundColor:[References lightGrey]];
//
//        [References textFieldInset:location];
//        [References cornerRadius:location radius:5];
//        [locationView addSubview:location];
//        UIButton *saveLocation = [[UIButton alloc] initWithFrame:CGRectMake(7.5, [References getBottomYofView:location]+7.5, location.frame.size.width, 44)];
//        [saveLocation setTitle:@"Update Location" forState:UIControlStateNormal];
//        [saveLocation setTitleColor:[References schoolMainColor] forState:UIControlStateNormal];
//        [saveLocation addTarget:self action:@selector(updateMeetingPoint) forControlEvents:UIControlEventTouchUpInside];
//        [locationView addSubview:saveLocation];
//        locationView.frame = CGRectMake(15, 0, [References screenWidth]-30, [References getBottomYofView:saveLocation]+7.5);
//        [detailsView addView:locationView];
//        if (![self->detailsView containsView:self->ratingView]) {
//            [self->detailsView addView:self->ratingView];
//        }
//        [detailsView setOkFunction:@selector(hideDetails)];
//
//    [detailsView showView];
//
//}

//-(void)checkIfTutor {
////    [[[connection connectionURL] child:@"tutor"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//
////        if (![snapshot.value isEqualToString:[FIRAuth auth].currentUser.uid]) {
//            self->ratingView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 0)];
//            self->ratingView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.25];
//            [References cornerRadius:self->ratingView radius:5];
//            self->ratingStars = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(30, 15, self->ratingView.frame.size.width-60, 30)];
//            self->ratingStars.backgroundColor = [UIColor clearColor];
//            self->ratingStars.emptyStarColor = [References lightGrey];
//            self->ratingStars.tintColor = [References schoolMainColor];
//            [self->ratingView addSubview:self->ratingStars];
//            UIButton *saveRating = [[UIButton alloc] initWithFrame:CGRectMake(7.5, [References getBottomYofView:self->ratingStars]+7.5, self->ratingView.frame.size.width-15, 44)];
//            [saveRating setTitle:@"Update Rating" forState:UIControlStateNormal];
//            [saveRating setTitleColor:[References schoolMainColor] forState:UIControlStateNormal];
//            [saveRating addTarget:self action:@selector(updateRating) forControlEvents:UIControlEventTouchUpInside];
//            [self->ratingView addSubview:saveRating];
//            self->ratingView.frame = CGRectMake(15, 0, [References screenWidth]-30, [References getBottomYofView:saveRating]+7.5);
//            if (self->detailsView) {
//                if (![self->detailsView containsView:self->ratingView]) {
//                    [self->detailsView addView:self->ratingView];
//                }
//            }
////        }
////    }];
//}

-(void)hideDetails {
    [detailsView hideView];
}
        

-(void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)keyboardWillChange:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"height: %f",keyboardRect.size.height);
    keyboardHeight = keyboardRect.size.height;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    if (textField != location) {
//        if (textField.text.length == 0) {
//            send.enabled = false;
//        } else {
//            send.enabled = true;
//        }
//    }

    return true;
}

- (void)keyboardWillShow:(NSNotification *)notification {
    [UIView animateWithDuration:0.33 animations:^{
        table.contentInset = UIEdgeInsetsMake(table.contentInset.top, 0, keyboardHeight+bottomBar.frame.size.height-[References bottomMargin], 0);
        table.scrollIndicatorInsets = UIEdgeInsetsMake(table.contentInset.top, 0, keyboardHeight+bottomBar.frame.size.height-[References bottomMargin], 0);
        if (bottomBar.frame.origin.y == [References screenHeight]-bottomBar.frame.size.height) {
            bottomBar.frame = CGRectMake(0, [References screenHeight]-keyboardHeight-bottomBar.frame.size.height+[References bottomMargin], bottomBar.frame.size.width, bottomBar.frame.size.height);
        }
    } completion:^(BOOL finished) {
        if ([connection messageCount] > 1) {
            [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[connection messageCount]-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.33 animations:^{
        table.contentInset = UIEdgeInsetsMake(table.contentInset.top, 0, bottomBar.frame.size.height, 0);
        table.scrollIndicatorInsets = UIEdgeInsetsMake(table.contentInset.top, 0, bottomBar.frame.size.height, 0);
        bottomBar.frame = CGRectMake(0, [References screenHeight]-bottomBar.frame.size.height, bottomBar.frame.size.width, bottomBar.frame.size.height);
    } completion:^(BOOL finished) {
        if ([connection messageCount] > 1) {
            [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[connection messageCount]-1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }];
}

-(void)sendMessage {
    if (message.text.length > 0) {
        self->send.enabled = NO;
        [References selectionFeedback:UIImpactFeedbackStyleHeavy];
        [connection sendMessage:message.text withCompletion:^(BOOL finished) {
            if (finished) {
                self->message.text = @"";
            }
        }];
    } else {
        [References warningFeedback];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

-(void)updateRating {
    MBProgressHUD *toast = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    toast.mode = MBProgressHUDModeText;
    toast.backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    toast.label.text = [NSString stringWithFormat:@"Thank you for leaving feedback!"];
    toast.margin = 10.f;
    [toast setOffset:CGPointMake(0, 150)];
    toast.removeFromSuperViewOnHide = YES;
    [connection updateRating:[NSNumber numberWithFloat:ratingStars.value] withCompletion:^(BOOL finished) {
        if (finished) {
            [toast hideAnimated:YES afterDelay:1.25];
        }
    }];
}

-(void)addMeetingToView:(NSString*)loc {
    self->meetingBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 0)];
    [References blurView:self->meetingBar];
    self->meetingLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, [References screenWidth]-30, 0)];
    self->meetingLabel.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
    self->meetingLabel.textColor = [References primaryText];
    self->meetingLabel.textAlignment = NSTextAlignmentCenter;
    self->meetingLabel.numberOfLines = 0;
    self->meetingLabel.text = [NSString stringWithFormat:@"Meet at %@",loc];
    self->meetingLabel.frame = CGRectMake(15, 5, [References screenWidth]-30, [References fixedWidthSizeWithFont:self->meetingLabel.font andString:self->meetingLabel.text andLabel:self->meetingLabel].size.height);
    self->meetingBar.frame = CGRectMake(0, [References topMargin]+10+44, self->meetingBar.frame.size.width, self->meetingLabel.frame.size.height+10);
    
    self->table.contentInset = UIEdgeInsetsMake([References topMargin]+10+44+10+self->meetingBar.frame.size.height, 0, self->bottomBar.frame.size.height, 0);
    [References createLine:self->meetingBar xPos:0 yPos:self->meetingBar.frame.size.height-1 inFront:true];
    [self->meetingBar addSubview:self->meetingLabel];
    self->meetingBar.alpha = 0;
    [self.view addSubview:self->meetingBar];
    [UIView animateWithDuration:0.25 animations:^{
        self->meetingBar.alpha = 1;
    }];
}

-(void)updateMeetingPoint:(NSString*)t {
    if (self->meetingBar) {
        [UIView animateWithDuration:0.15 animations:^{
            self->meetingBar.alpha = 0;
        } completion:^(BOOL finished) {
            if (finished) {
                [self->meetingBar removeFromSuperview];
                self->meetingLabel = nil;
                self->meetingBar = nil;
                [self addMeetingToView:t];
            }
        }];
    } else {
        [self addMeetingToView:t];
    }
    meetingPoint = t;
    NSDictionary *myname = [[NSUserDefaults standardUserDefaults] objectForKey:@"accountDatabase"];
    [connection updateMeetingPoint:meetingPoint withCompletion:^(BOOL finished) {
        if (finished) {
            [References sendNotification:[self->connection otherID] withSubject:@"Location Update" andMessage:[NSString stringWithFormat:@"%@ has updated the meeting point to %@.",myname[@"name"],self->meetingPoint] forceEmail:YES andCompletion:^(bool result) {
                if (result) {
                   
                }
            }];
        }
    }];
   
}

-(void)ratingValueChanged:(HCSStarRatingView*)ratingSender {
    NSLog(@"%f",ratingSender.value);
}


- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    messageReference *message = [connection messageAtIndex:indexPath.row];
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:[message messageID]];
    if (cell.tag == 0) {
        NSLog(@"making cell: %@",[message messageID]);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        [cell addSubview:[message messageView]];
        cell.tag = 1;
    }
    return cell;
    
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [connection messageCount];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    messageReference *message = [connection messageAtIndex:indexPath.row];
    return [message messageHeight];
    
}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 0;
//}
//
//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 60)];
//    view.backgroundColor = [UIColor clearColor];
//    UILabel *bg = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 45)];
//    bg.backgroundColor = [UIColor whiteColor];
//    [view addSubview:bg];
//    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-24, 30)];
//    hheader.textAlignment = NSTextAlignmentLeft;
//    hheader.textColor = [UIColor darkTextColor];
//    hheader.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
//    hheader.text = @"Messages";
//    [view addSubview:hheader];
//    [References createLine:view xPos:15 yPos:45 inFront:YES];
//    return view;
//}

@end
