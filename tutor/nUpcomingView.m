//
//  nUpcomingView.m
//  tutor
//
//  Created by Robert Crosby on 4/25/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "nUpcomingView.h"
#define CELL_HEIGHT 100
#define EXPANDED_CELL_HEIGHT 144

@implementation nUpcomingView

@synthesize view,height;

-(id)init {
    self = [super init];
    if (self) {
        selectedIndex = nil;
        dfmt = [[NSDateFormatter alloc] init];
        [self buildView];
        [self listenForUpcoming];
    }
    return self;
}

-(void)buildView {
    if (!view) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 0)];
        UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 0)];
        header.font = [UIFont systemFontOfSize:24 weight:UIFontWeightRegular];
        header.text = @"Upcoming";
        header.textColor = [References primaryText];
        //        [References adjustLabelCharacterSpacing:header withSpacing:0.59];
        
        [References adjustLabelHeight:header];
        [view addSubview:header];
//        dynamicHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:header], [References screenWidth]-30, 0)];
//        dynamicHeader.textColor = [header.textColor colorWithAlphaComponent:0.75];
//        dynamicHeader.text = @"One Second...";
//        dynamicHeader.font = header.font;
//        [References adjustLabelHeight:dynamicHeader];
//        [view addSubview:dynamicHeader];
        topPadding = [References getBottomYofView:header]+10;
        view.clipsToBounds = false;
        card = [[UIView alloc] initWithFrame:view.bounds];
        card.backgroundColor = [UIColor greenColor];
        
        [References bottomshadow:card];
        table = [[UITableView alloc] initWithFrame:view.bounds];
        table.delegate = self;
        table.dataSource = self;
        table.scrollEnabled = false;
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        [view addSubview:card];
        [view addSubview:table];
    }
}

-(void)updateView {
    if (upcoming.count == 0) {
        view.frame = CGRectMake(0, 0, [References screenWidth], 0);
        table.hidden = true;
        card.hidden = true;
        [table reloadData];
    } else {
        if (selectedIndex != nil) {
            height = [NSNumber numberWithFloat:((upcoming.count-1)*CELL_HEIGHT)+EXPANDED_CELL_HEIGHT+topPadding];
        } else {
            height = [NSNumber numberWithFloat:upcoming.count*CELL_HEIGHT+topPadding];
        }
        table.frame = CGRectMake(15, topPadding, [References screenWidth]-30, height.floatValue-topPadding);
        card.frame = CGRectMake(20, topPadding+5, [References screenWidth]-40, height.floatValue-10-topPadding);
        view.frame = CGRectMake(0, 5, [References screenWidth], height.floatValue-10);
        [References cornerRadius:table radius:10];
        [table reloadData];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadUpcoming" object:nil userInfo:nil];
}

-(void)listenForUpcoming {
    [[[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] child:@"sessions"] queryStartingAtValue:[NSNumber numberWithDouble:[NSDate date].timeIntervalSince1970-14400]] queryOrderedByChild:@"start"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->upcoming = [[NSMutableArray alloc] initWithCapacity:[snapshot.value allKeys].count];
            int today = 0;
            int tomorrow = 0;
            int other = 0;
            for (id key in snapshot.value) {
                NSDictionary *data = snapshot.value[key];
                NSDate *start = [NSDate dateWithTimeIntervalSince1970:[(NSNumber*)data[@"start"] doubleValue]];
                if ([[NSCalendar currentCalendar] isDateInToday:start]) {
                    today+=1;
                } else if ([[NSCalendar currentCalendar] isDateInTomorrow:start]) {
                    tomorrow+=1;
                } else {
                    other+=1;
                }
                NSDictionary *d = @{@"course":data[@"course"],@"start":start,@"end":[NSDate dateWithTimeIntervalSince1970:[(NSNumber*)data[@"end"] doubleValue]],@"status":data[@"status"],@"student":data[@"student"],@"other":data[@"other"]};
                [self->upcoming addObject:@[(NSString*)key,d]];
            }
            
        }
        [self updateView];
    }];
}

-(void)dynamicHeader:(int)today :(int)tomorrow :(int)other {
    if (today > 0) {
        self->dynamicHeader.text = [NSString stringWithFormat:@"%i %@ Today",today,today == 1 ? @"Sessions" : @"Session"];
    } else if (tomorrow > 0) {
        self->dynamicHeader.text = [NSString stringWithFormat:@"%i %@ Tomorrow",tomorrow,tomorrow == 1 ? @"Sessions" : @"Session"];
    } else {
        self->dynamicHeader.text = @"";
    }
    
}

-(void)populateUser:(NSString*)uid withCell:(UITableViewCell*)cell {
    NSDictionary *other = [[NSUserDefaults standardUserDefaults] dictionaryForKey:uid];
    if (other) {
        if (cell != nil) {
            [((UIImageView*)[cell viewWithTag:1]) sd_setImageWithURL:[NSURL URLWithString:other[@"profileURL"]]];
            [((UILabel*)[cell viewWithTag:2]) setText:other[@"name"]];
        }
    } else {
        NSLog(@"getting network");
        [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.exists) {
                [[NSUserDefaults standardUserDefaults] setObject:@{@"name":snapshot.value[@"name"], @"picture":snapshot.value[@"profileURL"]} forKey:uid];
                if (cell != nil) {
                    [((UIImageView*)[cell viewWithTag:1]) sd_setImageWithURL:[NSURL URLWithString:snapshot.value[@"profileURL"]]];
                    
                    [((UILabel*)[cell viewWithTag:2]) setText:snapshot.value[@"name"]];
                }
            }
        }];
    }
    
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[upcoming[indexPath.row] objectAtIndex:0]];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:[upcoming[indexPath.row] objectAtIndex:0]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSDictionary *event = [upcoming[indexPath.row] objectAtIndex:1];
        NSDate *start = event[@"start"];
//        NSDate *end = event[@"end"];
        UIImageView *otherImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 20, CELL_HEIGHT-40, CELL_HEIGHT-40)];
        otherImage.tag = 1;
        UILabel *otherName = [[UILabel alloc] initWithFrame:CGRectMake(15+otherImage.frame.size.width+10, otherImage.frame.origin.y+5, tableView.frame.size.width-((15+otherImage.frame.size.width+5)*2), (otherImage.frame.size.height-10)/3)];
        otherName.font = [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
        otherName.textColor = [References primaryText];
        otherName.tag = 2;
        UILabel *weekday = [[UILabel alloc] initWithFrame:CGRectMake(15+otherImage.frame.size.width+10, [References getBottomYofView:otherName]+3, tableView.frame.size.width-((15+otherImage.frame.size.width+5)*2), (otherImage.frame.size.height-10)/3)];
        weekday.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
        weekday.text = [NSString stringWithFormat:@"%@ at %@",[self dateToFormat:start format:@"EEEE, MMMM d"],[self dateToFormat:start format:@"h:mm a"]];
        weekday.textAlignment = NSTextAlignmentLeft;
        weekday.textColor = [References primaryText];
        UILabel *course = [[UILabel alloc] initWithFrame:CGRectMake(15+otherImage.frame.size.width+10, [References getBottomYofView:weekday], tableView.frame.size.width-((20+otherImage.frame.size.width+5)*2), (otherImage.frame.size.height-10)/3)];
        course.text = event[@"course"];
        course.font = [UIFont systemFontOfSize:12 weight:UIFontWeightSemibold];
        course.textColor = [References primaryText];
        [References cornerRadius:otherImage radius:otherImage.frame.size.height/2];
        otherImage.backgroundColor = [[References secondaryText] colorWithAlphaComponent:0.5];
        [cell addSubview:otherImage];
        [cell addSubview:otherName];
        [cell addSubview:course];
        [cell addSubview:weekday];
        UIToolbar *action = [[UIToolbar alloc] init];
        action.frame = CGRectMake(0, CELL_HEIGHT, tableView.frame.size.width, 44);
        UIBarButtonItem *messages = [[UIBarButtonItem alloc] initWithTitle:@"Messages" style:UIBarButtonItemStylePlain target:self action:nil];
        UIBarButtonItem *edit = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain target:self action:nil];
        UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        NSArray *buttons = [NSArray arrayWithObjects: messages,spacer, edit, nil];
        [action setItems: buttons animated:NO];
        action.tag = 3;
        action.alpha = 0;
        [cell addSubview:action];
        [self populateUser:event[@"other"] withCell:cell];
        
//        cell.textLabel.text = [event[@"course"] stringByReplacingOccurrencesOfString:@":" withString:@" "];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath == selectedIndex) {
        return EXPANDED_CELL_HEIGHT;
    } else {
        return CELL_HEIGHT;
    }
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath == selectedIndex) {
        return EXPANDED_CELL_HEIGHT;
    } else {
        return CELL_HEIGHT;
    }
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return upcoming.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *l = selectedIndex;
    if (selectedIndex == indexPath) {
        selectedIndex = nil;
        height = [NSNumber numberWithFloat:upcoming.count*CELL_HEIGHT+topPadding];
    } else {
        selectedIndex = indexPath;
        height = [NSNumber numberWithFloat:((upcoming.count-1)*CELL_HEIGHT)+EXPANDED_CELL_HEIGHT+topPadding];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadUpcoming" object:nil userInfo:nil];
    [UIView animateWithDuration:0.25 animations:^{
        
        [[tableView cellForRowAtIndexPath:l] viewWithTag:3].alpha = 0;
        @try {
            [[tableView cellForRowAtIndexPath:self->selectedIndex] viewWithTag:3].alpha = 1;
        } @catch (NSException *exception) {
            NSLog(@"no cell");
        }
        
        self->table.frame = CGRectMake(15, self->topPadding, [References screenWidth]-30, self->height.floatValue-self->topPadding);
        self->card.frame = CGRectMake(20, self->topPadding+5, [References screenWidth]-40, self->height.floatValue-10-self->topPadding);
        self->view.frame = CGRectMake(0, 0, [References screenWidth], self->height.floatValue-10);
        [tableView beginUpdates];
        [tableView endUpdates];
    }];

}

-(NSString*)dateToFormat:(NSDate*)date format:(NSString*)format {
    dfmt.dateFormat = format;
    return [dfmt stringFromDate:date];
}

@end
