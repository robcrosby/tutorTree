//
//  adminUsersViewController.h
//  tutor
//
//  Created by Robert Crosby on 1/3/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import <MBProgressHud.h>

#import <MessageUI/MessageUI.h>

@import Firebase;
NS_ASSUME_NONNULL_BEGIN

@interface adminUsersViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate> {
    NSMutableArray<NSDictionary*> *expand;
    NSDateFormatter *fmt;
    NSMutableArray *users;
    UITableView *table;
}


@end

NS_ASSUME_NONNULL_END
