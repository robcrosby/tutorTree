//
//  nScheduleViewController.m
//  tutor
//
//  Created by Robert Crosby on 5/8/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "nScheduleViewController.h"

@interface nScheduleViewController ()

@end

@implementation nScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(id)initWithCourse:(NSString *)tcourse andTutors:(NSArray *)ttutors andCourse:(nonnull courseDatabase *)tCourseD{
    self = [super init];
    if (self) {
        [self populateTutor:ttutors atIndex:0];
        self.view.backgroundColor = [References background];
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        
    }
    return self;
}

-(void)populateTutor:(NSArray*)array atIndex:(int)index {
    if (index == array.count) {
        NSLog(@"done");
    } else {
        [[[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:array[index]] child:@"sessions"] queryOrderedByChild:@"start"] queryStartingAtValue:[NSNumber numberWithDouble:[NSDate date].timeIntervalSince1970-86400]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSDictionary *t_sessions = snapshot.value;
            [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:array[index]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                if (snapshot.exists) {
                    NSArray *availability = snapshot.value[@"availability"];
                    NSNumber *cost = snapshot.value[@"PPH"];
                    tutorAvailability *tutor = [[tutorAvailability alloc] initWithID:array[index] andAvailability:availability andSessions:t_sessions andName:snapshot.value[@"name"] andURL:snapshot.value[@"profileURL"] andPPH:cost andToken:snapshot.value[@"token"]];
                    [self->tutors addObject:tutor];
                    [self populateTutor:array atIndex:index+1];
                }
            }];
        }];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
