//
//  nHomeViewController.m
//  tutor
//
//  Created by Robert Crosby on 4/23/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "nHomeViewController.h"


@interface nHomeViewController ()

@end

@implementation nHomeViewController

-(void)viewWillAppear:(BOOL)animated {
    sections = @[@"Upcoming"];
    table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    table.delegate = self;
    table.dataSource = self;
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.view.backgroundColor = [References background];
    [self.view addSubview:table];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadUpcoming) name:@"reloadUpcoming" object:nil];
}

- (void)viewDidLoad {
    upcoming = [[nUpcomingView alloc] init];
    [super viewDidLoad];
    [self.view addSubview:upcoming.view];
    // Do any additional setup after loading the view.
}

-(void)reloadUpcoming {
    [table beginUpdates];
    [table endUpdates];
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    return 54;
//}
//
//-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UILabel *v = (UILabel*)[tableView dequeueReusableHeaderFooterViewWithIdentifier:sections[section]];
//    if (v == nil) {
//        v = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 54)];
//        v.font = [UIFont systemFontOfSize:12 weight:UIFontWeightMedium];
//        v.textAlignment = NSTextAlignmentCenter;
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:sections[section]];
//        
//        float spacing = 1.2f;
//        [attributedString addAttribute:NSKernAttributeName
//                                 value:@(spacing)
//                                 range:NSMakeRange(0, [sections[section] length])];
//        
//        v.attributedText = attributedString;
//        [References textShadow:v];
//    }
//    return v;
//}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"upcoming"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"upcoming"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell addSubview:upcoming.view];
        }
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return upcoming.height.floatValue;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return upcoming.height.floatValue;
    }
    return 0;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

@end
