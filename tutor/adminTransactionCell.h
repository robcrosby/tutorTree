//
//  adminTransactionCell.h
//  tutor
//
//  Created by Robert Crosby on 1/23/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"

NS_ASSUME_NONNULL_BEGIN

@interface adminTransactionCell : UITableViewCell {
    bool loaded;

    NSString *invoiceID;
    NSInteger numberDate;
    UILabel *invoice,*fromName,*toName,*price,*date,*course;
    NSString *connectionID;
    NSString *tutorID,*studentID;
}

-(id)initWithID:(NSString*)tID andData:(NSDictionary*)data;
-(NSString*)getTutorID;
-(NSString*)getStudentID;
-(NSString*)amount;
-(NSString*)dateT;
-(NSString*)invoiceID;
-(NSInteger)numberDate;
-(void)allInfoCompletion:(void (^)(NSString *finished))complete;
@end

NS_ASSUME_NONNULL_END
