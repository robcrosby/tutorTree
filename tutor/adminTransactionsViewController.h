//
//  adminTransactionsViewController.h
//  tutor
//
//  Created by Robert Crosby on 1/3/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import <MBProgressHUD.h>
#import <MessageUI/MessageUI.h>
#import "adminTransactionCell.h"
@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface adminTransactionsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate> {
    NSMutableArray *expand;
    NSDateFormatter *fmt;
    NSMutableArray<adminTransactionCell*> *transactions;
    UITableView *table;
}

@end

NS_ASSUME_NONNULL_END
