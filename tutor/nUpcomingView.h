//
//  nUpcomingView.h
//  tutor
//
//  Created by Robert Crosby on 4/25/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "References.h"
#import <SDWebImage/UIImageView+WebCache.h>
@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface nUpcomingView : NSObject<UITableViewDataSource,UITableViewDelegate> {
    UITableView *table;
    NSMutableArray<NSArray*> *upcoming;
    UIView *card;
    NSIndexPath *selectedIndex;
    NSDateFormatter *dfmt;
    CGFloat topPadding;
    UILabel *dynamicHeader;
    
}

@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) NSNumber *height;

@end

NS_ASSUME_NONNULL_END
