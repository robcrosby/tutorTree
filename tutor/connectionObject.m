//
//  connectionObject.m
//  tutor
//
//  Created by Robert Crosby on 10/25/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "connectionObject.h"

#define profileImageDim 50
#define scheduleWidth 225
#define scheduleHeight 100

@import Firebase;

@implementation connectionObject

-(id)initWithID:(NSString*)tID {
    self = [super init];
    if (self) {
        NSArray *array = [tID componentsSeparatedByString:@":"];
        if ([array[0] isEqualToString:[FIRAuth auth].currentUser.uid]) {
            other = array[1];
        } else {
            other = array[0];
        }
        sessions = [[NSMutableDictionary alloc] init];
        [self initializeCellView];
        identification = tID;
        [self listenForProfileWithCompletion:^(bool finished) {
            [self getInitialMessages];
        }];
    }
    return self;
}

-(NSString*)otherID {
    return other;
}

-(void)addSession:(NSDictionary*)session {
    [sessions setObject:session forKey:session[@"connection"]];
}

-(void)initializeCellView {
    cellView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 100)];
    cellView.backgroundColor = [UIColor clearColor];
    cellImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, cellView.frame.size.height/2-(profileImageDim/2), profileImageDim, profileImageDim)];
    cellImage.backgroundColor = [UIColor lightGrayColor];
    [References cornerRadius:cellImage radius:cellImage.frame.size.height/2];
    cellName = [[UILabel alloc] initWithFrame:CGRectMake(cellImage.frame.origin.x+cellImage.frame.size.width+10, cellImage.frame.origin.y-7, [References screenWidth]-cellImage.frame.size.width-cellImage.frame.origin.x-10-15-10, 25)];
    cellName.textAlignment = NSTextAlignmentLeft;
    cellName.textColor = [References primaryText];
    cellName.font = [UIFont systemFontOfSize:20 weight:UIFontWeightMedium];
    cellMessage = [[UILabel alloc] initWithFrame:CGRectMake(cellName.frame.origin.x, [References getBottomYofView:cellName], cellName.frame.size.width, 0)];
    cellMessage.font = [UIFont systemFontOfSize:12 weight:UIFontWeightRegular];
    cellMessage.textColor = [References secondaryText];
    cellMessage.numberOfLines = 0;
    cellMessage.text = @"No Messages";
    cellMessage.frame = CGRectMake(cellMessage.frame.origin.x, cellMessage.frame.origin.y, cellMessage.frame.size.width, [References fixedWidthSizeWithFont:cellMessage.font andString:cellMessage.text andLabel:cellMessage].size.height);
    [cellView addSubview:cellMessage];
    [cellView addSubview:cellName];
    [cellView addSubview:cellImage];
    notificationBadge = [[UILabel alloc] initWithFrame:CGRectMake(cellImage.frame.origin.x+((cellImage.frame.size.width/2)-(25/2)), [References getBottomYofView:cellImage]-10, 25, 25)];
    notificationBadge.backgroundColor = [References colorFromHexString:@"#FF3824"];
    notificationBadge.textColor = [UIColor whiteColor];
    notificationBadge.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    notificationBadge.text = @"0";
    notificationBadge.textAlignment = NSTextAlignmentCenter;
    [References cornerRadius:notificationBadge radius:notificationBadge.frame.size.width/2];
    [cellView addSubview:notificationBadge];
    notificationBadge.hidden = YES;
}

-(void)updateColors {
    cellName.textColor = [References primaryText];
    cellMessage.textColor = [References secondaryText];
}

-(UIView*)getCellView {
    return cellView;
}

-(void)getInitialMessages {
//    __block bool sort = YES;
    messagesHeight = 0;
    messages = [[NSMutableArray alloc] init];
    messageIDs = [[NSMutableArray alloc] init];
    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:identification] child:@"messages"] queryOrderedByChild:@"metadata/date"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.hasChildren) {
            for (id key in snapshot.value) {
                NSDictionary *mD = snapshot.value[key];
                NSDictionary *mDMeta = mD[@"metadata"];
                    messageReference *msg = [[messageReference alloc] initWithDate:mDMeta[@"date"] andMessage:mD[@"message"] andSender:mD[@"sender"] andID:mDMeta[@"identifier"]];
                    [messages addObject:msg];
                    [messageIDs addObject:[msg messageID]];
                    cellMessage.text = mD[@"message"];
                    cellMessage.frame = CGRectMake(cellMessage.frame.origin.x, cellMessage.frame.origin.y, cellMessage.frame.size.width, [References fixedWidthSizeWithFont:cellMessage.font andString:cellMessage.text andLabel:cellMessage].size.height);
                    if (cellMessage.frame.size.height > (cellView.frame.size.height-[References getBottomYofView:cellName])-10) {
                        cellMessage.frame = CGRectMake(cellMessage.frame.origin.x, cellMessage.frame.origin.y, cellMessage.frame.size.width, (cellMessage.frame.size.height-[References getBottomYofView:cellName])-10);
                    }
//                    }
                    if (table) {
                        [table registerClass:[UITableViewCell class] forCellReuseIdentifier:[msg messageID]];
                    }
                    messagesHeight+=[msg messageHeight];
            }
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"index"
                                                         ascending:YES];
            messages = [[NSMutableArray alloc] initWithArray:[messages sortedArrayUsingDescriptors:@[sortDescriptor]]];
            [self listenForMessages];
        } else {
            NSLog(@"no messages yet");
        }
    }];
}

-(void)listenForMessages {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:identification] child:@"messages"] observeEventType:FIRDataEventTypeChildAdded withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            NSDictionary *mD = snapshot.value;
            NSDictionary *mDMeta = mD[@"metadata"];
            if (![messageIDs containsObject:mDMeta[@"identifier"]]) {
                messageReference *msg = [[messageReference alloc] initWithDate:mDMeta[@"date"] andMessage:mD[@"message"] andSender:mD[@"sender"] andID:mDMeta[@"identifier"]];
                [messages addObject:msg];
                [messageIDs addObject:[msg messageID]];
                cellMessage.text = mD[@"message"];
                cellMessage.frame = CGRectMake(cellMessage.frame.origin.x, cellMessage.frame.origin.y, cellMessage.frame.size.width, [References fixedWidthSizeWithFont:cellMessage.font andString:cellMessage.text andLabel:cellMessage].size.height);
                if (cellMessage.frame.size.height > (cellView.frame.size.height-[References getBottomYofView:cellName])-10) {
                    cellMessage.frame = CGRectMake(cellMessage.frame.origin.x, cellMessage.frame.origin.y, cellMessage.frame.size.width, (cellMessage.frame.size.height-[References getBottomYofView:cellName])-10);
                }
                //                    }
                if (table) {
                    [msg updateColors];
                    [table registerClass:[UITableViewCell class] forCellReuseIdentifier:[msg messageID]];
                    [table reloadData];
                }
                messagesHeight+=[msg messageHeight];
            }
        
    }];
}

-(void)listenForProfileWithCompletion:(void (^)(bool finished))complete {
    NSDictionary *offline = [[NSUserDefaults standardUserDefaults] objectForKey:other];
    if (offline) {
        
        self->otherToken = [offline objectForKey:@"token"];
        self->otherName = [offline objectForKey:@"name"];
        self->otherProfileURL = [offline objectForKey:@"profileURL"];
        self->cellName.text = self->otherName;
        [self->cellImage sd_setImageWithURL:[offline objectForKey:@"profileURL"]];
        NSLog(@"FOUND OFFLINE: %@",self->otherName);
        complete(YES);
    } else {
        [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:other] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            if (snapshot.exists) {
                [[NSUserDefaults standardUserDefaults] setObject:snapshot.value forKey:self->other];
                [[NSUserDefaults standardUserDefaults] synchronize];
                self->otherToken = [snapshot.value objectForKey:@"token"];
                self->otherName = [snapshot.value objectForKey:@"name"];
                self->otherProfileURL = [snapshot.value objectForKey:@"profileURL"];
                self->cellName.text = self->otherName;
                [self->cellImage sd_setImageWithURL:[snapshot.value objectForKey:@"profileURL"]];
                complete(YES);
            }
        }];
    }
    
}

-(void)messageTable:(UITableView*)tTable {
    table = tTable;
}
-(messageReference*)messageAtIndex:(NSInteger)index {
    return messages[index];
}

-(NSString*)messageIdAtIndex:(NSInteger)index {
    return messageIDs[index];
}
-(NSInteger)messageCount {
    return messages.count;
}

-(NSMutableArray*)messages {
    return messages;
}

-(FIRDatabaseReference*)connectionURL {
    return [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:identification];
}

-(FIRDatabaseReference*)profileURL {
    return [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:other];
}

-(NSURL*)profileImageURL {
    if (self->otherProfileURL) {
        return [NSURL URLWithString:otherProfileURL];
    } else {
        return nil;
    }
}

-(NSString*)name {
    if (self->otherName) {
        return otherName;
    } else {
        return nil;
    }
}

-(void)sendMessage:(NSString*)message withCompletion:(void (^)(BOOL))complete {
    NSDictionary *msg = @{@"message":message,@"sender":[FIRAuth auth].currentUser.uid,@"metadata":@{@"date":[NSNumber numberWithInteger:messages.count+1],@"identifier":[References randomStringWithLength:8]}};
    cellMessage.text = message;
    cellMessage.frame = CGRectMake(cellMessage.frame.origin.x, cellMessage.frame.origin.y, cellMessage.frame.size.width, [References fixedWidthSizeWithFont:cellMessage.font andString:cellMessage.text andLabel:cellMessage].size.height);
    if (cellMessage.frame.size.height > (cellView.frame.size.height-[References getBottomYofView:cellName])-10) {
        cellMessage.frame = CGRectMake(cellMessage.frame.origin.x, cellMessage.frame.origin.y, cellMessage.frame.size.width, (cellMessage.frame.size.height-[References getBottomYofView:cellName])-10);
    }
    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:identification] child:@"messages"] childByAutoId] setValue:msg withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            complete(YES);
            
            NSDictionary *account = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"accountDatabase"];
            [References sendNotification:self->other withSubject:account[@"name"] andMessage:message forceEmail:NO andCompletion:^(bool result) {
                NSLog(@"sent!");
                
            }];
            
        }
    }];
}

-(void)updateMeetingPoint:(NSString*)place withCompletion:(void (^)(BOOL))complete {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] child:identification] child:@"meetingPoint"] setValue:place withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        complete(YES);
    }];
}

//-(void)updateRating:(NSNumber*)rating withCompletion:(void (^)(BOOL))complete {
//    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:other] child:@"ratings"] child:[FIRAuth auth].currentUser.uid] setValue:rating withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
//        complete(YES);
//    }];
//}

-(void)sendNotification:(NSString*)notificationText withSubject:(NSString*)subject{
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:other] child:@"badge"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSNumber *badge;
        if (snapshot.exists) {
            badge = [NSNumber numberWithInteger:[(NSNumber*)snapshot.value integerValue] + 1];
        } else {
            badge = [NSNumber numberWithInteger:1];
        }
        NSString *url;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sandbox"] == true) {
            NSLog(@"sandbox");
            url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendSandboxNotification/%@/%@/%@/%@",subject,notificationText,self->otherToken,badge];
        } else {
            url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendNotification/%@/%@/%@/%@",subject,notificationText,self->otherToken,badge];
        }
        NSString *finalURL = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [self updateBadge:badge];
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }];
}

-(void)updateBadge:(NSNumber*)badge {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:other] child:@"badge"] setValue:badge];
}

@end
