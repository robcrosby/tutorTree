//
//  messageReference.m
//  tutor
//
//  Created by Robert Crosby on 7/5/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "messageReference.h"

@implementation messageReference

-(id)initWithDate:(NSNumber*)tDate andMessage:(NSString*)tMessage andSender:(NSString *)sender andID:(NSString*)tID{
    self = [super init];
    if (self) {
        message = tMessage;
        messageID = tID;
        index = tDate.integerValue;
        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 10, [References screenWidth]-50, 20)];
        messageLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
        messageLabel.textAlignment = NSTextAlignmentLeft;
        messageLabel.numberOfLines = 0;
        messageLabel.text = message;
        
        if ([sender isEqualToString:[FIRAuth auth].currentUser.uid]) {
            amSender = YES;
        }
        CGRect oneLine = CGRectMake(messageLabel.frame.origin.x, messageLabel.frame.origin.y, [References fixedHeightSizeWithFont:messageLabel.font andString:messageLabel.text andLabel:messageLabel].size.width, messageLabel.frame.size.height);
        if (oneLine.size.width < messageLabel.frame.size.width) {
            CGFloat x = 25;
            if (amSender) {
                x = [References screenWidth] - oneLine.size.width - 25;
            }
            messageLabel.frame = CGRectMake(x, messageLabel.frame.origin.y, oneLine.size.width, messageLabel.frame.size.height);
        } else {
            messageLabel.frame = CGRectMake(25, messageLabel.frame.origin.y, messageLabel.frame.size.width, [References fixedWidthSizeWithFont:messageLabel.font andString:messageLabel.text andLabel:messageLabel].size.height);
        }
        
        card = [[UILabel alloc] initWithFrame:CGRectMake(messageLabel.frame.origin.x-8, messageLabel.frame.origin.y-8, messageLabel.frame.size.width+16, messageLabel.frame.size.height+16)];
        if (amSender) {
            card.backgroundColor = [References chatSender];
        } else {
            card.backgroundColor = [References chatOther];
        }
        [References cornerRadius:card radius:8];
        messageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References getBottomYofView:card]+5)];
        [messageView addSubview:card];
        [messageView addSubview:messageLabel];
    }
    return self;
}
-(instancetype)withDate:(NSNumber*)tDate andMessage:(NSString*)tMessage andSender:(NSString *)sender andID:(NSString*)tID{
    return [[messageReference alloc] initWithDate:tDate andMessage:tMessage andSender:sender andID:tID];
}

-(UIView*)messageView{
    return messageView;
}

-(CGFloat)messageHeight {
    return messageView.frame.size.height;
}

-(NSString*)messageID {
    return messageID;
}

-(void)updateColors {
    if (amSender) {
        card.backgroundColor = [References chatSender];
        messageLabel.textColor = [References chatTextSender];
    } else {
        card.backgroundColor = [References chatOther];
        messageLabel.textColor = [References chatTextOther];
    }

}
@end
