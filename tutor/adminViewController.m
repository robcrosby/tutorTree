//
//  adminViewController.m
//  tutor
//
//  Created by Robert Crosby on 5/27/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "adminViewController.h"

@interface adminViewController ()

@end

@implementation adminViewController

- (void)viewDidLoad {
    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    [super viewDidLoad];
    [self createStatic];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createStatic {
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 96+[References topMargin])];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accent] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    UIButton *emails = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-50-15, [References topMargin], 50, 44)];
    [emails setTitle:@"Emails" forState:UIControlStateNormal];
    [emails setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [emails.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [emails setTitleColor:[References accent] forState:UIControlStateNormal];
    [emails addTarget:self action:@selector(showEmails:) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:emails];
    UIButton *addcourse = [[UIButton alloc] initWithFrame:CGRectMake(emails.frame.origin.x-50-15, [References topMargin], 50, 44)];
    [addcourse setTitle:@"Add Course" forState:UIControlStateNormal];
    [addcourse setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [addcourse.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [addcourse setTitleColor:[References accent] forState:UIControlStateNormal];
    [addcourse addTarget:self action:@selector(addCourse:) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:addcourse];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin]+44, [References screenWidth]-24, 52)];
    header.textAlignment = NSTextAlignmentLeft;
    header.text = @"Admin";
    header.textColor = [UIColor darkTextColor];
    header.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBold];
    [menuBar addSubview:header];
    UILabel *blur = [[UILabel alloc] initWithFrame:menuBar.bounds];
    [References blurView:blur];
    [menuBar addSubview:blur];
    [menuBar sendSubviewToBack:blur];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight]) style:UITableViewStyleGrouped];
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height-[References topMargin], 0, 44, 0);
    table.scrollIndicatorInsets = table.contentInset;
    table.dataSource = self;
    table.delegate = self;
    [self.view addSubview:table];
    [self.view addSubview:menuBar];
    bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, [References screenHeight]-44-[References bottomMargin], [References screenWidth], 44+[References bottomMargin])];
    
    UILabel *bottomBlur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, bottomBar.frame.size.width, bottomBar.frame.size.height)];
    [References blurView:bottomBlur];
    [bottomBar addSubview:bottomBlur];
    [References createLine:bottomBar xPos:0 yPos:0 inFront:YES];
    controller = [[UISegmentedControl alloc] initWithItems:@[@"Users",@"Withdrawals",@"Transactions",@"Codes",@"Courses"]];
    [controller setFrame:CGRectMake(10, 10, bottomBar.frame.size.width-20, controller.frame.size.height)];
    [controller setSelectedSegmentIndex:0];
    [controller addTarget:self action:@selector(changeController:) forControlEvents:UIControlEventValueChanged];
    controller.tintColor = [References schoolMainColor];
    [bottomBar addSubview:controller];
    [self.view addSubview:bottomBar];
    [self getUsers];
    [self getWithdrawals];
    [self getCodes];
    [self getInvoices];
    [self analyzeCourses];
}

-(void)addCourse:(UIButton*)sender {
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"uoregon"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            [self addShow:snapshot.value];
        }
    }];
}

-(void)addShow:(NSDictionary*)dict {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Choose Department" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    for (id key in dict) {
        [alert addAction:[UIAlertAction actionWithTitle:key style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UIAlertController *courseNumber = [UIAlertController alertControllerWithTitle:key message:@"Number or Name" preferredStyle:UIAlertControllerStyleAlert];
            [courseNumber addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                textField.placeholder = @"Number or Name";
            }];
            [courseNumber addAction:[UIAlertAction actionWithTitle:@"Create" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"uoregon"] child:courseNumber.title] child:[NSString stringWithFormat:@"%@",courseNumber.textFields[0].text]] setValue:@{@"info":@"nil"} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                    [References toastNotificationBottom:self.view andMessage:@"Added"];
                }];
            }]];
            [courseNumber addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [self presentViewController:courseNumber animated:YES completion:nil];
        }]];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)showEmails:(UIButton*)sender {
    if (!allEmails) {
        allEmails = [[UITextView alloc] initWithFrame:CGRectMake(0, 96+[References topMargin], [References screenWidth], bottomBar.frame.origin.y-(96+[References topMargin])+bottomBar.frame.size.height-[References bottomMargin]) textContainer:nil];
        allEmails.alwaysBounceVertical = YES;
        allEmails.allowsEditingTextAttributes = NO;
        allEmails.textContainerInset = UIEdgeInsetsMake(15, 15, 15, 15);
        allEmails.hidden = YES;
        [self.view addSubview:allEmails];
    }
    allEmails.text = emails;
    if (allEmails.hidden == NO) {
        allEmails.hidden = YES;
        [sender setTitle:@"Emails" forState:UIControlStateNormal];
    } else {
        allEmails.hidden = NO;
        [sender setTitle:@"Hide" forState:UIControlStateNormal];
    }
}

-(void)changeController:(UISegmentedControl*)sender {
    NSLog(@"%li",sender.selectedSegmentIndex);
    [table reloadData];
}

-(void)getUsers {
    users = [[NSMutableArray alloc] init];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->emails = @"";
            for (id key in snapshot.value) {
                NSString *name = [(NSDictionary*)snapshot.value[key] valueForKey:@"name"];
                self->emails = [self->emails stringByAppendingString:[NSString stringWithFormat:@"%@, ",[(NSDictionary*)snapshot.value[key] valueForKey:@"email"]]];
                NSLog(@"name: %@,email: %@",name,[(NSDictionary*)snapshot.value[key] valueForKey:@"email"]);
                if ([name isEqualToString:@"EMPTY"] || !name) {
                    name = @"ZZZ";
                }
                [self->users addObject:@{@"name":name,@"id":key,@"data":snapshot.value[key]}];
            }
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                         ascending:YES];
            self->users = [[NSMutableArray alloc] initWithArray:[self->users sortedArrayUsingDescriptors:@[sortDescriptor]]];
            [self->table reloadData];
        }
    }];
}

-(void)analyzeCourses {
    NSMutableDictionary *tcourses = [[NSMutableDictionary alloc] init];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"connections"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        for (id key in snapshot.value) {
            if ([snapshot.value[key] objectForKey:@"sessions"]) {
                for (id sesh in [snapshot.value[key] objectForKey:@"sessions"]) {
                    if ([[snapshot.value[key] objectForKey:@"sessions"] objectForKey:sesh]) {
                        if ([[[snapshot.value[key] objectForKey:@"sessions"] objectForKey:sesh] objectForKey:@"course"]) {
                            NSString *course = [[[snapshot.value[key] objectForKey:@"sessions"] objectForKey:sesh] objectForKey:@"course"];
                            if ([tcourses objectForKey:course]) {
                                NSNumber *count = [tcourses objectForKey:course];
                                count = [NSNumber numberWithInteger:count.integerValue + 1];
                                [tcourses setObject:count forKey:course];
                            } else {
                                [tcourses setObject:@1 forKey:course];
                            }
                        }
                        
                    }
                }
            }
        }
        self->courses = [[NSMutableArray alloc] init];
        for (id key in tcourses) {
            [self->courses addObject:@{@"course":key,@"count":tcourses[key]}];
        }
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"count"
                                                     ascending:NO];
        self->courses = [[NSMutableArray alloc] initWithArray:[self->courses sortedArrayUsingDescriptors:@[sortDescriptor]]];
    }];
}

-(void)getWithdrawals {
    withdrawals = [[NSMutableArray alloc] init];
    [[[[FIRDatabase database] reference] child:@"withdrawals"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            for (id key in snapshot.value) {
                NSNumber *date = [(NSDictionary*)snapshot.value[key] valueForKey:@"numberDate"];
                [self->withdrawals addObject:@{@"date":date,@"id":key,@"data":snapshot.value[key]}];
            }
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                         ascending:NO];
            self->withdrawals = [[NSMutableArray alloc] initWithArray:[self->withdrawals sortedArrayUsingDescriptors:@[sortDescriptor]]];
        }
    }];
}

-(void)getInvoices {
    invoices = [[NSMutableArray alloc] init];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"invoices"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            for (id key in snapshot.value) {
                
                NSNumber *date = [[(NSDictionary*)snapshot.value[key] objectForKey:@"metadata"] valueForKey:@"numberDate"];
                [self->invoices addObject:@{@"date":date,@"id":key,@"data":snapshot.value[key]}];
            }
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
                                                         ascending:NO];
            self->invoices = [[NSMutableArray alloc] initWithArray:[self->invoices sortedArrayUsingDescriptors:@[sortDescriptor]]];
        }
    }];
}

-(void)close {
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)getCodes {
    codes = [[NSMutableArray alloc] init];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"]child:@"tutorCodes"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.hasChildren) {
            [self->codes addObject:@"New Code"];
            for (id key in snapshot.value) {
                [self->codes addObject:key];
            }
        }
    }];
}

-(void)generateCode {
    NSString *code = [References randomNumberLetterStringWithLength:5];
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"]child:@"tutorCodes"] child:code] setValue:@0 withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        [self->codes addObject:code];
        [self->table reloadData];
    }];
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (controller.selectedSegmentIndex == 0) {
        // users
        NSDictionary *user = users[indexPath.row];
        NSDictionary *data = user[@"data"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:user[@"id"]];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:user[@"id"]];
            if ([data[@"name"] isEqualToString:@"EMPTY"]) {
                cell.textLabel.text = user[@"id"];
            } else {
                cell.textLabel.text = data[@"name"];
            }
            cell.detailTextLabel.text = user[@"id"];
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
        }
        return cell;
    } else if (controller.selectedSegmentIndex == 1) {
        // withdrawals
        NSDictionary *withdrawal = withdrawals[indexPath.row];
        NSDictionary *data = withdrawal[@"data"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:withdrawal[@"id"]];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:withdrawal[@"id"]];
            cell.textLabel.text = [NSString stringWithFormat:@"$%@ to %@",data[@"amount"],data[@"venmo"]];
            cell.detailTextLabel.text = data[@"status"];
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
        }
        return cell;
    }  else if (controller.selectedSegmentIndex == 2) {
        // withdrawals
        NSDictionary *invoice = invoices[indexPath.row];
        NSDictionary *data = invoice[@"data"];
        NSDictionary *metadata = data[@"metadata"];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:invoice[@"id"]];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:invoice[@"id"]];
            cell.textLabel.text = [NSString stringWithFormat:@"%@: $%@ on %@",invoice[@"id"],metadata[@"amount"],metadata[@"date"]];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ to %@",data[@"fromName"],data[@"toName"]];
            cell.accessoryType = UITableViewCellAccessoryDetailButton;
        }
        return cell;
    } else if (controller.selectedSegmentIndex == 3) {
        // withdrawals
        NSString *code = codes[indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:code];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:code];
            cell.textLabel.text = code;
        }
        return cell;
    } else if (controller.selectedSegmentIndex == 4) {
        // courses
        NSDictionary *code = courses[indexPath.row];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:code[@"course"]];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:code[@"course"]];
            cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",code[@"course"],code[@"count"]];
        }
        return cell;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // user
    
    if (controller.selectedSegmentIndex == 3) {
        if ([(NSString*)codes[indexPath.row] isEqualToString:@"New Code"]) {
            [self generateCode];
        } else {
            [self copyCode:indexPath.row];
        }
    } else if (controller.selectedSegmentIndex == 0) {
//        adminTutorViewController *adminTutorViewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"adminTutorViewController"];
//        adminTutorViewController.tutorDict = users[indexPath.row];
        adminUpcomingViewController *vc = [[adminUpcomingViewController alloc] init];
        NSDictionary *user = users[indexPath.row];
        vc.uid = user[@"id"];
        [self presentViewController:vc animated:YES completion:nil];
        //    if (!closePreview) {
    } else if (controller.selectedSegmentIndex == 1) {
        // withdrawals
        NSDictionary *invoice = invoices[indexPath.row];
        NSDictionary *data = invoice[@"data"];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Details" message:[NSString stringWithFormat:@"%@",invoice] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Mark %@ Complete",invoice[@"id"]] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[[[[[FIRDatabase database] reference] child:@"withdrawals"]child:invoice[@"id"]] child:@"status"] setValue:@"complete" withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                NSLog(@"complete");
            }];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)copyCode:(NSInteger)index {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        pasteboard.string = codes[index];
     [References toastNotificationBottom:self.view andMessage:@"Copied Code"];
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (controller.selectedSegmentIndex == 0) {
        return users.count;
    } else if (controller.selectedSegmentIndex == 1) {
        return withdrawals.count;
    } else if (controller.selectedSegmentIndex == 3) {
        return codes.count;
    }  else if (controller.selectedSegmentIndex == 4) {
        return courses.count;
    }  else if (controller.selectedSegmentIndex == 2) {
        return invoices.count;
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
@end
