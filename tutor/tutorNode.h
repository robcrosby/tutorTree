//
//  tutorObject.h
//  tutor
//
//  Created by Robert Crosby on 5/18/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "References.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <HCSStarRatingView.h>
#import <AFHTTPSessionManager.h>
#import "dayAvailability.h"

@import Firebase;

@interface tutorNode : NSObject <UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource> {
    BOOL populated;
    BOOL hasRatings;
    NSMutableArray *tutorFeedback,*ratingObjects;
    NSString *name, *profileURL, *uid,*email,*token,*bio;
    NSNumber *pphh,*version;
    CGFloat rating;
    NSMutableArray *availability,*timeButtons,*ratings;
    UILabel *label;
    BOOL skip;
    UIViewController *previewParentView;
    UIScrollView *previewView;
    
}

@property (strong, nonatomic) UIView *tutorPreview;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) tutorNode *next;
@property (strong, nonatomic) tutorNode *previous;
@property (strong, nonatomic) HCSStarRatingView *starRating;

-(id)initAsHead;
-(id)initLocallyWithIdentifier:(NSString*)identifier;
-(id)initWithIdentifier:(NSString*)identifier andImageView:(UIImageView*)image;
-(id)initWithIdentifier:(NSString*)identifier andOptionalButton:(UIButton *)label;
-(id)initWithIdentifier:(NSString*)identifier andOptionalButton:(UIButton *)button andImage:(UIImageView*)image;
-(bool)insertTutor:(tutorNode*)tutor;
-(void)printTutors;
-(NSString*)identifier;
-(NSString*)email;
-(NSString*)profileURL;
-(NSString*)tutorName;
-(NSInteger)tutorPPHH;
-(NSString*)bio;
-(void)updatePPH:(NSNumber*)newPPH;
-(tutorNode*)navigateToTutor:(NSInteger)index;
-(NSInteger)isAvailableOn:(NSInteger)weekDay atTime:(NSInteger)time;
-(NSInteger)earliestAvailabilityOn:(NSInteger)weekDay;
-(BOOL)isAvailableBefore:(NSInteger)weekDay atTime:(NSInteger)time;
-(BOOL)isAvailableAfter:(NSInteger)weekDay atTime:(NSInteger)time;
-(BOOL)isAvailableOn:(NSInteger)weekDay;
-(NSInteger)maxDuration:(NSInteger)weekDay atTime:(NSInteger)time;
-(void)setProfileImage:(UIImageView*)view;
-(void)updateFeaturedLabel:(UILabel*)label;
-(void)setSkip;
-(void)clearSkip;
-(BOOL)returnSkip;
-(void)createInvoices:(NSString*)transactionID andDuration:(NSInteger)duration andType:(NSString*)type studentPaid:(CGFloat)paid andBalance:(NSInteger)usedBalance withCompletion:(void(^)(BOOL finished))complete;
-(UIView*)getTutorPreview;
-(CGFloat)returnRating;
-(BOOL)hasRatings;
-(void)reloadAvailability;
-(void)reloadAvailabilityWithCompletion:(void(^)(BOOL finished))complete;
-(void)processAvailability:(NSMutableArray*)availaiblityStrings;
-(void)bookIntDate:(NSInteger)intDate andSeconds:(float)date andTime:(NSInteger)time forDuration:(NSInteger)duration andTransactionID:(NSString*)transactionID andAvailability:(dayAvailability*)availability withCompletion:(void(^)(BOOL finished))complete;
-(void)changeDateOnConnection:(NSString*)connectionID andIntDate:(NSNumber*)intDate andSeconds:(NSNumber*)date andTime:(NSNumber*)time forDuration:(NSNumber*)duration  withCompletion:(void(^)(BOOL finished))complete;
-(void)bookAdditionalIntDate:(NSInteger)intDate andSeconds:(float)date andTime:(NSInteger)time forDuration:(NSInteger)duration andTransactionID:(NSString*)transactionID withCompletion:(void(^)(BOOL finished))complete;


-(void)addPreviewToView:(UIViewController*)parent;
-(void)removeFromParent;
@end
