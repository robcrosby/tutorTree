//
//  initialViewController.h
//  tutor
//
//  Created by Robert Crosby on 1/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import <AVFoundation/AVFoundation.h>
#import <AFHTTPSessionManager.h>
@import PassKit;

@import Firebase;

@interface initialViewController : UIViewController <UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    UILabel *mainHeading,*subHeading,*mainInstruction;
    UIButton *next,*back,*profileImage,*signIn;
    bool initialLaunch;
    bool understood;
    UITextField *entryField;
    UIView *entryLine;
    bool isSignIn;
    bool becomeTutor;
    NSString *email,*name,*password,*age,*sex,*phone,*verifyCode;
    UIImage *chosenImage;
    //    int stage,date;
//    bool keyboardShowing;
//
//    __weak IBOutlet UISegmentedControl *changeModeButton;
//    __weak IBOutlet UITextField *emailField;
//    __weak IBOutlet UITextField *passwordField;
//    __weak IBOutlet UITextField *nameField;
//    __weak IBOutlet UIButton *ageButton;
//    __weak IBOutlet UIButton *genderButton;
//    __weak IBOutlet UIButton *pictureButton;
//    __weak IBOutlet UILabel *appInfo;
//    UIView *signupView;
//
//    NSMutableArray *scrollSubViews;
//    UIView *circleA,*circleB,*circleC;
//    int circleDiameter;
//    NSString *email,*password,*age,*sex,*name;
}


@property (nonatomic) AVPlayer *backgroundVideo;
@property (strong, nonatomic) FIRDatabaseReference *ref;


@end
