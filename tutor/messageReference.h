//
//  messageReference.h
//  tutor
//
//  Created by Robert Crosby on 7/5/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "References.h"

@import Firebase;

@interface messageReference : NSObject {
    BOOL amSender;
    NSInteger index;
    NSString *message,*messageID;
    UIView *messageView;
    UILabel *messageLabel,*card;
}

-(id)initWithDate:(NSNumber*)tDate andMessage:(NSString*)tMessage andSender:(NSString*)sender andID:(NSString*)tID;
-(instancetype)withDate:(NSNumber*)tDate andMessage:(NSString*)tMessage andSender:(NSString*)sender andID:(NSString*)tID;
-(UIView*)messageView;
-(CGFloat)messageHeight;
-(NSString*)messageID;
-(void)updateColors;
@end
