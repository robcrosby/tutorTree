//
//  manualBookingViewController.m
//  tutor
//
//  Created by Robert Crosby on 2/17/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "manualBookingViewController.h"

@interface manualBookingViewController ()

@end

@implementation manualBookingViewController

-(id)init {
    self=  [super init];
    if (self) {
        self.view.backgroundColor = [UIColor whiteColor];
        [self build];
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)build {

    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin]+5)];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *mainHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [References screenWidth]-30, 44)];
    mainHeader.textAlignment = NSTextAlignmentCenter;
    mainHeader.text = @"New Booking";
    mainHeader.textColor = [UIColor darkTextColor];
    mainHeader.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:mainHeader];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
//    tutorCourse = [[UIButton alloc] initWithFrame:CGRectMake([self screenWidth]-15-100, [References topMargin], 100, 44)];
//    [tutorCourse setTitle:@"Tutor Course" forState:UIControlStateNormal];
//    [tutorCourse setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
//    [tutorCourse.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
//    [tutorCourse setTitleColor:[References accentColor] forState:UIControlStateNormal];
//    [tutorCourse addTarget:self action:@selector(tutorCourse) forControlEvents:UIControlEventTouchUpInside];
//    tutorCourse.hidden = YES;
//    [menuBar addSubview:tutorCourse];
//    [menuBar addSubview:controller];
    view = [[UIScrollView alloc] initWithFrame:CGRectMake(0, [References getBottomYofView:menuBar]+5, [References screenWidth], [References screenHeight]-[References bottomMargin])];
    view.clipsToBounds = false;
    view.alwaysBounceVertical = true;
    [self createInputs];
    [self.view addSubview:view];
    [self.view addSubview:menuBar];

    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
}

-(void)close {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)createInputs {
    UILabel *startLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-20, 17)];
    startLabel.text = @"START TIME";
    startLabel.textAlignment = NSTextAlignmentCenter;
    startLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    startLabel.textColor = [UIColor lightGrayColor];
    [view addSubview:startLabel];
    startDatePicker = [[UIDatePicker alloc] init];
    startDatePicker.frame = CGRectMake(0, [References getBottomYofView:startLabel], [References screenWidth], startDatePicker.frame.size.height);
    startDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [view addSubview:startDatePicker];
    [References createLine:view xPos:0 yPos:[References getBottomYofView:startDatePicker] inFront:true];
    UILabel *endLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:startDatePicker]+10, [References screenWidth]-20, 17)];
    endLabel.text = @"END TIME";
    endLabel.textAlignment = NSTextAlignmentCenter;
    endLabel.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    endLabel.textColor = [UIColor lightGrayColor];
    [view addSubview:endLabel];
    endDatePicker = [[UIDatePicker alloc] init];
    endDatePicker.frame = CGRectMake(0, [References getBottomYofView:endLabel], [References screenWidth], startDatePicker.frame.size.height);
    endDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [view addSubview:endDatePicker];
    [References createLine:view xPos:0 yPos:[References getBottomYofView:endDatePicker] inFront:true];
    studentEmail = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:endDatePicker]+10, [References screenWidth]-20, 25)];
    [studentEmail setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [studentEmail setTitle:@"Student Email" forState:UIControlStateNormal];
    [studentEmail setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [studentEmail addTarget:self action:@selector(lookupEmail:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:studentEmail];
    tutorEmail = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:studentEmail]+15, [References screenWidth]-20, 25)];
    [tutorEmail setTitle:@"Tutor Email" forState:UIControlStateNormal];
    [tutorEmail setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [tutorEmail setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [tutorEmail addTarget:self action:@selector(lookupEmail:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:tutorEmail];
    [References createLine:view xPos:0 yPos:[References getBottomYofView:tutorEmail]+5 inFront:true];
    
    course = [[UITextField alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:tutorEmail]+20, [References screenWidth]-30, 34)];
    course.placeholder = @"Course (Can be anything)";
    course.delegate = self;
    course.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    [References textFieldInset:course];
    [References cornerRadius:course radius:5];
    course.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    course.textColor = [UIColor darkTextColor];
    [view addSubview:course];
    amountForTutor = [[UITextField alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:course]+10, [References screenWidth]-30, 34)];
    amountForTutor.placeholder = @"Income For Tutor (Without Fee)";
    amountForTutor.delegate = self;
    amountForTutor.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    amountForTutor.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    [References textFieldInset:amountForTutor];
    [References cornerRadius:amountForTutor radius:5];
    amountForTutor.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    amountForTutor.textColor = [UIColor darkTextColor];
    [view addSubview:amountForTutor];
    invoiceID = [[UITextField alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:amountForTutor]+10, [References screenWidth]-30, 34)];
    invoiceID.placeholder = @"Braintree ID (Optional)";
    invoiceID.delegate = self;
    invoiceID.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    [References textFieldInset:invoiceID];
    [References cornerRadius:invoiceID radius:5];
    invoiceID.font = [UIFont systemFontOfSize:16 weight:UIFontWeightRegular];
    invoiceID.textColor = [UIColor darkTextColor];
    [view addSubview:invoiceID];
    
    
    preconfirmed = [[UISwitch alloc] init];
    UILabel *confirmedLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:invoiceID]+10, [References screenWidth]-20, preconfirmed.frame.size.height)];
    preconfirmed.frame = CGRectMake([References screenWidth]-15-preconfirmed.frame.size.width, confirmedLabel.frame.origin.y, [References screenWidth]-30, preconfirmed.frame.size.height);
    
    confirmedLabel.text = @"Confirmed?";
    confirmedLabel.font = [UIFont systemFontOfSize:17 weight:UIFontWeightRegular];
    confirmedLabel.textColor = [UIColor lightGrayColor];
    [view addSubview:confirmedLabel];
    [view addSubview:preconfirmed];
    
    UIButton *create = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:confirmedLabel]+10, [References screenWidth]-30, 44)];
    [create setTitle:@"Create Session" forState:UIControlStateNormal];
    [create setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [view addSubview:create];
    [create addTarget:self action:@selector(createSessionWithID) forControlEvents:UIControlEventTouchUpInside];
    view.contentSize = CGSizeMake([References screenWidth], [References getBottomYofView:create]+400);
}

-(bool)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return true;
}

-(void)lookupEmail:(UIButton*)sender {
    UIAlertController *input = [UIAlertController alertControllerWithTitle:@"Lookup Email, Name or Phone" message:@"Enter the email of the user" preferredStyle:UIAlertControllerStyleAlert];
    [input addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Email Address";
    }];
    [input addAction:[UIAlertAction actionWithTitle:@"Search" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self queryEmail:input.textFields[0].text andSender:sender];
    }]];
    [input addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:input animated:true completion:nil];
}

-(void)queryEmail:(NSString*)value andSender:(UIButton*)sender {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSDictionary *list = snapshot.value;
        bool found = false;
        for (id key in list) {
            if ([[list[key] objectForKey:@"email"] isEqualToString:value] || [[list[key] objectForKey:@"name"] isEqualToString:value] || [[list[key] objectForKey:@"phone"] isEqualToString:value]) {
                if ([sender isEqual:self->studentEmail]) {
                    [sender setTitle:[NSString stringWithFormat:@"Student: %@",[list[key] objectForKey:@"name"]] forState:UIControlStateNormal];
                    self->studentID = (NSString*)key;
                    self->studentName = [list[key] objectForKey:@"name"];
                    self->studentToken = [list[key] objectForKey:@"token"] ? [list[key] objectForKey:@"token"] : @"NONE";
                } else {
                    [sender setTitle:[NSString stringWithFormat:@"Tutor: %@",[list[key] objectForKey:@"name"]] forState:UIControlStateNormal];
                    self->tutorID = (NSString*)key;
                    self->tutorName = [list[key] objectForKey:@"name"];
                    self->tutorToken = [list[key] objectForKey:@"token"] ? [list[key] objectForKey:@"token"] : @"NONE";
                }
                dispatch_after(0, dispatch_get_main_queue(), ^(void){
                    [MBProgressHUD hideHUDForView:self.view animated:true];
                });
                found = true;
                break;
            }
        }
        if (!found) {
            dispatch_after(0, dispatch_get_main_queue(), ^(void){
                [MBProgressHUD hideHUDForView:self.view animated:true];
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeText;
                hud.detailsLabel.text = @"Unable to find a user with that information.";
                hud.removeFromSuperViewOnHide = YES;
                hud.margin = 10.f;
                [hud hideAnimated:true afterDelay:1.5];
            });
        }
        
    }];
}


-(void)createSessionWithID {
    @try {
        CGFloat amount = amountForTutor.text.floatValue;
        NSString *tID = invoiceID.text.length > 0 ? invoiceID.text : [References randomNumberLetterStringWithLength:8];
        NSDictionary *data = @{@"student":studentID,@"tutor":tutorID,@"start":[NSNumber numberWithDouble:[startDatePicker.date timeIntervalSince1970]],@"end":[NSNumber numberWithDouble:[endDatePicker.date timeIntervalSince1970]],@"course":course.text ? course.text : @"Undefined",@"status":[NSNumber numberWithBool:preconfirmed.isOn]};
        NSDateFormatter *dfmt = [[NSDateFormatter alloc] init];
        dfmt.dateFormat = @"EEEE, MMM d";
        NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
        tfmt.dateFormat = @"h:mm a";
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Is this correct?" message:[NSString stringWithFormat:@"%@ and %@ on %@ at %@ until %@, Depositing $%.2f in %@'s account and creating an invoice of $%.2f in %@'s account.",studentName,tutorName,[dfmt stringFromDate:startDatePicker.date],[tfmt stringFromDate:startDatePicker.date],[tfmt stringFromDate:endDatePicker.date],amount,tutorName,amount+2.95,studentName] preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"Create Session" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
            fmt.dateFormat = @"MMM d, h:mm a";
            NSDictionary *spendingInvoice = @{@"amount":[NSNumber numberWithFloat:amount+2.95],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":@"Apple Pay",@"usedBalance":[NSNumber numberWithFloat:0]};
            NSDictionary *incomeInvoice = @{@"amount":[NSNumber numberWithFloat:amount],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":@"Apple Pay"};
            NSDictionary *bookingTutor = @{@"start":data[@"start"],@"end":data[@"end"],@"course":data[@"course"],@"other":data[@"student"],@"status":data[@"status"],@"student":@0};
            NSDictionary *bookingStudent = @{@"start":data[@"start"],@"end":data[@"end"],@"course":data[@"course"],@"other":data[@"tutor"],@"status":data[@"status"],@"student":@1};
            NSCalendar* cal = [NSCalendar currentCalendar];
            NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:startDatePicker.date];
            NSDictionary *legacy = @{@"course":data[@"course"],@"date":data[@"start"],@"weekInt":[NSNumber numberWithInteger:[comp weekday]],@"value":@0};
            NSDictionary *globalInvoice = @{@"fromEmail":data[@"student"],@"fromName":data[@"student"],@"metadata":@{@"amount":[NSNumber numberWithFloat:amount+2.95],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithInt:[[NSDate date] timeIntervalSince1970]],@"type":@"Apple Pay"},@"toName":data[@"tutor"],@"toEmail":data[@"tutor"]};
            NSArray *people = [@[data[@"student"],data[@"tutor"]] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            NSString *connectionID = [NSString stringWithFormat:@"%@:%@",people[0],people[1]];
            NSDictionary *delivery = @{[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",data[@"tutor"],tID]: bookingTutor,[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",data[@"student"],tID]:bookingStudent,[NSString stringWithFormat:@"updateDatabase/users/%@/income/%@",data[@"tutor"],tID]:incomeInvoice,[NSString stringWithFormat:@"updateDatabase/users/%@/spending/%@",data[@"student"],tID]:spendingInvoice,[NSString stringWithFormat:@"updateDatabase/invoices/%@",tID]:globalInvoice,[NSString stringWithFormat:@"updateDatabase/connections/%@/sessions/%@",connectionID,tID]:legacy,[NSString stringWithFormat:@"updateDatabase/connections/%@/tutor",connectionID]:data[@"tutor"],[NSString stringWithFormat:@"updateDatabase/connections/%@/student",connectionID]:data[@"student"]};
            [[[FIRDatabase database] reference] updateChildValues:delivery withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
                fmt.dateFormat = @"EEEE, MMM d";
                NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
                tfmt.dateFormat = @"h:mm a";
                NSString *tutorNotification,*studentNotification;
                if (self->preconfirmed.isOn) {
                    tutorNotification =[NSString stringWithFormat:@"%@ has booked you on %@ at %@. They have booked you for help with %@. This session has already been confirmed.",self->studentName,[fmt stringFromDate:self->startDatePicker.date],[tfmt stringFromDate:self->startDatePicker.date],data[@"course"]];
                    studentNotification =[NSString stringWithFormat:@"Your booking with %@ is confirmed for %@ at %@. Message them within the app to help them better prepare for helping you with %@",self->tutorName,[fmt stringFromDate:self->startDatePicker.date],[tfmt stringFromDate:self->startDatePicker.date],data[@"course"]];
                } else {
                    tutorNotification = [NSString stringWithFormat:@"%@ has booked you on %@ at %@. They have booked you for help with %@. This session is awaiting your confirmation.",self->studentName,[fmt stringFromDate:self->startDatePicker.date],[tfmt stringFromDate:self->startDatePicker.date],data[@"course"]];
                    studentNotification =[NSString stringWithFormat:@"Your booking with %@ is pending confirmation for %@ at %@. Message them within the app to help them better prepare for helping you with %@.",self->tutorName,[fmt stringFromDate:self->startDatePicker.date],[tfmt stringFromDate:self->startDatePicker.date],data[@"course"]];
                }
                [References sendNotification:data[@"tutor"] withSubject:@"New Booking" andMessage:tutorNotification forceEmail:YES andCompletion:^(bool result) {
                    if (result) {
                        [References sendNotification:data[@"student"] withSubject:@"Booking Update" andMessage:studentNotification forceEmail:YES andCompletion:^(bool complete) {
                            if (complete) {
                                [self dismissViewControllerAnimated:true completion:nil];
                                NSLog(@"COMPLETE");
                            }
                        }];
                    }
                }];
                
            }];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [self presentViewController:alert animated:true completion:nil];
        
    } @catch (NSException *exception) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeText;
        hud.detailsLabel.text = @"Somethings missing";
        hud.removeFromSuperViewOnHide = YES;
        hud.margin = 10.f;
        [hud hideAnimated:true afterDelay:1.5];
    }
    
    
//    NSString *tID = [transactionID uppercaseString];

}

@end
