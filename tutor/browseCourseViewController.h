//
//  browseCourseViewController.h
//  tutor
//
//  Created by Robert Crosby on 10/24/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "tutorContainerController.h"
#import "tutorObject.h"
#import "References.h"
#import "courseDatabase.h"
#import "tutorViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface browseCourseViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    UISegmentedControl *controller;
    UIButton *tutorCourse;
    NSInteger selectedCalendarTutor;
    tutorObject *currentTutor;
    BOOL amTutor;
    NSMutableArray *days,*tutors,*tList;
    UIView *calendarView;
    UIScrollView *tutorImages;
    UILabel *selectedTutor;
    UITableView *table;
    NSInteger rows;
    UIView *indicator;
}


@property (strong, nonatomic) courseDatabase *course;

@end

NS_ASSUME_NONNULL_END
