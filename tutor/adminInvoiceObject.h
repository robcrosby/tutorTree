//
//  adminInvoiceObject.h
//  tutor
//
//  Created by Robert Crosby on 5/30/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface adminInvoiceObject : NSObject {
    NSDate *dateObject;
    NSNumber *amount;
    NSString *fromEmail,*fromName,*toEmail,*toName, *date, *key,*status;
    BOOL isApple,isWithdrawal;
}
-(NSString*)toTwoLines;
-(NSString*)fromTwoLines;
-(NSString*)date;
-(NSString*)amount;
-(BOOL)isApple;
-(BOOL)isWithdrawal;
-(BOOL)isPending;
-(NSString*)transactionID;
-(id)initWithKey:(NSString*)tKey andDictionary:(NSDictionary*)dictionary;
-(id)initWithdrawalWithKey:(NSString*)tkey andDictionary:(NSDictionary*)dictionary;
+(instancetype)WithKey:(NSString*)tKey andDictionary:(NSDictionary*)dictionary;



@end
