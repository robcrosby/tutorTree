//
//  nGettingStartedViewController.h
//  tutor
//
//  Created by Robert Crosby on 1/31/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import "homeViewController.h"
#import "popoverView.h"
@import Firebase;
@import UserNotifications;

NS_ASSUME_NONNULL_BEGIN

@interface nGettingStartedViewController : UIViewController <UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UNUserNotificationCenterDelegate> {
    int currentPage;
    bool triedNotifications;
    popoverView *supportPopover;
    popoverView *tosPopover;
    UIView *introView,*completeSignUpView;
    UIScrollView *scroll,*bottomBar;
    UILabel *emailRightField;
    UITextField *email,*password,*name,*phone,*activeTF,*emailReturning,*passwordReturning;
    UIButton *picture,*emailNotifications,*smsNotifications,*pushNotifications,*terms,*activeBT,*nextPage,*backPage,*uo,*osu,*ucsb;
    UISwitch *emailSwitch,*smsSwitch,*pushSwitch,*tosSwitch;
    NSString *pictureURL,*emailString;
}

@end

NS_ASSUME_NONNULL_END
