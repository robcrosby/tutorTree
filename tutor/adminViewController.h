//
//  adminViewController.h
//  tutor
//
//  Created by Robert Crosby on 5/27/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import "adminInvoiceObject.h"
#import "adminTutorViewController.h"
#import "adminUpcomingViewController.h"

@import Firebase;

@interface adminViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
    UITableView *table;
    UITextView *allEmails;
    NSString *emails;
    UISegmentedControl *controller;
    NSMutableArray *users,*withdrawals,*codes,*invoices,*courses;
    UIView *bottomBar;
}

@end
