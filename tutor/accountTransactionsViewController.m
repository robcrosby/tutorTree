//
//  accountTransactionsViewController.m
//  tutor
//
//  Created by Robert Crosby on 1/2/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "accountTransactionsViewController.h"

@interface accountTransactionsViewController ()

@end

@implementation accountTransactionsViewController

-(void)viewWillAppear:(BOOL)animated {
//    populated = 0;
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(doneLoading)
//                                                 name:@"ListenForLoading"
//                                               object:nil];
}
//
//-(void)doneLoading {
//    populated++;
//    if (populated >= 2) {
//        dispatch_after(0, dispatch_get_main_queue(), ^(void){
//            [loading hideAnimated:true afterDelay:1];
//        });
//    }
//}

- (void)viewDidLoad {
    expand = [[NSMutableArray alloc] initWithArray:@[@1,@1,@1]];
    fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMMM d, YYYY, h:mm a"];
    self.view.backgroundColor = [References background];
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin])];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [References screenWidth]-30, 44)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = @"Transactions";
    
    header.textColor = [References primaryText];
    header.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:header];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accent] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    [self.view sendSubviewToBack:menuBar];
    table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height, 0, [References bottomMargin], 0);
    table.delegate = self;
    table.backgroundColor = [UIColor clearColor];
    table.dataSource = self;
    [self.view addSubview:table];
    [self.view sendSubviewToBack:table];
    currentBalance = 0;
//    loading = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    loading.mode = MBProgressHUDModeIndeterminate;
//    loading.detailsLabel.text = @"Loading...";
//    loading.removeFromSuperViewOnHide = YES;
//    loading.margin = 10.f;
    [self getTransactions];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)close {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return spending.count;
    } else if (section == 1) {
        return income.count;
    } else if (section == 2) {
        return withdrawals.count;
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
 if (indexPath.section == 0 && [expand[indexPath.section] isEqual:@1]) {
        return 44;
    } else if (indexPath.section == 1 && [expand[indexPath.section] isEqual:@1]) {
        return 44;
    } else if (indexPath.section == 2 && [expand[indexPath.section] isEqual:@1]) {
        return 44;
    } else {
        return 0;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 30)];
    view.backgroundColor = [UIColor clearColor];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 7.5, [References screenWidth]-24, 15)];
    hheader.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    hheader.textColor = [References primaryText];
    if (section == 0) {
        hheader.text = @"SPENDING";
        UIButton *expand = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 7.5, 100, 15)];
        [expand.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightBold]];
        [expand setTitleColor:[References secondaryText] forState:UIControlStateNormal];
        [expand setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [expand setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand.tag = section;
        [expand addTarget:self action:@selector(expandRows:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:expand];
    } else if (section == 1) {
        hheader.text = @"INCOME";
        UIButton *expand = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 7.5, 100, 15)];
        [expand.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightBold]];
        [expand setTitleColor:[References secondaryText] forState:UIControlStateNormal];
        [expand setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [expand setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand.tag = section;
        [expand addTarget:self action:@selector(expandRows:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:expand];
    } else if (section == 2) {
        hheader.text = @"WITHDRAWALS";
        UIButton *expand = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 7.5, 100, 15)];
        [expand.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightBold]];
        [expand setTitleColor:[References secondaryText] forState:UIControlStateNormal];
        [expand setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [expand setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand.tag = section;
        [expand addTarget:self action:@selector(expandRows:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:expand];
    }
    
    [view addSubview:hheader];
    return view;
}


-(void)expandRows:(UIButton*)sender {
    if ([expand[sender.tag] isEqual:@1]) {
        [sender setTitle:@"EXPAND" forState:UIControlStateNormal];
        expand[sender.tag] = @0;
    } else {
        [sender setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand[sender.tag] = @1;
    }
    [table beginUpdates];
    [table endUpdates];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (spending.count > 0) {
            NSDictionary *dict = [spending objectAtIndex:indexPath.row];
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"S%@",dict[@"id"]]];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"S%@",dict[@"id"]]];
                cell.textLabel.text = [NSString stringWithFormat:@"$%.2f",[(NSNumber*)dict[@"amount"] floatValue]];
                cell.detailTextLabel.text = [fmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:[dict[@"numberDate"] floatValue]]];
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake([References screenWidth]-15-23, cell.frame.size.height/2-(23/2), 23, 23)];
                if ([(NSString*)dict[@"type"] isEqualToString:@"Apple Pay"]) {
                    [img setImage:[UIImage imageNamed:@"wallet-app-icon.png"]];
                } else if ([(NSString*)dict[@"type"] isEqualToString:@"Venmo"]) {
                    [img setImage:[UIImage imageNamed:@"venmo-app-icon.jpg"]];
                } else {
                    [img setImage:[UIImage imageNamed:@"icon.png"]];
                }
                [References cornerRadius:img radius:5];
                [img setContentMode:UIViewContentModeScaleAspectFill];
                [cell addSubview:img];
                cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            }
            cell.clipsToBounds = true;
            cell.backgroundColor = [References cells];
            cell.textLabel.textColor = [References primaryText];
            cell.detailTextLabel.textColor = [References secondaryText];
            return cell;
        } else {
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"nospending"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"nospending"];
                cell.textLabel.text = @"No Spending";
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            cell.backgroundColor = [References cells];
            cell.textLabel.textColor = [References primaryText];
            cell.detailTextLabel.textColor = [References secondaryText];
            return cell;
        }
        
    } else if (indexPath.section == 1) {
        if (income.count > 0) {
            NSDictionary *dict = [income objectAtIndex:indexPath.row];
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"I%@",dict[@"id"]]];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"I%@",dict[@"id"]]];
                cell.textLabel.text = [NSString stringWithFormat:@"$%.2f",[(NSNumber*)dict[@"amount"] floatValue]];
                cell.detailTextLabel.text = [fmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:[dict[@"numberDate"] floatValue]]];
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            cell.backgroundColor = [References cells];
            cell.textLabel.textColor = [References primaryText];
            cell.detailTextLabel.textColor = [References secondaryText];
            return cell;
        } else {
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"noincome"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"noincome"];
                cell.textLabel.text = @"No Income";
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            cell.backgroundColor = [References cells];
            cell.textLabel.textColor = [References primaryText];
            cell.detailTextLabel.textColor = [References secondaryText];
            return cell;
        }
    } else {
        if (withdrawals.count > 0) {
            NSDictionary *dict = [withdrawals objectAtIndex:indexPath.row];
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"W%@",dict[@"id"]]];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"W%@",dict[@"id"]]];
                UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 0, 100, 44)];
                status.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
                status.textAlignment = NSTextAlignmentRight;
                if ([dict[@"status"] isEqual:@0]) {
                    status.text = @"PENDING";
                    status.textColor = [References colorFromHexString:@"#FFEA00"];
                } else {
                    status.text = @"COMPLETE";
                    status.textColor = [References accent];
                }
                [cell addSubview:status];
                cell.textLabel.text = [NSString stringWithFormat:@"$%.2f",[(NSNumber*)dict[@"amount"] floatValue]];
                cell.detailTextLabel.text = [fmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:[dict[@"numberDate"] floatValue]]];
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            cell.backgroundColor = [References cells];
            cell.textLabel.textColor = [References primaryText];
            cell.detailTextLabel.textColor = [References secondaryText];
            return cell;
        } else {
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"noincome"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"noincome"];
                cell.textLabel.text = @"No Withdrawals";
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            cell.backgroundColor = [References cells];
            cell.textLabel.textColor = [References primaryText];
            cell.detailTextLabel.textColor = [References secondaryText];
            return cell;
        }
    }
}


-(void)getTransactions {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:[FIRAuth auth].currentUser.uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->income = [[NSMutableArray alloc] init];
            self->spending = [[NSMutableArray alloc] init];
            self->withdrawals = [[NSMutableArray alloc] init];
            NSDictionary *income = snapshot.value[@"income"] ? snapshot.value[@"income"] : nil;
            NSDictionary *spending = snapshot.value[@"spending"] ? snapshot.value[@"spending"] : nil;
            if (income) {
                for (id key in income) {
                    [self->income addObject:@{@"id":key,@"amount":[income[key] valueForKey:@"amount"],@"numberDate":[NSNumber numberWithFloat:[[income[key] valueForKey:@"numberDate"] floatValue]],@"type":[income[key] valueForKey:@"type"]}];
                }
            }
            if (spending) {
                for (id key in spending) {
                    if ([[(NSString*)[spending[key] valueForKey:@"type"] lowercaseString] isEqualToString:@"withdrawal"]) {
                        [self->withdrawals addObject:@{@"id":key,@"amount":[spending[key] valueForKey:@"amount"],@"numberDate":[NSNumber numberWithFloat:[[spending[key] valueForKey:@"numberDate"] floatValue]],@"status":[spending[key] valueForKey:@"status"],@"type":[spending[key] valueForKey:@"type"]}];
                    } else {
                        [self->spending addObject:@{@"id":key,@"amount":[spending[key] valueForKey:@"amount"],@"numberDate":[NSNumber numberWithFloat:[[spending[key] valueForKey:@"numberDate"] floatValue]],@"type":[spending[key] valueForKey:@"type"]}];
                    }
                    
                }
                NSSortDescriptor *sortDescriptor;
                sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"numberDate"
                                                             ascending:NO];
                self->spending = [[NSMutableArray alloc] initWithArray:[self->spending sortedArrayUsingDescriptors:@[sortDescriptor]]];
                self->income = [[NSMutableArray alloc] initWithArray:[self->income sortedArrayUsingDescriptors:@[sortDescriptor]]];
                self->withdrawals = [[NSMutableArray alloc] initWithArray:[self->withdrawals sortedArrayUsingDescriptors:@[sortDescriptor]]];
                [self->table reloadData];
        }
        }
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return [References statusBarColor];
}


@end
