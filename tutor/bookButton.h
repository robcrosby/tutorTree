//
//  bookButton.h
//  tutor
//
//  Created by Robert Crosby on 10/31/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "References.h"
#import "tutorAvailability.h"

NS_ASSUME_NONNULL_BEGIN

@interface bookButton : UIButton {
    NSDate *date;
    NSInteger duration;
    tutorAvailability *tutor;
    NSDictionary *data;
}

-(id)initWithData:(NSDictionary*)tdata andTutor:(tutorAvailability*)ttutor;
-(UIView*)bookView;
-(tutorAvailability*)tutor;
-(void)printData;
-(NSDate*)startDate;
-(NSInteger)duration;

@end

NS_ASSUME_NONNULL_END
