//
//  paymentProcessingView.m
//  tutor
//
//  Created by Robert Crosby on 5/7/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "paymentProcessingView.h"

@implementation paymentProcessingView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.frame = UIScreen.mainScreen.bounds;
        self.alpha = 0;
        [References blurView:self];
    }
    return self;
}

-(id)initWithNonce:(NSString*)_nonce andAmount:(NSString*)_amount invoices:(NSDictionary*)invoices{
    self = [super initWithFrame:UIScreen.mainScreen.bounds];
    if (self) {
        amount = _amount;
        nonce = _nonce;
        step = 0;
        self.alpha = 0;
        [References blurView:self];
        statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, [References topMargin]+200, [References screenWidth]-60, 30)];
        statusLabel.textColor = [References primaryText];
        statusLabel.text = @"Processing Payment";
        statusLabel.textAlignment = NSTextAlignmentCenter;
        statusLabel.font = [UIFont systemFontOfSize:16];
        statusProgress = [[UIProgressView alloc] initWithFrame:CGRectMake(30, [References getBottomYofView:statusLabel], [References screenWidth]-60, 10)];
        statusProgress.progressTintColor = [References accent];
        [self addSubview:statusLabel];
        [self addSubview:statusProgress];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1;
    }];
}

-(void)updateStatus:(CGFloat)status withText:(NSString*)text completion:(void (^)(BOOL))complete{
    [UIView animateWithDuration:0.125 delay:0.25 options:UIViewAnimationOptionCurveLinear animations:^{
        self->statusLabel.alpha = 0;
        [self->statusProgress setProgress:status/2];
    } completion:^(BOOL finished) {
        if (finished) {
            self->statusLabel.text = text;
            [UIView animateWithDuration:0.125 animations:^{
                self->statusLabel.alpha = 1;
                [self->statusProgress setProgress:status];
            } completion:^(BOOL finished) {
                if (finished) {
                    complete(true);
                }
            }];
        }
    }];
}

- (void)passNonce {
    NSString *requestURL = @"https://tutortree-release.herokuapp.com/payment";
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"application/json"];
    NSDictionary *checkout = @{@"nonce":nonce,
                               @"amount":amount,
                               @"sandbox" : [[NSUserDefaults standardUserDefaults] boolForKey:@"sandbox"] ? @1 : @0
                               };
    [manager POST:requestURL parameters:checkout progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSDictionary *responseDict = responseObject;
        NSLog(@"%@",responseDict);
        [self updateStatus:0.33 withText:@"Creating Documents" completion:^(BOOL finished) {
            if (finished) {
                
            }
        }];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)createInvoices {
    [[[[FIRDatabase database] reference] child:@"updateDatabase"] updateChildValues:invoices withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (!error) {
            [self updateStatus:0.66 withText:@"Connecting You" completion:^(BOOL finished) {
                if (finished) {
                    
                }
            }];
        }
    }];
}


@end
