//
//  bookingList.h
//  tutor
//
//  Created by Robert Crosby on 6/5/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface bookingList : NSObject {
    NSDate *date;
    NSString *transactionID;
    NSNumber *duration,*intDate,*startTime;
}

-(id)initWithDictionary:(NSDictionary*)dictionary andID:(NSString*)tID;
+(instancetype)WithDictionary:(NSDictionary*)dictionary andID:(NSString*)tID;
-(NSNumber*)duration;
-(NSNumber*)intDate;
-(NSNumber*)startTime;
-(NSDate*)date;
-(BOOL)hasPassed;
-(NSString*)transactionID;
-(NSString*)dateAndTime;
-(float)timeSince1970;
@end

NS_ASSUME_NONNULL_END
