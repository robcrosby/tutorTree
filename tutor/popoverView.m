//
//  popoverView.m
//  tutor
//
//  Created by Robert Crosby on 10/10/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import "popoverView.h"

@implementation popoverView

-(id)initWithSuperview:(UIViewController*)controller {
    self = [super init];
    if (self) {
        parent = controller;
        self.frame = CGRectMake(0, 0, [References screenWidth], [References screenHeight]);
        scrollview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, [References screenWidth], [References screenHeight])];
        scrollview.contentInset = UIEdgeInsetsMake(0, 0, 44,0);
        scrollview.alwaysBounceVertical = YES;
        scrollview.showsVerticalScrollIndicator = NO;
        UIView *card = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height*20)];
        [References blurView:card];
        [References cornerRadius:card radius:5];
        [scrollview addSubview:card];
        
        bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height+44+[References bottomMargin], self.frame.size.width, 44+[References bottomMargin])];
        [References blurView:bottomBar];
        decline = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, bottomBar.frame.size.width/2, 44)];
        [decline setTitle:@"Decline" forState:UIControlStateNormal];
        [decline setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        decline.hidden = YES;
        [bottomBar addSubview:decline];
        accept = [[UIButton alloc] initWithFrame:CGRectMake(bottomBar.frame.size.width/2, 0, bottomBar.frame.size.width/2, 44)];
        [accept setTitleColor:[References schoolMainColor] forState:UIControlStateNormal];
        [accept setTitle:@"Accept" forState:UIControlStateNormal];
        accept.hidden = YES;
        [bottomBar addSubview:accept];
        ok = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, bottomBar.frame.size.width, 44)];
        [ok setTitleColor:[References schoolMainColor] forState:UIControlStateNormal];
        [ok setTitle:@"Ok" forState:UIControlStateNormal];
        ok.hidden = YES;
        [bottomBar addSubview:ok];
        [References createLine:bottomBar xPos:0 yPos:0 inFront:YES];
        [self addSubview:scrollview];
        [self addSubview:bottomBar];
        scrollview.tag = 0;
    }
    return self;
}

-(void)setCantHide {
    cantHide = YES;
}

-(void)showView {
    parentDarken = [[UILabel alloc] initWithFrame:parent.view.bounds];
    parentDarken.backgroundColor = [UIColor blackColor];
    parentDarken.alpha = 0;
    [parent.view addSubview:parentDarken];
    [parent.view addSubview:self];
    [UIView animateWithDuration:0.33 animations:^{
        self->bottomBar.frame = CGRectMake(self->bottomBar.frame.origin.x, self.frame.size.height-self->bottomBar.frame.size.height, self->bottomBar.frame.size.width, self->bottomBar.frame.size.height);
        self->parentDarken.alpha = 0.3;
        self->scrollview.frame = self.bounds;
    } completion:^(BOOL finished) {
        if (finished) {
            self->scrollview.delegate = self;
        }
    }];
}

-(void)hideView {
    [UIView animateWithDuration:0.33 animations:^{
        self->parentDarken.alpha = 0;
        self->scrollview.frame = CGRectMake(0, [References screenHeight], [References screenWidth], [References screenHeight]);
        self->bottomBar.frame = CGRectMake(self->bottomBar.frame.origin.x, self.frame.size.height, self->bottomBar.frame.size.width, self->bottomBar.frame.size.height);
    } completion:^(BOOL finished) {
        if (finished) {
            self->bottomBar = nil;
            self->scrollview.delegate = nil;
            self->scrollview = nil;
            [self->parentDarken removeFromSuperview];
            self->parentDarken = nil;
            [self removeFromSuperview];
        }
    }];
}

-(void)addTitle:(NSString*)title {
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, [References screenWidth]-24, 30)];
    header.textAlignment = NSTextAlignmentLeft;
    header.text = title;
    header.textColor = [References primaryText];
    header.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    scrollview.tag+=(10+header.frame.size.height);
    [scrollview addSubview:header];
}

-(BOOL)containsView:(UIView *)view {
    for (UIView *tv in scrollview.subviews) {
        if ([tv isEqual:view]) {
            return YES;
            break;
        }
    }
    return NO;
}

-(void)addView:(UIView*)view {
    view.frame = CGRectMake(view.frame.origin.x, scrollview.tag+15, view.frame.size.width, view.frame.size.height);
    scrollview.tag+=(10+view.frame.size.height);
    if (scrollview.tag > scrollview.frame.size.height-bottomBar.frame.size.height-[References topMargin]) {
        scrollview.contentSize = CGSizeMake(scrollview.frame.size.width, scrollview.tag+bottomBar.frame.size.height+25);
    }
    
    [scrollview addSubview:view];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y < -60) {
        if (!cantHide) {
            [self hideView];
        }
    }
}

-(void)setAcceptFunction:(SEL)function {
    ok.hidden = YES;
    accept.hidden = NO;
    [accept addTarget:parent action:function forControlEvents:UIControlEventTouchUpInside];
}
-(void)setDeclineFunction:(SEL)function {
    ok.hidden = YES;
    decline.hidden = NO;
    [decline addTarget:parent action:function forControlEvents:UIControlEventTouchUpInside];
}
-(void)setOkFunction:(SEL)function {
    ok.hidden = NO;
    accept.hidden = YES;
    decline.hidden = YES;
    [ok addTarget:parent action:function forControlEvents:UIControlEventTouchUpInside];
}
@end
