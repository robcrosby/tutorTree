//
//  tutor.h
//  tutor
//
//  Created by Robert Crosby on 10/23/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "References.h"
#import "BraintreeVenmo.h"
#import "BraintreeCore.h"
#import "BraintreeApplePay.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AFHTTPSessionManager.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "bookButton.h"
//#import "homeViewController.h"

@import PassKit;
@import Firebase;

NS_ASSUME_NONNULL_BEGIN

@interface tutorObject : NSObject <UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource,PKPaymentAuthorizationViewControllerDelegate> {
    UIView *statisticsView;
    UILabel *averageRatingLabel,*sessionsLabel,*hourlylabel;
    NSInteger upcomingSessionTotals,upcomingSessionCounter;
    NSInteger sessionCount;
    NSNumber *pph;
    UILabel *smallViewName,*smallViewDetails;
    UIImageView *smallViewImage;
    NSString *identifier,*name,*bio,*profileURL,*token;
    NSArray *courses;
    NSMutableArray *calAvailable,*availability,*availableSlots,*upcomingSessions,*ratings;
    UIScrollView *profileCoursesView;
    UIView *profileSummaryView,*bookHelpView,*bookView,*confirm,*overlay;
    CGFloat currentBalance,cost,discount,balanceUsed;
    BTVenmoAccountNonce *paymentVenmo;
    UITableView *table;
    UIView *view,*smallView,*internalAvailable;
    UIScrollView *availabilityView;
    NSDate *selectedStartDate,*selectedEndDate;
    NSInteger maxDuration,setDuration;
    NSString *selectedCourse;
    UIPickerView *startTimePicker;
    UILabel *durationLabel,*receiptStart,*receiptEnd,*receiptTotal,*finalReceiptTotal,*finalReceiptDiscount;
    UIButton *subtractTime,*addTime,*venmo,*selectCourse;
    BOOL expandSchedule,expandCourses,noAvailability;
    NSNumber *usedBalance;
    PKPaymentButton *apple;
    NSString *clientToken,*paymentType,*venmoNonce;
    CGFloat totalCost;
    UIViewController *parent;
    bookButton *selectedBook;
    UIView *calOverlay,*calParent;
    UILabel *calDuration,*calStart,*calEnd,*calEndLine;
    NSNumber *averageRating;
}

@property (nonatomic, strong) BTVenmoDriver *venmoDriver;
@property (nonatomic, strong) BTAPIClient *braintreeClient;

-(id)initWithID:(NSString*)ID andCompletion:(void (^)(bool finished))complete;
-(UIView*)bookTutorView:(UIViewController*)tParent withInsets:(UIEdgeInsets)insets;
-(UIView*)smallView;
-(void)getAvailableViewInView:(CGRect)rect inView:(UIView*)view andParentVC:(UIViewController*)vc withCourse:(NSString*)course ;
-(void)freeAvailableView;
-(BOOL)isAvailableOn:(NSInteger)weekday;
-(BOOL)getAvailabilityForWeekDay:(int)weekday andTime:(int)time;

-(NSString*)identifier;
-(NSURL*)profileURL;
-(NSString*)name;

@end

NS_ASSUME_NONNULL_END
