//
//  adminWithdrawalsViewController.m
//  tutor
//
//  Created by Robert Crosby on 1/3/19.
//  Copyright © 2019 Robert Crosby. All rights reserved.
//

#import "adminWithdrawalsViewController.h"

@interface adminWithdrawalsViewController ()

@end

@implementation adminWithdrawalsViewController

- (void)viewDidLoad {
    expand = [[NSMutableArray alloc] initWithArray:@[@1,@1,@1]];
    fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"MMMM d, YYYY, h:mm a"];
    self.view.backgroundColor = [UIColor whiteColor];
    UIView *menuBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 44+[References topMargin])];
    UILabel *blur = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, menuBar.frame.size.width, menuBar.frame.size.height)];
    [References blurView:blur];
    [menuBar addSubview:blur];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, [References topMargin], [References screenWidth]-30, 44)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = @"Withdrawals";
    
    header.textColor = [UIColor darkTextColor];
    header.font = [UIFont systemFontOfSize:17 weight:UIFontWeightSemibold];
    [menuBar addSubview:header];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, [References topMargin], 50, 44)];
    [back setTitle:@"Back" forState:UIControlStateNormal];
    [back setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [back.titleLabel setFont:[UIFont systemFontOfSize:17 weight:UIFontWeightRegular]];
    [back setTitleColor:[References accentColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [menuBar addSubview:back];
    [References createLine:menuBar xPos:0 yPos:menuBar.frame.size.height-1 inFront:YES];
    [self.view addSubview:menuBar];
    [self.view sendSubviewToBack:menuBar];
    table = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.contentInset = UIEdgeInsetsMake(menuBar.frame.size.height, 0, [References bottomMargin], 0);
    table.delegate = self;
    table.dataSource = self;
    [self.view addSubview:table];
    [self.view sendSubviewToBack:table];
    [self getWithdrawals];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)close {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (pending.count > 0) {
            return pending.count;
        } else {
            return 1;
        }
    } else if (section == 1) {
        if (complete.count > 0) {
            return complete.count;
        } else {
            return 1;
        }
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && [expand[indexPath.section] isEqual:@1]) {
        return 44;
    } else if (indexPath.section == 1 && [expand[indexPath.section] isEqual:@1]) {
        return 44;
    } else {
        return 0;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 30)];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 7.5, [References screenWidth]-24, 15)];
    hheader.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
    hheader.textColor = [UIColor lightGrayColor];
    if (section == 0) {
        hheader.text = @"PENDING";
        UIButton *expand = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 7.5, 100, 15)];
        [expand.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightBold]];
        [expand setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [expand setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [expand setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand.tag = section;
        [expand addTarget:self action:@selector(expandRows:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:expand];
    } else if (section == 1) {
        hheader.text = @"COMPLETE";
        UIButton *expand = [[UIButton alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 7.5, 100, 15)];
        [expand.titleLabel setFont:[UIFont systemFontOfSize:13 weight:UIFontWeightBold]];
        [expand setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [expand setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [expand setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand.tag = section;
        [expand addTarget:self action:@selector(expandRows:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:expand];
    }
    
    [view addSubview:hheader];
    return view;
}


-(void)expandRows:(UIButton*)sender {
    if ([expand[sender.tag] isEqual:@1]) {
        [sender setTitle:@"EXPAND" forState:UIControlStateNormal];
        expand[sender.tag] = @0;
    } else {
        [sender setTitle:@"COLLAPSE" forState:UIControlStateNormal];
        expand[sender.tag] = @1;
    }
    [table beginUpdates];
    [table endUpdates];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (pending.count > 0) {
            NSDictionary *dict = [pending objectAtIndex:indexPath.row];
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"P%@",dict[@"id"]]];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"P%@",dict[@"id"]]];
                UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 0, 100, 44)];
                status.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
                status.textAlignment = NSTextAlignmentRight;
                status.text = @"PENDING";
                status.textColor = [References colorFromHexString:@"#FFEA00"];
                [cell addSubview:status];
                cell.textLabel.text = [NSString stringWithFormat:@"$%.2f",[(NSNumber*)dict[@"amount"] floatValue]];
                cell.detailTextLabel.text = [fmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:[dict[@"numberDate"] floatValue]]];
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            return cell;
        } else {
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"nopending"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"nopending"];
                cell.textLabel.text = @"No Pending Withdrawals";
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            return cell;
        }
    } else {
        if (complete.count > 0) {
            NSDictionary *dict = [complete objectAtIndex:indexPath.row];
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"C%@",dict[@"id"]]];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:[NSString stringWithFormat:@"C%@",dict[@"id"]]];
                UILabel *status = [[UILabel alloc] initWithFrame:CGRectMake([References screenWidth]-15-100, 0, 100, 44)];
                status.font = [UIFont systemFontOfSize:13 weight:UIFontWeightRegular];
                status.textAlignment = NSTextAlignmentRight;
                status.text = @"COMPLETE";
                status.textColor = [References accentColor];
                [cell addSubview:status];
                cell.textLabel.text = [NSString stringWithFormat:@"$%.2f",[(NSNumber*)dict[@"amount"] floatValue]];
                cell.detailTextLabel.text = [fmt stringFromDate:[NSDate dateWithTimeIntervalSince1970:[dict[@"numberDate"] floatValue]]];
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            return cell;
        } else {
            UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:@"nocomplete"];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"nocomplete"];
                cell.textLabel.text = @"No Complete Withdrawals";
            }
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.clipsToBounds = true;
            return cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict;
    UIAlertController *alert;
    if (indexPath.section == 0) {
        dict = [pending objectAtIndex:indexPath.row];
        alert = [UIAlertController alertControllerWithTitle:dict[@"id"] message:[NSString stringWithFormat:@"Venmo: %@",dict[@"venmo"]] preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"Mark Complete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.detailsLabel.text = @"Processing";
            [References blurView:hud.backgroundView];
            hud.removeFromSuperViewOnHide = YES;
            hud.margin = 10.f;
            [[[[FIRDatabase database] reference] child:@"updateDatabase"] updateChildValues:@{[NSString stringWithFormat:@"/withdrawals/%@/status",dict[@"id"]]:@1,[NSString stringWithFormat:@"/users/%@/spending/%@/status",dict[@"uid"],dict[@"id"]]:@1} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    dispatch_after(0, dispatch_get_main_queue(), ^(void){
                        [hud hideAnimated:true afterDelay:1];
                    });
                }
            }];
        }]];
    } else {
        dict = [complete objectAtIndex:indexPath.row];
        alert = [UIAlertController alertControllerWithTitle:dict[@"id"] message:[NSString stringWithFormat:@"Venmo: %@",dict[@"venmo"]] preferredStyle:UIAlertControllerStyleActionSheet];
        [alert addAction:[UIAlertAction actionWithTitle:@"Mark Pending" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.detailsLabel.text = @"Processing";
            [References blurView:hud.backgroundView];
            hud.removeFromSuperViewOnHide = YES;
            hud.margin = 10.f;
            [[[[FIRDatabase database] reference] child:@"updateDatabase"] updateChildValues:@{[NSString stringWithFormat:@"/withdrawals/%@/status",dict[@"id"]]:@0,[NSString stringWithFormat:@"/users/%@/spending/%@/status",dict[@"uid"],dict[@"id"]]:@0} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
                if (!error) {
                    dispatch_after(0, dispatch_get_main_queue(), ^(void){
                        [hud hideAnimated:true afterDelay:1];
                    });
                }
            }];
        }]];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"Contact User Via Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:dict[@"uid"]] child:@"email"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            MFMailComposeViewController* composeVC = [[MFMailComposeViewController alloc] init];
            composeVC.mailComposeDelegate = self;
            
            // Configure the fields of the interface.
            [composeVC setToRecipients:@[snapshot.value]];
            [composeVC setSubject:@"Tutortree Withdrawal Request"];
            
            // Present the view controller modally.
            if ([MFMailComposeViewController canSendMail]) {
                // device is configured to send mail
                [self presentViewController:composeVC animated:YES completion:^{
                }];
            } else {
                [References toastNotificationBottom:self.view andMessage:@"Can't send mail."];
            }
            
        }];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [self presentViewController:alert animated:true completion:nil];
}

-(void)getWithdrawals {
    self->pending = [[NSMutableArray alloc] init];
    self->complete = [[NSMutableArray alloc] init];
    [[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"withdrawals"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            [self->pending removeAllObjects];
            [self->complete removeAllObjects];
            for (id key in snapshot.value) {
                if ([[snapshot.value[key] valueForKey:@"status"] isEqual:@0]) {
                    [self->pending addObject:@{@"id":key,@"amount":[snapshot.value[key] valueForKey:@"amount"],@"numberDate":[NSNumber numberWithFloat:[[snapshot.value[key] valueForKey:@"numberDate"] floatValue]],@"status":[snapshot.value[key] valueForKey:@"status"],@"type":[snapshot.value[key] valueForKey:@"type"],@"venmo":[snapshot.value[key] valueForKey:@"venmo"],@"uid":[snapshot.value[key] valueForKey:@"uid"]}];
                } else {
                   [self->complete addObject:@{@"id":key,@"amount":[snapshot.value[key] valueForKey:@"amount"],@"numberDate":[NSNumber numberWithFloat:[[snapshot.value[key] valueForKey:@"numberDate"] floatValue]],@"status":[snapshot.value[key] valueForKey:@"status"],@"type":[snapshot.value[key] valueForKey:@"type"],@"venmo":[snapshot.value[key] valueForKey:@"venmo"],@"uid":[snapshot.value[key] valueForKey:@"uid"]}];
                }
            }
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"numberDate"
                                                         ascending:NO];
            self->pending = [[NSMutableArray alloc] initWithArray:[self->pending sortedArrayUsingDescriptors:@[sortDescriptor]]];
            self->complete = [[NSMutableArray alloc] initWithArray:[self->complete sortedArrayUsingDescriptors:@[sortDescriptor]]];
            [self->table reloadData];
        }
    }];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    // Check the result or perform other tasks.
    if (result == MFMailComposeResultSent) {
        [References toastNotificationBottom:self.view andMessage:@"Sent"];
    }
    // Dismiss the mail compose view controller.
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
