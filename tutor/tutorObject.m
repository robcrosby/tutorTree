//
//  tutor.m
//  tutor
//
//  Created by Robert Crosby on 10/23/18.
//  Copyright © 2018 Robert Crosby. All rights reserved.
//
#define MSB 0x8000000000000000
#import "tutorObject.h"

@implementation tutorObject


-(id)initWithID:(NSString *)ID andCompletion:(void (^)(bool finished))complete{
    self = [super init];
    if (self) {
        expandSchedule = YES;
        expandCourses = YES;
        [self initializeTableCells];
        [self initializeSmallView];
        identifier = ID;
        [self listenForAvailabilityWithCompletion:^(bool finished) {
            if (finished) {
                [self listenForSessions:^(bool finished) {
                    if (finished) {
                        [self processAvailabilityAndCompletion:^(bool finished) {
                            if (finished) {
                                complete(YES);
                            }
                        }];
                        [self listenForProfileDetails];
                        [self listenForRatings];
                    }
                }];
            }
        }];
        
    }
    return self;
}

-(id)initWithID:(NSString *)ID andTable:(UITableView*)table {
    self = [super init];
    if (self) {
        identifier = ID;
        [self listenForAvailabilityWithCompletion:^(bool finished) {
            if (finished) {
                [self listenForSessions:^(bool finished) {
                    if (finished) {
                        [self processAvailabilityAndCompletion:^(bool finished) {
                            if (finished) {
                                NSLog(@"LOADED");
                                [table reloadData];
                            }
                        }];
                    }
                }];
            }
        }];
        
    }
    return self;
}


-(void)initializeSmallView {
    smallView = [[UIView alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-30, 90)];
    smallView.backgroundColor = [References background];
    [References cornerRadius:smallView radius:5];
    smallViewImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 70, 70)];
    [References cornerRadius:smallViewImage radius:5];
    [smallView addSubview:smallViewImage];
    smallViewName = [[UILabel alloc] initWithFrame:CGRectMake(10+70+10, 10, smallView.frame.size.width-(10+70+10+10), 20)];
    smallViewName.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    smallViewName.textColor = [References primaryText];
    [smallView addSubview:smallViewName];
    smallViewDetails = [[UILabel alloc] initWithFrame:CGRectMake(10+70+10, [References getBottomYofView:smallViewName], smallView.frame.size.width-(10+70+10+10), 10)];
    smallViewDetails.font = [UIFont systemFontOfSize:9 weight:UIFontWeightBold];
    smallViewDetails.textColor = [UIColor lightGrayColor];
    [smallView addSubview:smallViewDetails];
    internalAvailable = [[UIView alloc] initWithFrame:CGRectMake(smallViewName.frame.origin.x, [References getBottomYofView:smallViewDetails]+5, smallView.frame.size.width, 20)];
    [smallView addSubview:internalAvailable];
    
    
}

-(UIView*)smallView {
    return smallView;
}

-(NSString*)identifier {
    return identifier;
}

-(NSURL*)profileURL {
    return [NSURL URLWithString:profileURL];
}
-(NSString*)name {
    return name;
}



-(UILabel*)loadingPlaceHolderWithParent:(UIView*)parent {
    UILabel *loading = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, parent.frame.size.width, parent.frame.size.height)];
    loading.text = @"Loading";
    loading.textAlignment = NSTextAlignmentCenter;
    loading.textColor = [References primaryText];
    loading.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
    return loading;
}

-(void)initializeTableCells {
    profileSummaryView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 100)];
    profileSummaryView.backgroundColor = [References background];
    statisticsView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, [References screenWidth]-30, 70)];
    statisticsView.backgroundColor = [References cells];
    [References cornerRadius:statisticsView radius:5];
    CGFloat Swidth = statisticsView.frame.size.width/3;
    NSArray *statCats = @[@"SESSIONS",@"AVERAGE",@"HOURLY"];
    for (int a = 0; a < statCats.count; a++) {
        UILabel *cat = [[UILabel alloc] initWithFrame:CGRectMake(a*Swidth, 0, Swidth, 30)];
        cat.font = [UIFont systemFontOfSize:10 weight:UIFontWeightSemibold];
        cat.textAlignment = NSTextAlignmentCenter;
        cat.textColor = [References primaryText];
        cat.text = statCats[a];
        [statisticsView addSubview:cat];
    }
    sessionsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, Swidth, statisticsView.frame.size.height-45)];
    sessionsLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightSemibold];
    sessionsLabel.textColor = [References primaryText];
    sessionsLabel.textAlignment = NSTextAlignmentCenter;
    [statisticsView addSubview:sessionsLabel];
    averageRatingLabel = [[UILabel alloc] initWithFrame:CGRectMake(Swidth*1, 30, Swidth, statisticsView.frame.size.height-45)];
    averageRatingLabel.textAlignment = NSTextAlignmentCenter;
    averageRatingLabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightSemibold];
    averageRatingLabel.textColor = [References primaryText];
    [statisticsView addSubview:averageRatingLabel];
    hourlylabel = [[UILabel alloc] initWithFrame:CGRectMake(Swidth*2, 30, Swidth, statisticsView.frame.size.height-45)];
    hourlylabel.font = [UIFont systemFontOfSize:25 weight:UIFontWeightSemibold];
    hourlylabel.textColor = [References primaryText];
    hourlylabel.textAlignment = NSTextAlignmentCenter;
    [statisticsView addSubview:hourlylabel];
    [profileSummaryView addSubview:[self loadingPlaceHolderWithParent:profileSummaryView]];
    profileCoursesView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 125)];
    profileCoursesView.contentSize = CGSizeMake(profileCoursesView.frame.size.width, profileCoursesView.frame.size.height);
    profileCoursesView.backgroundColor = [References cells];
    profileCoursesView.showsHorizontalScrollIndicator = NO;
    profileCoursesView.alwaysBounceHorizontal = YES;
    [profileCoursesView addSubview:[self loadingPlaceHolderWithParent:profileCoursesView]];
    startTimePicker = [[UIPickerView alloc] init];
    startTimePicker.delegate = self;
    UILabel *card = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-30, 0)];
    card.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.15];

    selectCourse = [[UIButton alloc] initWithFrame:CGRectMake(20, 10+5, card.frame.size.width-10, 44)];
    [selectCourse.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightBold]];
    [selectCourse setTitle:@"Select Course" forState:UIControlStateNormal];
    [selectCourse setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [References cornerRadius:selectCourse radius:5];
    [selectCourse addTarget:self action:@selector(selectCourse:) forControlEvents:UIControlEventTouchUpInside];

    startTimePicker.frame = CGRectMake(15, [References getBottomYofView:selectCourse], card.frame.size.width, 150);
    startTimePicker.backgroundColor = [References lightGrey];
    startTimePicker.showsSelectionIndicator = YES;
//    startTimePicker.layer.borderColor = [UIColor blackColor].CGColor;
//    startTimePicker.layer.borderWidth = 0.2;
    bookView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 150)];
    bookView.clipsToBounds = NO;
    bookView.backgroundColor = [References lightGrey];
    
        [bookView addSubview:card];
        [bookView addSubview:selectCourse];
    CGFloat width = (startTimePicker.frame.size.width)/3;
    UILabel *durationCard = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x, [References getBottomYofView:startTimePicker], startTimePicker.frame.size.width, 40)];
    durationCard.backgroundColor = [References lightGrey];

    [bookView addSubview:durationCard];
    durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+width, [References getBottomYofView:startTimePicker], width, 40)];
    durationLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
//    durationLabel.backgroundColor = [References lightGrey];
    durationLabel.textAlignment = NSTextAlignmentCenter;
    durationLabel.textColor = [UIColor darkTextColor];
    durationLabel.text = @"30 MINUTES";
    [bookView addSubview:durationLabel];
    subtractTime = [[UIButton alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x,durationLabel.frame.origin.y,width,40)];
    [subtractTime setTitle:@"-30 MINUTES" forState:UIControlStateNormal];
    [subtractTime.titleLabel setFont:[UIFont systemFontOfSize:9]];
    [subtractTime addTarget:self action:@selector(subtractMinutes) forControlEvents:UIControlEventTouchUpInside];
    [subtractTime setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
//    [subtractTime setBackgroundColor:[References lightGrey]];
    [bookView addSubview:subtractTime];
    addTime = [[UIButton alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+width+width,durationLabel.frame.origin.y,width,40)];
    [addTime addTarget:self action:@selector(addMinutes) forControlEvents:UIControlEventTouchUpInside];
    [addTime setTitle:@"+30 MINUTES" forState:UIControlStateNormal];
    [addTime.titleLabel setFont:[UIFont systemFontOfSize:9]];
    [addTime setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
//    [addTime setBackgroundColor:[References lightGrey]];
    [bookView addSubview:addTime];
    [References createLinewithWidth:bookView xPos:card.frame.origin.x yPos:[References getBottomYofView:startTimePicker] inFront:YES andWidth:card.frame.size.width];
    [References createLinewithWidth:bookView xPos:card.frame.origin.x yPos:[References getBottomYofView:addTime] inFront:YES andWidth:card.frame.size.width];
    UILabel *startHeader = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+5, [References getBottomYofView:addTime]+10, startTimePicker.frame.size.width-10, 15)];
    startHeader.font = [UIFont systemFontOfSize:8 weight:UIFontWeightSemibold];
    startHeader.textColor = [UIColor lightGrayColor];
    startHeader.text = @"FROM";
    startHeader.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:startHeader];
    receiptStart = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+5, [References getBottomYofView:startHeader], startTimePicker.frame.size.width-10, 30)];
    receiptStart.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    receiptStart.textColor = [UIColor darkTextColor];
    receiptStart.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:receiptStart];
    UILabel *endHeader = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+5, [References getBottomYofView:receiptStart], startTimePicker.frame.size.width-10, 15)];
    endHeader.font = [UIFont systemFontOfSize:8 weight:UIFontWeightSemibold];
    endHeader.textColor = [UIColor lightGrayColor];
    endHeader.text = @"UNTIL";
    endHeader.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:endHeader];
    receiptEnd = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+5, [References getBottomYofView:endHeader], startTimePicker.frame.size.width-10, 30)];
    receiptEnd.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    receiptEnd.textColor = [UIColor darkTextColor];
    receiptEnd.textAlignment = NSTextAlignmentRight;
    [bookView addSubview:receiptEnd];
    UILabel *totalHeader = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+5, [References getBottomYofView:receiptEnd], startTimePicker.frame.size.width-10, 15)];
    totalHeader.font = [UIFont systemFontOfSize:8 weight:UIFontWeightSemibold];
    totalHeader.textColor = [UIColor lightGrayColor];
    totalHeader.textAlignment = NSTextAlignmentRight;
    totalHeader.text = @"TOTAL";
    [bookView addSubview:totalHeader];
    receiptTotal = [[UILabel alloc] initWithFrame:CGRectMake(startTimePicker.frame.origin.x+5, [References getBottomYofView:totalHeader], startTimePicker.frame.size.width-10, 30)];
    receiptTotal.font = [UIFont systemFontOfSize:20 weight:UIFontWeightBold];
    receiptTotal.textColor = [UIColor darkTextColor];
    receiptTotal.textAlignment = NSTextAlignmentRight;
    receiptTotal.text = @"$0.00";
    [bookView addSubview:receiptTotal];
    UIButton *confirm = [[UIButton alloc] initWithFrame:CGRectMake(card.frame.origin.x, [References getBottomYofView:receiptTotal]+10, card.frame.size.width, 54)];
    [References roundBottomCorners:confirm withRadius:5];
    confirm.backgroundColor = [References schoolMainColor];
    [confirm.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightBold]];
    [confirm setTitle:@"Continue" forState:UIControlStateNormal];
    [confirm setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confirm addTarget:self action:@selector(confirmScreen) forControlEvents:UIControlEventTouchUpInside];
    [bookView addSubview:confirm];
    [References cornerRadius:card radius:5];
    card.backgroundColor = [UIColor whiteColor];
    card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, [References getBottomYofView:confirm]-10);
    UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(card.frame.origin.x+5, card.frame.origin.y+5, card.frame.size.width-10, card.frame.size.height-10)];
    shadow.backgroundColor = [UIColor whiteColor];
    [References bottomshadow:shadow];
    [bookView addSubview:shadow];
    [bookView sendSubviewToBack:shadow];
    [bookView addSubview:startTimePicker];
    [References createLinewithWidth:bookView xPos:card.frame.origin.x yPos:[References getBottomYofView:selectCourse] inFront:YES andWidth:card.frame.size.width];
    bookView.frame = CGRectMake(bookView.frame.origin.x, bookView.frame.origin.y, bookView.frame.size.width, [References getBottomYofView:card]+10);
}

-(void)selectCourse:(UIButton*)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Select a Course"
                                                                   message:@"This allows the tutor to better prepare for the course." preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [References schoolMainColor];
    for (NSString *course in courses) {
        UIAlertAction* btn = [UIAlertAction actionWithTitle:[course stringByReplacingOccurrencesOfString:@":" withString:@" "] style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            self->selectedCourse = [course stringByReplacingOccurrencesOfString:@":" withString:@" "];
            [sender setTitle:self->selectedCourse forState:UIControlStateNormal];
            alert.view.tintColor = [References schoolMainColor];
        }];
        
        [alert addAction:btn];
    }
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    
    [parent presentViewController:alert animated:YES completion:nil];
    
}

-(void)addMinutes {
    NSLog(@"HEY");
    if (setDuration < maxDuration) {
        NSLog(@"%li of possible %li",setDuration,maxDuration);
        setDuration = setDuration + 1;
        durationLabel.text = [NSString stringWithFormat:@"%li MINUTES",setDuration*30];
        [self calculateReceipt];
    }
}

-(void)subtractMinutes {
    if (setDuration > 1) {
        NSLog(@"%li of possible %li",setDuration,maxDuration);
        setDuration = setDuration - 1;
        durationLabel.text = [NSString stringWithFormat:@"%li MINUTES",setDuration*30];
        [self calculateReceipt];
    }
}

-(void)calculateReceipt {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEEE, MMM d, h:mm a";
    selectedEndDate = [selectedStartDate dateByAddingTimeInterval:setDuration*1800];
    receiptStart.text = [fmt stringFromDate:selectedStartDate];
    receiptEnd.text = [fmt stringFromDate:selectedEndDate];
    totalCost = (pph.floatValue * setDuration) + 2.50;
    receiptTotal.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    finalReceiptTotal.text = [NSString stringWithFormat:@"$%.2f",totalCost];
}

-(void)listenForProfileDetails {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:identifier] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            if (snapshot.value[@"token"]) {
                self->token = snapshot.value[@"token"];
            }
            if (snapshot.value[@"courses"]) {
                self->courses = [snapshot.value[@"courses"] allKeys];
                for (UIView *view in self->profileCoursesView.subviews) {
                    [view removeFromSuperview];
                }
                CGFloat topWidth = 15;
                CGFloat bottomWidth = 15;
                for (int a = 0; a < self->courses.count; a++) {
                    UIButton *label;
                    if (a % 2 == 0) {
                        label = [[UIButton alloc] initWithFrame:CGRectMake(topWidth, 10, 0, 50)];
                        label.backgroundColor = [UIColor darkGrayColor];
                        label.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
                        label.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
                        [label setTitle:[self->courses[a] stringByReplacingOccurrencesOfString:@":" withString:@" "] forState:UIControlStateNormal];
                        label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, [References fixedHeightSizeWithFont:label.titleLabel.font andString:label.titleLabel.text andLabel:label].size.width+15, label.frame.size.height);
                        [label setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [References cornerRadius:label radius:5];
                        topWidth+=label.frame.size.width+5;
                    } else {
                        label = [[UIButton alloc] initWithFrame:CGRectMake(bottomWidth, 10+50+5, 0, 50)];
                        label.backgroundColor = [UIColor darkGrayColor];
                        label.titleLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightSemibold];
                        label.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
                        [label setTitle:[self->courses[a] stringByReplacingOccurrencesOfString:@":" withString:@" "] forState:UIControlStateNormal];
                        label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, [References fixedHeightSizeWithFont:label.titleLabel.font andString:label.titleLabel.text andLabel:label].size.width+15, label.frame.size.height);
                        [label setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [References cornerRadius:label radius:5];
                        bottomWidth+=label.frame.size.width+5;
                    }
                    [label addTarget:self action:@selector(tapCourse:) forControlEvents:UIControlEventTouchUpInside];
                    [self->profileCoursesView addSubview:label];
                }
                if (topWidth > bottomWidth) {
                    self->profileCoursesView.contentSize = CGSizeMake(topWidth+15, self->profileCoursesView.frame.size.height);
                } else {
                    self->profileCoursesView.contentSize = CGSizeMake(bottomWidth+15, self->profileCoursesView.frame.size.height);
                }
                [References createLine:self->profileCoursesView xPos:0 yPos:self->profileCoursesView.frame.size.height-1 inFront:YES];
            } else {
                for (UIView *view in self->profileCoursesView.subviews) {
                    [view removeFromSuperview];
                }
                UILabel *loading = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self->profileCoursesView.frame.size.width, self->profileCoursesView.frame.size.height)];
                loading.text = @"No Courses Tutored";
                loading.textAlignment = NSTextAlignmentCenter;
                loading.textColor = [UIColor grayColor];
                loading.font = [UIFont systemFontOfSize:12 weight:UIFontWeightLight];
                [self->profileCoursesView addSubview:loading];
                [References createLine:self->profileCoursesView xPos:0 yPos:self->profileCoursesView.frame.size.height-1 inFront:YES];
            }
            self->pph = snapshot.value[@"PPH"];
            hourlylabel.text = [NSString stringWithFormat:@"$%li",self->pph.integerValue*2];
            self->totalCost = (self->pph.floatValue * setDuration) + 2.50;
            self->receiptTotal.text = [NSString stringWithFormat:@"$%.2f",totalCost];
            self->name = snapshot.value[@"name"];
            self->profileURL = snapshot.value[@"profileURL"];
            if (smallViewImage) {
                [smallViewImage sd_setImageWithURL:[NSURL URLWithString:self->profileURL]];
            }
            if (smallViewName) {
                smallViewName.text = self->name;
            }
            self->bio = snapshot.value[@"bio"];
            
            for (UIView *view in self->profileSummaryView.subviews) {
                [view removeFromSuperview];
            }
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake([References screenWidth]/2-(50), 15, 100, 100)];
            [References cornerRadius:image radius:image.frame.size.width/2];
            [image sd_setImageWithURL:[NSURL URLWithString:self->profileURL]];
            [self->profileSummaryView addSubview:image];
            UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:image]+5, [References screenWidth]-30, 0)];
            name.textAlignment = NSTextAlignmentCenter;
            name.text = self->name;
            name.numberOfLines = 0;
            name.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
            name.textColor = [References primaryText];
            name.frame = CGRectMake(name.frame.origin.x, name.frame.origin.y, name.frame.size.width, [References fixedWidthSizeWithFont:name.font andString:name.text andLabel:name].size.height);
            [self->profileSummaryView addSubview:name];
            self->statisticsView.frame = CGRectMake(self->statisticsView.frame.origin.x, [References getBottomYofView:name]+10, self->statisticsView.frame.size.width, self->statisticsView.frame.size.height);
            UILabel *shadow = [[UILabel alloc] initWithFrame:CGRectMake(self->statisticsView.frame.origin.x+5, self->statisticsView.frame.origin.y+5, self->statisticsView.frame.size.width-10, self->statisticsView.frame.size.height-10)];
            shadow.backgroundColor = [UIColor whiteColor];
            [References bottomshadow:shadow];
            [self->profileSummaryView addSubview:shadow];
            [self->profileSummaryView addSubview:self->statisticsView];
            UILabel *bioL = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:self->statisticsView]+10, [References screenWidth]-30, 0)];
            bioL.text = self->bio;
            bioL.numberOfLines = 0;
            bioL.font = [UIFont systemFontOfSize:18 weight:UIFontWeightRegular];
            bioL.textColor = [References primaryText];
            bioL.frame = CGRectMake(bioL.frame.origin.x, bioL.frame.origin.y, bioL.frame.size.width, [References fixedWidthSizeWithFont:bioL.font andString:bioL.text andLabel:bioL].size.height);
            [self->profileSummaryView addSubview:bioL];
            self->profileSummaryView.frame = CGRectMake(0, 0, self->profileSummaryView.frame.size.width, [References getBottomYofView:bioL]+10);
            [self->table reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }];
}

-(void)listenForRatings {
    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:identifier] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->sessionCount = [(NSArray*)snapshot.value[@"income"] count];
            UILabel *ratingsLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-30, 0)];
            ratingsLabel.numberOfLines = 0;
            ratingsLabel.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
            ratingsLabel.textColor = [UIColor darkTextColor];
            NSDictionary *ratings = snapshot.value[@"ratings"];
            if (ratings.allKeys.count > 0) {
                CGFloat sum = 0;
                CGFloat count = 0;
                for (id key in ratings) {
                    if ([ratings[key] isKindOfClass:[NSDictionary class]]) {
                        NSNumber *rating = [ratings[key] objectForKey:@"rating"];
                        sum+=[rating floatValue];
                    } else {
                        NSNumber *rating = ratings[key];
                        sum+=[rating floatValue];
                    }
                    count+=1;
                }
                CGFloat rating = sum/count;
                averageRating = [NSNumber numberWithFloat:rating];
                averageRatingLabel.text = [NSString stringWithFormat:@"%.2f",rating];
                if (smallViewDetails) {
                    smallViewDetails.text = [NSString stringWithFormat:@"%.2f OUT OF 5",rating];
                }
            } else {
                averageRatingLabel.text = @"--";
                if (smallViewDetails) {
                    smallViewDetails.text = @"NEW TUTOR";
                }
            }
            ratingsLabel.frame = CGRectMake(ratingsLabel.frame.origin.x, ratingsLabel.frame.origin.y, ratingsLabel.frame.size.width, [References fixedWidthSizeWithFont:ratingsLabel.font andString:ratingsLabel.text andLabel:ratingsLabel].size.height);
            UILabel *sessions = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:ratingsLabel]+5, [References screenWidth]-30, 0)];
            sessions.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
            sessions.numberOfLines = 0;
            sessions.textColor = [UIColor darkTextColor];
            if (self->sessionCount > 0) {
                
                sessionsLabel.text = [NSString stringWithFormat:@"%li",self->sessionCount];
            } else {
                sessionsLabel.text = @"NEW";
            }
            sessions.frame = CGRectMake(sessions.frame.origin.x, sessions.frame.origin.y, sessions.frame.size.width, [References fixedWidthSizeWithFont:sessions.font andString:sessions.text andLabel:sessions].size.height);
        }
        
    }];
}



-(void)listenForAvailabilityWithCompletion:(void (^)(bool finished))complete {
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:identifier] child:@"availability"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            self->availability = nil;
            self->availability = [[NSMutableArray alloc] initWithArray:snapshot.value];
            NSLog(@"%@",self->availability);
            complete(YES);
        }
    }];
}

-(void)listenForSessions:(void (^)(bool finished))complete {
    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
    [components setHour: 0];
    [components setMinute: 0];
    [components setSecond: 0];
    double timeFrame = [[[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components] timeIntervalSince1970];
    [[[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:identifier] child:@"sessions"] queryOrderedByChild:@"start"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if (snapshot.exists) {
            for (NSInteger a = [[(NSDictionary*)snapshot.value allKeys] count]-1; a >= 0; a--) {
                NSDictionary *dict = [snapshot.value objectForKey:[[(NSDictionary*)snapshot.value allKeys] objectAtIndex:a]];
                if ([(NSNumber*)[dict valueForKey:@"start"] doubleValue] > timeFrame) {
                    double start = [(NSNumber*)[dict valueForKey:@"start"] doubleValue];
                    double finish = [(NSNumber*)[dict valueForKey:@"end"] doubleValue];
                    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:start];
                    double duration = ([[NSDate dateWithTimeIntervalSince1970:finish] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:start]]/60)/60;
                    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: startDate];
                    NSInteger hour = [components hour]*2;
                    NSCalendar* cal = [NSCalendar currentCalendar];
                    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:startDate];
                    NSInteger weekday = [comp weekday];
                    if (weekday == 1) {
                        weekday = 6;
                    } else {
                        weekday-=2;
                    }
                    if ([components minute] == 30) {
                        NSLog(@"STARTS AT 30");
                        hour+=1;
                    }
                    NSLog(@"%li: %li start, duration %f",weekday,hour,duration);
                    NSNumber *tAv = self->availability[weekday];
                    uint64_t og = tAv.unsignedLongLongValue;
                    while (duration > 0) {
                        NSLog(@"adding 30 minutes");
                        uint64_t msbAtIndex = MSB;
                        msbAtIndex = msbAtIndex >> hour;
                        msbAtIndex = ~msbAtIndex;
                        og = og & msbAtIndex;
                        hour+=1;
                        duration = duration - 0.5;
                    }
                    availability[weekday] = [NSNumber numberWithUnsignedLongLong:og];
                } else {
                    NSLog(@"complete");
                    complete(YES);
                }
            }
            complete(YES);
        } else {
            complete(YES);
        }
    }];
}

-(void)processSessionTowardsAvailabilityWithStartTime:(double)start andFinish:(double)finish {
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:start];
    double duration = ([[NSDate dateWithTimeIntervalSince1970:finish] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:start]]/60)/60;
    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: startDate];
    NSInteger hour = [components hour]*2;
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:startDate];
    NSInteger weekday = [comp weekday];
    if (weekday == 1) {
        weekday = 6;
    } else {
        weekday-=2;
    }
    if ([components minute] == 30) {
        NSLog(@"STARTS AT 30");
        hour+=1;
    }
    hour-=1;
    NSLog(@"%li: %li start, duration %f",weekday,hour,duration);
    [self updateAvailabilityOnDate:weekday AtHour:hour andDuration:duration];
}

-(void)updateAvailabilityOnDate:(NSInteger)weekday AtHour:(NSInteger)startIndex andDuration:(double)duration {
    while (duration > 0) {
        [self blockAvailabilityForDay:weekday atIndex:startIndex];
        startIndex+=1;
        duration = duration - 0.5;
    }
    [self processAvailabilityAndCompletion:nil];
}

-(void)printAvailabilityForDay:(NSInteger)weekDay {
    NSNumber *tAv = availability[weekDay];
    uint64_t tempAvailability = tAv.unsignedLongLongValue;
    for (int c = 0; c < 48; c++) {
        uint64_t d = tempAvailability & MSB;
        d = d >> 63;
        printf("%llu",d);
        tempAvailability = tempAvailability << 1;
    }
    printf("\n");
}

-(void)blockAvailabilityForDay:(NSInteger)weekDay atIndex:(NSInteger)index {
    NSNumber *tAv = availability[weekDay];
    uint64_t tempAvailability = tAv.unsignedLongLongValue;
    uint64_t msbAtIndex = MSB;
    msbAtIndex = msbAtIndex >> index;
    msbAtIndex = ~msbAtIndex;
    tempAvailability = tempAvailability & msbAtIndex;
    availability[weekDay] = [NSNumber numberWithUnsignedLongLong:tempAvailability];
}

-(BOOL)getAvailabilityForDay:(NSInteger)weekDay atIndex:(NSInteger)index {
    NSNumber *tAv = availability[weekDay];
    uint64_t tempAvailability = tAv.unsignedLongLongValue;
    uint64_t msbAtIndex = MSB;
    msbAtIndex = msbAtIndex >> index;
    uint64_t result = msbAtIndex & tempAvailability;
    if (result != 0) {
        return YES;
    }
    return NO;
}

-(BOOL)isAvailableOn:(NSInteger)weekday {
    NSLog(@"%@",availability);
    NSLog(@"%@",availability[weekday]);
    if ([(NSNumber*)availability[weekday] floatValue] != 0) {
        return YES;
    }
    return NO;
}

-(UIView*)bookTutorView:(UIViewController*)tParent withInsets:(UIEdgeInsets)insets {
    parent = tParent;
    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], [References screenHeight])];
    view.backgroundColor = [UIColor clearColor];
    table = [[UITableView alloc] initWithFrame:view.bounds style:UITableViewStylePlain];
    table.backgroundColor = [References background];
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    table.contentInset = insets;
    table.delegate = self;
    table.dataSource = self;
    [view addSubview:table];
    return view;
}

-(void)processAvailabilityAndCompletion:(void (^)(bool finished))complete {
    availableSlots = nil;
    availableSlots = [[NSMutableArray alloc] initWithCapacity:7];
    NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
    [components setHour: 0];
    [components setMinute: 0];
    [components setSecond: 0];
    NSDate *tDate = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components];
    NSLog(@"%@",tDate);
    NSMutableArray *days = [[NSMutableArray alloc] init];
    for (int a = 0; a < 7; a++) {

        [days addObject:tDate];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = 1;
        tDate = [[NSCalendar currentCalendar] dateByAddingComponents:dayComponent toDate:tDate options:0];
    }
    CGFloat height = 25;
    for (int a = 0; a < 7; a++) {
        NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: (NSDate*)days[a]];
        NSInteger weekday = [components weekday];
        if (weekday == 1) {
            weekday = 6;
        } else {
            weekday-=2;
        }
        NSInteger earliestAvailability = 48;
        if ([(NSNumber*)availability[weekday] unsignedLongLongValue] != 0) {
            NSDate *base = [NSDate dateWithTimeInterval:0 sinceDate:(NSDate*)days[a]];
            NSMutableArray *tAvailable = [[NSMutableArray alloc] init];
            CGFloat position = 0;
            for (int b = 0; b < 48; b++) {
                if ([self getAvailabilityForDay:weekday atIndex:b]) {
                    NSInteger maxDuration = 1;
                    for (int c = b+1; c < 48; c++) {
                        if ([self getAvailabilityForDay:weekday atIndex:c]) {
                            maxDuration = maxDuration + 1;
                        } else {
                            break;
                        }
                    }
                    [tAvailable addObject:@{@"date":base,@"duration":[NSNumber numberWithInteger:maxDuration]}];
                }
                base = [base dateByAddingTimeInterval:1800];
                position = position + height;
            }
            [availableSlots addObject:@{@"date":(NSDate*)days[a],@"available":tAvailable}];
            
        }
        
    }
    if (availableSlots.count > 0) {
        noAvailability = NO;
        [startTimePicker reloadAllComponents];
        NSDictionary *dictionary = availableSlots[0];
        NSArray *times = [dictionary objectForKey:@"available"];
        NSNumber *duration = (NSNumber*)[times[0] objectForKey:@"duration"];
        maxDuration = duration.integerValue;
        setDuration = 1;
        durationLabel.text = @"30 MINUTES";
        selectedStartDate = [times[0] objectForKey:@"date"];
        [self calculateReceipt];
    } else {
        noAvailability = YES;
    }
//    NSArray *tD = @[@"M",@"T",@"W",@"Th",@"F",@"S",@"Su"];
//    CGFloat og = 0;
//    for (UIView *subview in internalAvailable.subviews) {
//        [subview removeFromSuperview];
//    }
//    for (int a = 0; a < tD.count; a++) {
//        if ([self isAvailableOn:a]) {
//            UILabel *day = [[UILabel alloc] initWithFrame:CGRectMake(og, 0, 25, 20)];
//            day.backgroundColor = [UIColor lightGrayColor];
//            day.font = [UIFont systemFontOfSize:12 weight:UIFontWeightBold];
//            [References cornerRadius:day radius:5];
//            day.textAlignment = NSTextAlignmentCenter;
//            day.textColor = [UIColor whiteColor];
//            day.text = tD[a];
//            [internalAvailable addSubview:day];
//            og+=30;
//        }
//    }
    complete(YES);
}

//-(BOOL)getAvailabilityForWeekDay:(int)weekday andTime:(int)time {
//    for (NSDictionary *dict in availableSlots) {
//        if ([[dict valueForKey:@"weekday"] integerValue] == weekday) {
//            NSMutableArray *array = [dict objectForKey:@"available"];
//            if ([[array objectAtIndex:time] isKindOfClass:[NSDictionary class]]) {
//                return YES;
//            } else {
//                return NO;
//            }
//        }
//    }
//    return NO;
//}

-(void)getAvailableViewInView:(CGRect)rect inView:(UIView*)view andParentVC:(UIViewController*)vc withCourse:(NSString*)course {
    if (!availabilityView) {
        availabilityView = [[UIScrollView alloc] initWithFrame:rect];
        availabilityView.contentSize = CGSizeMake([References screenWidth], 25*48);
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"h:mm a";
        NSDateComponents *components = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] components: NSUIntegerMax fromDate: [NSDate date]];
        [components setHour: 0];
        [components setMinute: 0];
        [components setSecond: 0];
        NSDate *date = [[[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian] dateFromComponents: components];
        for (int a = 0; a < 7; a+=2) {
            UILabel *alt = [[UILabel alloc] initWithFrame:CGRectMake(55+((a*(([References screenWidth]-55)/7))), 0, (([References screenWidth]-55)/7), 49*25)];
            alt.backgroundColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.05];
            [availabilityView addSubview:alt];
        }
        for (int a = 0; a < 48; a+=2) {
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, a*25, 50, 10)];
            timeLabel.text = [fmt stringFromDate:date];
            timeLabel.font = [UIFont systemFontOfSize:8 weight:UIFontWeightBold];
            timeLabel.textColor = [UIColor lightGrayColor];
            timeLabel.textAlignment = NSTextAlignmentRight;
            [availabilityView addSubview:timeLabel];
//            [References createLine:availabilityView xPos:0 yPos:a*25 inFront:YES];
            date = [date dateByAddingTimeInterval:1800*2];
        }
    }
    [self processAvailabilityAndCompletion:^(bool finished) {
        if (finished) {
            NSLog(@"FINISHED");
        }
    }];
    availabilityView.directionalLockEnabled = YES;
    [view addSubview:availabilityView];
    calParent = view;
    parent = vc;
    selectedCourse = [course stringByReplacingOccurrencesOfString:@":" withString:@" "];
}

-(void)calTap:(bookButton*)sender {
    selectedStartDate = [sender startDate];
    selectedEndDate = [selectedStartDate dateByAddingTimeInterval:30*60];
    maxDuration = [sender duration];
    setDuration = 1;
    calOverlay = [[UIView alloc] initWithFrame:calParent.bounds];
    [References blurView:calOverlay];
    calOverlay.alpha = 0;
    [calParent addSubview:calOverlay];
    calStart = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, calOverlay.frame.size.width-30, 0)];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEEE, MMMM d";
    NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
    tfmt.dateFormat = @"h:mm a";
    calStart.text = [NSString stringWithFormat:@"%@ at %@",[fmt stringFromDate:[sender startDate]],[tfmt stringFromDate:[sender startDate]]];
    calStart.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    calStart.textColor = [UIColor darkTextColor];
    calStart.frame = CGRectMake(calStart.frame.origin.x, calStart.frame.origin.y, calStart.frame.size.width, [References fixedWidthSizeWithFont:calStart.font andString:calStart.text andLabel:calStart].size.height);
    calStart.numberOfLines = 0;
    [calOverlay addSubview:calStart];
    [References createLinewithWidth:calOverlay xPos:15 yPos:[References getBottomYofView:calStart]+5 inFront:YES andWidth:[References screenWidth]-30];
    calDuration = [[UILabel alloc] initWithFrame:CGRectMake(15+( ([References screenWidth]-30)/3), [References getBottomYofView:calStart]+10, ([References screenWidth]-30)/3, 40)];
    calDuration.text = @"30 MINUTES";
    calDuration.textAlignment = NSTextAlignmentCenter;
    calDuration.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
    calDuration.textColor = [UIColor darkTextColor];
    [calOverlay addSubview:calDuration];
    UIButton *calSubtractTime = [[UIButton alloc] initWithFrame:CGRectMake(15,calDuration.frame.origin.y,([References screenWidth]-30)/3,40)];
    [calSubtractTime setTitle:@"-30 MINUTES" forState:UIControlStateNormal];
    [calSubtractTime.titleLabel setFont:[UIFont systemFontOfSize:9]];
    [calSubtractTime addTarget:self action:@selector(calSubtract) forControlEvents:UIControlEventTouchUpInside];
    [calSubtractTime setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    //    [subtractTime setBackgroundColor:[References lightGrey]];
    [calOverlay addSubview:calSubtractTime];
    UIButton *calAddTime = [[UIButton alloc] initWithFrame:CGRectMake(calDuration.frame.origin.x+calDuration.frame.size.width,calDuration.frame.origin.y,([References screenWidth]-30)/3,40)];
    [calAddTime addTarget:self action:@selector(calAdd) forControlEvents:UIControlEventTouchUpInside];
    [calAddTime setTitle:@"+30 MINUTES" forState:UIControlStateNormal];
    [calAddTime.titleLabel setFont:[UIFont systemFontOfSize:9]];
    [calAddTime setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    //    [addTime setBackgroundColor:[References lightGrey]];
    [calOverlay addSubview:calAddTime];
    [References createLinewithWidth:calOverlay xPos:15 yPos:[References getBottomYofView:calDuration]+5 inFront:YES andWidth:[References screenWidth]-30];
    calEnd = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:calDuration]+10, calOverlay.frame.size.width-30, 0)];
    calEnd.text = [NSString stringWithFormat:@"%@ at %@",[fmt stringFromDate:selectedEndDate],[tfmt stringFromDate:selectedEndDate]];
    calEnd.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    calEnd.textColor = [UIColor darkTextColor];
    calEnd.frame = CGRectMake(calEnd.frame.origin.x, calEnd.frame.origin.y, calEnd.frame.size.width, [References fixedWidthSizeWithFont:calEnd.font andString:calEnd.text andLabel:calEnd].size.height);
    calEnd.numberOfLines = 0;
    [calOverlay addSubview:calEnd];
    UIButton *proceedToCheckout = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:calEnd]+25, [References screenWidth]-30, 44)];
    [proceedToCheckout.titleLabel setFont:[UIFont systemFontOfSize:24 weight:UIFontWeightRegular]];
    [proceedToCheckout setTitle:@"Proceed To Checkout" forState:UIControlStateNormal];
    [proceedToCheckout setTitleColor:[References schoolMainColor] forState:UIControlStateNormal];
    [proceedToCheckout addTarget:self action:@selector(confirmScreen) forControlEvents:UIControlEventTouchUpInside];
    [calOverlay addSubview:proceedToCheckout];
    UIButton *keepBrowsing = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:proceedToCheckout], [References screenWidth]-30, 44)];
    [keepBrowsing.titleLabel setFont:[UIFont systemFontOfSize:12 weight:UIFontWeightRegular]];
    [keepBrowsing setTitle:@"Keep Looking" forState:UIControlStateNormal];
    [keepBrowsing setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [calOverlay addSubview:keepBrowsing];
    [keepBrowsing addTarget:self action:@selector(hideCalOverlay) forControlEvents:UIControlEventTouchUpInside];
    [UIView animateWithDuration:0.25 animations:^{
        self->calOverlay.alpha = 1;
    }];
}

-(void)hideCalOverlay {
    [UIView animateWithDuration:0.25 animations:^{
        self->calOverlay.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self->calOverlay removeFromSuperview];
            self->calOverlay = nil;
        }
    }];
}

-(void)calAdd {
    if (setDuration < maxDuration) {
        setDuration++;
        calDuration.text = [NSString stringWithFormat:@"%li MINUTES",setDuration*30];
    }
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEEE, MMMM d";
    NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
    tfmt.dateFormat = @"h:mm a";
    selectedEndDate = [selectedStartDate dateByAddingTimeInterval:(setDuration*30)*60];
    calEnd.text = [NSString stringWithFormat:@"%@ at %@",[fmt stringFromDate:selectedEndDate],[tfmt stringFromDate:selectedEndDate]];
    calEnd.frame = CGRectMake(calEnd.frame.origin.x, calEnd.frame.origin.y, calEnd.frame.size.width, [References fixedWidthSizeWithFont:calEnd.font andString:calEnd.text andLabel:calEnd].size.height);
    [self calculateReceipt];
}

-(void)calSubtract {
    if (setDuration > 1) {
        setDuration--;
        calDuration.text = [NSString stringWithFormat:@"%li MINUTES",setDuration*30];
    }
    selectedEndDate = [selectedStartDate dateByAddingTimeInterval:(setDuration*30)*60];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"EEEE, MMMM d";
    NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
    tfmt.dateFormat = @"h:mm a";
    calEnd.text = [NSString stringWithFormat:@"%@ at %@",[fmt stringFromDate:selectedEndDate],[tfmt stringFromDate:selectedEndDate]];
    calEnd.frame = CGRectMake(calEnd.frame.origin.x, calEnd.frame.origin.y, calEnd.frame.size.width, [References fixedWidthSizeWithFont:calEnd.font andString:calEnd.text andLabel:calEnd].size.height);
    [self calculateReceipt];
}

-(void)freeAvailableView {
    [calOverlay removeFromSuperview];
    calOverlay = nil;
    [availabilityView removeFromSuperview];
    availabilityView = nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return profileSummaryView.frame.size.height;
    } else if (indexPath.section == 1) {
        if (expandCourses) {
            return profileCoursesView.frame.size.height;
        }
    }else if (indexPath.section == 2) {
        if (expandSchedule) {
            return bookView.frame.size.height;
        }
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return profileSummaryView.frame.size.height;
    } else if (indexPath.section == 1) {
        if (expandCourses) {
            return profileCoursesView.frame.size.height;
        }
    } else if (indexPath.section == 2) {
        if (expandSchedule) {
            return bookView.frame.size.height;
        }
    }
    return 0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"summary"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"summary"];
            [cell addSubview:profileSummaryView];
            [References createLine:cell xPos:0 yPos:profileSummaryView.frame.size.height-1 inFront:YES];
        }
    } else if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"courses"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"courses"];
            [cell addSubview:profileCoursesView];
        }
    } else if (indexPath.section == 2) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"bookView"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"bookView"];
            [cell addSubview:bookView];
            [References createLine:cell xPos:0 yPos:bookView.frame.size.height-1 inFront:YES];
            cell.clipsToBounds = NO;
        }
    }
    cell.clipsToBounds = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }
    return 50;
}

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 50)];
    view.backgroundColor = [UIColor clearColor];
    UILabel *hheader = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, [References screenWidth]-24, 30)];
    hheader.textAlignment = NSTextAlignmentLeft;
    UIButton *expand = [[UIButton alloc] initWithFrame:view.bounds];
    [expand setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.00]];
    if (section == 2) {
        [expand addTarget:self action:@selector(toggleSchedule) forControlEvents:UIControlEventTouchUpInside];
        hheader.text = @"Schedule Now";
    } else if (section == 1) {
        [expand addTarget:self action:@selector(toggleCourses) forControlEvents:UIControlEventTouchUpInside];
        hheader.text = @"Courses";
    }
    hheader.textColor = [References primaryText];
    hheader.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    [References createLine:view xPos:0 yPos:view.frame.size.height-1 inFront:YES];
    [view addSubview:hheader];
    [view addSubview:expand];
    return view;
}

-(void)tapCourse:(UIButton*)sender {
//    NSLog(@"%@",sender.titleLabel.text);
//    [[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"uoregon"] child:[[sender.titleLabel.text componentsSeparatedByString:@" "] objectAtIndex:0]] child:[[sender.titleLabel.text componentsSeparatedByString:@" "] objectAtIndex:1]] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
//        if (snapshot.exists) {
//            courseDatabase *cd = [[courseDatabase alloc] initWithDatabase:snapshot.value andName:sender.titleLabel.text andDeparment:[[sender.titleLabel.text componentsSeparatedByString:@" "] objectAtIndex:0]];
//            scheduleViewController *calendarViewController = [[scheduleViewController alloc] initWithCourse:[(courseDatabase*)courses[indexPath.item] courseDepartmentAndNumber] andTutors:[(courseDatabase*)courses[indexPath.item] tutors] andCourse:(courseDatabase*)courses[indexPath.item]];
//            [parent.navigationController pushViewController:calendarViewController animated:YES];
//        }
//    }];
//    selectedCourse = sender.titleLabel.text;
//    [selectCourse setTitle:selectedCourse forState:UIControlStateNormal];
//    [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)toggleCourses {
//    expandCourses = !expandCourses;
//    [table reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)toggleSchedule {
//    if (noAvailability) {
//        MBProgressHUD *toast = [MBProgressHUD showHUDAddedTo:parent.view animated:YES];
//        toast.mode = MBProgressHUDModeText;
//        toast.backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
//        toast.label.text = [NSString stringWithFormat:@"This tutor is unavailable"];
//        toast.margin = 10.f;
//        [toast setOffset:CGPointMake(0, 150)];
//        toast.removeFromSuperViewOnHide = YES;
//        [toast hideAnimated:YES afterDelay:1.25];
//    } else {
//        expandSchedule = !expandSchedule;
//        [table reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:2],[NSIndexPath indexPathForRow:0 inSection:3]] withRowAnimation:UITableViewRowAnimationFade];
//        [table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }

}









-(UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    if (component == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ([References screenWidth]-40)/2, 5+20+5+15+5)];
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"EEEE";
        UILabel *weekday = [[UILabel alloc] initWithFrame:CGRectMake(5, 7, view.frame.size.width-10, 20)];
        weekday.textAlignment = NSTextAlignmentRight;
        weekday.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
        weekday.text = [[fmt stringFromDate:[(NSDictionary*)availableSlots[row] objectForKey:@"date"]] uppercaseString];
        weekday.textColor = [UIColor darkTextColor];
        UILabel *monthday = [[UILabel alloc] initWithFrame:CGRectMake(5, 20+5+5+2, weekday.frame.size.width, 15)];
        monthday.textAlignment = NSTextAlignmentRight;
        monthday.font = [UIFont systemFontOfSize:9 weight:UIFontWeightMedium];
        monthday.textColor = [UIColor grayColor];
        fmt.dateFormat = @"MMMM d";
        monthday.text = [[fmt stringFromDate:[(NSDictionary*)availableSlots[row] objectForKey:@"date"]] uppercaseString];
        [view addSubview:weekday];
        [view addSubview:monthday];
        return view;
    } else {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ([References screenWidth]-40)/2, 5+20+5+15+5)];
        UILabel *time = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, view.frame.size.width, view.frame.size.height)];
        NSDictionary *dictionary = (NSDictionary*)availableSlots[[pickerView selectedRowInComponent:0]];
        NSArray *times = [dictionary objectForKey:@"available"];
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"h:mm a";
        time.text =  [fmt stringFromDate:[times[row] objectForKey:@"date"]];
        time.font =  [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
        time.textAlignment = NSTextAlignmentLeft;
        [view addSubview:time];
        return view;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (component == 0) {
        [pickerView reloadComponent:1];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        NSDictionary *dictionary = (NSDictionary*)availableSlots[[pickerView selectedRowInComponent:0]];
        NSArray *times = [dictionary objectForKey:@"available"];
        if (times.count > 0) {
            NSNumber *duration = (NSNumber*)[times[0] objectForKey:@"duration"];
            maxDuration = duration.integerValue;
            setDuration = 1;
            durationLabel.text = @"30 MINUTES";
            selectedStartDate = [times[0] objectForKey:@"date"];
        }
    } else {
        NSDictionary *dictionary = (NSDictionary*)availableSlots[[pickerView selectedRowInComponent:0]];
        NSArray *times = [dictionary objectForKey:@"available"];
        NSNumber *duration = (NSNumber*)[times[row] objectForKey:@"duration"];
        maxDuration = duration.integerValue;
        setDuration = 1;
        durationLabel.text = @"30 MINUTES";
        selectedStartDate = [times[row] objectForKey:@"date"];
    }
    [self calculateReceipt];
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return ([References screenWidth]-40)/2;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 5+20+5+15+5;
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"EEEE, MMM d";
        return [fmt stringFromDate:[(NSDictionary*)availableSlots[row] objectForKey:@"date"]];
    } else {
        NSDictionary *dictionary = (NSDictionary*)availableSlots[[pickerView selectedRowInComponent:0]];
        NSArray *times = [dictionary objectForKey:@"available"];
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"h:mm a";
        return [fmt stringFromDate:[times[row] objectForKey:@"date"]];
    }
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return availableSlots.count;
    } else {
        NSDictionary *dictionary = (NSDictionary*)availableSlots[[pickerView selectedRowInComponent:0]];
        NSArray *times = [dictionary objectForKey:@"available"];
        return times.count;
    }
}

-(void)confirmScreen {
    if (!selectedCourse) {
        [self selectCourse:selectCourse];
    } else {
    confirm = [[UIView alloc] initWithFrame:CGRectMake(0, [References screenHeight], [References screenWidth], 0)];
    NSDateFormatter *dfmt = [[NSDateFormatter alloc] init];
    dfmt.dateFormat = @"EEEE, MMMM d, YYYY";
    NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
    tfmt.dateFormat = @"h:mm a";
    overlay = [[UIView alloc] initWithFrame:parent.view.bounds];
    overlay.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.75];
    overlay.alpha = 0;
    confirm = [[UIView alloc] initWithFrame:CGRectMake(0, [References screenHeight], [References screenWidth], 0)];
    [References createLine:confirm xPos:0 yPos:0 inFront:YES];
    UILabel *blackHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, [References screenWidth], 70)];
    blackHeader.backgroundColor = [UIColor blackColor];
        [confirm addSubview:blackHeader];
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, [References screenWidth]-30, 30)];
    header.textAlignment = NSTextAlignmentCenter;
    header.text = @"Confirm Details";
    header.textColor = [UIColor whiteColor];
    header.font = [UIFont systemFontOfSize:24 weight:UIFontWeightBold];
    [confirm addSubview:header];
    UIImageView *otherImage = [[UIImageView alloc] initWithFrame:CGRectMake([References screenWidth]/2-(25), [References getBottomYofView:blackHeader]+15, 50, 50)];
    [References cornerRadius:otherImage radius:otherImage.frame.size.height/2];
    [otherImage sd_setImageWithURL:[NSURL URLWithString:profileURL]];
        [confirm addSubview:otherImage];
        UILabel *receiptName = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:otherImage]+5, [References screenWidth]-30, 20)];
        receiptName.text = name;
        receiptName.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
        receiptName.textColor = [UIColor darkTextColor];
        receiptName.textAlignment = NSTextAlignmentCenter;
        [confirm addSubview:receiptName];
        UILabel *receiptCourseHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptName]+10, 100, 20)];
        receiptCourseHeader.text = @"Course";
        receiptCourseHeader.font = receiptName.font;
        receiptCourseHeader.textColor = [UIColor darkTextColor];
        [confirm addSubview:receiptCourseHeader];
        UILabel *receiptCourse = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptCourseHeader.frame.origin.y, [References screenWidth]-15-(15+100+5), receiptCourseHeader.frame.size.height)];
        receiptCourse.text = selectedCourse;
        receiptCourse.font = receiptName.font;
        receiptCourse.textColor = [UIColor darkTextColor];
        receiptCourse.textAlignment = NSTextAlignmentRight;
        [confirm addSubview:receiptCourse];
        UILabel *receiptDateHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptCourseHeader]+10, 100, 20)];
        receiptDateHeader.text = @"Date";
        receiptDateHeader.font = receiptName.font;
        receiptDateHeader.textColor = [UIColor darkTextColor];
        [confirm addSubview:receiptDateHeader];
        UILabel *receiptDate = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptDateHeader.frame.origin.y, [References screenWidth]-15-(15+100+5), receiptDateHeader.frame.size.height)];
        receiptDate.text = [dfmt stringFromDate:selectedStartDate];
        receiptDate.font = receiptName.font;
        receiptDate.textColor = [UIColor darkTextColor];
        receiptDate.textAlignment = NSTextAlignmentRight;
        [confirm addSubview:receiptDate];
        UILabel *receiptStartHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptDateHeader]+10, 100, 20)];
        receiptStartHeader.text = @"Time";
        receiptStartHeader.font = receiptName.font;
        receiptStartHeader.textColor = [UIColor darkTextColor];
        [confirm addSubview:receiptStartHeader];
        UILabel *receiptTime = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptStartHeader.frame.origin.y, [References screenWidth]-15-(15+100+5), receiptStartHeader.frame.size.height)];
        receiptTime.text = [tfmt stringFromDate:selectedStartDate];
        receiptTime.font = receiptName.font;
        receiptTime.textColor = [UIColor darkTextColor];
        receiptTime.textAlignment = NSTextAlignmentRight;
        [confirm addSubview:receiptTime];
        UILabel *receiptDurationHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptStartHeader]+10, 100, 20)];
        receiptDurationHeader.text = @"Duration";
        receiptDurationHeader.font = receiptName.font;
        receiptDurationHeader.textColor = [UIColor darkTextColor];
        [confirm addSubview:receiptDurationHeader];
        UILabel *receiptDuration = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptDurationHeader.frame.origin.y, [References screenWidth]-15-(15+100+5), receiptDurationHeader.frame.size.height)];
        receiptDuration.text = [NSString stringWithFormat:@"%li Minutes",setDuration*30];
        receiptDuration.font = receiptName.font;
        receiptDuration.textColor = [UIColor darkTextColor];
        receiptDuration.textAlignment = NSTextAlignmentRight;
        [confirm addSubview:receiptDuration];
        [References createLinewithWidth:confirm xPos:15 yPos:[References getBottomYofView:receiptDurationHeader]+10 inFront:YES andWidth:[References screenWidth]-30];
        UILabel *receiptDiscountHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptDurationHeader]+20, 100, 20)];
        receiptDiscountHeader.text = @"Discount";
        receiptDiscountHeader.font = receiptName.font;
        receiptDiscountHeader.textColor = [UIColor darkTextColor];
        [confirm addSubview:receiptDiscountHeader];
        finalReceiptDiscount = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptDiscountHeader.frame.origin.y, [References screenWidth]-15-(15+100+5), receiptDiscountHeader.frame.size.height)];
        finalReceiptDiscount.text = [NSString stringWithFormat:@"$0.00"];
        finalReceiptDiscount.font = receiptName.font;
        finalReceiptDiscount.textColor = [UIColor darkTextColor];
        finalReceiptDiscount.textAlignment = NSTextAlignmentRight;
        [confirm addSubview:finalReceiptDiscount];
        UILabel *receiptCostHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptDiscountHeader]+10, 100, 20)];
        receiptCostHeader.text = @"Total Cost";
        receiptCostHeader.font = receiptName.font;
        receiptCostHeader.textColor = [UIColor darkTextColor];
        [confirm addSubview:receiptCostHeader];
        finalReceiptTotal = [[UILabel alloc] initWithFrame:CGRectMake(15+100+5, receiptCostHeader.frame.origin.y, [References screenWidth]-15-(15+100+5), receiptCostHeader.frame.size.height)];
        finalReceiptTotal.text = [NSString stringWithFormat:@"$%.2f",totalCost];
        finalReceiptTotal.font = receiptName.font;
        finalReceiptTotal.textColor = [UIColor darkTextColor];
        finalReceiptTotal.textAlignment = NSTextAlignmentRight;
        [confirm addSubview:finalReceiptTotal];
    apple = [[PKPaymentButton alloc] initWithPaymentButtonType:PKPaymentButtonTypePlain paymentButtonStyle:PKPaymentButtonStyleBlack];
    apple.enabled = NO;
    apple.alpha = 0.25;
    apple.frame = CGRectMake((([References screenWidth])/2)+2.5, [References getBottomYofView:receiptCostHeader]+10, ([References screenWidth]-35)/2, 50);
    venmo = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:receiptCostHeader]+10, ([References screenWidth]-35)/2, 50)];
        [venmo setBackgroundColor:[UIColor whiteColor]];
        UIImageView *v = [[UIImageView alloc] initWithFrame:CGRectMake(20, 20, venmo.frame.size.width-40, venmo.frame.size.height-40)];
        [v setImage:[UIImage imageNamed:@"venmo.png"]];
        v.contentMode = UIViewContentModeScaleAspectFit;
        [venmo addSubview:v];
        [confirm addSubview:venmo];
        [confirm addSubview:apple];
    UIButton *cancel = [[UIButton alloc] initWithFrame:CGRectMake(15, [References getBottomYofView:venmo]+5, venmo.frame.size.width, 50)];
        [cancel addTarget:self action:@selector(cancelButton:) forControlEvents:UIControlEventTouchUpInside];
    cancel.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.25];
    [cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancel.titleLabel setFont:[UIFont systemFontOfSize:16 weight:UIFontWeightBold]];
    [cancel setTitleColor:[References colorFromHexString:@"#FF1744"] forState:UIControlStateNormal];
    [References cornerRadius:cancel radius:5];
    [confirm addSubview:cancel];
        [References cornerRadius:apple radius:5.0];
        [References cornerRadius:venmo radius:5.0];
        UIButton *useBalance = [[UIButton alloc] initWithFrame:CGRectMake(apple.frame.origin.x, [References getBottomYofView:apple]+5, apple.frame.size.width, 50)];
        [useBalance setBackgroundColor:[References schoolMainColor]];
        UIImageView *balanceIcon = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, (useBalance.frame.size.height-20), (useBalance.frame.size.height-20))];
        [balanceIcon setImage:[UIImage imageNamed:@"invert-icon.png"]];
        balanceIcon.contentMode = UIViewContentModeScaleAspectFit;
        [useBalance addSubview:balanceIcon];
        [References cornerRadius:useBalance radius:5];
        UILabel *balancelabel = [[UILabel alloc] initWithFrame:CGRectMake(balanceIcon.frame.origin.x+balanceIcon.frame.size.width, balanceIcon.frame.origin.y,useBalance.frame.size.width-(balanceIcon.frame.origin.x+balanceIcon.frame.size.width),balanceIcon.frame.size.height)];
        balancelabel.font = [UIFont systemFontOfSize:16 weight:UIFontWeightBold];
        balancelabel.textColor = [UIColor whiteColor];
        
        balancelabel.text = @"$0";
        balancelabel.numberOfLines = 1;
        [useBalance addSubview:balancelabel];
         [confirm addSubview:useBalance];
    [venmo addTarget:self action:@selector(tappedVenmo) forControlEvents:UIControlEventTouchUpInside];
    [apple addTarget:self action:@selector(tappedApplePay) forControlEvents:UIControlEventTouchUpInside];
    venmo.enabled = NO;
    venmo.alpha = 0.25;
        
        [self getBalanceWithCompletion:^(bool finished) {
            if (finished) {
                CGFloat currentY = [References getBottomYofView:venmo];
                if (currentBalance > 0) {
                    if (currentBalance >= totalCost) {
                        balancelabel.text = [NSString stringWithFormat:@"$%.2f",self->currentBalance];
                    } else {
                        balancelabel.text = [NSString stringWithFormat:@"$%.2f",self->currentBalance];
                    }
                    [balancelabel setAdjustsFontSizeToFitWidth:YES];
                    [useBalance addTarget:self action:@selector(applyBalance:) forControlEvents:UIControlEventTouchUpInside];
                } else {
                    discount = 0;
                    useBalance.enabled = NO;
                    useBalance.alpha = 0.25;
                }
                [parent.view addSubview:overlay];
                [parent.view addSubview:confirm];
                
                confirm.frame = CGRectMake(0, [References screenHeight], confirm.frame.size.width, [References getBottomYofView:cancel]+15+[References bottomMargin]);
                [References blurView:confirm];
                [UIView animateWithDuration:0.25 animations:^{
                    overlay.alpha = 1;
                    confirm.frame = CGRectMake(confirm.frame.origin.x, confirm.frame.origin.y-confirm.frame.size.height, confirm.frame.size.width, confirm.frame.size.height);
                }];
                [self fetchClientToken];
            }
        }];
    }
}

-(void)applyBalance:(UIButton*)sender {
    if (currentBalance >= totalCost) {
        discount = totalCost;
        usedBalance = [NSNumber numberWithFloat:discount];
        [self checkoutFree];
    } else {
        discount = currentBalance;
        totalCost = totalCost - discount;
        usedBalance = [NSNumber numberWithFloat:discount];
        finalReceiptDiscount.text = [NSString stringWithFormat:@"$%.2f",discount];
        finalReceiptTotal.text = [NSString stringWithFormat:@"$%.2f",totalCost];
    }
    sender.enabled = NO;
    sender.alpha = 0.25;
}

-(void)tappedVenmo {
    [self.venmoDriver authorizeAccountAndVault:NO completion:^(BTVenmoAccountNonce * _Nullable venmoAccount, NSError * _Nullable error) {
        if (venmoAccount) {
            venmoNonce = venmoAccount.nonce;
            paymentVenmo = venmoAccount;
            [self returnedFromVenmo];
        }
    }];
}

-(void)returnedFromVenmo {
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:[NSString stringWithFormat:@"Confirm Booking"] message:[NSString stringWithFormat:@"The venmo account %@ will be charged %.2f",paymentVenmo.username,totalCost] preferredStyle:UIAlertControllerStyleAlert];
    [controller addAction:[UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Pay $%.2f",totalCost] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        paymentType = @"Venmo";
        [self postNonceToServer:venmoNonce andAmount:[NSString stringWithFormat:@"%.2f",totalCost]];
    }]];
    [controller addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
    [parent presentViewController:controller animated:YES completion:nil];

}

-(void)cancelButton:(UIButton*)sender {
    [self hideConfirmView:YES];
}

-(void)hideConfirmView:(BOOL)cancel {
    [UIView animateWithDuration:0.25 animations:^{
        self->overlay.alpha = 0;
        self->confirm.frame = CGRectMake(self->confirm.frame.origin.x, [References screenHeight], self->confirm.frame.size.width, self->confirm.frame.size.height);
    } completion:^(BOOL finished) {
        if (finished) {
            if (cancel) {
                self->discount = 0;
                self->totalCost = self->totalCost + self->discount;
                self->usedBalance = [NSNumber numberWithFloat:0];
                self->finalReceiptDiscount.text = [NSString stringWithFormat:@"$%.2f",self->discount];
                [self calculateReceipt];
            }
            self->confirm = nil;
            self->overlay = nil;
        }
    }];
}

-(void)checkoutFree {
    paymentType = @"Balance";
    balanceUsed = totalCost;
    [self hideConfirmView:NO];
    __block UIView *processingPayment = [References loadingView:@"Creating Session..."];
    [view addSubview:processingPayment];
    [UIView animateWithDuration:0.25 animations:^{
        processingPayment.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            [self createSessionWithID:[References randomStringWithLength:8]];
        }
    }];

}

-(void)getBalanceWithCompletion:(void (^)(bool finished))complete {
    currentBalance = 0;
    NSDictionary *myAccount = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"accountDatabase"];
    NSDictionary *spending = myAccount[@"spending"];
    NSDictionary *income = myAccount[@"income"];
    
    if (income) {
        for (id key in income) {
            NSDictionary *td = income[key];
            NSNumber *n = td[@"amount"];
            currentBalance+=n.floatValue;
            
        }
    }
    if (spending) {
        for (id key in spending) {
            NSDictionary *td = spending[key];
            NSNumber *n = td[@"amount"];
            if (![(NSString*)td[@"type"] isEqualToString:@"Apple Pay"] && ![(NSString*)td[@"type"] isEqualToString:@"Venmo"]) {
                currentBalance-=n.floatValue;
            } else if ([(NSNumber*)td[@"usedBalance"] floatValue] > 0) {
                currentBalance-=[(NSNumber*)td[@"usedBalance"] floatValue];
            }
        }
    }
    complete(YES);
}

-(void)removeSessionWithID:(NSString*)transactionID {
    [[[FIRDatabase database] reference] updateChildValues:@{[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",identifier,transactionID]: [NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",[FIRAuth auth].currentUser.uid,transactionID]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/income/%@",identifier,transactionID]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/users/%@/spending/%@",[FIRAuth auth].currentUser.uid,transactionID]:[NSNull null],[NSString stringWithFormat:@"updateDatabase/invoices/%@",transactionID]:[NSNull null]} withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        NSLog(@"DELETED");
    }];
}

-(void)createSessionWithID:(NSString*)transactionID {
    NSString *tID = [transactionID uppercaseString];
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"MMM d, h:mm a";
    NSDictionary *spendingInvoice = @{@"amount":[NSNumber numberWithFloat:totalCost],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":paymentType,@"usedBalance":[NSNumber numberWithFloat:discount]};
    NSDictionary *incomeInvoice = @{@"amount":[NSNumber numberWithFloat:(pph.floatValue * setDuration)],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":paymentType,};
    NSMutableDictionary *myAccount = [[NSMutableDictionary alloc] initWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"accountDatabase"]];
    NSMutableDictionary *spending = [[NSMutableDictionary alloc] initWithDictionary: myAccount[@"spending"]];
    [spending setObject:spendingInvoice forKey:transactionID];
    [myAccount setObject:spending forKey:@"spending"];
    [[NSUserDefaults standardUserDefaults] setObject:myAccount forKey:@"accountDatabase"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSDictionary *bookingTutor = @{@"start":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"end":[NSNumber numberWithDouble:[selectedEndDate timeIntervalSince1970]],@"course":selectedCourse,@"other":[FIRAuth auth].currentUser.uid};
    NSDictionary *bookingStudent = @{@"start":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"end":[NSNumber numberWithDouble:[selectedEndDate timeIntervalSince1970]],@"course":selectedCourse,@"other":identifier};
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:selectedStartDate];
    NSDictionary *legacy = @{@"course":selectedCourse,@"date":[NSNumber numberWithDouble:[selectedStartDate timeIntervalSince1970]],@"weekInt":[NSNumber numberWithInteger:[comp weekday]],@"value":@0};
    NSDictionary *globalInvoice = @{@"fromEmail":[FIRAuth auth].currentUser.uid,@"fromName":[FIRAuth auth].currentUser.uid,@"metadata":@{@"amount":[NSNumber numberWithFloat:totalCost-2.50],@"date":[fmt stringFromDate:[NSDate date]],@"numberDate":[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]],@"type":paymentType},@"toName":name,@"toEmail":identifier};
    NSArray *people = [@[[FIRAuth auth].currentUser.uid,identifier] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *connectionID = [NSString stringWithFormat:@"%@:%@",people[0],people[1]];
    NSDictionary *delivery = @{[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",identifier,tID]: bookingTutor,[NSString stringWithFormat:@"updateDatabase/users/%@/sessions/%@",[FIRAuth auth].currentUser.uid,tID]:bookingStudent,[NSString stringWithFormat:@"updateDatabase/users/%@/income/%@",identifier,tID]:incomeInvoice,[NSString stringWithFormat:@"updateDatabase/users/%@/spending/%@",[FIRAuth auth].currentUser.uid,tID]:spendingInvoice,[NSString stringWithFormat:@"updateDatabase/invoices/%@",tID]:globalInvoice,[NSString stringWithFormat:@"updateDatabase/connections/%@/sessions/%@",connectionID,tID]:legacy,[NSString stringWithFormat:@"updateDatabase/connections/%@/tutor",connectionID]:identifier,[NSString stringWithFormat:@"updateDatabase/connections/%@/student",connectionID]:[FIRAuth auth].currentUser.uid};
    [[[FIRDatabase database] reference] updateChildValues:delivery withCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
        fmt.dateFormat = @"EEEE, MMM d";
        NSDateFormatter *tfmt = [[NSDateFormatter alloc] init];
        tfmt.dateFormat = @"h:mm a";
        NSString *notification =[NSString stringWithFormat:@"%@ has booked you on %@ at %@. They have booked you for help with %@.",myAccount[@"name"],[fmt stringFromDate:self->selectedStartDate],[tfmt stringFromDate:self->selectedStartDate],selectedCourse];
        NSLog(@"%@",notification);
        [self sendNotification:notification withSubject:@"New Booking"];
        [self->parent.navigationController popViewControllerAnimated:YES];
        NSLog(@"COMPLETE");
    }];
}

- (void)fetchClientToken {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"clientToken"]) {
        clientToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"clientToken"];
        self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:clientToken];
        self.venmoDriver = [[BTVenmoDriver alloc] initWithAPIClient:self.braintreeClient];
        NSLog(@"have local");
        [UIView animateWithDuration:0.25 animations:^{
            venmo.alpha = 1;
            apple.alpha = 1;
        } completion:^(BOOL finished) {
            if (finished) {
                venmo.enabled = YES;
                apple.enabled = YES;
            }
        }];
    } else {
        NSString *requestURL = @"https://tutortree-development.herokuapp.com/getClientToken";
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:requestURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            clientToken = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            NSLog(@"client token: %@",clientToken);
            self.braintreeClient = [[BTAPIClient alloc] initWithAuthorization:clientToken];
            self.venmoDriver = [[BTVenmoDriver alloc] initWithAPIClient:[[BTAPIClient alloc] initWithAuthorization:clientToken]];
            [[NSUserDefaults standardUserDefaults] setObject:clientToken forKey:@"clientToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"pulled from heroku");
            [UIView animateWithDuration:0.25 animations:^{
                venmo.alpha = 1;
                apple.alpha = 1;
            } completion:^(BOOL finished) {
                if (finished) {
                    venmo.enabled = YES;
                    apple.enabled = YES;
                }
            }];
            //NSLog(@"apay: %@",_clientToken);
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
            //[References postError:error.slocalizedDescription];
        }];
        
    }
    
}



// APPLE PAY

- (IBAction)tappedApplePay {
    PKPaymentRequest *paymentRequest = [self paymentRequest];
    PKPaymentAuthorizationViewController *vc = [[PKPaymentAuthorizationViewController alloc] initWithPaymentRequest:paymentRequest];
    vc.delegate = self;
    paymentType = @"Apple Pay";
    [parent presentViewController:vc animated:YES completion:nil];
}

- (PKPaymentRequest *)paymentRequest {
    PKPaymentRequest *paymentRequest = [[PKPaymentRequest alloc] init];
    paymentRequest.merchantIdentifier = @"merchant.tutortree.ios";
    paymentRequest.supportedNetworks = @[PKPaymentNetworkAmex, PKPaymentNetworkVisa, PKPaymentNetworkMasterCard, PKPaymentNetworkDiscover];
    paymentRequest.merchantCapabilities = PKMerchantCapability3DS;
    paymentRequest.countryCode = @"US"; // e.g. US
    paymentRequest.currencyCode = @"USD"; // e.g. USD
    NSMutableArray *finalPriceBreakdown = [[NSMutableArray alloc] init];
    for (NSInteger a = 0; a < setDuration; a++) {
        [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"30 minutes" amount:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%li.00",pph.integerValue]]]];
    }
    if (discount != 0) {
        [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"Applied Balance" amount:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"-%li.00",(NSInteger)discount]]]];
    }
    [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"Service Fee" amount:[NSDecimalNumber decimalNumberWithString:@"2.50"]]];
    [finalPriceBreakdown addObject:[PKPaymentSummaryItem summaryItemWithLabel:@"TUTORTREE" amount:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%.2f",totalCost]]]];
    paymentRequest.paymentSummaryItems = finalPriceBreakdown;
    return paymentRequest;
}

- (void)paymentAuthorizationViewControllerDidFinish:(PKPaymentAuthorizationViewController *)controller {
    [parent dismissViewControllerAnimated:YES completion:nil];
}

- (void)paymentAuthorizationViewController:(PKPaymentAuthorizationViewController *)controller
                       didAuthorizePayment:(PKPayment *)payment
                                completion:(void (^)(PKPaymentAuthorizationStatus))completion {
    // Example: Tokenize the Apple Pay payment
    BTApplePayClient *applePayClient = [[BTApplePayClient alloc]
                                        initWithAPIClient:self.braintreeClient];
    [applePayClient tokenizeApplePayPayment:payment
                                 completion:^(BTApplePayCardNonce *tokenizedApplePayPayment,
                                              NSError *error) {
                                     if (tokenizedApplePayPayment) {
                                         NSString *totalCost = [NSString stringWithFormat:@"%.2f",self->totalCost];
                                         paymentType = @"Apple Pay";
                                         [self postNonceToServer:tokenizedApplePayPayment.nonce andAmount:totalCost];
                                         completion(PKPaymentAuthorizationStatusSuccess);
                                         // Then indicate success or failure via the completion callback, e.g.
                                         
                                     } else {
                                         [References toastMessage:@"Braintree Dev Mode" andView:self andClose:NO];
                                         
                                         NSLog(@"%@",error.localizedDescription);
                                         // Tokenization failed. Check `error` for the cause of the failure.
                                         
                                         // Indicate failure via the completion callback:
                                         completion(PKPaymentAuthorizationStatusFailure);
                                     }
                                 }];
}

- (void)postNonceToServer:(NSString *)paymentMethodNonce andAmount:(NSString*)amount{
    // Update URL with your server
    [self hideConfirmView:NO];
    __block UIView *processingPayment = [References loadingView:@"Processing Payment..."];
    [view addSubview:processingPayment];
    [UIView animateWithDuration:0.25 animations:^{
        processingPayment.alpha = 1;
    } completion:^(BOOL finished) {
        if (finished) {
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            NSString *url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/checkoutWithNonceAndAmount/%@/%@",paymentMethodNonce,[amount stringByReplacingOccurrencesOfString:@"." withString:@"decimal"]];
            manager.requestSerializer = [AFHTTPRequestSerializer serializer];
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                NSString *responseData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                NSLog(@"RESPONSE DATA: %@",responseData);
                if (responseData.length == 8) {
                    [self createSessionWithID:responseData];
                } else {
                    [UIView animateWithDuration:0.25 animations:^{
                        processingPayment.alpha = 0;
                    } completion:^(BOOL finished) {
                        if (finished) {
                            [processingPayment removeFromSuperview];
                            __block processingPayment = nil;
                            [References toastNotificationBottom:parent andMessage:@"Something's not right."];
                        }
                        
                    }];
                }
                //
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                NSLog(@"%@",error.localizedDescription);
                [UIView animateWithDuration:0.25 animations:^{
                    processingPayment.alpha = 0;
                } completion:^(BOOL finished) {
                    if (finished) {
                        [processingPayment removeFromSuperview];
                        processingPayment = nil;
                        [References toastNotificationBottom:parent andMessage:@"Something's not working."];
                    }
                    
                }];
                
                //[References postError:error.localizedDescription];
            }];
        }
        
    }];
    
    
}

-(void)sendNotification:(NSString*)notificationText withSubject:(NSString*)subject{
    [[[[[[[FIRDatabase database] reference] child:@"updateDatabase"] child:@"users"] child:identifier] child:@"badge"] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        NSNumber *badge;
        if (snapshot.exists) {
            badge = [NSNumber numberWithInteger:[(NSNumber*)snapshot.value integerValue] + 1];
        } else {
            badge = [NSNumber numberWithInteger:1];
        }
        NSString *url;
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"sandbox"] == true) {
            NSLog(@"sandbox");
            url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendSandboxNotification/%@/%@/%@/%@",subject,notificationText,self->token,badge];
        } else {
            url = [NSString stringWithFormat:@"https://tutortree-development.herokuapp.com/sendNotification/%@/%@/%@/%@",subject,notificationText,self->token,badge];
        }
        NSString *finalURL = [[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]] stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [manager GET:finalURL parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }];
}


@end
